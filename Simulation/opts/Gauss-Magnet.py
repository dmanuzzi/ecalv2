############################################################################
# File for running Gauss with all Baseline Upgrade detectors as of May 2015
############################################################################

from Configurables import Gauss, CondDB

CondDB().Upgrade = True

Gauss().DetectorGeo  = { "Detectors": ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Ecal', 'Hcal', 'Muon', 'Magnet', 'Infrastructure' ] }
Gauss().DetectorSim  = { "Detectors": ['Magnet'] }
Gauss().DetectorMoni = { "Detectors": ['Magnet'] }

Gauss().DataType = "Upgrade"

