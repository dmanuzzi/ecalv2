#!/bin/bash
. /cvmfs/lhcb.cern.ch/lib/LbLogin.sh
#. $VO_LHCB_SW_DIR/lib/LbEnv

index=$1
fileName1=$ProjectDir/storage/Bd_3pi_11102404_upgrade1_full_20200225/$index/tupleFullBrem.root
fileName2=/home/LHCB/perazzini/ECAL/Simulation.new/Upgrade1/Full/11102404/out/$index/tupleVanya.root
fileName3=$ProjectDir/storage/Bd_3pi_11102404_upgrade1_full_20200225/$index/tupleFullBremCorr.root
fileName4=$ProjectDir/storage/Bd_3pi_11102404_upgrade1_full_20200225/$index/tupleTES.root
$ProjectDir/Simulation/exe/vanyaCorr $fileName1 $fileName2 $fileName3
$ProjectDir/Simulation/exe/makeTES   $fileName3 $fileName4
