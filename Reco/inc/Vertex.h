#ifndef ROOT_Vertex_h
#define ROOT_Vertex_h
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <array>
#include <TObject.h>
using namespace std;

class Vertex: public TObject {
private:
	Double_t m_x;
	Double_t m_y;
	Double_t m_z;
	Double_t m_t;
	Int_t    m_type;
	Int_t    m_key;
	Int_t    m_evt;
	Int_t    m_run;
public:
	Vertex(){ initialise(); }
	Vertex(Double_t x,Double_t y,Double_t z,Double_t t);
	void initialise();
	void setX(Double_t v) { m_x = v; }
	void setY(Double_t v) { m_y = v; }
	void setZ(Double_t v) { m_z = v; }
	void setT(Double_t v) { m_t = v; }
	void setType(Double_t v) { m_type = v; }
	void setKey(Int_t v)  { m_key = v; }
	void setEvt(Int_t v)  { m_evt = v; }
	void setRun(Int_t v)  { m_run = v; }
	Double_t getX() { return m_x; }
	Double_t getY() { return m_y; }
	Double_t getZ() { return m_z; }
	Double_t getT() { return m_t; }
	Int_t    getType() { return m_type; }
	Int_t    getKey(){ return m_key;}
	Int_t    getEvt(){ return m_evt;}
	Int_t    getRun(){ return m_run;}
	
	array<Double_t,3> getPos();
	array<Double_t,4> getPosT();
	
	ClassDef(Vertex,1)
};

#endif