#ifndef ROOT_myTESevent_cxx
#define ROOT_myTESevent_cxx

#include "myTESevent.h"

myTESevent::myTESevent(){
		this->clear();
	}
myTESevent::myTESevent(Int_t run, Int_t evt){
		myTESevent();
		m_run=run, m_evt=evt;
	}
void myTESevent::clear(){
	m_run=-1, m_evt=-1;
	m_particles.clear();
	m_vertices.clear();
	m_nparticles= 0;
	m_nvertices = 0;
}
void myTESevent::reset(){
	m_run=-1, m_evt=-1;
	m_nparticles= 0;
	m_nvertices = 0;
	for (auto it = m_particles.begin(); it != m_particles.end(); ++it)
		it->second.clear();
	for (auto it = m_vertices.begin(); it != m_vertices.end(); ++it)
		it->second.clear();
}
Vertex* myTESevent::findVertex(Int_t typ, Int_t run, Int_t evt, Int_t key){
	vector<Vertex>* vertices = this->getVertices(typ);
	for (auto it_vert=vertices->begin(); it_vert != vertices->end(); ++it_vert){
    	if (it_vert->getRun() != run) continue;
    	if (it_vert->getEvt() != evt) continue;
    	if (it_vert->getKey() != key) continue;
    	return &(*it_vert);
  	}
  	return 0;
}

Vertex* myTESevent::findVertex(Int_t typ, Double_t x, Double_t y, Double_t z, Double_t t){
	vector<Vertex>* vertices = this->getVertices(typ);
	for (auto it_vert=vertices->begin(); it_vert != vertices->end(); ++it_vert){
    	if (abs(it_vert->getT()-t)>1.e-4) continue;
    	if (abs(it_vert->getZ()-z)>1.e-4) continue;
    	if (abs(it_vert->getX()-x)>1.e-4) continue;
    	if (abs(it_vert->getY()-y)>1.e-4) continue;
    	return &(*it_vert);
  	}
  	return 0;
}

Vertex* myTESevent::addVertex(Vertex& v){
	++m_nvertices;
	Int_t type = v.getType();
	if (m_vertices.at(type).size() == m_vertices.at(type).capacity()){
		cout << "myTESevent::addVertex   :  ERROR: overflow in vertex container of type " << type << ", all references are invalidated\n";
	} 
	m_vertices.at(type).push_back(v);
	return &(m_vertices.at(type).back());
}
myParticle* myTESevent::addParticle(myParticle& p){
	++m_nparticles;
	Int_t PID = p.getID();
	if (m_particles.at(PID).size() == m_particles.at(PID).capacity()){
		cout << "myTESevent::addParticle :  ERROR: overflow in particle container of PID " << PID << ", all references are invalidated\n";
	} 	
	m_particles.at(PID).push_back(p);
	return &(m_particles.at(PID).back());
}
myTrack* myTESevent::addTrack(myTrack& track){
	++m_ntracks;
	if (m_tracks.size() == m_tracks.capacity()){
		cout << "myTESevent::addTrack :  ERROR: overflow in track container, all references are invalidated\n";
	}
	m_tracks.push_back(track);
	return &(m_tracks.back());
}
void myTESevent::prepareParticleContainer(Int_t pid, UInt_t size){
	m_particles[pid].reserve(size);
}
void myTESevent::prepareVertexContainer(Int_t typ, UInt_t size){
	m_vertices[typ].reserve(size);
}
void myTESevent::prepareTrackContainer(UInt_t size){
	m_tracks.reserve(size);
}

void myTESevent::print(){
	cout << "=========myTESevent::Print ==========\n";
	printf("runNumber = %d,  eventNumber = %d \n", m_run, m_evt);
	cout << "Vertex containers: \n";
	for (auto& it : m_vertices)
		printf("    type: %d, %lu/%lu \n", it.first, it.second.size(), it.second.capacity());
	cout << "Particle containers: \n";
	for (auto& it : m_particles)
		printf("     PID: %d, %lu/%lu \n", it.first, it.second.size(), it.second.capacity());
	cout << "=====================================\n";

}

void myTESevent::linkRelativesParts(){
	Long64_t this_part_mkey=0, other_part_key=0;
  	for (auto it_thisCont = m_particles.begin(); it_thisCont != m_particles.end(); ++it_thisCont){    
    	//cout << "\nthis part in container: "<< p.first << "\n";
    	for (auto this_part = it_thisCont->second.begin(); this_part != it_thisCont->second.end(); ++this_part){
      		this_part_mkey = this_part->getMkey();
      		//printf("this_part: %d %lld \n", this_part->getID(), this_part->getKey()); 
      		for (auto it_otherCont = m_particles.begin(); it_otherCont != m_particles.end(); ++it_otherCont){
        		//if (it_thisCont->first == it_otherCont->first) continue;
        		//cout << "other part in container: "<< it_otherCont->first<< "\n";
        		for (auto other_part = it_otherCont->second.begin(); other_part != it_otherCont->second.end(); ++other_part){
          			other_part_key = other_part->getKey();
          			if (this_part_mkey == other_part_key){
            			this_part->set_mother(&(*other_part));
            			other_part->add_daughter(&(*this_part));
            			//printf("...Found mother %lld %lld\n", this_part_mkey, other_part_key);
            			break;
          			} 
       			}
      		}  
    	}   
  	}
}
void myTESevent::setGDparents(){
	for (auto it_cont = m_particles.begin(); it_cont != m_particles.end(); ++it_cont){
	    for (auto it_part = it_cont->second.begin(); it_part != it_cont->second.end(); ++it_part){
	    	myProtoParticle* mother = it_part->get_mother();
	    	if (mother == 0) continue;
	      	myProtoParticle* gd_mother = mother->get_mother();
	      	if (gd_mother == 0) continue;
	      	it_part->set_gd_mother(gd_mother);
	      	it_part->setGmid(gd_mother->getID());
	      	it_part->setGmkey(gd_mother->getKey());
	      	gd_mother->add_gd_daughter(&(*it_part));
	    }
	}
}



#endif