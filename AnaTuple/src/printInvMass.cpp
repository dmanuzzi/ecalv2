#include "TFile.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TH1D.h"
#include "TString.h"

void printInvMass(TString nfin, TString tag){
  
  TString t1 = "gamma1_CALOVT - sqrt((gamma1_CALOVX-pip_OVX)**2+(gamma1_CALOVY-pip_OVY)**2+(gamma1_CALOVZ-pip_OVZ)**2)/299.792458";
  TString t2 = "gamma2_CALOVT - sqrt((gamma2_CALOVX-pip_OVX)**2+(gamma2_CALOVY-pip_OVY)**2+(gamma2_CALOVZ-pip_OVZ)**2)/299.792458";
  TString dt     = Form("( (%s-pip_OVT)**2 + (%s-pip_OVT)**2 )*1000.", t1.Data(), t2.Data());
  TString dt_cut = Form("(%s < 2)", dt.Data());
  TFile* fin = TFile::Open(nfin.Data(), "READ");
  TTree* tin = (TTree*)fin->Get("ntp");
  
/*
  TCanvas* c_pi0_M = new TCanvas("c_pi0", "c_pi0_M", 700, 500);
  TH1D *h_pi0_tot = new TH1D("h_pi0_tot","tot; m(#gamma#gamma) [MeV]; Candidates",100, 40, 230);
  TH1D *h_pi0_sig = new TH1D("h_pi0_sig","sig; m(#gamma#gamma) [MeV]; Candidates",100, 40, 230);
  tin->Draw("pi0_M>>h_pi0_tot", "gamma1_PT>200 && gamma2_PT>200 && pi0_PT>1000 && "+dt_cut, "goff");
  tin->Draw("pi0_M>>h_pi0_sig", "150*(gamma1_PT>200 && gamma2_PT>200 && pi0_PT>1000 && isSig==1 && "+dt_cut+")", "goff");
  h_pi0_tot->SetLineColor(kBlue);
  h_pi0_tot->SetMinimum(0);
  h_pi0_tot->SetLineWidth(2);
  h_pi0_tot->GetXaxis()->SetTitleSize(0.05);
  h_pi0_tot->GetXaxis()->SetLabelSize(0.05);
  h_pi0_tot->GetYaxis()->SetTitleSize(0.05);
  h_pi0_tot->GetYaxis()->SetLabelSize(0.05);
  h_pi0_sig->SetLineColor(kRed);
  h_pi0_sig->SetLineWidth(2);
  h_pi0_tot->Draw();
  h_pi0_sig->Draw("same");
  c_pi0_M->SetGridx();
  c_pi0_M->SetGridy();
  c_pi0_M->BuildLegend();
  c_pi0_M->SetLeftMargin(0.15);
  c_pi0_M->SetBottomMargin(0.12);
  c_pi0_M->SaveAs("plots/"+tag+"_pi0_M.pdf");
*/
  TCanvas* c_B0_M = new TCanvas("c_B0", "c_B0_M", 700, 500);
  TH1D *h_B0_tot = new TH1D("h_B0_tot","tot; m(#pi^{+}#pi^{-}#pi^{0}) [GeV]; Candidates",100, 3, 7);
  TH1D *h_B0_sig = new TH1D("h_B0_sig","sig; m(#pi^{+}#pi^{-}#pi^{0}) [GeV]; Candidates",100, 3, 7);
  tin->Draw("B0_M/1000.>>h_B0_tot", "(gamma1_PT>200 && gamma2_PT>200 && pi0_PT>1000 && B0_PT>2000 && abs(pi0_M-135)<15 && "+dt_cut+")", "goff");
  tin->Draw("B0_M/1000.>>h_B0_sig", "(gamma1_PT>200 && gamma2_PT>200 && pi0_PT>1000 && B0_PT>2000 && abs(pi0_M-135)<15 && isSig==1 && "+dt_cut+")", "goff");
  h_B0_tot->SetLineColor(kBlue);
  h_B0_tot->SetMinimum(0);
  h_B0_tot->SetLineWidth(2);
  h_B0_tot->GetXaxis()->SetTitleSize(0.05);
  h_B0_tot->GetXaxis()->SetLabelSize(0.05);
  h_B0_tot->GetYaxis()->SetTitleSize(0.05);
  h_B0_tot->GetYaxis()->SetLabelSize(0.05);  
  h_B0_sig->SetLineWidth(2);
  h_B0_sig->SetLineColor(kRed);
  h_B0_tot->Draw();
  h_B0_sig->Draw("same");
  c_B0_M->SetGridx();
  c_B0_M->SetGridy();
  c_B0_M->BuildLegend();
  c_B0_M->SetLeftMargin(0.15);
  c_B0_M->SetBottomMargin(0.12);
  c_B0_M->SaveAs("plots/"+tag+"_B0_M.pdf");
  

  TCanvas* c_dt2 = new TCanvas("c_dt2", "c_dt2", 700, 500);
  TH2D* h_dt2_sig = new TH2D("h_dt2_sig", "sig; t_{1}-t_{1}^{expected} [ns]; t_{2}-t_{2}^{expected} [ns]", 75, -1, 1, 75, -1, 1);
  TH2D* h_dt2_bkg = new TH2D("h_dt2_bkg", "bkg; t_{1}-t_{1}^{expected} [ns]; t_{2}-t_{2}^{expected} [ns]", 75, -1, 1, 75, -1, 1);
  h_dt2_sig->SetLineColor(kRed);
  h_dt2_bkg->SetLineColor(kBlue);
  
  tin->Draw(Form("%s-pip_OVT:%s-pip_OVT>>h_dt2_sig", t2.Data(),t1.Data()), "(gamma1_PT>200 && gamma2_PT>200 && pi0_PT>1000 && B0_PT>2000 &&  abs(pi0_M-135)<15 && isSig==1)", "goff");
  tin->Draw(Form("%s-pip_OVT:%s-pip_OVT>>h_dt2_bkg", t2.Data(),t1.Data()), "(gamma1_PT>200 && gamma2_PT>200 && pi0_PT>1000 && B0_PT>2000 &&  abs(pi0_M-135)<15 && isSig!=1)", "goff");

  h_dt2_bkg->DrawNormalized("box");
  h_dt2_sig->DrawNormalized("box same");
  c_dt2->SetGridy();
  c_dt2->SetGridx();
  c_dt2->SaveAs("plots/"+tag+"_dt2.pdf");
  

  TCanvas* c_rt = new TCanvas("c_rt", "c_rt2", 700, 500);
  TH1D *h_rt_sig = new TH1D("h_rt_sig", "sig; (t_{1}-t_{1}^{expected})^{2} + (t_{2}-t_{2}^{expected})^{2} [ns^{2}]", 200, 0., 0.4);
  TH1D *h_rt_bkg = new TH1D("h_rt_bkg", "bkg; (t_{1}-t_{1}^{expected})^{2} + (t_{2}-t_{2}^{expected})^{2} [ns^{2}]", 200, 0., 0.4);
  h_rt_sig->SetLineColor(kRed);
  h_rt_bkg->SetLineColor(kBlue);
  
  tin->Draw(Form("sqrt((%s-pip_OVT)**2+(%s-pip_OVT)**2)>>h_rt_sig", t2.Data(),t1.Data()), "(gamma1_PT>200 && gamma2_PT>200 && pi0_PT>1000 && B0_PT>2000 &&  abs(pi0_M-135)<15 && isSig==1)", "goff  ");
  tin->Draw(Form("sqrt((%s-pip_OVT)**2+(%s-pip_OVT)**2)>>h_rt_bkg", t2.Data(),t1.Data()), "(gamma1_PT>200 && gamma2_PT>200 && pi0_PT>1000 && B0_PT>2000 &&  abs(pi0_M-135)<15 && isSig!=1)", "goff");
  h_rt_sig->DrawNormalized("hist");
  h_rt_bkg->DrawNormalized("hist same");
  c_rt->SetGridy();
  c_rt->SetGridx();
  c_rt->BuildLegend();
  c_rt->SaveAs("plots/"+tag+"_rt.pdf");
  

}
