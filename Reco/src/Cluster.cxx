#ifndef Cluster_cxx
#define Cluster_cxx


#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <numeric>
#include <Cluster.h>
#include <TLeaf.h>
#include <TFile.h>
#include <TF2.h>
using namespace std;

ClassImp(myProtoParticle )
ClassImp(Cell)

Cluster::Cluster(){
	m_vec_cellIDs.reserve(9);
	m_vec_e.reserve(9);
	m_ncells = 0;
	m_RecParticle = 0;
	m_calib_e = 0.0;
	m_calib_x = 0.0;
	m_calib_y = 0.0;
	m_closerTrack=0;
}
Cluster::Cluster(Cell *SeedCell){
	m_vec_cellIDs.reserve(9);
	m_vec_e.reserve(9);
	m_ncells = 0;
	m_ID = SeedCell->getID();
	m_z  = SeedCell->getZ();
	m_RecParticle = 0;
	m_calib_e = 0.0;
	m_calib_x = 0.0;
	m_calib_y = 0.0;
	m_closerTrack=0;
	this->addCell(SeedCell);
}

void Cluster::addCell(Cell *cell){
	//cout << "*** "<< cell <<"\n";
	m_vec_cellIDs.push_back(cell->getID());
	m_vec_e.push_back(cell->getE());
	m_cells.push_back(cell);
	++m_ncells;
	m_cellMap[cell->getID()] = m_ncells-1;
}

Cell * Cluster::getSeed(){
	if (m_cells.size()==0) return nullptr;	
	Cell * cell = m_cells[0];
	if (cell->getID() == m_ID) return cell;
	else return nullptr;
}

/*
Double_t Cluster::func(Double_t *var, Double_t *par) {

  Double_t x = var[0];
  Double_t y = var[1];
  Double_t f = par[0];
  f = 0.1032578;//probability of core/tail
  Double_t s = 1.57363;//
  Double_t s2 = s*s;
  Double_t R = 0.13315;
  Double_t r = sqrt(x*x+y*y);
  Double_t value = f/s2*exp(-(r*r)/s2/2) + (1-f)*2*pow(R,2)*pow(r*r+R*R,-2);
  return value/(2.0*TMath::Pi());
}

Double_t Cluster::fracEnergy(Double_t xMin, Double_t xMax, Double_t yMin, Double_t yMax) {

  TF2 f = TF2("fracEnergy",func,xMin,xMax,yMin,yMax);
  Double_t ret = f.Integral(xMin,xMax,yMin,yMax);
  return ret;
}
*/
void Cluster::calibrate(Calibrations * calibs){
	Int_t region_i = getSeedRegion()-1;
	m_calib_e = m_e/(1+calibs->Ecalib[region_i]);
	//m_calib_e = m_e/(1+calibs->Ecalib[region_i].Interpolate(m_e));
	Cell * seedCell = getSeed();
	Double_t seed_x = seedCell->getX();
	Double_t seed_y = seedCell->getY();
	Double_t half_size = seedCell->getSize()*0.5;
	
	//Double_t e_interp = (m_e<10000)?m_e:10000;
	//e_interp = (m_e>1)?m_e:1;
	if (abs(m_x-seed_x)<half_size-0.001)
		m_calib_x = seed_x + calibs->Xcalib[region_i].Interpolate(m_x-seed_x);
		//m_calib_x = seed_x + calibs->Xcalib[region_i].Interpolate(m_x-seed_x,e_interp);
	else
		m_calib_x = m_x;
	if (abs(m_y-seed_y)<half_size-0.001)
		m_calib_y = seed_y + calibs->Ycalib[region_i].Interpolate(m_y-seed_y);
		//m_calib_y = seed_y + calibs->Ycalib[region_i].Interpolate(m_y-seed_y,e_interp);
	else
		m_calib_y = m_y;
	//printf("region: %d; -  x: %.2f -> %.2f; y: %.2f -> %.2f; E: %.2f -> %.2f;   \n", region_i, m_x, m_calib_x, m_y, m_calib_y, m_e, m_calib_e);
}
	
void Cluster::finalize(Calibrations * calibs){
	m_e = accumulate(m_vec_e.begin(), m_vec_e.end(),0.0);
	m_x = 0.0;
	m_y = 0.0;
	for (Int_t i=0; i<m_ncells; ++i){
		Cell *cell = (Cell*)m_cells[i];
		m_x += m_vec_e[i]*cell->getX();
		m_y += m_vec_e[i]*cell->getY();
		m_t += m_vec_e[i]*cell->getT();
	}
	m_x = m_x/m_e;
	m_y = m_y/m_e;
	m_t = m_t/m_e;
	if (calibs != 0) 
		calibrate(calibs);
}

Cell * Cluster::getCellFromID(Int_t id) {
   if(id <= 0) {
      //printf("ID OUT OF RANGE\n");
      return NULL;
   }
   Int_t pos = m_cellMap.find(id)->second;
   return m_cells.at(pos);
}
void Cluster::setE_fromID(Int_t id, Double_t value){
	Int_t pos = m_cellMap.find(id)->second;
	m_vec_e[pos] = value;
}

vector<myProtoParticle*> Cluster::getHittingPhotons(Double_t Eth){
	vector<myProtoParticle*> hittingPhotons;
	//cout << "Cluster::getHittingPhotons, # cells :" <<  m_cells.size() << endl;
	for (auto it_cell = m_cells.begin(); it_cell != m_cells.end(); ++it_cell){
	//	cout << ".";
		Cell * cell = (Cell*)(*it_cell);
		auto tmp_hittingPhotons = cell->getHittingPhotons(Eth);
	//	cout << cell->getID() << "  N hits = " << tmp_hitingPhotons.size() << endl;
		hittingPhotons.insert(hittingPhotons.end(), tmp_hittingPhotons.begin(), tmp_hittingPhotons.end());
	}
	//cout << "\nCluster::getHittingPhotons :" <<  hittingPhotons.size() << endl;
	return hittingPhotons;
}

vector<myProtoParticle*> Cluster::getContributingParticles(Double_t Eth){
	vector<myProtoParticle*> photons;
	for (auto it_cell = m_cells.begin(); it_cell != m_cells.end(); ++it_cell){
		Cell * cell = (Cell*)(*it_cell);
		auto tmp_photons = cell->getContributingPhotons(Eth);
		vector<myProtoParticle*> tmp2_photons;
		for_each(tmp_photons.begin(), tmp_photons.end(), [&tmp2_photons, &photons](myProtoParticle* part){
			Bool_t found = false;
			for (auto& p1 : photons){
				if (p1->getKey() == part->getKey()){
					found=true;
					break;
				}
			}
			if (found == false) tmp2_photons.push_back(part);
		});
		photons.insert(photons.end(), tmp2_photons.begin(), tmp2_photons.end());
	}
	return photons;	
}

Int_t Cluster::getNphotons(){
	vector<myProtoParticle*> hittingPhotons;
	Int_t Nphotons = 0;
	for (auto it_cell = m_cells.begin(); it_cell != m_cells.end(); ++it_cell){
		Cell * cell = (Cell*)(*it_cell);
		Nphotons += cell->getNphotons();
	}
	return Nphotons;
}

myProtoParticle* Cluster::getCloserPhoton(Double_t Eth){
	vector<Long64_t> hitIDs;
	hitIDs.reserve(5);
	Double_t dist=9999999., tmp_dist=0., tmp_photonX=0.,tmp_photonY=0.;
	myProtoParticle * closerPhoton = 0;
	auto hitPhotons = this->getHittingPhotons(Eth);
	for (auto& hitPhoton : hitPhotons){
		if (find(hitIDs.begin(),hitIDs.end(), hitPhoton->getKey()) != hitIDs.end()) continue;
		hitIDs.push_back(hitPhoton->getKey());
		tmp_photonX = hitPhoton->getCaloVertexX();
		tmp_photonY = hitPhoton->getCaloVertexY();
		if (m_calib_x*m_calib_x+m_calib_y*m_calib_y > 1)
			tmp_dist = (tmp_photonX-m_calib_x)*(tmp_photonX-m_calib_x) + (tmp_photonY-m_calib_y)*(tmp_photonY-m_calib_y);
		else
			tmp_dist = (tmp_photonX-m_x)*(tmp_photonX-m_x) + (tmp_photonY-m_y)*(tmp_photonY-m_y);			
		if (tmp_dist > dist) continue;
		dist = tmp_dist;
		closerPhoton = hitPhoton; 
	}
	//if (closerPhoton == 0){
	//	cout << "WARNING: Cluster::getCloserPhoton -> no photon found!!\n";
	//	cout.flush();
	//}
	return closerPhoton;
}

myProtoParticle* Cluster::getCloserParticle(Double_t Eth, Short_t option){
	//// options:
	//// 0 : nothing
	//// 1 : is tracked
	//// 2 : is not tracked 
	vector<Long64_t> hitKeys;
	hitKeys.reserve(5);
	myProtoParticle* closerParticle = 0;
	Double_t dist=9999999., tmp_dist=0., tmp_X=0.,tmp_Y=0.;
	auto hits = this->getHittingPhotons(Eth);
	Bool_t isTracked=false;
	Int_t PID = 0;
	Double_t oz=0.;
	
	for (auto& hit : hits){
		if (find(hitKeys.begin(),hitKeys.end(), hit->getKey()) != hitKeys.end()) continue;
		hitKeys.push_back(hit->getKey());
		PID  = abs(hit->getID());
		oz   = hit->getOrigVertexZ(); 
		isTracked = false;
		if (abs(PID) == 11 && oz<2000) isTracked = true;
		if (option==1 && !isTracked) continue;
		if (option==2 &&  isTracked) continue;
		tmp_X = hit->getCaloVertexX();
		tmp_Y = hit->getCaloVertexY();
		if (m_calib_x*m_calib_x+m_calib_y*m_calib_y > 1)
			tmp_dist = (tmp_X-m_calib_x)*(tmp_X-m_calib_x) + (tmp_Y-m_calib_y)*(tmp_Y-m_calib_y);
		else
			tmp_dist = (tmp_X-m_x)*(tmp_X-m_x) + (tmp_Y-m_y)*(tmp_Y-m_y);			
		//printf("ID: %d;  x: %f;  y:%f;  dist: %f\n", PID, tmp_X, tmp_Y, tmp_dist);
		if (tmp_dist > dist) continue;
		dist = tmp_dist;
		closerParticle = hit; 
	}
	return closerParticle;
}

Double_t Cluster::getPx(){
	return m_calib_e*m_calib_x / sqrt(m_calib_x*m_calib_x+m_calib_y*m_calib_y+m_z*m_z);
}
Double_t Cluster::getPy(){
	return m_calib_e*m_calib_y / sqrt(m_calib_x*m_calib_x+m_calib_y*m_calib_y+m_z*m_z);
}
Double_t Cluster::getPz(){
	return m_calib_e*m_z / sqrt(m_calib_x*m_calib_x+m_calib_y*m_calib_y+m_z*m_z);
}
Double_t Cluster::getPt(){
	return sqrt(getPt2());
}
Double_t Cluster::getPt2(){
	return      m_calib_e*m_calib_e*(m_calib_x*m_calib_x+m_calib_y*m_calib_y) / (m_calib_x*m_calib_x+m_calib_y*m_calib_y+m_z*m_z);
}

Cell* Cluster::get2Ecell(){
	if (m_cells.size()<2) return 0;
	vector<pair<Double_t, Cell*>> cont(m_cells.size());
	transform(m_cells.begin(), m_cells.end(), cont.begin(),  [](Cell* cell){ return make_pair(cell->getE(), cell); });
	sort(cont.begin(), cont.end());
	return cont[1].second;
}



Double_t Cluster::getDistanceCloserTrack(){
	if (m_closerTrack == 0) return -1;
	return ( sqrt(pow(m_calib_x-m_closerTrack->getCaloVertexX(), 2)+pow(m_calib_y-m_closerTrack->getCaloVertexY(), 2)) );
}




#endif