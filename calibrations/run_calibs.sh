#!/bin/bash

#mv ./out/alibs.root ./out/calibs.root.bak

cd src/ 
make makeCalibHists 
cd .. 
alpha=5
beta=1
for alpha in 5 10 15 20; do
	for beta in 1 2; do
		echo $alpha'  '$beta
		nfout=./outNew/calibs_1000_alpha"$alpha"beta"$beta".root
		echo $nfout
		#./exe/makeCalibHists 121.2 35 $alpha $beta $nfout &> log_120_35.txt &
		#sleep 3
		./exe/makeCalibHists  80.8 35 $alpha $beta $nfout &> log_80_35.txt &
		sleep 3
#		./exe/makeCalibHists  60.6 35 $alpha $beta $nfout &> log_60_35.txt &
#		sleep 3
#		./exe/makeCalibHists  40.4 35 $alpha $beta $nfout &> log_40_35.txt &
#		sleep 3
#		./exe/makeCalibHists  30.3 35 $alpha $beta $nfout &> log_30_35.txt &
#		sleep 3
#		./exe/makeCalibHists  30.3 30 $alpha $beta $nfout &> log_30_30.txt &
#		sleep 3
#		./exe/makeCalibHists  30.3 15 $alpha $beta $nfout &> log_30_15.txt &
#		sleep 3

#		./exe/makeCalibHists  20.2 30 $alpha $beta $nfout &> log_20_30.txt 
#		sleep 3

#		./exe/makeCalibHists  20.2 15 $alpha $beta $nfout &> log_20_15.txt &
#		sleep 3
#		./exe/makeCalibHists  20.2 13 $alpha $beta $nfout &> log_20_13.txt &
#		sleep 3
#		./exe/makeCalibHists  15.15 15 $alpha $beta $nfout &> log_15_15.txt &
#		sleep 3
#		./exe/makeCalibHists  15.15 13 $alpha $beta $nfout &> log_15_13.txt &
#		sleep 3
#		./exe/makeCalibHists  10.1 15 $alpha $beta $nfout &> log_10_15.txt &
#		sleep 3

#		./exe/makeCalibHists  10.1 13 $alpha $beta $nfout &> log_10_13.txt 
#		sleep 1
	done;
done
