#!/bin/bash

runGauss=1
runDaVinci=0

index=$1
. /cvmfs/lhcb.cern.ch/lib/LbLogin.sh 
#. $VO_LHCB_SW_DIR/lib/LbEnv


opts=/home/LHCB-T3/dmanuzzi/newECAL/Simulation/opts/
mkdir -p /home/LHCB-T3/dmanuzzi/newECAL/storage/Bd2Kstee_11124007_upgrade0_full_20200526/$index
#cd /home/LHCB-T3/dmanuzzi/newECAL/storage/Bd2Kstee_11124007_upgrade0_full_20200526/$index

if [[ $runGauss == 1 ]]; then
    CMTCONFIG=x86_64-slc6-gcc49-opt
    echo $CMTCONFIG
    cp /home/LHCB-T3/dmanuzzi/newECAL/Simulation/opts//job.py .
    echo "gaussGen.RunNumber = $index" >> job.py
    /home/LHCB-T3/dmanuzzi/newECAL/GaussDev_v51r2/build.x86_64-slc6-gcc49-opt/run gaudirun.py \
	$opts/Gauss-Job.py \
	$opts/Gauss-Full.py \
	$opts/jobVanya.py \
#	$opts/Beam6500GeV-md100-nu1.6.py \
	$opts/Beam7000GeV-md100-nu71.5.py \
	$opts/Pythia8.py \
	$opts/11124007.py \
	job.py
fi

if [[ $runDaVinci == 1 ]]; then
    CMTCONFIG=x86_64-centos7-gcc8-opt
    echo $CMTCONFIG
    fileName=`ls ./*-20ev-*.sim`
    #fileName=`ls ./*-5ev-*.sim`
    #fileName=`ls ./*-1ev-*.sim`
    echo $fileName
    lb-run DaVinci/latest python  /home/LHCB-T3/dmanuzzi/newECAL/Simulation/src/readFullBrem.py -i $fileName
    #lb-run DaVinci/latest python /home/LHCB-T3/dmanuzzi/newECAL/Simulation/src/readTES.py -i $fileName
fi
