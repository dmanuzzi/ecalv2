#ifndef CHECKSMEARING_C
#define CHECKSMEARING_C

#include <iostream>
#include <TFile.h>
#include <TString.h>
#include <TTree.h>
#include <TChain.h>
#include <CaloEvent.h>
#include <myParticle.h>
#include <Cell.h>
#include <TROOT.h>
#include <TSystem.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TProfile.h>
#include <vector>
using namespace std;

void checkSmearing(TTree* tin, CaloEvent * event, TString nfout, TString option){
  cout << "******* checkSmearing starts\n";
  
  TFile fout(nfout, option);
  //vector<Double_t> binsE = {50, 1000, 2000, 4000, 8000,  12000, 16000, 20000, 25000, 30000, 36000, 42000, 50000};
  vector<Double_t> binsE;
  for (Int_t i =0; i<=50; ++i) binsE.push_back(i*1000.0);
  TH2D     h2_Ecell_res("h2_Ecell_res", "Energy Resolution of cells; E [MeV]; (E-TRUE_E)/TRUE_E", binsE.size()-1, binsE.data(), 200,-0.2,0.2);
  //TProfile h1_Ecell_res("h1_Ecell_res", "Energy Resolution of cells; E [MeV]; (E-TRUE_E)/TRUE_E", binsE.size()-1, binsE.data(), -0.2,0.2, "s");
  TH2D     h2_Tcell_res("h2_Tcell_res", "Time Resolution of cells; E [MeV]; t-TRUE_t [ps]", binsE.size()-1, binsE.data(), 200, -1000,1000);
  //TProfile h1_Tcell_res("h1_Tcell_res", "Time Resolution of cells; E [MeV]; t-TRUE_t [ps]", binsE.size()-1, binsE.data(), -1000,1000, "s");
  //TH1D h_Ecell_res_expected("h_Ecell_res_expected", "Energy Resolution of cells; E [MeV]; Resolution" ,binsE.size()-1, binsE.data());
  //TH1D h_Edistrib("h_Edistrib", "h_Edistrib" ,1000, 0,50000);
  //TF1 func("func_res", "sqrt( (0.1/sqrt(x/1000.0))**2+0.01**2 )", binsE[0], binsE.back());
  //func.SetNpx(100000);
  Double_t Ereco=0., Etrue=0., Treco=0, Ttrue=0;
  for (int i=0, nevents = tin->GetEntries(); i<nevents; ++i){
    // delete event;
    // event = 0;
    event->Clear();
    tin->GetEntry(i);
    
    if (i%1==0){
      cout << "\r at entry "<< i;
      cout.flush();
    }
    auto cells   = event->getCells();
    for (auto it_cell = cells->begin(); it_cell != cells->end(); ++it_cell){ // loop on all cells
      Cell * cell = (Cell*)(*it_cell);
      //if (cell->getRegion() != 1) continue;
      if (cell->getNphotons()!=1 || cell->getE()<50 || cell->getE()>50000) continue;
      //cout << cell->getPhotons().GetEntries() << endl;
      Ereco = cell->getE();
      Etrue = cell->getEtrue();
      Treco = cell->getT();
      Ttrue = cell->getT_Eavg_true();
      h2_Ecell_res.Fill(Ereco, (Ereco-Etrue)/Etrue);
      //h1_Ecell_res.Fill(Ereco, (Ereco-Etrue)/Etrue);
      h2_Tcell_res.Fill(Ereco, Treco - Ttrue);
      //h1_Tcell_res.Fill(Ereco, Treco - Ttrue);
      //cout << Ereco << "   " << count << "    " << func.Eval(Ereco) << "\n";
      //h_Ecell_res_expected.Fill(Ereco, func.Eval(Ereco)/count);
      //if (count % 10000==0)
      /*
          std::cout << "Nphotons= " << cell->getNphotons()
                    << "  Ereco=" << Ereco 
                    << "  Etrue=" << Etrue 
                    << "  Treco=" << Treco
                    << "  Ttrue=" << Ttrue 
                    << "\n";
      */
    }
  }
  cout << endl;
  event->Clear();
  /*
  TH1D h_Ecell_res("h_Ecell_res", "Energy Resolution of cells; E [MeV]; Resolution" ,binsE.size()-1, binsE.data());
  TH1D h_Tcell_res("h_Tcell_res", "Time Resolution of cells; E [MeV]; Resolution" ,binsE.size()-1, binsE.data());
  for (int i=1, nbins=binsE.size(); i<nbins;++i){
    h_Ecell_res.SetBinContent(i, h1_Ecell_res.GetBinError(i));
    h_Tcell_res.SetBinContent(i, h1_Tcell_res.GetBinError(i));
    h_Ecell_res_expected.SetBinContent(i, func.Integral(binsE[i-1], binsE[i])/(binsE[i]-binsE[i-1]));
  }
  h_Ecell_res_expected.Print();
  */
  //fout.WriteTObject(&h_Edistrib, h_Edistrib.GetName());
  fout.WriteTObject(&h2_Ecell_res, h2_Ecell_res.GetName());
  //fout.WriteTObject(&h1_Ecell_res, h1_Ecell_res.GetName());
  //fout.WriteTObject(&h_Ecell_res,  h_Ecell_res.GetName());
  //fout.WriteTObject(&func,  func.GetName());
  //fout.WriteTObject(&h_Ecell_res_expected,  h_Ecell_res_expected.GetName());
  //fout.WriteTObject(&h_Tcell_res,  h_Tcell_res.GetName());
  //fout.WriteTObject(&h1_Tcell_res, h1_Tcell_res.GetName());
  fout.WriteTObject(&h2_Tcell_res, h2_Tcell_res.GetName());
  fout.Close();
  cout << "******* checkSmearing ends\n";
}


#endif
