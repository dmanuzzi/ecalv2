#include <iostream>
#include <stdio.h>
#include <stdlib.h>

#include <TROOT.h>
#include <TSystem.h>
#include <TFile.h>
#include <TString.h>
#include <TTree.h>
#include <TChain.h>
#include <TH1D.h>
#include <TH2D.h>


#include "./checkSmearing.C"
#include "./checkResolution.C"
#include "./checkPi0.C"
#include "./checkCalibs.C"
// #include "./generalTrueInfo.C"
#include "./checkBd3pi.C"
using namespace std;

int main(Int_t argc, Char_t *argv[]){
  Int_t ijob = atoi(argv[1]);
  TString RunCond = argv[2];
  TString NU = "", dirTag = "";
  if      (RunCond.Contains("upgrade2")){
    NU = "nu71";
    dirTag = "out";
  } else if (RunCond.Contains("upgrade0")){
    NU = "nu2";
    dirTag = "out_Run1";
  }
  TString PATHin1 = Form("$ProjectDir/storage/stand_Bd_3pi_"+NU+"_20200106/evtType_11102404/%d/%s/",ijob, RunCond.Data());
  //TString PATHin2 = Form("$ProjectDir/storage/stand_Bd_3pi_"+NU+"_20200106/evtType_11102404/%d/",ijob);
  TString PATHin2 = Form("$ProjectDir/../../perazzini/ECAL/Simulation/%s/%d/Standalone/",dirTag.Data(),ijob);
  TFile *finEv  = TFile::Open(PATHin1+"fillTuple.root");
  TTree *tinEv  = (TTree*)finEv->Get("ECALevt");
  TFile *finPart= TFile::Open(PATHin2+"tupleStand.root");
  TTree *tinPart= (TTree*)finPart->Get("ntp");
  cout << PATHin1+"fillTuple.root\n";
  TString PATHout= PATHin1+"plots/";
  system("mkdir "+PATHout);
  CaloEvent * event = new CaloEvent;
  tinEv->SetBranchAddress("evt",&event);
  checkSmearing(tinEv, event,PATHout+"testSmearing.root", "RECREATE");
  checkCalibs(tinEv, event,PATHout+"testCalibs.root", "RECREATE");
  checkResolution(tinEv, event, PATHout+"testResolution.root", "RECREATE");
  checkPi0(tinEv, event, tinPart, PATHout+"testPi0.root", "RECREATE");
  //generalTrueInfo(chain, chainAll, PATHout+"generalTrueInfo.root", "RECREATE");
  checkBd3pi(tinEv, event, tinPart, PATHout+"testBd3pi.root", "RECREATE");
  finEv->Close();
  finPart->Close();
  //delete chain;
  return 1;
}
