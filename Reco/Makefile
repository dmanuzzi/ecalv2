ROOTCONFIG  := root-config
ARCH        := $(shell $(ROOTCONFIG) --arch)
PLATFORM    := $(shell $(ROOTCONFIG) --platform)
ROOTVERSION := $(shell $(ROOTCONFIG) --version | awk -F. '{print $$1}')

INCLUDES = 
SRCDIR   = src
INCDIR   = inc
LIBDIR   = lib
WORKDIR  = tmp

CXX=$(shell root-config --cxx)
LD=$(shell root-config --ld)
CXXFLAGS = -Ofast -Wall -Wextra -Wshadow -Woverloaded-virtual -Werror -fPIC
#CXXFLAGS = -fsanitize=address -fno-omit-frame-pointer -Ofast -Wall -Wextra -Wshadow -Woverloaded-virtual -fPIC 
MFLAGS   = -MM
#LDFLAGS  = -shared -fsanitize=address -fno-omit-frame-pointer
LDFLAGS  = -shared

PACKAGE = ECAL
DEPDIR=$(WORKDIR)/dependencies
OBJDIR=$(WORKDIR)/objects

INCLUDES += -I$(INCDIR)
CXXFLAGS += $(INCLUDES)
CXXFLAGS += $(shell $(ROOTCONFIG) --cflags)
LDFLAGS  += $(shell $(ROOTCONFIG) --ldflags)
CINTFILE  = $(WORKDIR)/$(PACKAGE)Cint.cc
CINTOBJ   = $(OBJDIR)/$(PACKAGE)Cint.o
LIBFILE   = $(LIBDIR)/lib$(PACKAGE).a
SHLIBFILE = $(LIBDIR)/lib$(PACKAGE).so
ROOTMAPFILE := $(patsubst %.so,%.rootmap,$(SHLIBFILE))

all: shlib lib

# List of all header files
HHLIST:=$(wildcard $(INCDIR)/*.h)

# List of all source files to build
CXXLIST:=$(wildcard $(SRCDIR)/*.cxx)

# List of all object files to build
OLIST:=$(patsubst %.cxx,%.o,$(addprefix $(OBJDIR)/,$(notdir $(CXXLIST))))

# List of all dependency files to make
DLIST:=$(patsubst %.cxx,%.d,$(addprefix $(DEPDIR)/,$(notdir $(CXXLIST))))

# Implicit rule making all dependency Makefiles included at the end of this makefile
$(DEPDIR)/%.d: $(SRCDIR)/%.cxx
	@echo "Making $@"
	@mkdir -p $(DEPDIR)
	@set -e; $(CXX) $(MFLAGS) $(CXXFLAGS) $< \
	          | sed 's#\($(notdir $*)\)\.o[ :]*#$(OBJDIR)/\1.o $@ : #g' > $@; \
	        [ -s $@ ] || rm -f $@
# Implicit rule to compile all classes
$(OBJDIR)/%.o : $(SRCDIR)/%.cxx
	@echo "Compiling $<"
	@mkdir -p $(OBJDIR)
	@$(CXX) $(CXXFLAGS) -c $< -o $@

$(CINTOBJ): $(HHLIST) $(INCDIR)/$(PACKAGE)_LinkDef.h
	@mkdir -p $(OBJDIR)
	@echo "Running rootcling"
	@rootcling -f $(CINTFILE) -s $(SHLIBFILE) -rml $(SHLIBFILE) -rml libEG.so -rml libHist.so -rml libMatrix.so -rml libNet.so -rml libRIO.so -rml libTree.so -rml libMathCore.so -rml libCore.so -rmf $(ROOTMAPFILE) -c $(INCLUDES) $(notdir $(HHLIST)) 
	@echo "Compiling $(CINTFILE)"
	@$(CXX) $(CXXFLAGS) -c $(CINTFILE) -o $(CINTOBJ)

$(LIBFILE): $(OLIST) $(CINTOBJ)
	@echo "Making $(LIBFILE)"
	@mkdir -p $(LIBDIR)
	@rm -f $(LIBFILE)
	@ar rcs $(LIBFILE) $(OLIST) $(CINTOBJ) 

# Rule to combine objects into a shared library
$(SHLIBFILE): $(OLIST) $(CINTOBJ)
	@echo "Making $(SHLIBFILE)"
	@mkdir -p $(LIBDIR)
	@rm -f $(SHLIBFILE)
	@$(CXX) $(OLIST) $(CINTOBJ) $(LDFLAGS) -o $(SHLIBFILE)

shlib: $(SHLIBFILE)

lib: $(LIBFILE)

clean:
	rm -rf $(WORKDIR)
	rm -f $(LIBFILE)
	rm -f $(SHLIBFILE)
	rm -f $(ROOTMAPFILE)
	rm -f $(ROOTMAPFILE)

.PHONY : shlib lib all clean

-include $(DLIST)
