#ifndef myDaVinci_cxx
#define myDaVinci_cxx

#include <myDaVinci.h>
using namespace std;
ClassImp(CaloEvent);






myDaVinci::myDaVinci(){
	m_tin = 0;
	m_tout = 0;
	m_nfout = "";
	m_ncontainers.clear();
	m_Ncontainers = 0;
	m_NullVertex = new Vertex();
	m_NullPart = new myParticle();
	m_NullPart->setOrigVertex(m_NullVertex);
	m_NullPart->setCaloVertex(m_NullVertex);
	m_NullPart->setEndVertex(m_NullVertex);
	m_cluster = 0;
	m_NullCell = new Cell();
	m_NullCell->initialise();
	m_NullCluster = new Cluster(m_NullCell);
	m_NullTrack = new myTrack(4);
	Double_t NullArr[4] = {-9999.,-9999.,-9999.,-9999.};
	m_NullTrack->setX(NullArr);
	m_NullTrack->setY(NullArr);
	m_NullTrack->setZ(NullArr);
	m_NullTrack->setT(NullArr);
	m_NullTrack->setPX(NullArr);
	m_NullTrack->setPY(NullArr);
	m_NullTrack->setPZ(NullArr);
	m_AllClusters = 0;
	
	m_Dvar.clear();
	m_Dfunctors.clear();
	m_nDvar = { "M"      ,
     			"PX"     , "PY"     , "PZ"     , "E"     , "PT"    ,
			    "CALOPX" , "CALOPY" , "CALOPZ" , "CALOE" , "CALOPT", "CALOEraw",
				"PVX"    , "PVY"    , "PVZ"    , "PVT"   ,
				"OVX"    , "OVY"    , "OVZ"    , "OVT"   ,
				"CALOVX" , "CALOVY" , "CALOVZ" , "CALOVT", "CALOVXraw", "CALOVYraw",
				"ENDVX"  , "ENDVY"  , "ENDVZ"  , "ENDVT" ,
			    "TRUE_M" ,
			    "TRUE_PX"    , "TRUE_PY"    , "TRUE_PZ"    , "TRUE_E"     , "TRUE_PT"    ,
				"TRUE_CALOPX", "TRUE_CALOPY", "TRUE_CALOPZ", "TRUE_CALOE" , "TRUE_CALOPT",
				"TRUE_PVX"   , "TRUE_PVY"   , "TRUE_PVZ"   , "TRUE_PVT"   ,
				"TRUE_OVX"   , "TRUE_OVY"   , "TRUE_OVZ"   , "TRUE_OVT"   ,
				"TRUE_CALOVX", "TRUE_CALOVY", "TRUE_CALOVZ", "TRUE_CALOVT",
				"TRUE_ENDVX" , "TRUE_ENDVY" , "TRUE_ENDVZ" , "TRUE_ENDVT" ,
				"1SEEDX","1SEEDY", "1SEEDE", "1SEEDT", "1SEED_SIZE",
				"2SEEDX","2SEEDY", "2SEEDE", "2SEEDT",
				"TRACK_CALOVX", "TRACK_CALOVY", "TRACK_CALOVT", "TRACK_DIST",
				"TRACK_PX", "TRACK_PY", "TRACK_PZ", 
				"TRACK1_X", "TRACK1_Y", "TRACK1_Z", "TRACK1_T", "TRACK1_PX", "TRACK1_PY", "TRACK1_PZ",   
				"TRACK2_X", "TRACK2_Y", "TRACK2_Z", "TRACK2_T", "TRACK2_PX", "TRACK2_PY", "TRACK2_PZ",   
				"TRACK3_X", "TRACK3_Y", "TRACK3_Z", "TRACK3_T", "TRACK3_PX", "TRACK3_PY", "TRACK3_PZ",   
				"TRACK4_X", "TRACK4_Y", "TRACK4_Z", "TRACK4_T", "TRACK4_PX", "TRACK4_PY", "TRACK4_PZ",
				"TRUE_TRACK_PX", "TRUE_TRACK_PY", "TRUE_TRACK_PZ", 
				"TRUE_TRACK1_X", "TRUE_TRACK1_Y", "TRUE_TRACK1_Z", "TRUE_TRACK1_T", "TRUE_TRACK1_PX", "TRUE_TRACK1_PY", "TRUE_TRACK1_PZ",   
				"TRUE_TRACK2_X", "TRUE_TRACK2_Y", "TRUE_TRACK2_Z", "TRUE_TRACK2_T", "TRUE_TRACK2_PX", "TRUE_TRACK2_PY", "TRUE_TRACK2_PZ",   
				"TRUE_TRACK3_X", "TRUE_TRACK3_Y", "TRUE_TRACK3_Z", "TRUE_TRACK3_T", "TRUE_TRACK3_PX", "TRUE_TRACK3_PY", "TRUE_TRACK3_PZ",   
				"TRUE_TRACK4_X", "TRUE_TRACK4_Y", "TRUE_TRACK4_Z", "TRUE_TRACK4_T", "TRUE_TRACK4_PX", "TRUE_TRACK4_PY", "TRUE_TRACK4_PZ",
				"BREMRECOV_OVEXTRAP_X", "BREMRECOV_OVEXTRAP_Y", 
				"BREMRECOV_UTEXTRAP_X", "BREMRECOV_UTEXTRAP_Y" 
			};

	m_NDvar = m_nDvar.size();
	m_Dfunctors.push_back([this](){ return m_part->getM()     ;}); // M
	m_Dfunctors.push_back([this](){ return m_part->getOrigPx();}); // PX
	m_Dfunctors.push_back([this](){ return m_part->getOrigPy();}); // PY
	m_Dfunctors.push_back([this](){ return m_part->getOrigPz();}); // PZ 
	m_Dfunctors.push_back([this](){ return m_part->getOrigE() ;}); // E
	m_Dfunctors.push_back([this](){ return m_part->getOrigPt();}); // PT
	m_Dfunctors.push_back([this](){ return m_part->getCaloPx();}); // CALOPX 
	m_Dfunctors.push_back([this](){ return m_part->getCaloPy();}); // CALOPY
	m_Dfunctors.push_back([this](){ return m_part->getCaloPz();}); // CALOPZ
	m_Dfunctors.push_back([this](){ return m_part->getCaloE() ;}); // CALOE 
	m_Dfunctors.push_back([this](){ return m_part->getCaloPt();}); // CALOPT 
	m_Dfunctors.push_back([this](){ return m_cluster->getE();});   // CALOEraw
	m_Dfunctors.push_back([this](){ return m_PV->getX()    ;}); // PVX     
	m_Dfunctors.push_back([this](){ return m_PV->getY()    ;}); // PVY        
	m_Dfunctors.push_back([this](){ return m_PV->getZ()    ;}); // PVZ     
	m_Dfunctors.push_back([this](){ return m_PV->getT()    ;}); // PVT
	m_Dfunctors.push_back([this](){ return m_origV->getX() ;}); // OVX   
	m_Dfunctors.push_back([this](){ return m_origV->getY() ;}); // OVY      
	m_Dfunctors.push_back([this](){ return m_origV->getZ() ;}); // OVZ   
	m_Dfunctors.push_back([this](){ return m_origV->getT() ;}); // OVT
	m_Dfunctors.push_back([this](){ return m_caloV->getX() ;}); // CALOVX  
	m_Dfunctors.push_back([this](){ return m_caloV->getY() ;}); // CALOVY  
	m_Dfunctors.push_back([this](){ return m_caloV->getZ() ;}); // CALOVZ  
	m_Dfunctors.push_back([this](){ return m_caloV->getT() ;});	// CALOVT 
	m_Dfunctors.push_back([this](){ return m_cluster->getX();}); // CALOVXraw 
	m_Dfunctors.push_back([this](){ return m_cluster->getY();}); // CALOVYraw
	m_Dfunctors.push_back([this](){ return m_endV->getX()  ;}); // ENDVX  
	m_Dfunctors.push_back([this](){ return m_endV->getY()  ;}); // ENDVY  
	m_Dfunctors.push_back([this](){ return m_endV->getZ()  ;}); // ENDVZ  
	m_Dfunctors.push_back([this](){ return m_endV->getT()  ;});	// ENDVT
	m_Dfunctors.push_back([this](){ return m_matchedPart->getM()      ;}); // TRUE_M
	m_Dfunctors.push_back([this](){ return m_matchedPart->getOrigPx() ;}); // TRUE_PX
	m_Dfunctors.push_back([this](){ return m_matchedPart->getOrigPy() ;}); // TRUE_PY
	m_Dfunctors.push_back([this](){ return m_matchedPart->getOrigPz() ;}); // TRUE_PZ
	m_Dfunctors.push_back([this](){ return m_matchedPart->getOrigE()  ;}); // TRUE_E
	m_Dfunctors.push_back([this](){ return m_matchedPart->getOrigPt() ;}); // TRUE_PT
	m_Dfunctors.push_back([this](){ return m_matchedPart->getCaloPx() ;}); // TRUE_CALOPX
	m_Dfunctors.push_back([this](){ return m_matchedPart->getCaloPy() ;}); // TRUE_CALOPY
	m_Dfunctors.push_back([this](){ return m_matchedPart->getCaloPz() ;}); // TRUE_CALOPZ
	m_Dfunctors.push_back([this](){ return m_matchedPart->getCaloE()  ;}); // TRUE_CALOE
	m_Dfunctors.push_back([this](){ return m_matchedPart->getCaloPt() ;}); // TRUE_CALOPT	
	m_Dfunctors.push_back([this](){ return m_matchedPV->getX()    ;}); // TRUE_PVX
	m_Dfunctors.push_back([this](){ return m_matchedPV->getY()    ;}); // TRUE_PVY
	m_Dfunctors.push_back([this](){ return m_matchedPV->getZ()    ;}); // TRUE_PVZ
	m_Dfunctors.push_back([this](){ return m_matchedPV->getT()    ;}); // TRUE_PVT
	m_Dfunctors.push_back([this](){ return m_matchedOrigV->getX() ;}); // TRUE_OVX
	m_Dfunctors.push_back([this](){ return m_matchedOrigV->getY() ;}); // TRUE_OVY
	m_Dfunctors.push_back([this](){ return m_matchedOrigV->getZ() ;}); // TRUE_OVZ
	m_Dfunctors.push_back([this](){ return m_matchedOrigV->getT() ;}); // TRUE_OVT
	m_Dfunctors.push_back([this](){ return m_matchedCaloV->getX() ;}); // TRUE_CALOVX
	m_Dfunctors.push_back([this](){ return m_matchedCaloV->getY() ;}); // TRUE_CALOVY
	m_Dfunctors.push_back([this](){ return m_matchedCaloV->getZ() ;}); // TRUE_CALOVZ
	m_Dfunctors.push_back([this](){ return m_matchedCaloV->getT() ;}); // TRUE_CALOVT
	m_Dfunctors.push_back([this](){ return m_matchedEndV->getX()  ;}); // TRUE_ENDVX
	m_Dfunctors.push_back([this](){ return m_matchedEndV->getY()  ;}); // TRUE_ENDVY
	m_Dfunctors.push_back([this](){ return m_matchedEndV->getZ()  ;}); // TRUE_ENDVZ
	m_Dfunctors.push_back([this](){ return m_matchedEndV->getT()  ;}); // TRUE_ENDVT	
	m_Dfunctors.push_back([this](){ return (m_cluster->getSeed()   != 0)? m_cluster->getSeed()->getX()   : -9999999 ;}); // 1SEEDX 
	m_Dfunctors.push_back([this](){ return (m_cluster->getSeed()   != 0)? m_cluster->getSeed()->getY()   : -9999999 ;}); // 1SEEDY 
	m_Dfunctors.push_back([this](){ return (m_cluster->getSeed()   != 0)? m_cluster->getSeed()->getE()   : -9999999 ;}); // 1SEEDE 
	m_Dfunctors.push_back([this](){ return (m_cluster->getSeed()   != 0)? m_cluster->getSeed()->getT()   : -9999999 ;}); // 1SEEDT 
	m_Dfunctors.push_back([this](){ return (m_cluster->getSeed()   != 0)? m_cluster->getSeed()->getSize(): -9999999 ;}); // 1SEED_SIZE
	m_Dfunctors.push_back([this](){ return (m_cluster->get2Ecell() != 0)? m_cluster->get2Ecell()->getX() : -9999999 ;}); // 2SEEDX
	m_Dfunctors.push_back([this](){ return (m_cluster->get2Ecell() != 0)? m_cluster->get2Ecell()->getY() : -9999999 ;}); // 2SEEDY
	m_Dfunctors.push_back([this](){ return (m_cluster->get2Ecell() != 0)? m_cluster->get2Ecell()->getE() : -9999999 ;}); // 2SEEDE
	m_Dfunctors.push_back([this](){ return (m_cluster->get2Ecell() != 0)? m_cluster->get2Ecell()->getT() : -9999999 ;}); // 2SEEDT
	m_Dfunctors.push_back([this](){ return m_closerTrack->getCaloVertexX();}); // TRACK_CALOVX
	m_Dfunctors.push_back([this](){ return m_closerTrack->getCaloVertexY();}); // TRACK_CALOVY
	m_Dfunctors.push_back([this](){ return m_closerTrack->getCaloVertexT();}); // TRACK_CALOVT
	m_Dfunctors.push_back([this](){ return sqrt(pow(m_caloV->getX()-m_closerTrack->getCaloVertexX(),2) + pow(m_caloV->getY()-m_closerTrack->getCaloVertexY(),2)) ;}); // TRACK_DIST
	m_Dfunctors.push_back([this](){ return m_track->getMeasPX();}); // TRACK_PX
	m_Dfunctors.push_back([this](){ return m_track->getMeasPY();}); // TRACK_PY
	m_Dfunctors.push_back([this](){ return m_track->getMeasPZ();}); // TRACK_PZ
	m_Dfunctors.push_back([this](){ return m_track->getX(0);  }); // TRACK1_X
	m_Dfunctors.push_back([this](){ return m_track->getY(0);  }); // TRACK1_Y
	m_Dfunctors.push_back([this](){ return m_track->getZ(0);  }); // TRACK1_Z
	m_Dfunctors.push_back([this](){ return m_track->getT(0);  }); // TRACK1_T
	m_Dfunctors.push_back([this](){ return m_track->getPX(0); }); // TRACK1_PX
	m_Dfunctors.push_back([this](){ return m_track->getPY(0); }); // TRACK1_PY
	m_Dfunctors.push_back([this](){ return m_track->getPZ(0); }); // TRACK1_PZ
	m_Dfunctors.push_back([this](){ return m_track->getX(1);  }); // TRACK2_X
	m_Dfunctors.push_back([this](){ return m_track->getY(1);  }); // TRACK2_Y
	m_Dfunctors.push_back([this](){ return m_track->getZ(1);  }); // TRACK2_Z
	m_Dfunctors.push_back([this](){ return m_track->getT(1);  }); // TRACK2_T
	m_Dfunctors.push_back([this](){ return m_track->getPX(1); }); // TRACK2_PX
	m_Dfunctors.push_back([this](){ return m_track->getPY(1); }); // TRACK2_PY
	m_Dfunctors.push_back([this](){ return m_track->getPZ(1); }); // TRACK2_PZ
	m_Dfunctors.push_back([this](){ return m_track->getX(2);  }); // TRACK3_X
	m_Dfunctors.push_back([this](){ return m_track->getY(2);  }); // TRACK3_Y
	m_Dfunctors.push_back([this](){ return m_track->getZ(2);  }); // TRACK3_Z
	m_Dfunctors.push_back([this](){ return m_track->getT(2);  }); // TRACK3_T
	m_Dfunctors.push_back([this](){ return m_track->getPX(2); }); // TRACK3_PX
	m_Dfunctors.push_back([this](){ return m_track->getPY(2); }); // TRACK3_PY
	m_Dfunctors.push_back([this](){ return m_track->getPZ(2); }); // TRACK3_PZ
	m_Dfunctors.push_back([this](){ return m_track->getX(3);  }); // TRACK4_X
	m_Dfunctors.push_back([this](){ return m_track->getY(3);  }); // TRACK4_Y
	m_Dfunctors.push_back([this](){ return m_track->getZ(3);  }); // TRACK4_Z
	m_Dfunctors.push_back([this](){ return m_track->getT(3);  }); // TRACK4_T
	m_Dfunctors.push_back([this](){ return m_track->getPX(3); }); // TRACK4_PX
	m_Dfunctors.push_back([this](){ return m_track->getPY(3); }); // TRACK4_PY
	m_Dfunctors.push_back([this](){ return m_track->getPZ(3); }); // TRACK4_PZ
	m_Dfunctors.push_back([this](){ return m_matchedTrack->getMeasPX();}); // TRUE_TRACK_PX
	m_Dfunctors.push_back([this](){ return m_matchedTrack->getMeasPY();}); // TRUE_TRACK_PY
	m_Dfunctors.push_back([this](){ return m_matchedTrack->getMeasPZ();}); // TRUE_TRACK_PZ
	m_Dfunctors.push_back([this](){ return m_matchedTrack->getX(0);  }); // TRUE_TRACK1_X
	m_Dfunctors.push_back([this](){ return m_matchedTrack->getY(0);  }); // TRUE_TRACK1_Y
	m_Dfunctors.push_back([this](){ return m_matchedTrack->getZ(0);  }); // TRUE_TRACK1_Z
	m_Dfunctors.push_back([this](){ return m_matchedTrack->getT(0);  }); // TRUE_TRACK1_T
	m_Dfunctors.push_back([this](){ return m_matchedTrack->getPX(0); }); // TRUE_TRACK1_PX
	m_Dfunctors.push_back([this](){ return m_matchedTrack->getPY(0); }); // TRUE_TRACK1_PY
	m_Dfunctors.push_back([this](){ return m_matchedTrack->getPZ(0); }); // TRUE_TRACK1_PZ
	m_Dfunctors.push_back([this](){ return m_matchedTrack->getX(1);  }); // TRUE_TRACK2_X
	m_Dfunctors.push_back([this](){ return m_matchedTrack->getY(1);  }); // TRUE_TRACK2_Y
	m_Dfunctors.push_back([this](){ return m_matchedTrack->getZ(1);  }); // TRUE_TRACK2_Z
	m_Dfunctors.push_back([this](){ return m_matchedTrack->getT(1);  }); // TRUE_TRACK2_T
	m_Dfunctors.push_back([this](){ return m_matchedTrack->getPX(1); }); // TRUE_TRACK2_PX
	m_Dfunctors.push_back([this](){ return m_matchedTrack->getPY(1); }); // TRUE_TRACK2_PY
	m_Dfunctors.push_back([this](){ return m_matchedTrack->getPZ(1); }); // TRUE_TRACK2_PZ
	m_Dfunctors.push_back([this](){ return m_matchedTrack->getX(2);  }); // TRUE_TRACK3_X
	m_Dfunctors.push_back([this](){ return m_matchedTrack->getY(2);  }); // TRUE_TRACK3_Y
	m_Dfunctors.push_back([this](){ return m_matchedTrack->getZ(2);  }); // TRUE_TRACK3_Z
	m_Dfunctors.push_back([this](){ return m_matchedTrack->getT(2);  }); // TRUE_TRACK3_T
	m_Dfunctors.push_back([this](){ return m_matchedTrack->getPX(2); }); // TRUE_TRACK3_PX
	m_Dfunctors.push_back([this](){ return m_matchedTrack->getPY(2); }); // TRUE_TRACK3_PY
	m_Dfunctors.push_back([this](){ return m_matchedTrack->getPZ(2); }); // TRUE_TRACK3_PZ
	m_Dfunctors.push_back([this](){ return m_matchedTrack->getX(3);  }); // TRUE_TRACK4_X
	m_Dfunctors.push_back([this](){ return m_matchedTrack->getY(3);  }); // TRUE_TRACK4_Y
	m_Dfunctors.push_back([this](){ return m_matchedTrack->getZ(3);  }); // TRUE_TRACK4_Z
	m_Dfunctors.push_back([this](){ return m_matchedTrack->getT(3);  }); // TRUE_TRACK4_T
	m_Dfunctors.push_back([this](){ return m_matchedTrack->getPX(3); }); // TRUE_TRACK4_PX
	m_Dfunctors.push_back([this](){ return m_matchedTrack->getPY(3); }); // TRUE_TRACK4_PY
	m_Dfunctors.push_back([this](){ return m_matchedTrack->getPZ(3); }); // TRUE_TRACK4_PZ
	m_Dfunctors.push_back([this](){ return m_BremRecov_OVExtrap_x;   }); // BREMRECOV_OVEXTRAP_X
	m_Dfunctors.push_back([this](){ return m_BremRecov_OVExtrap_y;   }); // BREMRECOV_OVEXTRAP_Y
	m_Dfunctors.push_back([this](){ return m_BremRecov_UTExtrap_x;   }); // BREMRECOV_UTEXTRAP_X
	m_Dfunctors.push_back([this](){ return m_BremRecov_UTExtrap_y;   }); // BREMRECOV_UTEXTRAP_Y

	m_nDvarUser.clear();
	m_DfunctorsUser.clear();
	m_DvarUser.clear();
	m_NDvarUser.clear();
	
	m_Ivar.clear();
	m_Ifunctors.clear();
	m_nIvar = {"isMatched","isSig", "ID", "MC_MOTHER_ID", "MC_GD_MOTHER_ID",
				"PVKEY", "OVKEY", "OVTYP", "ENDVKEY", "ENDVTYP", 
			    "TRUE_isSig", "TRUE_ID", "TRUE_MC_MOTHER_ID", "TRUE_MC_GD_MOTHER_ID", 
			    "TRUE_PVKEY", "TRUE_OVKEY", "TRUE_OVTYP", "TRUE_ENDVKEY", "TRUE_ENDVTYP", 
			    "NHITS", "NCONTRIB","NCONTRIB50",
			    "SEED_REGION", "SEED_TYPE", "NCELLS",  "pi0Type", "TRUE_pi0Type", 
			    "INCALOACC", "ALIVEATCALOZ", "ISTRACKABLE", "HitCaloSensDet", "isRecoveredByVanya", 
				"TRUE_INCALOACC", "TRUE_ALIVEATCALOZ", "TRUE_ISTRACKABLE", "TRUE_HitCaloSensDet", "TRUE_isRecoveredByVanya",
				"HasTrackableAncestor", "TRUE_HasTrackableAncestor",
				"TRACK_ID", "TrackableAncestor_ID", "TRUE_TrackableAncestor_ID",
				"TRACK_MATCHED", "isClosestToTrack",
				"BREMN", "CLUSTNEIGH100_N", "BREMRECOV_N"
			};
	m_NIvar = m_nIvar.size();
	m_Ifunctors.push_back([this](){ return (m_matchedPart!=m_NullPart)?1:0;});
	m_Ifunctors.push_back([this](){ return m_part->getIsSig()         ;});
	m_Ifunctors.push_back([this](){ return m_part->getID()            ;});
	m_Ifunctors.push_back([this](){ return m_part->getMid()           ;});
	m_Ifunctors.push_back([this](){ return m_part->getGmid()          ;});
	m_Ifunctors.push_back([this](){ return m_PV->getKey()             ;});
	m_Ifunctors.push_back([this](){ return m_origV->getKey()          ;});
	m_Ifunctors.push_back([this](){ return m_origV->getType()         ;});
	m_Ifunctors.push_back([this](){ return m_endV->getKey()           ;});
	m_Ifunctors.push_back([this](){ return m_endV->getType()          ;});
	m_Ifunctors.push_back([this](){ return m_matchedPart->getIsSig()  ;});
	m_Ifunctors.push_back([this](){ return m_matchedPart->getID()     ;});
	m_Ifunctors.push_back([this](){ return m_matchedPart->getMid()    ;});
	m_Ifunctors.push_back([this](){ return m_matchedPart->getGmid()   ;});
	m_Ifunctors.push_back([this](){ return m_matchedPV->getKey()      ;});
	m_Ifunctors.push_back([this](){ return m_matchedOrigV->getKey()   ;});
	m_Ifunctors.push_back([this](){ return m_matchedOrigV->getType()  ;});
	m_Ifunctors.push_back([this](){ return m_matchedEndV->getKey()    ;});
	m_Ifunctors.push_back([this](){ return m_matchedEndV->getType()   ;});
	m_Ifunctors.push_back([this](){ return m_HittingPhotons.size()    ;});	
	m_Ifunctors.push_back([this](){ return m_cluster->getContributingParticles( 0).size();});	
	m_Ifunctors.push_back([this](){ return m_cluster->getContributingParticles(50).size();});	
	m_Ifunctors.push_back([this](){ return m_cluster->getSeedRegion() ;});
	m_Ifunctors.push_back([this](){ return m_cluster->getSeed()->getSeedType();});
	m_Ifunctors.push_back([this](){ return m_cluster->getCellIDs().size();});	
	m_Ifunctors.push_back([this](){ return m_part->getPi0Type()       ;});
	m_Ifunctors.push_back([this](){ return m_matchedPart->getPi0Type();});
	m_Ifunctors.push_back([this](){ return m_part->getInCaloAcc();});
	m_Ifunctors.push_back([this](){ return m_part->getAliveAtCaloZ();});
	m_Ifunctors.push_back([this](){ return m_part->getTrackable();});
	m_Ifunctors.push_back([this](){ return m_part->getHitCaloSensDet();});
	m_Ifunctors.push_back([this](){ return m_part->getIsRecoveredByVanya();});
	m_Ifunctors.push_back([this](){ return m_matchedPart->getInCaloAcc();});
	m_Ifunctors.push_back([this](){ return m_matchedPart->getAliveAtCaloZ();});
	m_Ifunctors.push_back([this](){ return m_matchedPart->getTrackable();});
	m_Ifunctors.push_back([this](){ return m_matchedPart->getHitCaloSensDet();});
	m_Ifunctors.push_back([this](){ return m_matchedPart->getIsRecoveredByVanya();});

	m_Ifunctors.push_back([this](){ auto parents = m_part->getParents(); 
									for (auto parent : parents)
										if (parent->getTrackable()==1) return 1;
									return 0;
	                			});
	m_Ifunctors.push_back([this](){ auto parents = m_matchedPart->getParents(); 
									for (auto parent : parents)
										if (parent->getTrackable()==1) return 1;
									return 0;
	                			});

	m_Ifunctors.push_back([this](){ return m_closerTrack->getID(); }); 
	m_Ifunctors.push_back([this](){ auto parents = m_part->getParents(); 
									for (auto parent : parents)
										if (parent->getTrackable()==1) return parent->getID();
									return 0;
	                			});
	m_Ifunctors.push_back([this](){ auto parents = m_matchedPart->getParents(); 
									for (auto parent : parents)
										if (parent->getTrackable()==1) return parent->getID();
									return 0;
	                			});
	m_Ifunctors.push_back([this](){ 
									Long64_t key = m_closerTrack->getKey();
									if (key==m_matchedPart->getKey())     return 1;
		                            auto parents = m_matchedPart->getParents(); 
									for (auto parent : parents)
										if (key==parent->getKey()) return 1;
									return 0;
								});
	m_Ifunctors.push_back([this](){
									Double_t x = m_closerTrack->getCaloVertexX();
									Double_t y = m_closerTrack->getCaloVertexX();
									Double_t dist = pow(x-m_part->getCaloVertexX(),2)+
									                pow(y-m_part->getCaloVertexY(),2);
									for (auto it_cluster = m_AllClusters->begin(); it_cluster != m_AllClusters->end(); ++it_cluster){
										Cluster * cluster = (Cluster*)(*it_cluster);
										if (pow(x-cluster->getCalibX(),2)+pow(y-cluster->getCalibY(),2) < dist) return 0;
									}
									return 1;
								});
	m_Ifunctors.push_back([this](){
									auto daughters = m_part->get_daughters();
									Int_t nbrem = 0;
									for_each(daughters.begin(), daughters.end(), [&nbrem](myProtoParticle* daughter){
										if (daughter->getOrigVertex()->getType() == 101) ++nbrem;
									});
									return nbrem; 
								});
	m_Ifunctors.push_back([this](){ return (m_ClustWithin100.size()<50)?m_ClustWithin100.size():50; });
	m_Ifunctors.push_back([this](){ return (m_BremRecovClust.size()<50)?m_BremRecovClust.size():50; });

	m_nIvarUser.clear();
	m_IfunctorsUser.clear();
	m_IvarUser.clear();
	m_NIvarUser.clear();
		

	m_Lvar.clear();
	m_Lfunctors.clear();
	m_nLvar = {     "KEY",      "MC_MOTHER_KEY",      "MC_GD_MOTHER_KEY",
			   "TRUE_KEY", "TRUE_MC_MOTHER_KEY", "TRUE_MC_GD_MOTHER_KEY"};
	m_NLvar = m_nLvar.size();
	m_Lfunctors.push_back([this](){ return m_part->getKey()  ;});
	m_Lfunctors.push_back([this](){ return m_part->getMkey() ;});
	m_Lfunctors.push_back([this](){ return m_part->getGmkey();});
	m_Lfunctors.push_back([this](){ return m_matchedPart->getKey()   ;});
	m_Lfunctors.push_back([this](){ return m_matchedPart->getMkey()  ;});
	m_Lfunctors.push_back([this](){ return m_matchedPart->getGmkey() ;});
	m_nLvarUser.clear();
	m_LfunctorsUser.clear();
	m_LvarUser.clear();
	m_NLvarUser.clear();
	

	m_VDvar.clear();
	m_VDfunctors.clear();
	m_nVDvar = {"BREMX","BREMY","BREMZ",
				"BREMPX", "BREMPY", "BREMPZ", "BREME", 
				"BREMCALOX","BREMCALOY", "BREMCALOT", "BREMCALOE",
				"CLUSTNEIGH100_X", "CLUSTNEIGH100_Y", "CLUSTNEIGH100_T", "CLUSTNEIGH100_E",
				"BREMRECOV_X", "BREMRECOV_Y", "BREMRECOV_T", "BREMRECOV_E", 
				"BREMRECOV_1SEED_SIZE", "BREMRECOV_TRACK_DIST", "BREMRECOV_TRACK_DIST_NOTSIG"
			};
	m_NVDvar = m_nVDvar.size();
	m_VDfunctors.push_back([this](const char* ncontainer, int k){ //BREMX
			auto daughters = m_part->get_daughters();
			Int_t i=0;
			for (auto daughter : daughters){
				if (daughter->getOrigVertex()->getType() != 101) continue;
				m_VDvarUser[string(ncontainer)][k][i] = daughter->getOrigVertexX();
				++i;
				if (i==49) break;
			}				
		});
	m_VDfunctors.push_back([this](const char* ncontainer, int k){ //BREMY
			auto daughters = m_part->get_daughters();
			Int_t i=0;
			for (auto daughter : daughters){
				if (daughter->getOrigVertex()->getType() != 101) continue;
				m_VDvarUser[string(ncontainer)][k][i] = daughter->getOrigVertexY();
				++i;
				if (i==49) break;
			}				
		});
	m_VDfunctors.push_back([this](const char* ncontainer, int k){ //BREMZ
			auto daughters = m_part->get_daughters();
			Int_t i=0;
			for (auto daughter : daughters){
				if (daughter->getOrigVertex()->getType() != 101) continue;
				m_VDvarUser[string(ncontainer)][k][i] = daughter->getOrigVertexZ();
				++i;
				if (i==49) break;
			}				
		});
	m_VDfunctors.push_back([this](const char* ncontainer, int k){  //BREMPX
			auto daughters = m_part->get_daughters();
			Int_t i=0;
			for (auto daughter : daughters){
				if (daughter->getOrigVertex()->getType() != 101) continue;
				m_VDvarUser[string(ncontainer)][k][i] = daughter->getOrigPx();
				++i;
				if (i==49) break;
			}				
		});
	m_VDfunctors.push_back([this](const char* ncontainer, int k){  //BREMPY
			auto daughters = m_part->get_daughters();
			Int_t i=0;
			for (auto daughter : daughters){
				if (daughter->getOrigVertex()->getType() != 101) continue;
				m_VDvarUser[string(ncontainer)][k][i] = daughter->getOrigPy();
				++i;
				if (i==49) break;
			}				
		});
	m_VDfunctors.push_back([this](const char* ncontainer, int k){  //BREMPZ
			auto daughters = m_part->get_daughters();
			Int_t i=0;
			for (auto daughter : daughters){
				if (daughter->getOrigVertex()->getType() != 101) continue;
				m_VDvarUser[string(ncontainer)][k][i] = daughter->getOrigPz();
				++i;
				if (i==49) break;
			}				
		});
	m_VDfunctors.push_back([this](const char* ncontainer, int k){  //BREME
			auto daughters = m_part->get_daughters();
			Int_t i=0;
			for (auto daughter : daughters){
				if (daughter->getOrigVertex()->getType() != 101) continue;
				m_VDvarUser[string(ncontainer)][k][i] = daughter->getOrigE();
				++i;
				if (i==49) break;
			}				
		});
	m_VDfunctors.push_back([this](const char* ncontainer, int k){  // BREMCALOX
			auto daughters = m_part->get_daughters();
			Int_t i=0;
			for (auto daughter : daughters){
				if (daughter->getOrigVertex()->getType() != 101) continue;
				if (((myParticle*)daughter)->getHitCaloSensDet()!=1 ||
					((myParticle*)daughter)->getInCaloAcc()!=1      ||
					daughter->getCaloVertex() == 0)
					m_VDvarUser[string(ncontainer)][k][i] = -9999999.0;
				else		
					m_VDvarUser[string(ncontainer)][k][i] = daughter->getCaloVertexX();
				++i;
				if (i==49) break;
			}				
		});
	m_VDfunctors.push_back([this](const char* ncontainer, int k){ //BREMCALOY
			auto daughters = m_part->get_daughters();
			Int_t i=0;
			for (auto daughter : daughters){
				if (daughter->getOrigVertex()->getType() != 101) continue;
				if (((myParticle*)daughter)->getHitCaloSensDet()!=1 ||
					((myParticle*)daughter)->getInCaloAcc()!=1      ||
					daughter->getCaloVertex() == 0)
					m_VDvarUser[string(ncontainer)][k][i] = -9999999.0;
				else
					m_VDvarUser[string(ncontainer)][k][i] = daughter->getCaloVertexY();
				++i;
				if (i==49) break;
			}				
		});
	m_VDfunctors.push_back([this](const char* ncontainer, int k){ //BREMCALOT
			auto daughters = m_part->get_daughters();
			Int_t i=0;
			for (auto daughter : daughters){
				if (daughter->getOrigVertex()->getType() != 101) continue;
				if (((myParticle*)daughter)->getHitCaloSensDet()!=1 ||
					((myParticle*)daughter)->getInCaloAcc()!=1      ||
					daughter->getCaloVertex() == 0)
					m_VDvarUser[string(ncontainer)][k][i] = -9999999.0;
				else
					m_VDvarUser[string(ncontainer)][k][i] = daughter->getCaloVertexT();
				++i;
				if (i==49) break;
			}				
		});
	m_VDfunctors.push_back([this](const char* ncontainer, int k){ //BREMCALOE
			Int_t i=0;
			m_VDvarUser[string(ncontainer)][k][i] = 0.;
			auto daughters = m_part->get_daughters();
			for (auto daughter : daughters){
				if (daughter->getOrigVertex()->getType() != 101) continue; // skip not brem. photons
				Double_t calo_x = daughter->getOrigVertexX() + (event->getZ()-daughter->getOrigVertexZ())*daughter->getOrigPx()/daughter->getOrigPz();
				Double_t calo_y = daughter->getOrigVertexY() + (event->getZ()-daughter->getOrigVertexZ())*daughter->getOrigPy()/daughter->getOrigPz();
				Int_t ID = event->getIDFromXY(calo_x,calo_y);
				if (ID<1) continue; // skip brem. photons out of ECAL acceptance
				if (((myParticle*)daughter)->getHitCaloSensDet()==1 &&
					((myParticle*)daughter)->getInCaloAcc()==1      &&
					daughter->getCaloVertex() != 0){ // brem. photon hits the ECAL:
					m_VDvarUser[string(ncontainer)][k][i] = daughter->getCaloE(); //   - save its energy and continue
				} else {                             // brem. photons does not hit the ECAL: 
					vector<myParticle*> descendants;
					((myParticle*)daughter)->getDescendants(descendants);
					for (auto descendant : descendants){
						if (descendant->getHitCaloSensDet()!=1 ||
							descendant->getInCaloAcc()!=1      ||
							descendant->getCaloVertex() == 0) continue; //    - check whether its daugther does;
						Double_t dist = pow(calo_x - descendant->getCaloVertexX(),2) + pow(calo_y - descendant->getCaloVertexY(),2);
						if (dist > 100*100) continue;                  //    - include hits "close" to brem. photon extrapolation;
						m_VDvarUser[string(ncontainer)][k][i] += descendant->getCaloE();
					}
				}
				++i;
				if (i==49) break;	
			}				
		});
	m_VDfunctors.push_back([this](const char* ncontainer, int k){ //CLUSTNEIGH100_X
			Int_t i=0;
			for (auto cluster : m_ClustWithin100){
				m_VDvarUser[string(ncontainer)][k][i] = cluster->getCalibX();
				++i;
				if (i==49) break;
			}
		});
	m_VDfunctors.push_back([this](const char* ncontainer, int k){ //CLUSTNEIGH100_Y
			Int_t i=0;
			for (auto cluster : m_ClustWithin100){
				m_VDvarUser[string(ncontainer)][k][i] = cluster->getCalibY();
				++i;
				if (i==49) break;
			}
		});
	m_VDfunctors.push_back([this](const char* ncontainer, int k){ //CLUSTNEIGH100_T
			Int_t i=0;
			for (auto cluster : m_ClustWithin100){
				m_VDvarUser[string(ncontainer)][k][i] = cluster->getSeed()->getT();
				++i;
				if (i==49) break;
			}
		});
	m_VDfunctors.push_back([this](const char* ncontainer, int k){ //CLUSTNEIGH100_E
			Int_t i=0;
			for (auto cluster : m_ClustWithin100){
				m_VDvarUser[string(ncontainer)][k][i] = cluster->getCalibE();
				++i;
				if (i==49) break;
			}
		});
	m_VDfunctors.push_back([this](const char* ncontainer, int k){ //BREMRECOV_X
			Int_t i=0;
			for (auto cluster : m_BremRecovClust){
				m_VDvarUser[string(ncontainer)][k][i] = cluster->getCalibX();
				++i;
				if (i==49) break;
			}
		});
	m_VDfunctors.push_back([this](const char* ncontainer, int k){ //BREMRECOV_Y
			Int_t i=0;
			for (auto cluster : m_BremRecovClust){
				m_VDvarUser[string(ncontainer)][k][i] = cluster->getCalibY();
				++i;
				if (i==49) break;
			}
		});
	m_VDfunctors.push_back([this](const char* ncontainer, int k){ //BREMRECOV_T
			Int_t i=0;
			for (auto cluster : m_BremRecovClust){
				m_VDvarUser[string(ncontainer)][k][i] = cluster->getSeed()->getT();
				++i;
				if (i==49) break;
			}
		});
	m_VDfunctors.push_back([this](const char* ncontainer, int k){ //BREMRECOV_E
			Int_t i=0;
			for (auto cluster : m_BremRecovClust){
				m_VDvarUser[string(ncontainer)][k][i] = cluster->getCalibE();
				++i;
				if (i==49) break;
			}
		});
	m_VDfunctors.push_back([this](const char* ncontainer, int k){ //BREMRECOV_1SEED_SIZE
			Int_t i=0;
			for (auto cluster : m_BremRecovClust){
				m_VDvarUser[string(ncontainer)][k][i] = cluster->getSeed()->getSize();
				++i;
				if (i==49) break;
			}
		});
	m_VDfunctors.push_back([this](const char* ncontainer, int k){ //BREMRECOV_TRACK_DIST
			Int_t i=0;
			for (auto cluster : m_BremRecovClust){
				auto closerTrack = cluster->getCloserTrack();
				if (closerTrack != 0)
					m_VDvarUser[string(ncontainer)][k][i] = sqrt(pow(cluster->getCalibX()-closerTrack->getCaloVertexX(),2)+ 
						                                         pow(cluster->getCalibY()-closerTrack->getCaloVertexY(),2));
				else
					m_VDvarUser[string(ncontainer)][k][i] = 9999999;
				++i;
				if (i==49) break;
			}
		});
	m_VDfunctors.push_back([this](const char* ncontainer, int k){ //BREMRECOV_TRACK_DIST_NOTSIG, working only when m_part is the true signal particle
			Int_t i=0;
			auto tracks = event->getTracks();
			Long64_t sig_key = m_part->getKey();
			Double_t closer_dist = -1, tmp_dist = -1;
			Int_t T_closer=-1;	
			for (auto cluster : m_BremRecovClust){
				closer_dist = -1;
				tmp_dist    = -1;
  				T_closer    = -1;
  				for (Int_t T=0; T<(int)tracks->size(); ++T){
  					if (tracks->part[T]->getKey() == sig_key) continue;
      				tmp_dist = pow(cluster->getCalibX()-tracks->x[T], 2)+ 
      				           pow(cluster->getCalibY()-tracks->y[T], 2);
      				if (tmp_dist>closer_dist && closer_dist>0) continue;
      				closer_dist = tmp_dist;
      				T_closer=T;
    			}
    			if (T_closer != -1) 
					m_VDvarUser[string(ncontainer)][k][i] = sqrt(closer_dist);
				else
					m_VDvarUser[string(ncontainer)][k][i] = 9999999;
				++i;
				if (i==49) break;
			}
		});
		

	m_nVDvarUser.clear();
	m_VDfunctorsUser.clear();
	m_VDvarUser.clear();
	m_NVDvarUser.clear();

	m_VIvar.clear();
	m_VIfunctors.clear();
	m_nVIvar = {"CLUSTNEIGH100_ISFROMTRACK", "CLUSTNEIGH100_ISFROMTRACKbrem",
				"BREMRECOV_ISFROMTRACKBREM1", "BREMRECOV_ISFROMTRACKBREM2", "BREMRECOV_SEED_REGION"
			};
	m_NVIvar = m_nVIvar.size();
	m_VIfunctors.push_back([this](const char* ncontainer, int k){ //CLUSTNEIGH100_ISFROMTRACK
			Int_t i=0;
			vector<myParticle*> descendants;
			m_part->getDescendants(descendants);
			vector<Long64_t> keys; 
			keys.push_back(m_part->getKey());
			for_each(descendants.begin(), descendants.end(), [&keys](myParticle *descendant){ keys.push_back(descendant->getKey()); });
			for (auto cluster : m_ClustWithin100){
				m_VIvarUser[string(ncontainer)][k][i] = 0;
				auto hittingParts = cluster->getHittingPhotons();
				for (auto hittingPart : hittingParts){
					if (find(keys.begin(), keys.end(), hittingPart->getKey()) == keys.end()) continue;
					m_VIvarUser[string(ncontainer)][k][i] = 1;
					break;	
				}
				++i;
				if (i==49) break;
			}
		});
	m_VIfunctors.push_back([this](const char* ncontainer, int k){ //CLUSTNEIGH100_ISFROMTRACKbrem
			Int_t i=0;
			vector<myProtoParticle*> daughters = m_part->get_daughters();
			vector<Long64_t> keys; 
			keys.push_back(m_part->getKey());
			for_each(daughters.begin(), daughters.end(), [&keys](myProtoParticle *daughter){ 
				if (daughter->getOrigVertex()->getType()==101)
					keys.push_back(daughter->getKey()); 
			});
			for (auto cluster : m_ClustWithin100){
				m_VIvarUser[string(ncontainer)][k][i] = 0;
				auto hittingParts = cluster->getHittingPhotons();
				for (auto hittingPart : hittingParts){
					if (find(keys.begin(), keys.end(), hittingPart->getKey()) == keys.end()) continue;
					m_VIvarUser[string(ncontainer)][k][i] = 1;
					break;	
				}
				++i;
				if (i==49) break;
			}
		});

	m_VIfunctors.push_back([this](const char* ncontainer, int k){ //BREMRECOV_ISFROMTRACKBREM1, true if a brem photon hits the cluster
			Int_t i=0;
			vector<myProtoParticle*> daughters = m_part->get_daughters();
			vector<Long64_t> keys; 
			keys.push_back(m_part->getKey());
			for_each(daughters.begin(), daughters.end(), [&keys](myProtoParticle *daughter){ 
				if (daughter->getOrigVertex()->getType()==101)
					keys.push_back(daughter->getKey()); 
			});
			for (auto cluster : m_BremRecovClust){
				m_VIvarUser[string(ncontainer)][k][i] = 0;
				auto hittingParts = cluster->getHittingPhotons();
				for (auto hittingPart : hittingParts){
					if (find(keys.begin(), keys.end(), hittingPart->getKey()) == keys.end()) continue;
					m_VIvarUser[string(ncontainer)][k][i] = 1;
					break;	
				}
				++i;
				if (i==49) break;
			}
		});
	m_VIfunctors.push_back([this](const char* ncontainer, int k){ //BREMRECOV_ISFROMTRACKBREM2, true if a brem photon or its daughter, hits the cluster
			Int_t i=0;
			vector<myProtoParticle*> daughters = m_part->get_daughters();
			vector<Long64_t> keys; 
			keys.push_back(m_part->getKey());
			for_each(daughters.begin(), daughters.end(), [&keys](myProtoParticle *daughter){ 
				if (daughter->getOrigVertex()->getType()==101)
					keys.push_back(daughter->getKey()); 
			});
			for (auto cluster : m_BremRecovClust){
				m_VIvarUser[string(ncontainer)][k][i] = 0;
				auto hittingParts = cluster->getHittingPhotons();
				for (auto hittingPart : hittingParts){
					if (find_if(keys.begin(), keys.end(), [hittingPart](Long64_t key){ 
									return (hittingPart->getKey() == key) || (hittingPart->getMkey() == key); 
						} ) == keys.end()) continue;
					m_VIvarUser[string(ncontainer)][k][i] = 1;
					break;	
				}
				++i;
				if (i==49) break;
			}
		});
	m_VIfunctors.push_back([this](const char* ncontainer, int k){ // BREMRECOV_REGION
		Int_t i=0;
		for (auto cluster : m_BremRecovClust){
			m_VIvarUser[string(ncontainer)][k][i] = cluster->getSeedRegion();
			++i;
			if (i==49) break;
		}
	});
	m_nVIvarUser.clear();
	m_VIfunctorsUser.clear();
	m_VIvarUser.clear();
	m_NVIvarUser.clear();

	m_checkMatch = [](){ return false; };
	m_exceptions = []() {};
}
myDaVinci::~myDaVinci(){
	delete m_NullVertex;
	delete m_NullPart;
}

TTree * myDaVinci::getOutTree() {
	return m_tout;
}
vector<myParticle>* myDaVinci::getParts(){
	return &m_parts;
}
void myDaVinci::setCheckMatch(function<Bool_t()> checkMatch){
	m_checkMatch = checkMatch;
};
void myDaVinci::setExceptions(function<void()> exceptions){
	m_exceptions = exceptions;
}
void myDaVinci::initialize(TTree *tin, vector<string> ncontainers, map<string, vector<string>> allBranches,
						   function<vector<myParticle>* (CaloEvent*)> getMotherContainer,
	                       function<vector<myParticle>(myParticle*)> getParts){
	cout << "myDaVinci::initialize starts\n";
	m_tin = tin;
	//tin->Print();

	m_ncontainers = ncontainers;
	m_Ncontainers = ncontainers.size();
	m_nmother     = m_ncontainers[0];
	m_allBranches = allBranches;

	m_getParts = getParts;
	m_getMotherContainer = getMotherContainer;
	m_tout = new TTree("ntp", "ntp");
	m_tout->SetDirectory(0);

	for (auto part_branches : m_allBranches){
		auto ncontainer = part_branches.first;
		auto nbranches  = part_branches.second;
		//double
		m_NDvarUser[ncontainer] = 0;
		m_NIvarUser[ncontainer] = 0;
		m_NLvarUser[ncontainer] = 0;
		
		m_DvarUser[ncontainer] = vector<Double_t>(m_NDvar, 0.0);
		m_IvarUser[ncontainer] = vector<Int_t>   (m_NIvar, 0  );
		m_LvarUser[ncontainer] = vector<Long64_t>(m_NDvar, 0  );
		
		m_VDvarUser[ncontainer]= vector<vector<double>>(m_NDvar, vector<double>(50, 0.0));
		m_VIvarUser[ncontainer]= vector<vector<int   >>(m_NDvar, vector<int   >(50, 0  ));
		
		for (auto nbranch : nbranches){
			//double
			auto it_D = find(m_nDvar.begin(), m_nDvar.end(), nbranch);
			if (it_D != m_nDvar.end()){
				auto bname = ncontainer+"_"+nbranch;
				m_nDvarUser[ncontainer].push_back(nbranch);
    			m_tout->Branch( bname.data(), &(m_DvarUser[ncontainer][m_NDvarUser[ncontainer]]), (bname+"/D").data() );
				m_NDvarUser[ncontainer] += 1;
				Int_t iDfunctor = distance(m_nDvar.begin(), it_D);
				m_DfunctorsUser[ncontainer].push_back(m_Dfunctors[iDfunctor]);
			}
			auto it_I = find(m_nIvar.begin(), m_nIvar.end(), nbranch);
			if (it_I != m_nIvar.end()){
				auto bname = ncontainer+"_"+nbranch;
				m_nIvarUser[ncontainer].push_back(nbranch);
    			m_tout->Branch( bname.data(), &(m_IvarUser[ncontainer][m_NIvarUser[ncontainer]]), (bname+"/I").data() );
				m_NIvarUser[ncontainer] += 1;
				Int_t iIfunctor = distance(m_nIvar.begin(), it_I);
				m_IfunctorsUser[ncontainer].push_back(m_Ifunctors[iIfunctor]);
			}
			auto it_L = find(m_nLvar.begin(), m_nLvar.end(), nbranch);
			if (it_L != m_nLvar.end()){
				auto bname = ncontainer+"_"+nbranch;
				m_nLvarUser[ncontainer].push_back(nbranch);
    			m_tout->Branch( bname.data(), &(m_LvarUser[ncontainer][m_NLvarUser[ncontainer]]), (bname+"/L").data() );
				m_NLvarUser[ncontainer] += 1;
				Int_t iLfunctor = distance(m_nLvar.begin(), it_L);
				m_LfunctorsUser[ncontainer].push_back(m_Lfunctors[iLfunctor]);
			}
			string VsizeName = "";
			if      (nbranch.find(string("BREMRECOV")) != string::npos)
				VsizeName = "["+ncontainer+"_BREMRECOV_N]";
			else if (nbranch.find(string("BREM")) != string::npos)
				VsizeName = "["+ncontainer+"_BREMN]";
			else if (nbranch.find(string("CLUSTNEIGH100")) != string::npos) 
				VsizeName = "["+ncontainer+"_CLUSTNEIGH100_N]";

			auto it_VD = find(m_nVDvar.begin(), m_nVDvar.end(), nbranch);
			if (it_VD != m_nVDvar.end()){
				auto bname = ncontainer+"_"+nbranch;
				auto leaf_name = ncontainer+"_"+nbranch+VsizeName;
				m_nVDvarUser[ncontainer].push_back(nbranch);
				m_tout->Branch( bname.data(), m_VDvarUser[ncontainer][m_NVDvarUser[ncontainer]].data(), (leaf_name+"/D").data() );
				m_NVDvarUser[ncontainer] += 1;
				Int_t iVDfunctor = distance(m_nVDvar.begin(), it_VD);
				m_VDfunctorsUser[ncontainer].push_back(m_VDfunctors[iVDfunctor]);
			}
			auto it_VI = find(m_nVIvar.begin(), m_nVIvar.end(), nbranch);
			if (it_VI != m_nVIvar.end()){
				auto bname = ncontainer+"_"+nbranch;
				auto leaf_name = ncontainer+"_"+nbranch+VsizeName; 
				m_nVIvarUser[ncontainer].push_back(nbranch);
				m_tout->Branch( bname.data(), m_VIvarUser[ncontainer][m_NVIvarUser[ncontainer]].data(), (leaf_name+"/I").data() );
				m_NVIvarUser[ncontainer] += 1;
				Int_t iVIfunctor = distance(m_nVIvar.begin(), it_VI);
				m_VIfunctorsUser[ncontainer].push_back(m_VIfunctors[iVIfunctor]);
			}
		}
	}
    m_tout->Branch( "evtNumber", &m_evtNumb, "evtNumber/I");
    m_tout->Branch( "runNumber", &m_runNumb, "runNumber/I");
    m_tout->Branch( "isMatched", &m_isMatched, "isMatched/I");
    m_tout->Branch( "isSig", &m_isSig, "isSig/I");
    // m_tout->Print();
    cout << "myDaVinci::initialize ends\n";
}

void myDaVinci::execute(){
	cout << "myDaVinci::execute starts\n";
	//CaloEvent *event = new CaloEvent();
	event = new CaloEvent();
	m_tin->SetBranchAddress("evt", &event);
	cout.flush();
	Int_t nevents = m_tin->GetEntries();
 	cout << "Nevents: " << nevents << endl;
 	for (Int_t i=0; i<nevents; ++i){
	  event->Clear();
	  //cout << "here1\n";
	  m_tin->GetEntry(i);
	  //cout << "here2\n";
	  if (i%1 ==0){
            cout << "\revent " << i;
            cout.flush();
        }
        //Int_t count=0;
     	m_AllClusters = event->getClusters();
    	vector<myParticle> * mothers = m_getMotherContainer(event);        	
    	cout << "Number of mothers: " << mothers->size() << endl;
    	for (auto it_mother = mothers->begin(); it_mother != mothers->end(); ++it_mother){
	    	auto mother = &(*it_mother);
	    	m_parts = m_getParts(mother); 
	    	if (m_parts.size() == (size_t)0) continue;
		    ////////////////////
		    m_exceptions();
		    ////////////////////	
            for (Int_t ipart=0; ipart<m_Ncontainers; ++ipart){
    			string ncontainer = m_ncontainers[ipart];
    			m_part         = &m_parts[ipart];
				m_matchedPart  = m_part->getMatchedPart();
				if (m_matchedPart  == 0) m_matchedPart = m_NullPart; 
    			
    			m_PV           = m_part->getPV();
    			m_origV        = m_part->getOrigVertex();
    			m_caloV        = m_part->getCaloVertex();
    			m_endV         = m_part->getEndVertex();
    			m_matchedPV    = m_matchedPart->getPV();
    			m_matchedOrigV = m_matchedPart->getOrigVertex();
    			m_matchedCaloV = m_matchedPart->getCaloVertex();
    			m_matchedEndV  = m_matchedPart->getEndVertex();

				if (m_PV           == 0) m_PV           = m_NullVertex;
    			if (m_origV        == 0) m_origV        = m_NullVertex;
    			if (m_caloV        == 0) m_caloV        = m_NullVertex;
    			if (m_endV         == 0) m_endV         = m_NullVertex;
    			if (m_matchedPV    == 0) m_matchedPV    = m_NullVertex;
    			if (m_matchedOrigV == 0) m_matchedOrigV = m_NullVertex;
    			if (m_matchedCaloV == 0) m_matchedCaloV = m_NullVertex;
    			if (m_matchedEndV  == 0) m_matchedEndV  = m_NullVertex;

    			vector<Cluster*> clusters = ((myParticle*)m_part)->getClusters();
		    	m_cluster =  (clusters.size() > 0)? clusters[0] : m_NullCluster;
		    	m_HittingPhotons = m_cluster->getHittingPhotons(0);
	    		m_closerTrack = (myParticle*)m_cluster->getCloserTrack();
	    		if (m_closerTrack == 0) m_closerTrack = m_NullPart;
	    		m_track = m_closerTrack->getTrack();
	    		if (m_track==0) m_track = m_part->getTrack();
	    		m_matchedTrack = m_matchedPart->getTrack();
	    		if (m_track==0) m_track = m_NullTrack;
	    		if (m_matchedTrack==0) m_matchedTrack = m_NullTrack;

	    		m_ClustWithin100.clear();
    			if (find(m_nIvarUser[ncontainer].begin(),
    				     m_nIvarUser[ncontainer].end(), 
    				     "CLUSTNEIGH100_N") != m_nIvarUser[ncontainer].end() &&
    				m_part->getTrackable() == 1 && m_part->getInCaloAcc() == 1 && m_track->getSize()>=2){
					Double_t x = m_track->getX(1) + m_track->getPX(1)/m_track->getPZ(1)*(event->getZ()-m_track->getZ(1));    				
    				Double_t y = m_track->getY(1) + m_track->getPY(1)/m_track->getPZ(1)*(event->getZ()-m_track->getZ(1));    				
    				//printf("x: %.3f, y: %.3f \n", x,y);
    				m_ClustWithin100 = event->getClustersWithinR(x, y, 100.);
    			}
    			//printf("m_ClustWithin100.size()= %d \n", (Int_t)m_ClustWithin100.size());
    			m_BremRecovClust.clear();
    			m_BremRecov_OVExtrap_x = 0.;
				m_BremRecov_OVExtrap_y = 0.;
				m_BremRecov_UTExtrap_x = 0.;
				m_BremRecov_UTExtrap_y = 0.;
    			if (find(m_nIvarUser[ncontainer].begin(),
    				     m_nIvarUser[ncontainer].end(), 
    				     "BREMRECOV_N") != m_nIvarUser[ncontainer].end() &&
    				m_part->getTrackable() == 1 && m_part->getInCaloAcc() == 1 && m_track->getSize()>=2){
					m_BremRecov_OVExtrap_x = m_origV->getX()+m_part->getOrigPx()/m_part->getOrigPz()*(event->getZ()-m_origV->getZ()); 
    				m_BremRecov_OVExtrap_y = m_origV->getY()+m_part->getOrigPy()/m_part->getOrigPz()*(event->getZ()-m_origV->getZ()); 
    				m_BremRecov_UTExtrap_x = m_track->getX(1)+m_track->getPX(1)/m_track->getPZ(1)*(event->getZ()-m_track->getZ(1)); 
					m_BremRecov_UTExtrap_y = m_track->getY(1)+m_track->getPY(1)/m_track->getPZ(1)*(event->getZ()-m_track->getZ(1)); 

					Double_t Dx = m_BremRecov_UTExtrap_x-m_BremRecov_OVExtrap_x;
					Double_t Dy = m_BremRecov_UTExtrap_y-m_BremRecov_OVExtrap_y;
					Double_t D  = sqrt(Dx*Dx+Dy*Dy); 
					Double_t ds  = 10.;
					Double_t dx = Dx*ds/D;
					Double_t dy = Dy*ds/D;
					Double_t d = 0;
					Double_t x = m_BremRecov_UTExtrap_x;
					Double_t y = m_BremRecov_UTExtrap_y;
					//printf("*** BREM RECOV *** x: %.3f, y: %.3f, d: %.3f\n", x,y,D);
					m_BremRecovClust = event->getClustersWithinR(x, y, 20*ds);
					
					x = m_BremRecov_OVExtrap_x;
					y = m_BremRecov_OVExtrap_y;
					while (d<D){
						//printf("*** BREM RECOV *** x: %.3f, y: %.3f, d: %.3f \n", x,y,d);
						auto tmp_clusters = event->getClustersWithinR(x, y, 20*ds);
						for (auto tmp_cluster : tmp_clusters){
							if (find_if(m_BremRecovClust.begin(), 
										m_BremRecovClust.end(), 
										[tmp_cluster](Cluster* bremClust){ return tmp_cluster->getID() == bremClust->getID(); }
										) != m_BremRecovClust.end() ) continue;
							m_BremRecovClust.push_back(tmp_cluster);
						}
						x += dx;
						y += dy;
						d += ds;
					}
    			}
    				

	    		transform(m_DfunctorsUser[ncontainer].begin(), m_DfunctorsUser[ncontainer].end(), 
	    			      m_DvarUser[ncontainer].begin(),
			    		  [](function<double()> functor){ return functor(); }  );
	     		transform(m_IfunctorsUser[ncontainer].begin(), m_IfunctorsUser[ncontainer].end(), 
	    			      m_IvarUser[ncontainer].begin(),
			    		  [](function<int()> functor){ return functor(); }  );
	     		transform(m_LfunctorsUser[ncontainer].begin(), m_LfunctorsUser[ncontainer].end(), 
	    			      m_LvarUser[ncontainer].begin(),
			    		  [](function<long int()> functor){ return functor(); }  );

	     		Int_t k_VD=0;
	     		for_each(m_VDfunctorsUser[ncontainer].begin(), m_VDfunctorsUser[ncontainer].end(), 
	     				[ncontainer, &k_VD](function<void(const char* , int)> functor){
	     					functor(ncontainer.data(), k_VD); ++k_VD;
	     				} ); 
	     		Int_t k_VI=0;
	     		for_each(m_VIfunctorsUser[ncontainer].begin(), m_VIfunctorsUser[ncontainer].end(), 
	     				[ncontainer, &k_VI](function<void(const char* , int)> functor){
	     					functor(ncontainer.data(), k_VI); ++k_VI;
	     				} ); 
    		}



    		m_evtNumb = event->getEvt();
    		m_runNumb = event->getRun();
    		m_isMatched = m_checkMatch();
    		//m_isSig = -1;
    		// m_matchedPart = m_parts[0]->getMatchedPart();
    		// if (m_matchedPart != 0)
    		// 	m_isSig = m_matchedPart->getIsSig();
    		// else m_isSig = -1;
    		// m_isSig = m_parts[0].getIsSig();
    		m_tout->Fill();
    	} 
    }

    //m_tout->Print();
    cout << "Tot. entries :" << m_tout->GetEntries() << endl;
    cout << "myDaVinci::execute ends\n";
	
}


void myDaVinci::finalize(TString nfout){
	cout << "myDaVinci::finalize starts\n";
	TFile fout(nfout, "RECREATE");
	fout.WriteTObject(m_tout, "", "overwrite");
	fout.Print();
	fout.Close();
	delete m_tout;
	cout << "myDaVinci::finalize ends\n";
}





#endif
