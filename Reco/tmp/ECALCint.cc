// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME tmpdIECALCint

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "myParticle.h"
#include "myTESevent.h"
#include "Vertex.h"
#include "CaloEvent.h"
#include "Photon.h"
#include "Cluster.h"
#include "sigShape.h"
#include "Cell.h"
#include "myTrack.h"
#include "MCtruth_TOOLS.h"
#include "myDaVinci.h"
#include "myProtoParticle.h"
#include "myTagManager.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static void *new_Vertex(void *p = 0);
   static void *newArray_Vertex(Long_t size, void *p);
   static void delete_Vertex(void *p);
   static void deleteArray_Vertex(void *p);
   static void destruct_Vertex(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Vertex*)
   {
      ::Vertex *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Vertex >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Vertex", ::Vertex::Class_Version(), "Vertex.h", 10,
                  typeid(::Vertex), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Vertex::Dictionary, isa_proxy, 4,
                  sizeof(::Vertex) );
      instance.SetNew(&new_Vertex);
      instance.SetNewArray(&newArray_Vertex);
      instance.SetDelete(&delete_Vertex);
      instance.SetDeleteArray(&deleteArray_Vertex);
      instance.SetDestructor(&destruct_Vertex);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Vertex*)
   {
      return GenerateInitInstanceLocal((::Vertex*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Vertex*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_myProtoParticle(void *p = 0);
   static void *newArray_myProtoParticle(Long_t size, void *p);
   static void delete_myProtoParticle(void *p);
   static void deleteArray_myProtoParticle(void *p);
   static void destruct_myProtoParticle(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::myProtoParticle*)
   {
      ::myProtoParticle *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::myProtoParticle >(0);
      static ::ROOT::TGenericClassInfo 
         instance("myProtoParticle", ::myProtoParticle::Class_Version(), "myProtoParticle.h", 13,
                  typeid(::myProtoParticle), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::myProtoParticle::Dictionary, isa_proxy, 4,
                  sizeof(::myProtoParticle) );
      instance.SetNew(&new_myProtoParticle);
      instance.SetNewArray(&newArray_myProtoParticle);
      instance.SetDelete(&delete_myProtoParticle);
      instance.SetDeleteArray(&deleteArray_myProtoParticle);
      instance.SetDestructor(&destruct_myProtoParticle);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::myProtoParticle*)
   {
      return GenerateInitInstanceLocal((::myProtoParticle*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::myProtoParticle*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static TClass *EntryTimes_Dictionary();
   static void EntryTimes_TClassManip(TClass*);
   static void *new_EntryTimes(void *p = 0);
   static void *newArray_EntryTimes(Long_t size, void *p);
   static void delete_EntryTimes(void *p);
   static void deleteArray_EntryTimes(void *p);
   static void destruct_EntryTimes(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::EntryTimes*)
   {
      ::EntryTimes *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::EntryTimes));
      static ::ROOT::TGenericClassInfo 
         instance("EntryTimes", "sigShape.h", 18,
                  typeid(::EntryTimes), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &EntryTimes_Dictionary, isa_proxy, 4,
                  sizeof(::EntryTimes) );
      instance.SetNew(&new_EntryTimes);
      instance.SetNewArray(&newArray_EntryTimes);
      instance.SetDelete(&delete_EntryTimes);
      instance.SetDeleteArray(&deleteArray_EntryTimes);
      instance.SetDestructor(&destruct_EntryTimes);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::EntryTimes*)
   {
      return GenerateInitInstanceLocal((::EntryTimes*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::EntryTimes*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *EntryTimes_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::EntryTimes*)0x0)->GetClass();
      EntryTimes_TClassManip(theClass);
   return theClass;
   }

   static void EntryTimes_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static void delete_sigShape(void *p);
   static void deleteArray_sigShape(void *p);
   static void destruct_sigShape(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::sigShape*)
   {
      ::sigShape *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::sigShape >(0);
      static ::ROOT::TGenericClassInfo 
         instance("sigShape", ::sigShape::Class_Version(), "sigShape.h", 27,
                  typeid(::sigShape), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::sigShape::Dictionary, isa_proxy, 4,
                  sizeof(::sigShape) );
      instance.SetDelete(&delete_sigShape);
      instance.SetDeleteArray(&deleteArray_sigShape);
      instance.SetDestructor(&destruct_sigShape);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::sigShape*)
   {
      return GenerateInitInstanceLocal((::sigShape*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::sigShape*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static TClass *PhContrib_Dictionary();
   static void PhContrib_TClassManip(TClass*);
   static void *new_PhContrib(void *p = 0);
   static void *newArray_PhContrib(Long_t size, void *p);
   static void delete_PhContrib(void *p);
   static void deleteArray_PhContrib(void *p);
   static void destruct_PhContrib(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::PhContrib*)
   {
      ::PhContrib *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::PhContrib));
      static ::ROOT::TGenericClassInfo 
         instance("PhContrib", "Cell.h", 18,
                  typeid(::PhContrib), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &PhContrib_Dictionary, isa_proxy, 4,
                  sizeof(::PhContrib) );
      instance.SetNew(&new_PhContrib);
      instance.SetNewArray(&newArray_PhContrib);
      instance.SetDelete(&delete_PhContrib);
      instance.SetDeleteArray(&deleteArray_PhContrib);
      instance.SetDestructor(&destruct_PhContrib);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::PhContrib*)
   {
      return GenerateInitInstanceLocal((::PhContrib*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::PhContrib*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *PhContrib_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::PhContrib*)0x0)->GetClass();
      PhContrib_TClassManip(theClass);
   return theClass;
   }

   static void PhContrib_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *Calibrations_Dictionary();
   static void Calibrations_TClassManip(TClass*);
   static void *new_Calibrations(void *p = 0);
   static void *newArray_Calibrations(Long_t size, void *p);
   static void delete_Calibrations(void *p);
   static void deleteArray_Calibrations(void *p);
   static void destruct_Calibrations(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Calibrations*)
   {
      ::Calibrations *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Calibrations));
      static ::ROOT::TGenericClassInfo 
         instance("Calibrations", "Cell.h", 24,
                  typeid(::Calibrations), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &Calibrations_Dictionary, isa_proxy, 4,
                  sizeof(::Calibrations) );
      instance.SetNew(&new_Calibrations);
      instance.SetNewArray(&newArray_Calibrations);
      instance.SetDelete(&delete_Calibrations);
      instance.SetDeleteArray(&deleteArray_Calibrations);
      instance.SetDestructor(&destruct_Calibrations);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Calibrations*)
   {
      return GenerateInitInstanceLocal((::Calibrations*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Calibrations*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *Calibrations_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Calibrations*)0x0)->GetClass();
      Calibrations_TClassManip(theClass);
   return theClass;
   }

   static void Calibrations_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static void *new_Cell(void *p = 0);
   static void *newArray_Cell(Long_t size, void *p);
   static void delete_Cell(void *p);
   static void deleteArray_Cell(void *p);
   static void destruct_Cell(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Cell*)
   {
      ::Cell *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Cell >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Cell", ::Cell::Class_Version(), "Cell.h", 38,
                  typeid(::Cell), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Cell::Dictionary, isa_proxy, 4,
                  sizeof(::Cell) );
      instance.SetNew(&new_Cell);
      instance.SetNewArray(&newArray_Cell);
      instance.SetDelete(&delete_Cell);
      instance.SetDeleteArray(&deleteArray_Cell);
      instance.SetDestructor(&destruct_Cell);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Cell*)
   {
      return GenerateInitInstanceLocal((::Cell*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Cell*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_Cluster(void *p = 0);
   static void *newArray_Cluster(Long_t size, void *p);
   static void delete_Cluster(void *p);
   static void deleteArray_Cluster(void *p);
   static void destruct_Cluster(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Cluster*)
   {
      ::Cluster *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Cluster >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Cluster", ::Cluster::Class_Version(), "Cluster.h", 18,
                  typeid(::Cluster), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Cluster::Dictionary, isa_proxy, 4,
                  sizeof(::Cluster) );
      instance.SetNew(&new_Cluster);
      instance.SetNewArray(&newArray_Cluster);
      instance.SetDelete(&delete_Cluster);
      instance.SetDeleteArray(&deleteArray_Cluster);
      instance.SetDestructor(&destruct_Cluster);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Cluster*)
   {
      return GenerateInitInstanceLocal((::Cluster*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Cluster*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_myTrack(void *p = 0);
   static void *newArray_myTrack(Long_t size, void *p);
   static void delete_myTrack(void *p);
   static void deleteArray_myTrack(void *p);
   static void destruct_myTrack(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::myTrack*)
   {
      ::myTrack *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::myTrack >(0);
      static ::ROOT::TGenericClassInfo 
         instance("myTrack", ::myTrack::Class_Version(), "myTrack.h", 13,
                  typeid(::myTrack), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::myTrack::Dictionary, isa_proxy, 4,
                  sizeof(::myTrack) );
      instance.SetNew(&new_myTrack);
      instance.SetNewArray(&newArray_myTrack);
      instance.SetDelete(&delete_myTrack);
      instance.SetDeleteArray(&deleteArray_myTrack);
      instance.SetDestructor(&destruct_myTrack);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::myTrack*)
   {
      return GenerateInitInstanceLocal((::myTrack*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::myTrack*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_myParticle(void *p = 0);
   static void *newArray_myParticle(Long_t size, void *p);
   static void delete_myParticle(void *p);
   static void deleteArray_myParticle(void *p);
   static void destruct_myParticle(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::myParticle*)
   {
      ::myParticle *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::myParticle >(0);
      static ::ROOT::TGenericClassInfo 
         instance("myParticle", ::myParticle::Class_Version(), "myParticle.h", 15,
                  typeid(::myParticle), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::myParticle::Dictionary, isa_proxy, 4,
                  sizeof(::myParticle) );
      instance.SetNew(&new_myParticle);
      instance.SetNewArray(&newArray_myParticle);
      instance.SetDelete(&delete_myParticle);
      instance.SetDeleteArray(&deleteArray_myParticle);
      instance.SetDestructor(&destruct_myParticle);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::myParticle*)
   {
      return GenerateInitInstanceLocal((::myParticle*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::myParticle*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_myTESevent(void *p = 0);
   static void *newArray_myTESevent(Long_t size, void *p);
   static void delete_myTESevent(void *p);
   static void deleteArray_myTESevent(void *p);
   static void destruct_myTESevent(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::myTESevent*)
   {
      ::myTESevent *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::myTESevent >(0);
      static ::ROOT::TGenericClassInfo 
         instance("myTESevent", ::myTESevent::Class_Version(), "myTESevent.h", 18,
                  typeid(::myTESevent), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::myTESevent::Dictionary, isa_proxy, 4,
                  sizeof(::myTESevent) );
      instance.SetNew(&new_myTESevent);
      instance.SetNewArray(&newArray_myTESevent);
      instance.SetDelete(&delete_myTESevent);
      instance.SetDeleteArray(&deleteArray_myTESevent);
      instance.SetDestructor(&destruct_myTESevent);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::myTESevent*)
   {
      return GenerateInitInstanceLocal((::myTESevent*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::myTESevent*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static TClass *myTracks_Dictionary();
   static void myTracks_TClassManip(TClass*);
   static void *new_myTracks(void *p = 0);
   static void *newArray_myTracks(Long_t size, void *p);
   static void delete_myTracks(void *p);
   static void deleteArray_myTracks(void *p);
   static void destruct_myTracks(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::myTracks*)
   {
      ::myTracks *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::myTracks));
      static ::ROOT::TGenericClassInfo 
         instance("myTracks", "CaloEvent.h", 21,
                  typeid(::myTracks), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &myTracks_Dictionary, isa_proxy, 4,
                  sizeof(::myTracks) );
      instance.SetNew(&new_myTracks);
      instance.SetNewArray(&newArray_myTracks);
      instance.SetDelete(&delete_myTracks);
      instance.SetDeleteArray(&deleteArray_myTracks);
      instance.SetDestructor(&destruct_myTracks);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::myTracks*)
   {
      return GenerateInitInstanceLocal((::myTracks*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::myTracks*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *myTracks_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::myTracks*)0x0)->GetClass();
      myTracks_TClassManip(theClass);
   return theClass;
   }

   static void myTracks_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static void *new_CaloEvent(void *p = 0);
   static void *newArray_CaloEvent(Long_t size, void *p);
   static void delete_CaloEvent(void *p);
   static void deleteArray_CaloEvent(void *p);
   static void destruct_CaloEvent(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::CaloEvent*)
   {
      ::CaloEvent *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::CaloEvent >(0);
      static ::ROOT::TGenericClassInfo 
         instance("CaloEvent", ::CaloEvent::Class_Version(), "CaloEvent.h", 33,
                  typeid(::CaloEvent), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::CaloEvent::Dictionary, isa_proxy, 4,
                  sizeof(::CaloEvent) );
      instance.SetNew(&new_CaloEvent);
      instance.SetNewArray(&newArray_CaloEvent);
      instance.SetDelete(&delete_CaloEvent);
      instance.SetDeleteArray(&deleteArray_CaloEvent);
      instance.SetDestructor(&destruct_CaloEvent);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::CaloEvent*)
   {
      return GenerateInitInstanceLocal((::CaloEvent*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::CaloEvent*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_Photon(void *p = 0);
   static void *newArray_Photon(Long_t size, void *p);
   static void delete_Photon(void *p);
   static void deleteArray_Photon(void *p);
   static void destruct_Photon(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Photon*)
   {
      ::Photon *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Photon >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Photon", ::Photon::Class_Version(), "Photon.h", 11,
                  typeid(::Photon), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Photon::Dictionary, isa_proxy, 4,
                  sizeof(::Photon) );
      instance.SetNew(&new_Photon);
      instance.SetNewArray(&newArray_Photon);
      instance.SetDelete(&delete_Photon);
      instance.SetDeleteArray(&deleteArray_Photon);
      instance.SetDestructor(&destruct_Photon);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Photon*)
   {
      return GenerateInitInstanceLocal((::Photon*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Photon*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static TClass *MCtruth_TOOLS_Dictionary();
   static void MCtruth_TOOLS_TClassManip(TClass*);
   static void *new_MCtruth_TOOLS(void *p = 0);
   static void *newArray_MCtruth_TOOLS(Long_t size, void *p);
   static void delete_MCtruth_TOOLS(void *p);
   static void deleteArray_MCtruth_TOOLS(void *p);
   static void destruct_MCtruth_TOOLS(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::MCtruth_TOOLS*)
   {
      ::MCtruth_TOOLS *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::MCtruth_TOOLS));
      static ::ROOT::TGenericClassInfo 
         instance("MCtruth_TOOLS", "MCtruth_TOOLS.h", 17,
                  typeid(::MCtruth_TOOLS), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &MCtruth_TOOLS_Dictionary, isa_proxy, 4,
                  sizeof(::MCtruth_TOOLS) );
      instance.SetNew(&new_MCtruth_TOOLS);
      instance.SetNewArray(&newArray_MCtruth_TOOLS);
      instance.SetDelete(&delete_MCtruth_TOOLS);
      instance.SetDeleteArray(&deleteArray_MCtruth_TOOLS);
      instance.SetDestructor(&destruct_MCtruth_TOOLS);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::MCtruth_TOOLS*)
   {
      return GenerateInitInstanceLocal((::MCtruth_TOOLS*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::MCtruth_TOOLS*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *MCtruth_TOOLS_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::MCtruth_TOOLS*)0x0)->GetClass();
      MCtruth_TOOLS_TClassManip(theClass);
   return theClass;
   }

   static void MCtruth_TOOLS_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static void *new_myDaVinci(void *p = 0);
   static void *newArray_myDaVinci(Long_t size, void *p);
   static void delete_myDaVinci(void *p);
   static void deleteArray_myDaVinci(void *p);
   static void destruct_myDaVinci(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::myDaVinci*)
   {
      ::myDaVinci *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::myDaVinci >(0);
      static ::ROOT::TGenericClassInfo 
         instance("myDaVinci", ::myDaVinci::Class_Version(), "myDaVinci.h", 23,
                  typeid(::myDaVinci), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::myDaVinci::Dictionary, isa_proxy, 4,
                  sizeof(::myDaVinci) );
      instance.SetNew(&new_myDaVinci);
      instance.SetNewArray(&newArray_myDaVinci);
      instance.SetDelete(&delete_myDaVinci);
      instance.SetDeleteArray(&deleteArray_myDaVinci);
      instance.SetDestructor(&destruct_myDaVinci);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::myDaVinci*)
   {
      return GenerateInitInstanceLocal((::myDaVinci*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::myDaVinci*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_myTagManager(void *p = 0);
   static void *newArray_myTagManager(Long_t size, void *p);
   static void delete_myTagManager(void *p);
   static void deleteArray_myTagManager(void *p);
   static void destruct_myTagManager(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::myTagManager*)
   {
      ::myTagManager *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::myTagManager >(0);
      static ::ROOT::TGenericClassInfo 
         instance("myTagManager", ::myTagManager::Class_Version(), "myTagManager.h", 16,
                  typeid(::myTagManager), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::myTagManager::Dictionary, isa_proxy, 4,
                  sizeof(::myTagManager) );
      instance.SetNew(&new_myTagManager);
      instance.SetNewArray(&newArray_myTagManager);
      instance.SetDelete(&delete_myTagManager);
      instance.SetDeleteArray(&deleteArray_myTagManager);
      instance.SetDestructor(&destruct_myTagManager);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::myTagManager*)
   {
      return GenerateInitInstanceLocal((::myTagManager*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::myTagManager*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

//______________________________________________________________________________
atomic_TClass_ptr Vertex::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *Vertex::Class_Name()
{
   return "Vertex";
}

//______________________________________________________________________________
const char *Vertex::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Vertex*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int Vertex::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Vertex*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *Vertex::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Vertex*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *Vertex::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Vertex*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr myProtoParticle::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *myProtoParticle::Class_Name()
{
   return "myProtoParticle";
}

//______________________________________________________________________________
const char *myProtoParticle::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::myProtoParticle*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int myProtoParticle::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::myProtoParticle*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *myProtoParticle::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::myProtoParticle*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *myProtoParticle::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::myProtoParticle*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr sigShape::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *sigShape::Class_Name()
{
   return "sigShape";
}

//______________________________________________________________________________
const char *sigShape::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::sigShape*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int sigShape::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::sigShape*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *sigShape::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::sigShape*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *sigShape::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::sigShape*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr Cell::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *Cell::Class_Name()
{
   return "Cell";
}

//______________________________________________________________________________
const char *Cell::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Cell*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int Cell::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Cell*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *Cell::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Cell*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *Cell::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Cell*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr Cluster::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *Cluster::Class_Name()
{
   return "Cluster";
}

//______________________________________________________________________________
const char *Cluster::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Cluster*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int Cluster::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Cluster*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *Cluster::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Cluster*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *Cluster::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Cluster*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr myTrack::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *myTrack::Class_Name()
{
   return "myTrack";
}

//______________________________________________________________________________
const char *myTrack::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::myTrack*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int myTrack::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::myTrack*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *myTrack::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::myTrack*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *myTrack::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::myTrack*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr myParticle::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *myParticle::Class_Name()
{
   return "myParticle";
}

//______________________________________________________________________________
const char *myParticle::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::myParticle*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int myParticle::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::myParticle*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *myParticle::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::myParticle*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *myParticle::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::myParticle*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr myTESevent::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *myTESevent::Class_Name()
{
   return "myTESevent";
}

//______________________________________________________________________________
const char *myTESevent::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::myTESevent*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int myTESevent::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::myTESevent*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *myTESevent::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::myTESevent*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *myTESevent::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::myTESevent*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr CaloEvent::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *CaloEvent::Class_Name()
{
   return "CaloEvent";
}

//______________________________________________________________________________
const char *CaloEvent::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::CaloEvent*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int CaloEvent::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::CaloEvent*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *CaloEvent::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::CaloEvent*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *CaloEvent::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::CaloEvent*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr Photon::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *Photon::Class_Name()
{
   return "Photon";
}

//______________________________________________________________________________
const char *Photon::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Photon*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int Photon::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Photon*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *Photon::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Photon*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *Photon::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Photon*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr myDaVinci::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *myDaVinci::Class_Name()
{
   return "myDaVinci";
}

//______________________________________________________________________________
const char *myDaVinci::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::myDaVinci*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int myDaVinci::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::myDaVinci*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *myDaVinci::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::myDaVinci*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *myDaVinci::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::myDaVinci*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr myTagManager::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *myTagManager::Class_Name()
{
   return "myTagManager";
}

//______________________________________________________________________________
const char *myTagManager::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::myTagManager*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int myTagManager::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::myTagManager*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *myTagManager::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::myTagManager*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *myTagManager::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::myTagManager*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
void Vertex::Streamer(TBuffer &R__b)
{
   // Stream an object of class Vertex.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Vertex::Class(),this);
   } else {
      R__b.WriteClassBuffer(Vertex::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_Vertex(void *p) {
      return  p ? new(p) ::Vertex : new ::Vertex;
   }
   static void *newArray_Vertex(Long_t nElements, void *p) {
      return p ? new(p) ::Vertex[nElements] : new ::Vertex[nElements];
   }
   // Wrapper around operator delete
   static void delete_Vertex(void *p) {
      delete ((::Vertex*)p);
   }
   static void deleteArray_Vertex(void *p) {
      delete [] ((::Vertex*)p);
   }
   static void destruct_Vertex(void *p) {
      typedef ::Vertex current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Vertex

//______________________________________________________________________________
void myProtoParticle::Streamer(TBuffer &R__b)
{
   // Stream an object of class myProtoParticle.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(myProtoParticle::Class(),this);
   } else {
      R__b.WriteClassBuffer(myProtoParticle::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_myProtoParticle(void *p) {
      return  p ? new(p) ::myProtoParticle : new ::myProtoParticle;
   }
   static void *newArray_myProtoParticle(Long_t nElements, void *p) {
      return p ? new(p) ::myProtoParticle[nElements] : new ::myProtoParticle[nElements];
   }
   // Wrapper around operator delete
   static void delete_myProtoParticle(void *p) {
      delete ((::myProtoParticle*)p);
   }
   static void deleteArray_myProtoParticle(void *p) {
      delete [] ((::myProtoParticle*)p);
   }
   static void destruct_myProtoParticle(void *p) {
      typedef ::myProtoParticle current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::myProtoParticle

namespace ROOT {
   // Wrappers around operator new
   static void *new_EntryTimes(void *p) {
      return  p ? new(p) ::EntryTimes : new ::EntryTimes;
   }
   static void *newArray_EntryTimes(Long_t nElements, void *p) {
      return p ? new(p) ::EntryTimes[nElements] : new ::EntryTimes[nElements];
   }
   // Wrapper around operator delete
   static void delete_EntryTimes(void *p) {
      delete ((::EntryTimes*)p);
   }
   static void deleteArray_EntryTimes(void *p) {
      delete [] ((::EntryTimes*)p);
   }
   static void destruct_EntryTimes(void *p) {
      typedef ::EntryTimes current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::EntryTimes

//______________________________________________________________________________
void sigShape::Streamer(TBuffer &R__b)
{
   // Stream an object of class sigShape.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(sigShape::Class(),this);
   } else {
      R__b.WriteClassBuffer(sigShape::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_sigShape(void *p) {
      delete ((::sigShape*)p);
   }
   static void deleteArray_sigShape(void *p) {
      delete [] ((::sigShape*)p);
   }
   static void destruct_sigShape(void *p) {
      typedef ::sigShape current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::sigShape

namespace ROOT {
   // Wrappers around operator new
   static void *new_PhContrib(void *p) {
      return  p ? new(p) ::PhContrib : new ::PhContrib;
   }
   static void *newArray_PhContrib(Long_t nElements, void *p) {
      return p ? new(p) ::PhContrib[nElements] : new ::PhContrib[nElements];
   }
   // Wrapper around operator delete
   static void delete_PhContrib(void *p) {
      delete ((::PhContrib*)p);
   }
   static void deleteArray_PhContrib(void *p) {
      delete [] ((::PhContrib*)p);
   }
   static void destruct_PhContrib(void *p) {
      typedef ::PhContrib current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::PhContrib

namespace ROOT {
   // Wrappers around operator new
   static void *new_Calibrations(void *p) {
      return  p ? new(p) ::Calibrations : new ::Calibrations;
   }
   static void *newArray_Calibrations(Long_t nElements, void *p) {
      return p ? new(p) ::Calibrations[nElements] : new ::Calibrations[nElements];
   }
   // Wrapper around operator delete
   static void delete_Calibrations(void *p) {
      delete ((::Calibrations*)p);
   }
   static void deleteArray_Calibrations(void *p) {
      delete [] ((::Calibrations*)p);
   }
   static void destruct_Calibrations(void *p) {
      typedef ::Calibrations current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Calibrations

//______________________________________________________________________________
void Cell::Streamer(TBuffer &R__b)
{
   // Stream an object of class Cell.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Cell::Class(),this);
   } else {
      R__b.WriteClassBuffer(Cell::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_Cell(void *p) {
      return  p ? new(p) ::Cell : new ::Cell;
   }
   static void *newArray_Cell(Long_t nElements, void *p) {
      return p ? new(p) ::Cell[nElements] : new ::Cell[nElements];
   }
   // Wrapper around operator delete
   static void delete_Cell(void *p) {
      delete ((::Cell*)p);
   }
   static void deleteArray_Cell(void *p) {
      delete [] ((::Cell*)p);
   }
   static void destruct_Cell(void *p) {
      typedef ::Cell current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Cell

//______________________________________________________________________________
void Cluster::Streamer(TBuffer &R__b)
{
   // Stream an object of class Cluster.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Cluster::Class(),this);
   } else {
      R__b.WriteClassBuffer(Cluster::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_Cluster(void *p) {
      return  p ? new(p) ::Cluster : new ::Cluster;
   }
   static void *newArray_Cluster(Long_t nElements, void *p) {
      return p ? new(p) ::Cluster[nElements] : new ::Cluster[nElements];
   }
   // Wrapper around operator delete
   static void delete_Cluster(void *p) {
      delete ((::Cluster*)p);
   }
   static void deleteArray_Cluster(void *p) {
      delete [] ((::Cluster*)p);
   }
   static void destruct_Cluster(void *p) {
      typedef ::Cluster current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Cluster

//______________________________________________________________________________
void myTrack::Streamer(TBuffer &R__b)
{
   // Stream an object of class myTrack.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(myTrack::Class(),this);
   } else {
      R__b.WriteClassBuffer(myTrack::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_myTrack(void *p) {
      return  p ? new(p) ::myTrack : new ::myTrack;
   }
   static void *newArray_myTrack(Long_t nElements, void *p) {
      return p ? new(p) ::myTrack[nElements] : new ::myTrack[nElements];
   }
   // Wrapper around operator delete
   static void delete_myTrack(void *p) {
      delete ((::myTrack*)p);
   }
   static void deleteArray_myTrack(void *p) {
      delete [] ((::myTrack*)p);
   }
   static void destruct_myTrack(void *p) {
      typedef ::myTrack current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::myTrack

//______________________________________________________________________________
void myParticle::Streamer(TBuffer &R__b)
{
   // Stream an object of class myParticle.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(myParticle::Class(),this);
   } else {
      R__b.WriteClassBuffer(myParticle::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_myParticle(void *p) {
      return  p ? new(p) ::myParticle : new ::myParticle;
   }
   static void *newArray_myParticle(Long_t nElements, void *p) {
      return p ? new(p) ::myParticle[nElements] : new ::myParticle[nElements];
   }
   // Wrapper around operator delete
   static void delete_myParticle(void *p) {
      delete ((::myParticle*)p);
   }
   static void deleteArray_myParticle(void *p) {
      delete [] ((::myParticle*)p);
   }
   static void destruct_myParticle(void *p) {
      typedef ::myParticle current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::myParticle

//______________________________________________________________________________
void myTESevent::Streamer(TBuffer &R__b)
{
   // Stream an object of class myTESevent.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(myTESevent::Class(),this);
   } else {
      R__b.WriteClassBuffer(myTESevent::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_myTESevent(void *p) {
      return  p ? new(p) ::myTESevent : new ::myTESevent;
   }
   static void *newArray_myTESevent(Long_t nElements, void *p) {
      return p ? new(p) ::myTESevent[nElements] : new ::myTESevent[nElements];
   }
   // Wrapper around operator delete
   static void delete_myTESevent(void *p) {
      delete ((::myTESevent*)p);
   }
   static void deleteArray_myTESevent(void *p) {
      delete [] ((::myTESevent*)p);
   }
   static void destruct_myTESevent(void *p) {
      typedef ::myTESevent current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::myTESevent

namespace ROOT {
   // Wrappers around operator new
   static void *new_myTracks(void *p) {
      return  p ? new(p) ::myTracks : new ::myTracks;
   }
   static void *newArray_myTracks(Long_t nElements, void *p) {
      return p ? new(p) ::myTracks[nElements] : new ::myTracks[nElements];
   }
   // Wrapper around operator delete
   static void delete_myTracks(void *p) {
      delete ((::myTracks*)p);
   }
   static void deleteArray_myTracks(void *p) {
      delete [] ((::myTracks*)p);
   }
   static void destruct_myTracks(void *p) {
      typedef ::myTracks current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::myTracks

//______________________________________________________________________________
void CaloEvent::Streamer(TBuffer &R__b)
{
   // Stream an object of class CaloEvent.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(CaloEvent::Class(),this);
   } else {
      R__b.WriteClassBuffer(CaloEvent::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_CaloEvent(void *p) {
      return  p ? new(p) ::CaloEvent : new ::CaloEvent;
   }
   static void *newArray_CaloEvent(Long_t nElements, void *p) {
      return p ? new(p) ::CaloEvent[nElements] : new ::CaloEvent[nElements];
   }
   // Wrapper around operator delete
   static void delete_CaloEvent(void *p) {
      delete ((::CaloEvent*)p);
   }
   static void deleteArray_CaloEvent(void *p) {
      delete [] ((::CaloEvent*)p);
   }
   static void destruct_CaloEvent(void *p) {
      typedef ::CaloEvent current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::CaloEvent

//______________________________________________________________________________
void Photon::Streamer(TBuffer &R__b)
{
   // Stream an object of class Photon.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Photon::Class(),this);
   } else {
      R__b.WriteClassBuffer(Photon::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_Photon(void *p) {
      return  p ? new(p) ::Photon : new ::Photon;
   }
   static void *newArray_Photon(Long_t nElements, void *p) {
      return p ? new(p) ::Photon[nElements] : new ::Photon[nElements];
   }
   // Wrapper around operator delete
   static void delete_Photon(void *p) {
      delete ((::Photon*)p);
   }
   static void deleteArray_Photon(void *p) {
      delete [] ((::Photon*)p);
   }
   static void destruct_Photon(void *p) {
      typedef ::Photon current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Photon

namespace ROOT {
   // Wrappers around operator new
   static void *new_MCtruth_TOOLS(void *p) {
      return  p ? new(p) ::MCtruth_TOOLS : new ::MCtruth_TOOLS;
   }
   static void *newArray_MCtruth_TOOLS(Long_t nElements, void *p) {
      return p ? new(p) ::MCtruth_TOOLS[nElements] : new ::MCtruth_TOOLS[nElements];
   }
   // Wrapper around operator delete
   static void delete_MCtruth_TOOLS(void *p) {
      delete ((::MCtruth_TOOLS*)p);
   }
   static void deleteArray_MCtruth_TOOLS(void *p) {
      delete [] ((::MCtruth_TOOLS*)p);
   }
   static void destruct_MCtruth_TOOLS(void *p) {
      typedef ::MCtruth_TOOLS current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::MCtruth_TOOLS

//______________________________________________________________________________
void myDaVinci::Streamer(TBuffer &R__b)
{
   // Stream an object of class myDaVinci.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(myDaVinci::Class(),this);
   } else {
      R__b.WriteClassBuffer(myDaVinci::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_myDaVinci(void *p) {
      return  p ? new(p) ::myDaVinci : new ::myDaVinci;
   }
   static void *newArray_myDaVinci(Long_t nElements, void *p) {
      return p ? new(p) ::myDaVinci[nElements] : new ::myDaVinci[nElements];
   }
   // Wrapper around operator delete
   static void delete_myDaVinci(void *p) {
      delete ((::myDaVinci*)p);
   }
   static void deleteArray_myDaVinci(void *p) {
      delete [] ((::myDaVinci*)p);
   }
   static void destruct_myDaVinci(void *p) {
      typedef ::myDaVinci current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::myDaVinci

//______________________________________________________________________________
void myTagManager::Streamer(TBuffer &R__b)
{
   // Stream an object of class myTagManager.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(myTagManager::Class(),this);
   } else {
      R__b.WriteClassBuffer(myTagManager::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_myTagManager(void *p) {
      return  p ? new(p) ::myTagManager : new ::myTagManager;
   }
   static void *newArray_myTagManager(Long_t nElements, void *p) {
      return p ? new(p) ::myTagManager[nElements] : new ::myTagManager[nElements];
   }
   // Wrapper around operator delete
   static void delete_myTagManager(void *p) {
      delete ((::myTagManager*)p);
   }
   static void deleteArray_myTagManager(void *p) {
      delete [] ((::myTagManager*)p);
   }
   static void destruct_myTagManager(void *p) {
      typedef ::myTagManager current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::myTagManager

namespace ROOT {
   static TClass *vectorlEvectorlEintgRsPgR_Dictionary();
   static void vectorlEvectorlEintgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEintgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEintgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEintgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEintgRsPgR(void *p);
   static void destruct_vectorlEvectorlEintgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<int> >*)
   {
      vector<vector<int> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<int> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<int> >", -2, "vector", 339,
                  typeid(vector<vector<int> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEintgRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<vector<int> >) );
      instance.SetNew(&new_vectorlEvectorlEintgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEintgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEintgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEintgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEintgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<int> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<vector<int> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEintgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<int> >*)0x0)->GetClass();
      vectorlEvectorlEintgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEintgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEintgRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<vector<int> > : new vector<vector<int> >;
   }
   static void *newArray_vectorlEvectorlEintgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<vector<int> >[nElements] : new vector<vector<int> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEintgRsPgR(void *p) {
      delete ((vector<vector<int> >*)p);
   }
   static void deleteArray_vectorlEvectorlEintgRsPgR(void *p) {
      delete [] ((vector<vector<int> >*)p);
   }
   static void destruct_vectorlEvectorlEintgRsPgR(void *p) {
      typedef vector<vector<int> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<int> >

namespace ROOT {
   static TClass *vectorlEvectorlEdoublegRsPgR_Dictionary();
   static void vectorlEvectorlEdoublegRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEdoublegRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEdoublegRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEdoublegRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEdoublegRsPgR(void *p);
   static void destruct_vectorlEvectorlEdoublegRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<double> >*)
   {
      vector<vector<double> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<double> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<double> >", -2, "vector", 339,
                  typeid(vector<vector<double> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEdoublegRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<vector<double> >) );
      instance.SetNew(&new_vectorlEvectorlEdoublegRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEdoublegRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEdoublegRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEdoublegRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEdoublegRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<double> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<vector<double> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEdoublegRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<double> >*)0x0)->GetClass();
      vectorlEvectorlEdoublegRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEdoublegRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEdoublegRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<vector<double> > : new vector<vector<double> >;
   }
   static void *newArray_vectorlEvectorlEdoublegRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<vector<double> >[nElements] : new vector<vector<double> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEdoublegRsPgR(void *p) {
      delete ((vector<vector<double> >*)p);
   }
   static void deleteArray_vectorlEvectorlEdoublegRsPgR(void *p) {
      delete [] ((vector<vector<double> >*)p);
   }
   static void destruct_vectorlEvectorlEdoublegRsPgR(void *p) {
      typedef vector<vector<double> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<double> >

namespace ROOT {
   static TClass *vectorlEstringgR_Dictionary();
   static void vectorlEstringgR_TClassManip(TClass*);
   static void *new_vectorlEstringgR(void *p = 0);
   static void *newArray_vectorlEstringgR(Long_t size, void *p);
   static void delete_vectorlEstringgR(void *p);
   static void deleteArray_vectorlEstringgR(void *p);
   static void destruct_vectorlEstringgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<string>*)
   {
      vector<string> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<string>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<string>", -2, "vector", 339,
                  typeid(vector<string>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEstringgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<string>) );
      instance.SetNew(&new_vectorlEstringgR);
      instance.SetNewArray(&newArray_vectorlEstringgR);
      instance.SetDelete(&delete_vectorlEstringgR);
      instance.SetDeleteArray(&deleteArray_vectorlEstringgR);
      instance.SetDestructor(&destruct_vectorlEstringgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<string> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<string>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEstringgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<string>*)0x0)->GetClass();
      vectorlEstringgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEstringgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEstringgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<string> : new vector<string>;
   }
   static void *newArray_vectorlEstringgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<string>[nElements] : new vector<string>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEstringgR(void *p) {
      delete ((vector<string>*)p);
   }
   static void deleteArray_vectorlEstringgR(void *p) {
      delete [] ((vector<string>*)p);
   }
   static void destruct_vectorlEstringgR(void *p) {
      typedef vector<string> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<string>

namespace ROOT {
   static TClass *vectorlEpairlEstringcOvectorlEstringgRsPgRsPgR_Dictionary();
   static void vectorlEpairlEstringcOvectorlEstringgRsPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEpairlEstringcOvectorlEstringgRsPgRsPgR(void *p = 0);
   static void *newArray_vectorlEpairlEstringcOvectorlEstringgRsPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEpairlEstringcOvectorlEstringgRsPgRsPgR(void *p);
   static void deleteArray_vectorlEpairlEstringcOvectorlEstringgRsPgRsPgR(void *p);
   static void destruct_vectorlEpairlEstringcOvectorlEstringgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<pair<string,vector<string> > >*)
   {
      vector<pair<string,vector<string> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<pair<string,vector<string> > >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<pair<string,vector<string> > >", -2, "vector", 339,
                  typeid(vector<pair<string,vector<string> > >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEpairlEstringcOvectorlEstringgRsPgRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<pair<string,vector<string> > >) );
      instance.SetNew(&new_vectorlEpairlEstringcOvectorlEstringgRsPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEpairlEstringcOvectorlEstringgRsPgRsPgR);
      instance.SetDelete(&delete_vectorlEpairlEstringcOvectorlEstringgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEpairlEstringcOvectorlEstringgRsPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEpairlEstringcOvectorlEstringgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<pair<string,vector<string> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<pair<string,vector<string> > >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEpairlEstringcOvectorlEstringgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<pair<string,vector<string> > >*)0x0)->GetClass();
      vectorlEpairlEstringcOvectorlEstringgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEpairlEstringcOvectorlEstringgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEpairlEstringcOvectorlEstringgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<pair<string,vector<string> > > : new vector<pair<string,vector<string> > >;
   }
   static void *newArray_vectorlEpairlEstringcOvectorlEstringgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<pair<string,vector<string> > >[nElements] : new vector<pair<string,vector<string> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEpairlEstringcOvectorlEstringgRsPgRsPgR(void *p) {
      delete ((vector<pair<string,vector<string> > >*)p);
   }
   static void deleteArray_vectorlEpairlEstringcOvectorlEstringgRsPgRsPgR(void *p) {
      delete [] ((vector<pair<string,vector<string> > >*)p);
   }
   static void destruct_vectorlEpairlEstringcOvectorlEstringgRsPgRsPgR(void *p) {
      typedef vector<pair<string,vector<string> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<pair<string,vector<string> > >

namespace ROOT {
   static TClass *vectorlEpairlEintcOintgRsPgR_Dictionary();
   static void vectorlEpairlEintcOintgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEpairlEintcOintgRsPgR(void *p = 0);
   static void *newArray_vectorlEpairlEintcOintgRsPgR(Long_t size, void *p);
   static void delete_vectorlEpairlEintcOintgRsPgR(void *p);
   static void deleteArray_vectorlEpairlEintcOintgRsPgR(void *p);
   static void destruct_vectorlEpairlEintcOintgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<pair<int,int> >*)
   {
      vector<pair<int,int> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<pair<int,int> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<pair<int,int> >", -2, "vector", 339,
                  typeid(vector<pair<int,int> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEpairlEintcOintgRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<pair<int,int> >) );
      instance.SetNew(&new_vectorlEpairlEintcOintgRsPgR);
      instance.SetNewArray(&newArray_vectorlEpairlEintcOintgRsPgR);
      instance.SetDelete(&delete_vectorlEpairlEintcOintgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEpairlEintcOintgRsPgR);
      instance.SetDestructor(&destruct_vectorlEpairlEintcOintgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<pair<int,int> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<pair<int,int> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEpairlEintcOintgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<pair<int,int> >*)0x0)->GetClass();
      vectorlEpairlEintcOintgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEpairlEintcOintgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEpairlEintcOintgRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<pair<int,int> > : new vector<pair<int,int> >;
   }
   static void *newArray_vectorlEpairlEintcOintgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<pair<int,int> >[nElements] : new vector<pair<int,int> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEpairlEintcOintgRsPgR(void *p) {
      delete ((vector<pair<int,int> >*)p);
   }
   static void deleteArray_vectorlEpairlEintcOintgRsPgR(void *p) {
      delete [] ((vector<pair<int,int> >*)p);
   }
   static void destruct_vectorlEpairlEintcOintgRsPgR(void *p) {
      typedef vector<pair<int,int> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<pair<int,int> >

namespace ROOT {
   static TClass *vectorlEpairlEdoublecOdoublegRsPgR_Dictionary();
   static void vectorlEpairlEdoublecOdoublegRsPgR_TClassManip(TClass*);
   static void *new_vectorlEpairlEdoublecOdoublegRsPgR(void *p = 0);
   static void *newArray_vectorlEpairlEdoublecOdoublegRsPgR(Long_t size, void *p);
   static void delete_vectorlEpairlEdoublecOdoublegRsPgR(void *p);
   static void deleteArray_vectorlEpairlEdoublecOdoublegRsPgR(void *p);
   static void destruct_vectorlEpairlEdoublecOdoublegRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<pair<double,double> >*)
   {
      vector<pair<double,double> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<pair<double,double> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<pair<double,double> >", -2, "vector", 339,
                  typeid(vector<pair<double,double> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEpairlEdoublecOdoublegRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<pair<double,double> >) );
      instance.SetNew(&new_vectorlEpairlEdoublecOdoublegRsPgR);
      instance.SetNewArray(&newArray_vectorlEpairlEdoublecOdoublegRsPgR);
      instance.SetDelete(&delete_vectorlEpairlEdoublecOdoublegRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEpairlEdoublecOdoublegRsPgR);
      instance.SetDestructor(&destruct_vectorlEpairlEdoublecOdoublegRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<pair<double,double> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<pair<double,double> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEpairlEdoublecOdoublegRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<pair<double,double> >*)0x0)->GetClass();
      vectorlEpairlEdoublecOdoublegRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEpairlEdoublecOdoublegRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEpairlEdoublecOdoublegRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<pair<double,double> > : new vector<pair<double,double> >;
   }
   static void *newArray_vectorlEpairlEdoublecOdoublegRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<pair<double,double> >[nElements] : new vector<pair<double,double> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEpairlEdoublecOdoublegRsPgR(void *p) {
      delete ((vector<pair<double,double> >*)p);
   }
   static void deleteArray_vectorlEpairlEdoublecOdoublegRsPgR(void *p) {
      delete [] ((vector<pair<double,double> >*)p);
   }
   static void destruct_vectorlEpairlEdoublecOdoublegRsPgR(void *p) {
      typedef vector<pair<double,double> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<pair<double,double> >

namespace ROOT {
   static TClass *vectorlEmyTrackgR_Dictionary();
   static void vectorlEmyTrackgR_TClassManip(TClass*);
   static void *new_vectorlEmyTrackgR(void *p = 0);
   static void *newArray_vectorlEmyTrackgR(Long_t size, void *p);
   static void delete_vectorlEmyTrackgR(void *p);
   static void deleteArray_vectorlEmyTrackgR(void *p);
   static void destruct_vectorlEmyTrackgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<myTrack>*)
   {
      vector<myTrack> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<myTrack>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<myTrack>", -2, "vector", 339,
                  typeid(vector<myTrack>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEmyTrackgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<myTrack>) );
      instance.SetNew(&new_vectorlEmyTrackgR);
      instance.SetNewArray(&newArray_vectorlEmyTrackgR);
      instance.SetDelete(&delete_vectorlEmyTrackgR);
      instance.SetDeleteArray(&deleteArray_vectorlEmyTrackgR);
      instance.SetDestructor(&destruct_vectorlEmyTrackgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<myTrack> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<myTrack>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEmyTrackgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<myTrack>*)0x0)->GetClass();
      vectorlEmyTrackgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEmyTrackgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEmyTrackgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<myTrack> : new vector<myTrack>;
   }
   static void *newArray_vectorlEmyTrackgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<myTrack>[nElements] : new vector<myTrack>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEmyTrackgR(void *p) {
      delete ((vector<myTrack>*)p);
   }
   static void deleteArray_vectorlEmyTrackgR(void *p) {
      delete [] ((vector<myTrack>*)p);
   }
   static void destruct_vectorlEmyTrackgR(void *p) {
      typedef vector<myTrack> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<myTrack>

namespace ROOT {
   static TClass *vectorlEmyProtoParticlemUgR_Dictionary();
   static void vectorlEmyProtoParticlemUgR_TClassManip(TClass*);
   static void *new_vectorlEmyProtoParticlemUgR(void *p = 0);
   static void *newArray_vectorlEmyProtoParticlemUgR(Long_t size, void *p);
   static void delete_vectorlEmyProtoParticlemUgR(void *p);
   static void deleteArray_vectorlEmyProtoParticlemUgR(void *p);
   static void destruct_vectorlEmyProtoParticlemUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<myProtoParticle*>*)
   {
      vector<myProtoParticle*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<myProtoParticle*>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<myProtoParticle*>", -2, "vector", 339,
                  typeid(vector<myProtoParticle*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEmyProtoParticlemUgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<myProtoParticle*>) );
      instance.SetNew(&new_vectorlEmyProtoParticlemUgR);
      instance.SetNewArray(&newArray_vectorlEmyProtoParticlemUgR);
      instance.SetDelete(&delete_vectorlEmyProtoParticlemUgR);
      instance.SetDeleteArray(&deleteArray_vectorlEmyProtoParticlemUgR);
      instance.SetDestructor(&destruct_vectorlEmyProtoParticlemUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<myProtoParticle*> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<myProtoParticle*>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEmyProtoParticlemUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<myProtoParticle*>*)0x0)->GetClass();
      vectorlEmyProtoParticlemUgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEmyProtoParticlemUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEmyProtoParticlemUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<myProtoParticle*> : new vector<myProtoParticle*>;
   }
   static void *newArray_vectorlEmyProtoParticlemUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<myProtoParticle*>[nElements] : new vector<myProtoParticle*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEmyProtoParticlemUgR(void *p) {
      delete ((vector<myProtoParticle*>*)p);
   }
   static void deleteArray_vectorlEmyProtoParticlemUgR(void *p) {
      delete [] ((vector<myProtoParticle*>*)p);
   }
   static void destruct_vectorlEmyProtoParticlemUgR(void *p) {
      typedef vector<myProtoParticle*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<myProtoParticle*>

namespace ROOT {
   static TClass *vectorlEmyParticlegR_Dictionary();
   static void vectorlEmyParticlegR_TClassManip(TClass*);
   static void *new_vectorlEmyParticlegR(void *p = 0);
   static void *newArray_vectorlEmyParticlegR(Long_t size, void *p);
   static void delete_vectorlEmyParticlegR(void *p);
   static void deleteArray_vectorlEmyParticlegR(void *p);
   static void destruct_vectorlEmyParticlegR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<myParticle>*)
   {
      vector<myParticle> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<myParticle>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<myParticle>", -2, "vector", 339,
                  typeid(vector<myParticle>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEmyParticlegR_Dictionary, isa_proxy, 0,
                  sizeof(vector<myParticle>) );
      instance.SetNew(&new_vectorlEmyParticlegR);
      instance.SetNewArray(&newArray_vectorlEmyParticlegR);
      instance.SetDelete(&delete_vectorlEmyParticlegR);
      instance.SetDeleteArray(&deleteArray_vectorlEmyParticlegR);
      instance.SetDestructor(&destruct_vectorlEmyParticlegR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<myParticle> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<myParticle>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEmyParticlegR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<myParticle>*)0x0)->GetClass();
      vectorlEmyParticlegR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEmyParticlegR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEmyParticlegR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<myParticle> : new vector<myParticle>;
   }
   static void *newArray_vectorlEmyParticlegR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<myParticle>[nElements] : new vector<myParticle>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEmyParticlegR(void *p) {
      delete ((vector<myParticle>*)p);
   }
   static void deleteArray_vectorlEmyParticlegR(void *p) {
      delete [] ((vector<myParticle>*)p);
   }
   static void destruct_vectorlEmyParticlegR(void *p) {
      typedef vector<myParticle> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<myParticle>

namespace ROOT {
   static TClass *vectorlEmyParticlemUgR_Dictionary();
   static void vectorlEmyParticlemUgR_TClassManip(TClass*);
   static void *new_vectorlEmyParticlemUgR(void *p = 0);
   static void *newArray_vectorlEmyParticlemUgR(Long_t size, void *p);
   static void delete_vectorlEmyParticlemUgR(void *p);
   static void deleteArray_vectorlEmyParticlemUgR(void *p);
   static void destruct_vectorlEmyParticlemUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<myParticle*>*)
   {
      vector<myParticle*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<myParticle*>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<myParticle*>", -2, "vector", 339,
                  typeid(vector<myParticle*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEmyParticlemUgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<myParticle*>) );
      instance.SetNew(&new_vectorlEmyParticlemUgR);
      instance.SetNewArray(&newArray_vectorlEmyParticlemUgR);
      instance.SetDelete(&delete_vectorlEmyParticlemUgR);
      instance.SetDeleteArray(&deleteArray_vectorlEmyParticlemUgR);
      instance.SetDestructor(&destruct_vectorlEmyParticlemUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<myParticle*> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<myParticle*>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEmyParticlemUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<myParticle*>*)0x0)->GetClass();
      vectorlEmyParticlemUgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEmyParticlemUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEmyParticlemUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<myParticle*> : new vector<myParticle*>;
   }
   static void *newArray_vectorlEmyParticlemUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<myParticle*>[nElements] : new vector<myParticle*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEmyParticlemUgR(void *p) {
      delete ((vector<myParticle*>*)p);
   }
   static void deleteArray_vectorlEmyParticlemUgR(void *p) {
      delete [] ((vector<myParticle*>*)p);
   }
   static void destruct_vectorlEmyParticlemUgR(void *p) {
      typedef vector<myParticle*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<myParticle*>

namespace ROOT {
   static TClass *vectorlEintgR_Dictionary();
   static void vectorlEintgR_TClassManip(TClass*);
   static void *new_vectorlEintgR(void *p = 0);
   static void *newArray_vectorlEintgR(Long_t size, void *p);
   static void delete_vectorlEintgR(void *p);
   static void deleteArray_vectorlEintgR(void *p);
   static void destruct_vectorlEintgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<int>*)
   {
      vector<int> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<int>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<int>", -2, "vector", 339,
                  typeid(vector<int>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEintgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<int>) );
      instance.SetNew(&new_vectorlEintgR);
      instance.SetNewArray(&newArray_vectorlEintgR);
      instance.SetDelete(&delete_vectorlEintgR);
      instance.SetDeleteArray(&deleteArray_vectorlEintgR);
      instance.SetDestructor(&destruct_vectorlEintgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<int> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<int>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEintgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<int>*)0x0)->GetClass();
      vectorlEintgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEintgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEintgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<int> : new vector<int>;
   }
   static void *newArray_vectorlEintgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<int>[nElements] : new vector<int>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEintgR(void *p) {
      delete ((vector<int>*)p);
   }
   static void deleteArray_vectorlEintgR(void *p) {
      delete [] ((vector<int>*)p);
   }
   static void destruct_vectorlEintgR(void *p) {
      typedef vector<int> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<int>

namespace ROOT {
   static TClass *vectorlEfunctionlEvoidoPconstsPcharmUcOintcPgRsPgR_Dictionary();
   static void vectorlEfunctionlEvoidoPconstsPcharmUcOintcPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEfunctionlEvoidoPconstsPcharmUcOintcPgRsPgR(void *p = 0);
   static void *newArray_vectorlEfunctionlEvoidoPconstsPcharmUcOintcPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEfunctionlEvoidoPconstsPcharmUcOintcPgRsPgR(void *p);
   static void deleteArray_vectorlEfunctionlEvoidoPconstsPcharmUcOintcPgRsPgR(void *p);
   static void destruct_vectorlEfunctionlEvoidoPconstsPcharmUcOintcPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<function<void(const char*,int)> >*)
   {
      vector<function<void(const char*,int)> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<function<void(const char*,int)> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<function<void(const char*,int)> >", -2, "vector", 339,
                  typeid(vector<function<void(const char*,int)> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEfunctionlEvoidoPconstsPcharmUcOintcPgRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<function<void(const char*,int)> >) );
      instance.SetNew(&new_vectorlEfunctionlEvoidoPconstsPcharmUcOintcPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEfunctionlEvoidoPconstsPcharmUcOintcPgRsPgR);
      instance.SetDelete(&delete_vectorlEfunctionlEvoidoPconstsPcharmUcOintcPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEfunctionlEvoidoPconstsPcharmUcOintcPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEfunctionlEvoidoPconstsPcharmUcOintcPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<function<void(const char*,int)> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<function<void(const char*,int)> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEfunctionlEvoidoPconstsPcharmUcOintcPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<function<void(const char*,int)> >*)0x0)->GetClass();
      vectorlEfunctionlEvoidoPconstsPcharmUcOintcPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEfunctionlEvoidoPconstsPcharmUcOintcPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEfunctionlEvoidoPconstsPcharmUcOintcPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<function<void(const char*,int)> > : new vector<function<void(const char*,int)> >;
   }
   static void *newArray_vectorlEfunctionlEvoidoPconstsPcharmUcOintcPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<function<void(const char*,int)> >[nElements] : new vector<function<void(const char*,int)> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEfunctionlEvoidoPconstsPcharmUcOintcPgRsPgR(void *p) {
      delete ((vector<function<void(const char*,int)> >*)p);
   }
   static void deleteArray_vectorlEfunctionlEvoidoPconstsPcharmUcOintcPgRsPgR(void *p) {
      delete [] ((vector<function<void(const char*,int)> >*)p);
   }
   static void destruct_vectorlEfunctionlEvoidoPconstsPcharmUcOintcPgRsPgR(void *p) {
      typedef vector<function<void(const char*,int)> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<function<void(const char*,int)> >

namespace ROOT {
   static TClass *vectorlEfunctionlElongoPcPgRsPgR_Dictionary();
   static void vectorlEfunctionlElongoPcPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEfunctionlElongoPcPgRsPgR(void *p = 0);
   static void *newArray_vectorlEfunctionlElongoPcPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEfunctionlElongoPcPgRsPgR(void *p);
   static void deleteArray_vectorlEfunctionlElongoPcPgRsPgR(void *p);
   static void destruct_vectorlEfunctionlElongoPcPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<function<long()> >*)
   {
      vector<function<long()> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<function<long()> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<function<long()> >", -2, "vector", 339,
                  typeid(vector<function<long()> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEfunctionlElongoPcPgRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<function<long()> >) );
      instance.SetNew(&new_vectorlEfunctionlElongoPcPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEfunctionlElongoPcPgRsPgR);
      instance.SetDelete(&delete_vectorlEfunctionlElongoPcPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEfunctionlElongoPcPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEfunctionlElongoPcPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<function<long()> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<function<long()> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEfunctionlElongoPcPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<function<long()> >*)0x0)->GetClass();
      vectorlEfunctionlElongoPcPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEfunctionlElongoPcPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEfunctionlElongoPcPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<function<long()> > : new vector<function<long()> >;
   }
   static void *newArray_vectorlEfunctionlElongoPcPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<function<long()> >[nElements] : new vector<function<long()> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEfunctionlElongoPcPgRsPgR(void *p) {
      delete ((vector<function<long()> >*)p);
   }
   static void deleteArray_vectorlEfunctionlElongoPcPgRsPgR(void *p) {
      delete [] ((vector<function<long()> >*)p);
   }
   static void destruct_vectorlEfunctionlElongoPcPgRsPgR(void *p) {
      typedef vector<function<long()> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<function<long()> >

namespace ROOT {
   static TClass *vectorlEfunctionlEintoPcPgRsPgR_Dictionary();
   static void vectorlEfunctionlEintoPcPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEfunctionlEintoPcPgRsPgR(void *p = 0);
   static void *newArray_vectorlEfunctionlEintoPcPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEfunctionlEintoPcPgRsPgR(void *p);
   static void deleteArray_vectorlEfunctionlEintoPcPgRsPgR(void *p);
   static void destruct_vectorlEfunctionlEintoPcPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<function<int()> >*)
   {
      vector<function<int()> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<function<int()> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<function<int()> >", -2, "vector", 339,
                  typeid(vector<function<int()> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEfunctionlEintoPcPgRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<function<int()> >) );
      instance.SetNew(&new_vectorlEfunctionlEintoPcPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEfunctionlEintoPcPgRsPgR);
      instance.SetDelete(&delete_vectorlEfunctionlEintoPcPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEfunctionlEintoPcPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEfunctionlEintoPcPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<function<int()> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<function<int()> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEfunctionlEintoPcPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<function<int()> >*)0x0)->GetClass();
      vectorlEfunctionlEintoPcPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEfunctionlEintoPcPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEfunctionlEintoPcPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<function<int()> > : new vector<function<int()> >;
   }
   static void *newArray_vectorlEfunctionlEintoPcPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<function<int()> >[nElements] : new vector<function<int()> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEfunctionlEintoPcPgRsPgR(void *p) {
      delete ((vector<function<int()> >*)p);
   }
   static void deleteArray_vectorlEfunctionlEintoPcPgRsPgR(void *p) {
      delete [] ((vector<function<int()> >*)p);
   }
   static void destruct_vectorlEfunctionlEintoPcPgRsPgR(void *p) {
      typedef vector<function<int()> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<function<int()> >

namespace ROOT {
   static TClass *vectorlEfunctionlEdoubleoPcPgRsPgR_Dictionary();
   static void vectorlEfunctionlEdoubleoPcPgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEfunctionlEdoubleoPcPgRsPgR(void *p = 0);
   static void *newArray_vectorlEfunctionlEdoubleoPcPgRsPgR(Long_t size, void *p);
   static void delete_vectorlEfunctionlEdoubleoPcPgRsPgR(void *p);
   static void deleteArray_vectorlEfunctionlEdoubleoPcPgRsPgR(void *p);
   static void destruct_vectorlEfunctionlEdoubleoPcPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<function<double()> >*)
   {
      vector<function<double()> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<function<double()> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<function<double()> >", -2, "vector", 339,
                  typeid(vector<function<double()> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEfunctionlEdoubleoPcPgRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<function<double()> >) );
      instance.SetNew(&new_vectorlEfunctionlEdoubleoPcPgRsPgR);
      instance.SetNewArray(&newArray_vectorlEfunctionlEdoubleoPcPgRsPgR);
      instance.SetDelete(&delete_vectorlEfunctionlEdoubleoPcPgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEfunctionlEdoubleoPcPgRsPgR);
      instance.SetDestructor(&destruct_vectorlEfunctionlEdoubleoPcPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<function<double()> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<function<double()> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEfunctionlEdoubleoPcPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<function<double()> >*)0x0)->GetClass();
      vectorlEfunctionlEdoubleoPcPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEfunctionlEdoubleoPcPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEfunctionlEdoubleoPcPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<function<double()> > : new vector<function<double()> >;
   }
   static void *newArray_vectorlEfunctionlEdoubleoPcPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<function<double()> >[nElements] : new vector<function<double()> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEfunctionlEdoubleoPcPgRsPgR(void *p) {
      delete ((vector<function<double()> >*)p);
   }
   static void deleteArray_vectorlEfunctionlEdoubleoPcPgRsPgR(void *p) {
      delete [] ((vector<function<double()> >*)p);
   }
   static void destruct_vectorlEfunctionlEdoubleoPcPgRsPgR(void *p) {
      typedef vector<function<double()> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<function<double()> >

namespace ROOT {
   static TClass *vectorlEfloatgR_Dictionary();
   static void vectorlEfloatgR_TClassManip(TClass*);
   static void *new_vectorlEfloatgR(void *p = 0);
   static void *newArray_vectorlEfloatgR(Long_t size, void *p);
   static void delete_vectorlEfloatgR(void *p);
   static void deleteArray_vectorlEfloatgR(void *p);
   static void destruct_vectorlEfloatgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<float>*)
   {
      vector<float> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<float>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<float>", -2, "vector", 339,
                  typeid(vector<float>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEfloatgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<float>) );
      instance.SetNew(&new_vectorlEfloatgR);
      instance.SetNewArray(&newArray_vectorlEfloatgR);
      instance.SetDelete(&delete_vectorlEfloatgR);
      instance.SetDeleteArray(&deleteArray_vectorlEfloatgR);
      instance.SetDestructor(&destruct_vectorlEfloatgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<float> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<float>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEfloatgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<float>*)0x0)->GetClass();
      vectorlEfloatgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEfloatgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEfloatgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<float> : new vector<float>;
   }
   static void *newArray_vectorlEfloatgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<float>[nElements] : new vector<float>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEfloatgR(void *p) {
      delete ((vector<float>*)p);
   }
   static void deleteArray_vectorlEfloatgR(void *p) {
      delete [] ((vector<float>*)p);
   }
   static void destruct_vectorlEfloatgR(void *p) {
      typedef vector<float> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<float>

namespace ROOT {
   static TClass *vectorlEdoublegR_Dictionary();
   static void vectorlEdoublegR_TClassManip(TClass*);
   static void *new_vectorlEdoublegR(void *p = 0);
   static void *newArray_vectorlEdoublegR(Long_t size, void *p);
   static void delete_vectorlEdoublegR(void *p);
   static void deleteArray_vectorlEdoublegR(void *p);
   static void destruct_vectorlEdoublegR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<double>*)
   {
      vector<double> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<double>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<double>", -2, "vector", 339,
                  typeid(vector<double>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEdoublegR_Dictionary, isa_proxy, 0,
                  sizeof(vector<double>) );
      instance.SetNew(&new_vectorlEdoublegR);
      instance.SetNewArray(&newArray_vectorlEdoublegR);
      instance.SetDelete(&delete_vectorlEdoublegR);
      instance.SetDeleteArray(&deleteArray_vectorlEdoublegR);
      instance.SetDestructor(&destruct_vectorlEdoublegR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<double> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<double>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEdoublegR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<double>*)0x0)->GetClass();
      vectorlEdoublegR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEdoublegR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEdoublegR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<double> : new vector<double>;
   }
   static void *newArray_vectorlEdoublegR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<double>[nElements] : new vector<double>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEdoublegR(void *p) {
      delete ((vector<double>*)p);
   }
   static void deleteArray_vectorlEdoublegR(void *p) {
      delete [] ((vector<double>*)p);
   }
   static void destruct_vectorlEdoublegR(void *p) {
      typedef vector<double> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<double>

namespace ROOT {
   static TClass *vectorlEVertexgR_Dictionary();
   static void vectorlEVertexgR_TClassManip(TClass*);
   static void *new_vectorlEVertexgR(void *p = 0);
   static void *newArray_vectorlEVertexgR(Long_t size, void *p);
   static void delete_vectorlEVertexgR(void *p);
   static void deleteArray_vectorlEVertexgR(void *p);
   static void destruct_vectorlEVertexgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<Vertex>*)
   {
      vector<Vertex> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<Vertex>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<Vertex>", -2, "vector", 339,
                  typeid(vector<Vertex>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEVertexgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<Vertex>) );
      instance.SetNew(&new_vectorlEVertexgR);
      instance.SetNewArray(&newArray_vectorlEVertexgR);
      instance.SetDelete(&delete_vectorlEVertexgR);
      instance.SetDeleteArray(&deleteArray_vectorlEVertexgR);
      instance.SetDestructor(&destruct_vectorlEVertexgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<Vertex> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<Vertex>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEVertexgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<Vertex>*)0x0)->GetClass();
      vectorlEVertexgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEVertexgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEVertexgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<Vertex> : new vector<Vertex>;
   }
   static void *newArray_vectorlEVertexgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<Vertex>[nElements] : new vector<Vertex>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEVertexgR(void *p) {
      delete ((vector<Vertex>*)p);
   }
   static void deleteArray_vectorlEVertexgR(void *p) {
      delete [] ((vector<Vertex>*)p);
   }
   static void destruct_vectorlEVertexgR(void *p) {
      typedef vector<Vertex> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<Vertex>

namespace ROOT {
   static TClass *vectorlETProfilegR_Dictionary();
   static void vectorlETProfilegR_TClassManip(TClass*);
   static void *new_vectorlETProfilegR(void *p = 0);
   static void *newArray_vectorlETProfilegR(Long_t size, void *p);
   static void delete_vectorlETProfilegR(void *p);
   static void deleteArray_vectorlETProfilegR(void *p);
   static void destruct_vectorlETProfilegR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<TProfile>*)
   {
      vector<TProfile> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<TProfile>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<TProfile>", -2, "vector", 339,
                  typeid(vector<TProfile>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlETProfilegR_Dictionary, isa_proxy, 0,
                  sizeof(vector<TProfile>) );
      instance.SetNew(&new_vectorlETProfilegR);
      instance.SetNewArray(&newArray_vectorlETProfilegR);
      instance.SetDelete(&delete_vectorlETProfilegR);
      instance.SetDeleteArray(&deleteArray_vectorlETProfilegR);
      instance.SetDestructor(&destruct_vectorlETProfilegR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<TProfile> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<TProfile>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlETProfilegR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<TProfile>*)0x0)->GetClass();
      vectorlETProfilegR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlETProfilegR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlETProfilegR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<TProfile> : new vector<TProfile>;
   }
   static void *newArray_vectorlETProfilegR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<TProfile>[nElements] : new vector<TProfile>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlETProfilegR(void *p) {
      delete ((vector<TProfile>*)p);
   }
   static void deleteArray_vectorlETProfilegR(void *p) {
      delete [] ((vector<TProfile>*)p);
   }
   static void destruct_vectorlETProfilegR(void *p) {
      typedef vector<TProfile> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<TProfile>

namespace ROOT {
   static TClass *vectorlETGraphErrorsgR_Dictionary();
   static void vectorlETGraphErrorsgR_TClassManip(TClass*);
   static void *new_vectorlETGraphErrorsgR(void *p = 0);
   static void *newArray_vectorlETGraphErrorsgR(Long_t size, void *p);
   static void delete_vectorlETGraphErrorsgR(void *p);
   static void deleteArray_vectorlETGraphErrorsgR(void *p);
   static void destruct_vectorlETGraphErrorsgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<TGraphErrors>*)
   {
      vector<TGraphErrors> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<TGraphErrors>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<TGraphErrors>", -2, "vector", 339,
                  typeid(vector<TGraphErrors>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlETGraphErrorsgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<TGraphErrors>) );
      instance.SetNew(&new_vectorlETGraphErrorsgR);
      instance.SetNewArray(&newArray_vectorlETGraphErrorsgR);
      instance.SetDelete(&delete_vectorlETGraphErrorsgR);
      instance.SetDeleteArray(&deleteArray_vectorlETGraphErrorsgR);
      instance.SetDestructor(&destruct_vectorlETGraphErrorsgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<TGraphErrors> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<TGraphErrors>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlETGraphErrorsgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<TGraphErrors>*)0x0)->GetClass();
      vectorlETGraphErrorsgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlETGraphErrorsgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlETGraphErrorsgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<TGraphErrors> : new vector<TGraphErrors>;
   }
   static void *newArray_vectorlETGraphErrorsgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<TGraphErrors>[nElements] : new vector<TGraphErrors>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlETGraphErrorsgR(void *p) {
      delete ((vector<TGraphErrors>*)p);
   }
   static void deleteArray_vectorlETGraphErrorsgR(void *p) {
      delete [] ((vector<TGraphErrors>*)p);
   }
   static void destruct_vectorlETGraphErrorsgR(void *p) {
      typedef vector<TGraphErrors> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<TGraphErrors>

namespace ROOT {
   static TClass *vectorlETGraphErrorsmUgR_Dictionary();
   static void vectorlETGraphErrorsmUgR_TClassManip(TClass*);
   static void *new_vectorlETGraphErrorsmUgR(void *p = 0);
   static void *newArray_vectorlETGraphErrorsmUgR(Long_t size, void *p);
   static void delete_vectorlETGraphErrorsmUgR(void *p);
   static void deleteArray_vectorlETGraphErrorsmUgR(void *p);
   static void destruct_vectorlETGraphErrorsmUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<TGraphErrors*>*)
   {
      vector<TGraphErrors*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<TGraphErrors*>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<TGraphErrors*>", -2, "vector", 339,
                  typeid(vector<TGraphErrors*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlETGraphErrorsmUgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<TGraphErrors*>) );
      instance.SetNew(&new_vectorlETGraphErrorsmUgR);
      instance.SetNewArray(&newArray_vectorlETGraphErrorsmUgR);
      instance.SetDelete(&delete_vectorlETGraphErrorsmUgR);
      instance.SetDeleteArray(&deleteArray_vectorlETGraphErrorsmUgR);
      instance.SetDestructor(&destruct_vectorlETGraphErrorsmUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<TGraphErrors*> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<TGraphErrors*>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlETGraphErrorsmUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<TGraphErrors*>*)0x0)->GetClass();
      vectorlETGraphErrorsmUgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlETGraphErrorsmUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlETGraphErrorsmUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<TGraphErrors*> : new vector<TGraphErrors*>;
   }
   static void *newArray_vectorlETGraphErrorsmUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<TGraphErrors*>[nElements] : new vector<TGraphErrors*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlETGraphErrorsmUgR(void *p) {
      delete ((vector<TGraphErrors*>*)p);
   }
   static void deleteArray_vectorlETGraphErrorsmUgR(void *p) {
      delete [] ((vector<TGraphErrors*>*)p);
   }
   static void destruct_vectorlETGraphErrorsmUgR(void *p) {
      typedef vector<TGraphErrors*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<TGraphErrors*>

namespace ROOT {
   static TClass *vectorlELong64_tgR_Dictionary();
   static void vectorlELong64_tgR_TClassManip(TClass*);
   static void *new_vectorlELong64_tgR(void *p = 0);
   static void *newArray_vectorlELong64_tgR(Long_t size, void *p);
   static void delete_vectorlELong64_tgR(void *p);
   static void deleteArray_vectorlELong64_tgR(void *p);
   static void destruct_vectorlELong64_tgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<Long64_t>*)
   {
      vector<Long64_t> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<Long64_t>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<Long64_t>", -2, "vector", 339,
                  typeid(vector<Long64_t>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlELong64_tgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<Long64_t>) );
      instance.SetNew(&new_vectorlELong64_tgR);
      instance.SetNewArray(&newArray_vectorlELong64_tgR);
      instance.SetDelete(&delete_vectorlELong64_tgR);
      instance.SetDeleteArray(&deleteArray_vectorlELong64_tgR);
      instance.SetDestructor(&destruct_vectorlELong64_tgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<Long64_t> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<Long64_t>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlELong64_tgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<Long64_t>*)0x0)->GetClass();
      vectorlELong64_tgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlELong64_tgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlELong64_tgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<Long64_t> : new vector<Long64_t>;
   }
   static void *newArray_vectorlELong64_tgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<Long64_t>[nElements] : new vector<Long64_t>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlELong64_tgR(void *p) {
      delete ((vector<Long64_t>*)p);
   }
   static void deleteArray_vectorlELong64_tgR(void *p) {
      delete [] ((vector<Long64_t>*)p);
   }
   static void destruct_vectorlELong64_tgR(void *p) {
      typedef vector<Long64_t> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<Long64_t>

namespace ROOT {
   static TClass *vectorlEEntryTimesgR_Dictionary();
   static void vectorlEEntryTimesgR_TClassManip(TClass*);
   static void *new_vectorlEEntryTimesgR(void *p = 0);
   static void *newArray_vectorlEEntryTimesgR(Long_t size, void *p);
   static void delete_vectorlEEntryTimesgR(void *p);
   static void deleteArray_vectorlEEntryTimesgR(void *p);
   static void destruct_vectorlEEntryTimesgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<EntryTimes>*)
   {
      vector<EntryTimes> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<EntryTimes>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<EntryTimes>", -2, "vector", 339,
                  typeid(vector<EntryTimes>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEEntryTimesgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<EntryTimes>) );
      instance.SetNew(&new_vectorlEEntryTimesgR);
      instance.SetNewArray(&newArray_vectorlEEntryTimesgR);
      instance.SetDelete(&delete_vectorlEEntryTimesgR);
      instance.SetDeleteArray(&deleteArray_vectorlEEntryTimesgR);
      instance.SetDestructor(&destruct_vectorlEEntryTimesgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<EntryTimes> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<EntryTimes>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEEntryTimesgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<EntryTimes>*)0x0)->GetClass();
      vectorlEEntryTimesgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEEntryTimesgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEEntryTimesgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<EntryTimes> : new vector<EntryTimes>;
   }
   static void *newArray_vectorlEEntryTimesgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<EntryTimes>[nElements] : new vector<EntryTimes>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEEntryTimesgR(void *p) {
      delete ((vector<EntryTimes>*)p);
   }
   static void deleteArray_vectorlEEntryTimesgR(void *p) {
      delete [] ((vector<EntryTimes>*)p);
   }
   static void destruct_vectorlEEntryTimesgR(void *p) {
      typedef vector<EntryTimes> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<EntryTimes>

namespace ROOT {
   static TClass *vectorlEClustermUgR_Dictionary();
   static void vectorlEClustermUgR_TClassManip(TClass*);
   static void *new_vectorlEClustermUgR(void *p = 0);
   static void *newArray_vectorlEClustermUgR(Long_t size, void *p);
   static void delete_vectorlEClustermUgR(void *p);
   static void deleteArray_vectorlEClustermUgR(void *p);
   static void destruct_vectorlEClustermUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<Cluster*>*)
   {
      vector<Cluster*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<Cluster*>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<Cluster*>", -2, "vector", 339,
                  typeid(vector<Cluster*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEClustermUgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<Cluster*>) );
      instance.SetNew(&new_vectorlEClustermUgR);
      instance.SetNewArray(&newArray_vectorlEClustermUgR);
      instance.SetDelete(&delete_vectorlEClustermUgR);
      instance.SetDeleteArray(&deleteArray_vectorlEClustermUgR);
      instance.SetDestructor(&destruct_vectorlEClustermUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<Cluster*> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<Cluster*>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEClustermUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<Cluster*>*)0x0)->GetClass();
      vectorlEClustermUgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEClustermUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEClustermUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<Cluster*> : new vector<Cluster*>;
   }
   static void *newArray_vectorlEClustermUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<Cluster*>[nElements] : new vector<Cluster*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEClustermUgR(void *p) {
      delete ((vector<Cluster*>*)p);
   }
   static void deleteArray_vectorlEClustermUgR(void *p) {
      delete [] ((vector<Cluster*>*)p);
   }
   static void destruct_vectorlEClustermUgR(void *p) {
      typedef vector<Cluster*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<Cluster*>

namespace ROOT {
   static TClass *vectorlECellmUgR_Dictionary();
   static void vectorlECellmUgR_TClassManip(TClass*);
   static void *new_vectorlECellmUgR(void *p = 0);
   static void *newArray_vectorlECellmUgR(Long_t size, void *p);
   static void delete_vectorlECellmUgR(void *p);
   static void deleteArray_vectorlECellmUgR(void *p);
   static void destruct_vectorlECellmUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<Cell*>*)
   {
      vector<Cell*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<Cell*>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<Cell*>", -2, "vector", 339,
                  typeid(vector<Cell*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlECellmUgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<Cell*>) );
      instance.SetNew(&new_vectorlECellmUgR);
      instance.SetNewArray(&newArray_vectorlECellmUgR);
      instance.SetDelete(&delete_vectorlECellmUgR);
      instance.SetDeleteArray(&deleteArray_vectorlECellmUgR);
      instance.SetDestructor(&destruct_vectorlECellmUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<Cell*> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<Cell*>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlECellmUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<Cell*>*)0x0)->GetClass();
      vectorlECellmUgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlECellmUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlECellmUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<Cell*> : new vector<Cell*>;
   }
   static void *newArray_vectorlECellmUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<Cell*>[nElements] : new vector<Cell*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlECellmUgR(void *p) {
      delete ((vector<Cell*>*)p);
   }
   static void deleteArray_vectorlECellmUgR(void *p) {
      delete [] ((vector<Cell*>*)p);
   }
   static void destruct_vectorlECellmUgR(void *p) {
      typedef vector<Cell*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<Cell*>

namespace ROOT {
   static TClass *maplEstringcOvectorlEvectorlEintgRsPgRsPgR_Dictionary();
   static void maplEstringcOvectorlEvectorlEintgRsPgRsPgR_TClassManip(TClass*);
   static void *new_maplEstringcOvectorlEvectorlEintgRsPgRsPgR(void *p = 0);
   static void *newArray_maplEstringcOvectorlEvectorlEintgRsPgRsPgR(Long_t size, void *p);
   static void delete_maplEstringcOvectorlEvectorlEintgRsPgRsPgR(void *p);
   static void deleteArray_maplEstringcOvectorlEvectorlEintgRsPgRsPgR(void *p);
   static void destruct_maplEstringcOvectorlEvectorlEintgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,vector<vector<int> > >*)
   {
      map<string,vector<vector<int> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,vector<vector<int> > >));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,vector<vector<int> > >", -2, "map", 100,
                  typeid(map<string,vector<vector<int> > >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOvectorlEvectorlEintgRsPgRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(map<string,vector<vector<int> > >) );
      instance.SetNew(&new_maplEstringcOvectorlEvectorlEintgRsPgRsPgR);
      instance.SetNewArray(&newArray_maplEstringcOvectorlEvectorlEintgRsPgRsPgR);
      instance.SetDelete(&delete_maplEstringcOvectorlEvectorlEintgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOvectorlEvectorlEintgRsPgRsPgR);
      instance.SetDestructor(&destruct_maplEstringcOvectorlEvectorlEintgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,vector<vector<int> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<string,vector<vector<int> > >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOvectorlEvectorlEintgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,vector<vector<int> > >*)0x0)->GetClass();
      maplEstringcOvectorlEvectorlEintgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOvectorlEvectorlEintgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOvectorlEvectorlEintgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,vector<vector<int> > > : new map<string,vector<vector<int> > >;
   }
   static void *newArray_maplEstringcOvectorlEvectorlEintgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,vector<vector<int> > >[nElements] : new map<string,vector<vector<int> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOvectorlEvectorlEintgRsPgRsPgR(void *p) {
      delete ((map<string,vector<vector<int> > >*)p);
   }
   static void deleteArray_maplEstringcOvectorlEvectorlEintgRsPgRsPgR(void *p) {
      delete [] ((map<string,vector<vector<int> > >*)p);
   }
   static void destruct_maplEstringcOvectorlEvectorlEintgRsPgRsPgR(void *p) {
      typedef map<string,vector<vector<int> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,vector<vector<int> > >

namespace ROOT {
   static TClass *maplEstringcOvectorlEvectorlEdoublegRsPgRsPgR_Dictionary();
   static void maplEstringcOvectorlEvectorlEdoublegRsPgRsPgR_TClassManip(TClass*);
   static void *new_maplEstringcOvectorlEvectorlEdoublegRsPgRsPgR(void *p = 0);
   static void *newArray_maplEstringcOvectorlEvectorlEdoublegRsPgRsPgR(Long_t size, void *p);
   static void delete_maplEstringcOvectorlEvectorlEdoublegRsPgRsPgR(void *p);
   static void deleteArray_maplEstringcOvectorlEvectorlEdoublegRsPgRsPgR(void *p);
   static void destruct_maplEstringcOvectorlEvectorlEdoublegRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,vector<vector<double> > >*)
   {
      map<string,vector<vector<double> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,vector<vector<double> > >));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,vector<vector<double> > >", -2, "map", 100,
                  typeid(map<string,vector<vector<double> > >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOvectorlEvectorlEdoublegRsPgRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(map<string,vector<vector<double> > >) );
      instance.SetNew(&new_maplEstringcOvectorlEvectorlEdoublegRsPgRsPgR);
      instance.SetNewArray(&newArray_maplEstringcOvectorlEvectorlEdoublegRsPgRsPgR);
      instance.SetDelete(&delete_maplEstringcOvectorlEvectorlEdoublegRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOvectorlEvectorlEdoublegRsPgRsPgR);
      instance.SetDestructor(&destruct_maplEstringcOvectorlEvectorlEdoublegRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,vector<vector<double> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<string,vector<vector<double> > >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOvectorlEvectorlEdoublegRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,vector<vector<double> > >*)0x0)->GetClass();
      maplEstringcOvectorlEvectorlEdoublegRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOvectorlEvectorlEdoublegRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOvectorlEvectorlEdoublegRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,vector<vector<double> > > : new map<string,vector<vector<double> > >;
   }
   static void *newArray_maplEstringcOvectorlEvectorlEdoublegRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,vector<vector<double> > >[nElements] : new map<string,vector<vector<double> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOvectorlEvectorlEdoublegRsPgRsPgR(void *p) {
      delete ((map<string,vector<vector<double> > >*)p);
   }
   static void deleteArray_maplEstringcOvectorlEvectorlEdoublegRsPgRsPgR(void *p) {
      delete [] ((map<string,vector<vector<double> > >*)p);
   }
   static void destruct_maplEstringcOvectorlEvectorlEdoublegRsPgRsPgR(void *p) {
      typedef map<string,vector<vector<double> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,vector<vector<double> > >

namespace ROOT {
   static TClass *maplEstringcOvectorlEstringgRsPgR_Dictionary();
   static void maplEstringcOvectorlEstringgRsPgR_TClassManip(TClass*);
   static void *new_maplEstringcOvectorlEstringgRsPgR(void *p = 0);
   static void *newArray_maplEstringcOvectorlEstringgRsPgR(Long_t size, void *p);
   static void delete_maplEstringcOvectorlEstringgRsPgR(void *p);
   static void deleteArray_maplEstringcOvectorlEstringgRsPgR(void *p);
   static void destruct_maplEstringcOvectorlEstringgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,vector<string> >*)
   {
      map<string,vector<string> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,vector<string> >));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,vector<string> >", -2, "map", 100,
                  typeid(map<string,vector<string> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOvectorlEstringgRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(map<string,vector<string> >) );
      instance.SetNew(&new_maplEstringcOvectorlEstringgRsPgR);
      instance.SetNewArray(&newArray_maplEstringcOvectorlEstringgRsPgR);
      instance.SetDelete(&delete_maplEstringcOvectorlEstringgRsPgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOvectorlEstringgRsPgR);
      instance.SetDestructor(&destruct_maplEstringcOvectorlEstringgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,vector<string> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<string,vector<string> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOvectorlEstringgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,vector<string> >*)0x0)->GetClass();
      maplEstringcOvectorlEstringgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOvectorlEstringgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOvectorlEstringgRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,vector<string> > : new map<string,vector<string> >;
   }
   static void *newArray_maplEstringcOvectorlEstringgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,vector<string> >[nElements] : new map<string,vector<string> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOvectorlEstringgRsPgR(void *p) {
      delete ((map<string,vector<string> >*)p);
   }
   static void deleteArray_maplEstringcOvectorlEstringgRsPgR(void *p) {
      delete [] ((map<string,vector<string> >*)p);
   }
   static void destruct_maplEstringcOvectorlEstringgRsPgR(void *p) {
      typedef map<string,vector<string> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,vector<string> >

namespace ROOT {
   static TClass *maplEstringcOvectorlEmyParticlegRmUgR_Dictionary();
   static void maplEstringcOvectorlEmyParticlegRmUgR_TClassManip(TClass*);
   static void *new_maplEstringcOvectorlEmyParticlegRmUgR(void *p = 0);
   static void *newArray_maplEstringcOvectorlEmyParticlegRmUgR(Long_t size, void *p);
   static void delete_maplEstringcOvectorlEmyParticlegRmUgR(void *p);
   static void deleteArray_maplEstringcOvectorlEmyParticlegRmUgR(void *p);
   static void destruct_maplEstringcOvectorlEmyParticlegRmUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,vector<myParticle>*>*)
   {
      map<string,vector<myParticle>*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,vector<myParticle>*>));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,vector<myParticle>*>", -2, "map", 100,
                  typeid(map<string,vector<myParticle>*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOvectorlEmyParticlegRmUgR_Dictionary, isa_proxy, 0,
                  sizeof(map<string,vector<myParticle>*>) );
      instance.SetNew(&new_maplEstringcOvectorlEmyParticlegRmUgR);
      instance.SetNewArray(&newArray_maplEstringcOvectorlEmyParticlegRmUgR);
      instance.SetDelete(&delete_maplEstringcOvectorlEmyParticlegRmUgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOvectorlEmyParticlegRmUgR);
      instance.SetDestructor(&destruct_maplEstringcOvectorlEmyParticlegRmUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,vector<myParticle>*> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<string,vector<myParticle>*>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOvectorlEmyParticlegRmUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,vector<myParticle>*>*)0x0)->GetClass();
      maplEstringcOvectorlEmyParticlegRmUgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOvectorlEmyParticlegRmUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOvectorlEmyParticlegRmUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,vector<myParticle>*> : new map<string,vector<myParticle>*>;
   }
   static void *newArray_maplEstringcOvectorlEmyParticlegRmUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,vector<myParticle>*>[nElements] : new map<string,vector<myParticle>*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOvectorlEmyParticlegRmUgR(void *p) {
      delete ((map<string,vector<myParticle>*>*)p);
   }
   static void deleteArray_maplEstringcOvectorlEmyParticlegRmUgR(void *p) {
      delete [] ((map<string,vector<myParticle>*>*)p);
   }
   static void destruct_maplEstringcOvectorlEmyParticlegRmUgR(void *p) {
      typedef map<string,vector<myParticle>*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,vector<myParticle>*>

namespace ROOT {
   static TClass *maplEstringcOvectorlEmyParticlegRsPgR_Dictionary();
   static void maplEstringcOvectorlEmyParticlegRsPgR_TClassManip(TClass*);
   static void *new_maplEstringcOvectorlEmyParticlegRsPgR(void *p = 0);
   static void *newArray_maplEstringcOvectorlEmyParticlegRsPgR(Long_t size, void *p);
   static void delete_maplEstringcOvectorlEmyParticlegRsPgR(void *p);
   static void deleteArray_maplEstringcOvectorlEmyParticlegRsPgR(void *p);
   static void destruct_maplEstringcOvectorlEmyParticlegRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,vector<myParticle> >*)
   {
      map<string,vector<myParticle> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,vector<myParticle> >));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,vector<myParticle> >", -2, "map", 100,
                  typeid(map<string,vector<myParticle> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOvectorlEmyParticlegRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(map<string,vector<myParticle> >) );
      instance.SetNew(&new_maplEstringcOvectorlEmyParticlegRsPgR);
      instance.SetNewArray(&newArray_maplEstringcOvectorlEmyParticlegRsPgR);
      instance.SetDelete(&delete_maplEstringcOvectorlEmyParticlegRsPgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOvectorlEmyParticlegRsPgR);
      instance.SetDestructor(&destruct_maplEstringcOvectorlEmyParticlegRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,vector<myParticle> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<string,vector<myParticle> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOvectorlEmyParticlegRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,vector<myParticle> >*)0x0)->GetClass();
      maplEstringcOvectorlEmyParticlegRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOvectorlEmyParticlegRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOvectorlEmyParticlegRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,vector<myParticle> > : new map<string,vector<myParticle> >;
   }
   static void *newArray_maplEstringcOvectorlEmyParticlegRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,vector<myParticle> >[nElements] : new map<string,vector<myParticle> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOvectorlEmyParticlegRsPgR(void *p) {
      delete ((map<string,vector<myParticle> >*)p);
   }
   static void deleteArray_maplEstringcOvectorlEmyParticlegRsPgR(void *p) {
      delete [] ((map<string,vector<myParticle> >*)p);
   }
   static void destruct_maplEstringcOvectorlEmyParticlegRsPgR(void *p) {
      typedef map<string,vector<myParticle> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,vector<myParticle> >

namespace ROOT {
   static TClass *maplEstringcOvectorlEintgRsPgR_Dictionary();
   static void maplEstringcOvectorlEintgRsPgR_TClassManip(TClass*);
   static void *new_maplEstringcOvectorlEintgRsPgR(void *p = 0);
   static void *newArray_maplEstringcOvectorlEintgRsPgR(Long_t size, void *p);
   static void delete_maplEstringcOvectorlEintgRsPgR(void *p);
   static void deleteArray_maplEstringcOvectorlEintgRsPgR(void *p);
   static void destruct_maplEstringcOvectorlEintgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,vector<int> >*)
   {
      map<string,vector<int> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,vector<int> >));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,vector<int> >", -2, "map", 100,
                  typeid(map<string,vector<int> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOvectorlEintgRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(map<string,vector<int> >) );
      instance.SetNew(&new_maplEstringcOvectorlEintgRsPgR);
      instance.SetNewArray(&newArray_maplEstringcOvectorlEintgRsPgR);
      instance.SetDelete(&delete_maplEstringcOvectorlEintgRsPgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOvectorlEintgRsPgR);
      instance.SetDestructor(&destruct_maplEstringcOvectorlEintgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,vector<int> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<string,vector<int> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOvectorlEintgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,vector<int> >*)0x0)->GetClass();
      maplEstringcOvectorlEintgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOvectorlEintgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOvectorlEintgRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,vector<int> > : new map<string,vector<int> >;
   }
   static void *newArray_maplEstringcOvectorlEintgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,vector<int> >[nElements] : new map<string,vector<int> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOvectorlEintgRsPgR(void *p) {
      delete ((map<string,vector<int> >*)p);
   }
   static void deleteArray_maplEstringcOvectorlEintgRsPgR(void *p) {
      delete [] ((map<string,vector<int> >*)p);
   }
   static void destruct_maplEstringcOvectorlEintgRsPgR(void *p) {
      typedef map<string,vector<int> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,vector<int> >

namespace ROOT {
   static TClass *maplEstringcOvectorlEfunctionlEvoidoPconstsPcharmUcOintcPgRsPgRsPgR_Dictionary();
   static void maplEstringcOvectorlEfunctionlEvoidoPconstsPcharmUcOintcPgRsPgRsPgR_TClassManip(TClass*);
   static void *new_maplEstringcOvectorlEfunctionlEvoidoPconstsPcharmUcOintcPgRsPgRsPgR(void *p = 0);
   static void *newArray_maplEstringcOvectorlEfunctionlEvoidoPconstsPcharmUcOintcPgRsPgRsPgR(Long_t size, void *p);
   static void delete_maplEstringcOvectorlEfunctionlEvoidoPconstsPcharmUcOintcPgRsPgRsPgR(void *p);
   static void deleteArray_maplEstringcOvectorlEfunctionlEvoidoPconstsPcharmUcOintcPgRsPgRsPgR(void *p);
   static void destruct_maplEstringcOvectorlEfunctionlEvoidoPconstsPcharmUcOintcPgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,vector<function<void(const char*,int)> > >*)
   {
      map<string,vector<function<void(const char*,int)> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,vector<function<void(const char*,int)> > >));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,vector<function<void(const char*,int)> > >", -2, "map", 100,
                  typeid(map<string,vector<function<void(const char*,int)> > >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOvectorlEfunctionlEvoidoPconstsPcharmUcOintcPgRsPgRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(map<string,vector<function<void(const char*,int)> > >) );
      instance.SetNew(&new_maplEstringcOvectorlEfunctionlEvoidoPconstsPcharmUcOintcPgRsPgRsPgR);
      instance.SetNewArray(&newArray_maplEstringcOvectorlEfunctionlEvoidoPconstsPcharmUcOintcPgRsPgRsPgR);
      instance.SetDelete(&delete_maplEstringcOvectorlEfunctionlEvoidoPconstsPcharmUcOintcPgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOvectorlEfunctionlEvoidoPconstsPcharmUcOintcPgRsPgRsPgR);
      instance.SetDestructor(&destruct_maplEstringcOvectorlEfunctionlEvoidoPconstsPcharmUcOintcPgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,vector<function<void(const char*,int)> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<string,vector<function<void(const char*,int)> > >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOvectorlEfunctionlEvoidoPconstsPcharmUcOintcPgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,vector<function<void(const char*,int)> > >*)0x0)->GetClass();
      maplEstringcOvectorlEfunctionlEvoidoPconstsPcharmUcOintcPgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOvectorlEfunctionlEvoidoPconstsPcharmUcOintcPgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOvectorlEfunctionlEvoidoPconstsPcharmUcOintcPgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,vector<function<void(const char*,int)> > > : new map<string,vector<function<void(const char*,int)> > >;
   }
   static void *newArray_maplEstringcOvectorlEfunctionlEvoidoPconstsPcharmUcOintcPgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,vector<function<void(const char*,int)> > >[nElements] : new map<string,vector<function<void(const char*,int)> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOvectorlEfunctionlEvoidoPconstsPcharmUcOintcPgRsPgRsPgR(void *p) {
      delete ((map<string,vector<function<void(const char*,int)> > >*)p);
   }
   static void deleteArray_maplEstringcOvectorlEfunctionlEvoidoPconstsPcharmUcOintcPgRsPgRsPgR(void *p) {
      delete [] ((map<string,vector<function<void(const char*,int)> > >*)p);
   }
   static void destruct_maplEstringcOvectorlEfunctionlEvoidoPconstsPcharmUcOintcPgRsPgRsPgR(void *p) {
      typedef map<string,vector<function<void(const char*,int)> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,vector<function<void(const char*,int)> > >

namespace ROOT {
   static TClass *maplEstringcOvectorlEfunctionlElongoPcPgRsPgRsPgR_Dictionary();
   static void maplEstringcOvectorlEfunctionlElongoPcPgRsPgRsPgR_TClassManip(TClass*);
   static void *new_maplEstringcOvectorlEfunctionlElongoPcPgRsPgRsPgR(void *p = 0);
   static void *newArray_maplEstringcOvectorlEfunctionlElongoPcPgRsPgRsPgR(Long_t size, void *p);
   static void delete_maplEstringcOvectorlEfunctionlElongoPcPgRsPgRsPgR(void *p);
   static void deleteArray_maplEstringcOvectorlEfunctionlElongoPcPgRsPgRsPgR(void *p);
   static void destruct_maplEstringcOvectorlEfunctionlElongoPcPgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,vector<function<long()> > >*)
   {
      map<string,vector<function<long()> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,vector<function<long()> > >));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,vector<function<long()> > >", -2, "map", 100,
                  typeid(map<string,vector<function<long()> > >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOvectorlEfunctionlElongoPcPgRsPgRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(map<string,vector<function<long()> > >) );
      instance.SetNew(&new_maplEstringcOvectorlEfunctionlElongoPcPgRsPgRsPgR);
      instance.SetNewArray(&newArray_maplEstringcOvectorlEfunctionlElongoPcPgRsPgRsPgR);
      instance.SetDelete(&delete_maplEstringcOvectorlEfunctionlElongoPcPgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOvectorlEfunctionlElongoPcPgRsPgRsPgR);
      instance.SetDestructor(&destruct_maplEstringcOvectorlEfunctionlElongoPcPgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,vector<function<long()> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<string,vector<function<long()> > >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOvectorlEfunctionlElongoPcPgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,vector<function<long()> > >*)0x0)->GetClass();
      maplEstringcOvectorlEfunctionlElongoPcPgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOvectorlEfunctionlElongoPcPgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOvectorlEfunctionlElongoPcPgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,vector<function<long()> > > : new map<string,vector<function<long()> > >;
   }
   static void *newArray_maplEstringcOvectorlEfunctionlElongoPcPgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,vector<function<long()> > >[nElements] : new map<string,vector<function<long()> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOvectorlEfunctionlElongoPcPgRsPgRsPgR(void *p) {
      delete ((map<string,vector<function<long()> > >*)p);
   }
   static void deleteArray_maplEstringcOvectorlEfunctionlElongoPcPgRsPgRsPgR(void *p) {
      delete [] ((map<string,vector<function<long()> > >*)p);
   }
   static void destruct_maplEstringcOvectorlEfunctionlElongoPcPgRsPgRsPgR(void *p) {
      typedef map<string,vector<function<long()> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,vector<function<long()> > >

namespace ROOT {
   static TClass *maplEstringcOvectorlEfunctionlEintoPcPgRsPgRsPgR_Dictionary();
   static void maplEstringcOvectorlEfunctionlEintoPcPgRsPgRsPgR_TClassManip(TClass*);
   static void *new_maplEstringcOvectorlEfunctionlEintoPcPgRsPgRsPgR(void *p = 0);
   static void *newArray_maplEstringcOvectorlEfunctionlEintoPcPgRsPgRsPgR(Long_t size, void *p);
   static void delete_maplEstringcOvectorlEfunctionlEintoPcPgRsPgRsPgR(void *p);
   static void deleteArray_maplEstringcOvectorlEfunctionlEintoPcPgRsPgRsPgR(void *p);
   static void destruct_maplEstringcOvectorlEfunctionlEintoPcPgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,vector<function<int()> > >*)
   {
      map<string,vector<function<int()> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,vector<function<int()> > >));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,vector<function<int()> > >", -2, "map", 100,
                  typeid(map<string,vector<function<int()> > >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOvectorlEfunctionlEintoPcPgRsPgRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(map<string,vector<function<int()> > >) );
      instance.SetNew(&new_maplEstringcOvectorlEfunctionlEintoPcPgRsPgRsPgR);
      instance.SetNewArray(&newArray_maplEstringcOvectorlEfunctionlEintoPcPgRsPgRsPgR);
      instance.SetDelete(&delete_maplEstringcOvectorlEfunctionlEintoPcPgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOvectorlEfunctionlEintoPcPgRsPgRsPgR);
      instance.SetDestructor(&destruct_maplEstringcOvectorlEfunctionlEintoPcPgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,vector<function<int()> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<string,vector<function<int()> > >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOvectorlEfunctionlEintoPcPgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,vector<function<int()> > >*)0x0)->GetClass();
      maplEstringcOvectorlEfunctionlEintoPcPgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOvectorlEfunctionlEintoPcPgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOvectorlEfunctionlEintoPcPgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,vector<function<int()> > > : new map<string,vector<function<int()> > >;
   }
   static void *newArray_maplEstringcOvectorlEfunctionlEintoPcPgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,vector<function<int()> > >[nElements] : new map<string,vector<function<int()> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOvectorlEfunctionlEintoPcPgRsPgRsPgR(void *p) {
      delete ((map<string,vector<function<int()> > >*)p);
   }
   static void deleteArray_maplEstringcOvectorlEfunctionlEintoPcPgRsPgRsPgR(void *p) {
      delete [] ((map<string,vector<function<int()> > >*)p);
   }
   static void destruct_maplEstringcOvectorlEfunctionlEintoPcPgRsPgRsPgR(void *p) {
      typedef map<string,vector<function<int()> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,vector<function<int()> > >

namespace ROOT {
   static TClass *maplEstringcOvectorlEfunctionlEdoubleoPcPgRsPgRsPgR_Dictionary();
   static void maplEstringcOvectorlEfunctionlEdoubleoPcPgRsPgRsPgR_TClassManip(TClass*);
   static void *new_maplEstringcOvectorlEfunctionlEdoubleoPcPgRsPgRsPgR(void *p = 0);
   static void *newArray_maplEstringcOvectorlEfunctionlEdoubleoPcPgRsPgRsPgR(Long_t size, void *p);
   static void delete_maplEstringcOvectorlEfunctionlEdoubleoPcPgRsPgRsPgR(void *p);
   static void deleteArray_maplEstringcOvectorlEfunctionlEdoubleoPcPgRsPgRsPgR(void *p);
   static void destruct_maplEstringcOvectorlEfunctionlEdoubleoPcPgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,vector<function<double()> > >*)
   {
      map<string,vector<function<double()> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,vector<function<double()> > >));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,vector<function<double()> > >", -2, "map", 100,
                  typeid(map<string,vector<function<double()> > >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOvectorlEfunctionlEdoubleoPcPgRsPgRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(map<string,vector<function<double()> > >) );
      instance.SetNew(&new_maplEstringcOvectorlEfunctionlEdoubleoPcPgRsPgRsPgR);
      instance.SetNewArray(&newArray_maplEstringcOvectorlEfunctionlEdoubleoPcPgRsPgRsPgR);
      instance.SetDelete(&delete_maplEstringcOvectorlEfunctionlEdoubleoPcPgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOvectorlEfunctionlEdoubleoPcPgRsPgRsPgR);
      instance.SetDestructor(&destruct_maplEstringcOvectorlEfunctionlEdoubleoPcPgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,vector<function<double()> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<string,vector<function<double()> > >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOvectorlEfunctionlEdoubleoPcPgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,vector<function<double()> > >*)0x0)->GetClass();
      maplEstringcOvectorlEfunctionlEdoubleoPcPgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOvectorlEfunctionlEdoubleoPcPgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOvectorlEfunctionlEdoubleoPcPgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,vector<function<double()> > > : new map<string,vector<function<double()> > >;
   }
   static void *newArray_maplEstringcOvectorlEfunctionlEdoubleoPcPgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,vector<function<double()> > >[nElements] : new map<string,vector<function<double()> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOvectorlEfunctionlEdoubleoPcPgRsPgRsPgR(void *p) {
      delete ((map<string,vector<function<double()> > >*)p);
   }
   static void deleteArray_maplEstringcOvectorlEfunctionlEdoubleoPcPgRsPgRsPgR(void *p) {
      delete [] ((map<string,vector<function<double()> > >*)p);
   }
   static void destruct_maplEstringcOvectorlEfunctionlEdoubleoPcPgRsPgRsPgR(void *p) {
      typedef map<string,vector<function<double()> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,vector<function<double()> > >

namespace ROOT {
   static TClass *maplEstringcOvectorlEdoublegRsPgR_Dictionary();
   static void maplEstringcOvectorlEdoublegRsPgR_TClassManip(TClass*);
   static void *new_maplEstringcOvectorlEdoublegRsPgR(void *p = 0);
   static void *newArray_maplEstringcOvectorlEdoublegRsPgR(Long_t size, void *p);
   static void delete_maplEstringcOvectorlEdoublegRsPgR(void *p);
   static void deleteArray_maplEstringcOvectorlEdoublegRsPgR(void *p);
   static void destruct_maplEstringcOvectorlEdoublegRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,vector<double> >*)
   {
      map<string,vector<double> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,vector<double> >));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,vector<double> >", -2, "map", 100,
                  typeid(map<string,vector<double> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOvectorlEdoublegRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(map<string,vector<double> >) );
      instance.SetNew(&new_maplEstringcOvectorlEdoublegRsPgR);
      instance.SetNewArray(&newArray_maplEstringcOvectorlEdoublegRsPgR);
      instance.SetDelete(&delete_maplEstringcOvectorlEdoublegRsPgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOvectorlEdoublegRsPgR);
      instance.SetDestructor(&destruct_maplEstringcOvectorlEdoublegRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,vector<double> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<string,vector<double> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOvectorlEdoublegRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,vector<double> >*)0x0)->GetClass();
      maplEstringcOvectorlEdoublegRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOvectorlEdoublegRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOvectorlEdoublegRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,vector<double> > : new map<string,vector<double> >;
   }
   static void *newArray_maplEstringcOvectorlEdoublegRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,vector<double> >[nElements] : new map<string,vector<double> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOvectorlEdoublegRsPgR(void *p) {
      delete ((map<string,vector<double> >*)p);
   }
   static void deleteArray_maplEstringcOvectorlEdoublegRsPgR(void *p) {
      delete [] ((map<string,vector<double> >*)p);
   }
   static void destruct_maplEstringcOvectorlEdoublegRsPgR(void *p) {
      typedef map<string,vector<double> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,vector<double> >

namespace ROOT {
   static TClass *maplEstringcOvectorlELong64_tgRsPgR_Dictionary();
   static void maplEstringcOvectorlELong64_tgRsPgR_TClassManip(TClass*);
   static void *new_maplEstringcOvectorlELong64_tgRsPgR(void *p = 0);
   static void *newArray_maplEstringcOvectorlELong64_tgRsPgR(Long_t size, void *p);
   static void delete_maplEstringcOvectorlELong64_tgRsPgR(void *p);
   static void deleteArray_maplEstringcOvectorlELong64_tgRsPgR(void *p);
   static void destruct_maplEstringcOvectorlELong64_tgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,vector<Long64_t> >*)
   {
      map<string,vector<Long64_t> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,vector<Long64_t> >));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,vector<Long64_t> >", -2, "map", 100,
                  typeid(map<string,vector<Long64_t> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOvectorlELong64_tgRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(map<string,vector<Long64_t> >) );
      instance.SetNew(&new_maplEstringcOvectorlELong64_tgRsPgR);
      instance.SetNewArray(&newArray_maplEstringcOvectorlELong64_tgRsPgR);
      instance.SetDelete(&delete_maplEstringcOvectorlELong64_tgRsPgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOvectorlELong64_tgRsPgR);
      instance.SetDestructor(&destruct_maplEstringcOvectorlELong64_tgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,vector<Long64_t> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<string,vector<Long64_t> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOvectorlELong64_tgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,vector<Long64_t> >*)0x0)->GetClass();
      maplEstringcOvectorlELong64_tgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOvectorlELong64_tgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOvectorlELong64_tgRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,vector<Long64_t> > : new map<string,vector<Long64_t> >;
   }
   static void *newArray_maplEstringcOvectorlELong64_tgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,vector<Long64_t> >[nElements] : new map<string,vector<Long64_t> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOvectorlELong64_tgRsPgR(void *p) {
      delete ((map<string,vector<Long64_t> >*)p);
   }
   static void deleteArray_maplEstringcOvectorlELong64_tgRsPgR(void *p) {
      delete [] ((map<string,vector<Long64_t> >*)p);
   }
   static void destruct_maplEstringcOvectorlELong64_tgRsPgR(void *p) {
      typedef map<string,vector<Long64_t> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,vector<Long64_t> >

namespace ROOT {
   static TClass *maplEstringcOstringgR_Dictionary();
   static void maplEstringcOstringgR_TClassManip(TClass*);
   static void *new_maplEstringcOstringgR(void *p = 0);
   static void *newArray_maplEstringcOstringgR(Long_t size, void *p);
   static void delete_maplEstringcOstringgR(void *p);
   static void deleteArray_maplEstringcOstringgR(void *p);
   static void destruct_maplEstringcOstringgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,string>*)
   {
      map<string,string> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,string>));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,string>", -2, "map", 100,
                  typeid(map<string,string>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOstringgR_Dictionary, isa_proxy, 0,
                  sizeof(map<string,string>) );
      instance.SetNew(&new_maplEstringcOstringgR);
      instance.SetNewArray(&newArray_maplEstringcOstringgR);
      instance.SetDelete(&delete_maplEstringcOstringgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOstringgR);
      instance.SetDestructor(&destruct_maplEstringcOstringgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,string> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<string,string>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOstringgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,string>*)0x0)->GetClass();
      maplEstringcOstringgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOstringgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOstringgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,string> : new map<string,string>;
   }
   static void *newArray_maplEstringcOstringgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,string>[nElements] : new map<string,string>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOstringgR(void *p) {
      delete ((map<string,string>*)p);
   }
   static void deleteArray_maplEstringcOstringgR(void *p) {
      delete [] ((map<string,string>*)p);
   }
   static void destruct_maplEstringcOstringgR(void *p) {
      typedef map<string,string> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,string>

namespace ROOT {
   static TClass *maplEstringcOpairlEdoublecOdoublegRsPgR_Dictionary();
   static void maplEstringcOpairlEdoublecOdoublegRsPgR_TClassManip(TClass*);
   static void *new_maplEstringcOpairlEdoublecOdoublegRsPgR(void *p = 0);
   static void *newArray_maplEstringcOpairlEdoublecOdoublegRsPgR(Long_t size, void *p);
   static void delete_maplEstringcOpairlEdoublecOdoublegRsPgR(void *p);
   static void deleteArray_maplEstringcOpairlEdoublecOdoublegRsPgR(void *p);
   static void destruct_maplEstringcOpairlEdoublecOdoublegRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,pair<double,double> >*)
   {
      map<string,pair<double,double> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,pair<double,double> >));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,pair<double,double> >", -2, "map", 100,
                  typeid(map<string,pair<double,double> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOpairlEdoublecOdoublegRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(map<string,pair<double,double> >) );
      instance.SetNew(&new_maplEstringcOpairlEdoublecOdoublegRsPgR);
      instance.SetNewArray(&newArray_maplEstringcOpairlEdoublecOdoublegRsPgR);
      instance.SetDelete(&delete_maplEstringcOpairlEdoublecOdoublegRsPgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOpairlEdoublecOdoublegRsPgR);
      instance.SetDestructor(&destruct_maplEstringcOpairlEdoublecOdoublegRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,pair<double,double> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<string,pair<double,double> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOpairlEdoublecOdoublegRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,pair<double,double> >*)0x0)->GetClass();
      maplEstringcOpairlEdoublecOdoublegRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOpairlEdoublecOdoublegRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOpairlEdoublecOdoublegRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,pair<double,double> > : new map<string,pair<double,double> >;
   }
   static void *newArray_maplEstringcOpairlEdoublecOdoublegRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,pair<double,double> >[nElements] : new map<string,pair<double,double> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOpairlEdoublecOdoublegRsPgR(void *p) {
      delete ((map<string,pair<double,double> >*)p);
   }
   static void deleteArray_maplEstringcOpairlEdoublecOdoublegRsPgR(void *p) {
      delete [] ((map<string,pair<double,double> >*)p);
   }
   static void destruct_maplEstringcOpairlEdoublecOdoublegRsPgR(void *p) {
      typedef map<string,pair<double,double> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,pair<double,double> >

namespace ROOT {
   static TClass *maplEstringcOintgR_Dictionary();
   static void maplEstringcOintgR_TClassManip(TClass*);
   static void *new_maplEstringcOintgR(void *p = 0);
   static void *newArray_maplEstringcOintgR(Long_t size, void *p);
   static void delete_maplEstringcOintgR(void *p);
   static void deleteArray_maplEstringcOintgR(void *p);
   static void destruct_maplEstringcOintgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,int>*)
   {
      map<string,int> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,int>));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,int>", -2, "map", 100,
                  typeid(map<string,int>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOintgR_Dictionary, isa_proxy, 0,
                  sizeof(map<string,int>) );
      instance.SetNew(&new_maplEstringcOintgR);
      instance.SetNewArray(&newArray_maplEstringcOintgR);
      instance.SetDelete(&delete_maplEstringcOintgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOintgR);
      instance.SetDestructor(&destruct_maplEstringcOintgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,int> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<string,int>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOintgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,int>*)0x0)->GetClass();
      maplEstringcOintgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOintgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOintgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,int> : new map<string,int>;
   }
   static void *newArray_maplEstringcOintgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,int>[nElements] : new map<string,int>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOintgR(void *p) {
      delete ((map<string,int>*)p);
   }
   static void deleteArray_maplEstringcOintgR(void *p) {
      delete [] ((map<string,int>*)p);
   }
   static void destruct_maplEstringcOintgR(void *p) {
      typedef map<string,int> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,int>

namespace ROOT {
   static TClass *maplEstringcOdoublegR_Dictionary();
   static void maplEstringcOdoublegR_TClassManip(TClass*);
   static void *new_maplEstringcOdoublegR(void *p = 0);
   static void *newArray_maplEstringcOdoublegR(Long_t size, void *p);
   static void delete_maplEstringcOdoublegR(void *p);
   static void deleteArray_maplEstringcOdoublegR(void *p);
   static void destruct_maplEstringcOdoublegR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,double>*)
   {
      map<string,double> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,double>));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,double>", -2, "map", 100,
                  typeid(map<string,double>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOdoublegR_Dictionary, isa_proxy, 0,
                  sizeof(map<string,double>) );
      instance.SetNew(&new_maplEstringcOdoublegR);
      instance.SetNewArray(&newArray_maplEstringcOdoublegR);
      instance.SetDelete(&delete_maplEstringcOdoublegR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOdoublegR);
      instance.SetDestructor(&destruct_maplEstringcOdoublegR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,double> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<string,double>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOdoublegR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,double>*)0x0)->GetClass();
      maplEstringcOdoublegR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOdoublegR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOdoublegR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,double> : new map<string,double>;
   }
   static void *newArray_maplEstringcOdoublegR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,double>[nElements] : new map<string,double>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOdoublegR(void *p) {
      delete ((map<string,double>*)p);
   }
   static void deleteArray_maplEstringcOdoublegR(void *p) {
      delete [] ((map<string,double>*)p);
   }
   static void destruct_maplEstringcOdoublegR(void *p) {
      typedef map<string,double> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,double>

namespace ROOT {
   static TClass *maplEintcOvectorlEmyParticlegRsPgR_Dictionary();
   static void maplEintcOvectorlEmyParticlegRsPgR_TClassManip(TClass*);
   static void *new_maplEintcOvectorlEmyParticlegRsPgR(void *p = 0);
   static void *newArray_maplEintcOvectorlEmyParticlegRsPgR(Long_t size, void *p);
   static void delete_maplEintcOvectorlEmyParticlegRsPgR(void *p);
   static void deleteArray_maplEintcOvectorlEmyParticlegRsPgR(void *p);
   static void destruct_maplEintcOvectorlEmyParticlegRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<int,vector<myParticle> >*)
   {
      map<int,vector<myParticle> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<int,vector<myParticle> >));
      static ::ROOT::TGenericClassInfo 
         instance("map<int,vector<myParticle> >", -2, "map", 100,
                  typeid(map<int,vector<myParticle> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEintcOvectorlEmyParticlegRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(map<int,vector<myParticle> >) );
      instance.SetNew(&new_maplEintcOvectorlEmyParticlegRsPgR);
      instance.SetNewArray(&newArray_maplEintcOvectorlEmyParticlegRsPgR);
      instance.SetDelete(&delete_maplEintcOvectorlEmyParticlegRsPgR);
      instance.SetDeleteArray(&deleteArray_maplEintcOvectorlEmyParticlegRsPgR);
      instance.SetDestructor(&destruct_maplEintcOvectorlEmyParticlegRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<int,vector<myParticle> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<int,vector<myParticle> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEintcOvectorlEmyParticlegRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<int,vector<myParticle> >*)0x0)->GetClass();
      maplEintcOvectorlEmyParticlegRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEintcOvectorlEmyParticlegRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEintcOvectorlEmyParticlegRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<int,vector<myParticle> > : new map<int,vector<myParticle> >;
   }
   static void *newArray_maplEintcOvectorlEmyParticlegRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<int,vector<myParticle> >[nElements] : new map<int,vector<myParticle> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEintcOvectorlEmyParticlegRsPgR(void *p) {
      delete ((map<int,vector<myParticle> >*)p);
   }
   static void deleteArray_maplEintcOvectorlEmyParticlegRsPgR(void *p) {
      delete [] ((map<int,vector<myParticle> >*)p);
   }
   static void destruct_maplEintcOvectorlEmyParticlegRsPgR(void *p) {
      typedef map<int,vector<myParticle> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<int,vector<myParticle> >

namespace ROOT {
   static TClass *maplEintcOvectorlEVertexgRsPgR_Dictionary();
   static void maplEintcOvectorlEVertexgRsPgR_TClassManip(TClass*);
   static void *new_maplEintcOvectorlEVertexgRsPgR(void *p = 0);
   static void *newArray_maplEintcOvectorlEVertexgRsPgR(Long_t size, void *p);
   static void delete_maplEintcOvectorlEVertexgRsPgR(void *p);
   static void deleteArray_maplEintcOvectorlEVertexgRsPgR(void *p);
   static void destruct_maplEintcOvectorlEVertexgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<int,vector<Vertex> >*)
   {
      map<int,vector<Vertex> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<int,vector<Vertex> >));
      static ::ROOT::TGenericClassInfo 
         instance("map<int,vector<Vertex> >", -2, "map", 100,
                  typeid(map<int,vector<Vertex> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEintcOvectorlEVertexgRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(map<int,vector<Vertex> >) );
      instance.SetNew(&new_maplEintcOvectorlEVertexgRsPgR);
      instance.SetNewArray(&newArray_maplEintcOvectorlEVertexgRsPgR);
      instance.SetDelete(&delete_maplEintcOvectorlEVertexgRsPgR);
      instance.SetDeleteArray(&deleteArray_maplEintcOvectorlEVertexgRsPgR);
      instance.SetDestructor(&destruct_maplEintcOvectorlEVertexgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<int,vector<Vertex> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<int,vector<Vertex> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEintcOvectorlEVertexgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<int,vector<Vertex> >*)0x0)->GetClass();
      maplEintcOvectorlEVertexgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEintcOvectorlEVertexgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEintcOvectorlEVertexgRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<int,vector<Vertex> > : new map<int,vector<Vertex> >;
   }
   static void *newArray_maplEintcOvectorlEVertexgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<int,vector<Vertex> >[nElements] : new map<int,vector<Vertex> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEintcOvectorlEVertexgRsPgR(void *p) {
      delete ((map<int,vector<Vertex> >*)p);
   }
   static void deleteArray_maplEintcOvectorlEVertexgRsPgR(void *p) {
      delete [] ((map<int,vector<Vertex> >*)p);
   }
   static void destruct_maplEintcOvectorlEVertexgRsPgR(void *p) {
      typedef map<int,vector<Vertex> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<int,vector<Vertex> >

namespace ROOT {
   static TClass *maplEintcOintgR_Dictionary();
   static void maplEintcOintgR_TClassManip(TClass*);
   static void *new_maplEintcOintgR(void *p = 0);
   static void *newArray_maplEintcOintgR(Long_t size, void *p);
   static void delete_maplEintcOintgR(void *p);
   static void deleteArray_maplEintcOintgR(void *p);
   static void destruct_maplEintcOintgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<int,int>*)
   {
      map<int,int> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<int,int>));
      static ::ROOT::TGenericClassInfo 
         instance("map<int,int>", -2, "map", 100,
                  typeid(map<int,int>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEintcOintgR_Dictionary, isa_proxy, 0,
                  sizeof(map<int,int>) );
      instance.SetNew(&new_maplEintcOintgR);
      instance.SetNewArray(&newArray_maplEintcOintgR);
      instance.SetDelete(&delete_maplEintcOintgR);
      instance.SetDeleteArray(&deleteArray_maplEintcOintgR);
      instance.SetDestructor(&destruct_maplEintcOintgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<int,int> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<int,int>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEintcOintgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<int,int>*)0x0)->GetClass();
      maplEintcOintgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEintcOintgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEintcOintgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<int,int> : new map<int,int>;
   }
   static void *newArray_maplEintcOintgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<int,int>[nElements] : new map<int,int>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEintcOintgR(void *p) {
      delete ((map<int,int>*)p);
   }
   static void deleteArray_maplEintcOintgR(void *p) {
      delete [] ((map<int,int>*)p);
   }
   static void destruct_maplEintcOintgR(void *p) {
      typedef map<int,int> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<int,int>

namespace ROOT {
   static TClass *maplELong64_tcOintgR_Dictionary();
   static void maplELong64_tcOintgR_TClassManip(TClass*);
   static void *new_maplELong64_tcOintgR(void *p = 0);
   static void *newArray_maplELong64_tcOintgR(Long_t size, void *p);
   static void delete_maplELong64_tcOintgR(void *p);
   static void deleteArray_maplELong64_tcOintgR(void *p);
   static void destruct_maplELong64_tcOintgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<Long64_t,int>*)
   {
      map<Long64_t,int> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<Long64_t,int>));
      static ::ROOT::TGenericClassInfo 
         instance("map<Long64_t,int>", -2, "map", 100,
                  typeid(map<Long64_t,int>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplELong64_tcOintgR_Dictionary, isa_proxy, 0,
                  sizeof(map<Long64_t,int>) );
      instance.SetNew(&new_maplELong64_tcOintgR);
      instance.SetNewArray(&newArray_maplELong64_tcOintgR);
      instance.SetDelete(&delete_maplELong64_tcOintgR);
      instance.SetDeleteArray(&deleteArray_maplELong64_tcOintgR);
      instance.SetDestructor(&destruct_maplELong64_tcOintgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<Long64_t,int> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<Long64_t,int>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplELong64_tcOintgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<Long64_t,int>*)0x0)->GetClass();
      maplELong64_tcOintgR_TClassManip(theClass);
   return theClass;
   }

   static void maplELong64_tcOintgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplELong64_tcOintgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<Long64_t,int> : new map<Long64_t,int>;
   }
   static void *newArray_maplELong64_tcOintgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<Long64_t,int>[nElements] : new map<Long64_t,int>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplELong64_tcOintgR(void *p) {
      delete ((map<Long64_t,int>*)p);
   }
   static void deleteArray_maplELong64_tcOintgR(void *p) {
      delete [] ((map<Long64_t,int>*)p);
   }
   static void destruct_maplELong64_tcOintgR(void *p) {
      typedef map<Long64_t,int> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<Long64_t,int>

namespace ROOT {
   static TClass *maplELong64_tcOPhContribgR_Dictionary();
   static void maplELong64_tcOPhContribgR_TClassManip(TClass*);
   static void *new_maplELong64_tcOPhContribgR(void *p = 0);
   static void *newArray_maplELong64_tcOPhContribgR(Long_t size, void *p);
   static void delete_maplELong64_tcOPhContribgR(void *p);
   static void deleteArray_maplELong64_tcOPhContribgR(void *p);
   static void destruct_maplELong64_tcOPhContribgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<Long64_t,PhContrib>*)
   {
      map<Long64_t,PhContrib> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<Long64_t,PhContrib>));
      static ::ROOT::TGenericClassInfo 
         instance("map<Long64_t,PhContrib>", -2, "map", 100,
                  typeid(map<Long64_t,PhContrib>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplELong64_tcOPhContribgR_Dictionary, isa_proxy, 0,
                  sizeof(map<Long64_t,PhContrib>) );
      instance.SetNew(&new_maplELong64_tcOPhContribgR);
      instance.SetNewArray(&newArray_maplELong64_tcOPhContribgR);
      instance.SetDelete(&delete_maplELong64_tcOPhContribgR);
      instance.SetDeleteArray(&deleteArray_maplELong64_tcOPhContribgR);
      instance.SetDestructor(&destruct_maplELong64_tcOPhContribgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<Long64_t,PhContrib> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<Long64_t,PhContrib>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplELong64_tcOPhContribgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<Long64_t,PhContrib>*)0x0)->GetClass();
      maplELong64_tcOPhContribgR_TClassManip(theClass);
   return theClass;
   }

   static void maplELong64_tcOPhContribgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplELong64_tcOPhContribgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<Long64_t,PhContrib> : new map<Long64_t,PhContrib>;
   }
   static void *newArray_maplELong64_tcOPhContribgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<Long64_t,PhContrib>[nElements] : new map<Long64_t,PhContrib>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplELong64_tcOPhContribgR(void *p) {
      delete ((map<Long64_t,PhContrib>*)p);
   }
   static void deleteArray_maplELong64_tcOPhContribgR(void *p) {
      delete [] ((map<Long64_t,PhContrib>*)p);
   }
   static void destruct_maplELong64_tcOPhContribgR(void *p) {
      typedef map<Long64_t,PhContrib> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<Long64_t,PhContrib>

namespace {
  void TriggerDictionaryInitialization_libECAL_Impl() {
    static const char* headers[] = {
"myParticle.h",
"myTESevent.h",
"Vertex.h",
"CaloEvent.h",
"Photon.h",
"Cluster.h",
"sigShape.h",
"Cell.h",
"myTrack.h",
"MCtruth_TOOLS.h",
"myDaVinci.h",
"myProtoParticle.h",
"myTagManager.h",
0
    };
    static const char* includePaths[] = {
"inc",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/ROOT/6.14.04-820c6/x86_64-centos7-gcc8-opt/include",
"/home/LHCB-T3/dmanuzzi/newECAL/Reco/",
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "libECAL dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
class __attribute__((annotate("$clingAutoload$Vertex.h")))  __attribute__((annotate("$clingAutoload$myParticle.h")))  Vertex;
class __attribute__((annotate("$clingAutoload$myProtoParticle.h")))  __attribute__((annotate("$clingAutoload$myParticle.h")))  myProtoParticle;
struct __attribute__((annotate("$clingAutoload$sigShape.h")))  __attribute__((annotate("$clingAutoload$myParticle.h")))  EntryTimes;
class __attribute__((annotate("$clingAutoload$sigShape.h")))  __attribute__((annotate("$clingAutoload$myParticle.h")))  sigShape;
struct __attribute__((annotate("$clingAutoload$Cell.h")))  __attribute__((annotate("$clingAutoload$myParticle.h")))  PhContrib;
struct __attribute__((annotate("$clingAutoload$Cell.h")))  __attribute__((annotate("$clingAutoload$myParticle.h")))  Calibrations;
class __attribute__((annotate("$clingAutoload$Cell.h")))  __attribute__((annotate("$clingAutoload$myParticle.h")))  Cell;
class __attribute__((annotate("$clingAutoload$Cluster.h")))  __attribute__((annotate("$clingAutoload$myParticle.h")))  Cluster;
class __attribute__((annotate("$clingAutoload$myTrack.h")))  __attribute__((annotate("$clingAutoload$myParticle.h")))  myTrack;
class __attribute__((annotate("$clingAutoload$myParticle.h")))  myParticle;
class __attribute__((annotate("$clingAutoload$myTESevent.h")))  myTESevent;
struct __attribute__((annotate("$clingAutoload$CaloEvent.h")))  myTracks;
class __attribute__((annotate("$clingAutoload$CaloEvent.h")))  CaloEvent;
class __attribute__((annotate("$clingAutoload$Photon.h")))  Photon;
class __attribute__((annotate("$clingAutoload$MCtruth_TOOLS.h")))  MCtruth_TOOLS;
class __attribute__((annotate("$clingAutoload$myDaVinci.h")))  myDaVinci;
class __attribute__((annotate("$clingAutoload$myTagManager.h")))  myTagManager;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "libECAL dictionary payload"

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#include "myParticle.h"
#include "myTESevent.h"
#include "Vertex.h"
#include "CaloEvent.h"
#include "Photon.h"
#include "Cluster.h"
#include "sigShape.h"
#include "Cell.h"
#include "myTrack.h"
#include "MCtruth_TOOLS.h"
#include "myDaVinci.h"
#include "myProtoParticle.h"
#include "myTagManager.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"Calibrations", payloadCode, "@",
"CaloEvent", payloadCode, "@",
"Cell", payloadCode, "@",
"Cluster", payloadCode, "@",
"EntryTimes", payloadCode, "@",
"MCtruth_TOOLS", payloadCode, "@",
"PhContrib", payloadCode, "@",
"Photon", payloadCode, "@",
"Vertex", payloadCode, "@",
"myDaVinci", payloadCode, "@",
"myParticle", payloadCode, "@",
"myProtoParticle", payloadCode, "@",
"myTESevent", payloadCode, "@",
"myTagManager", payloadCode, "@",
"myTrack", payloadCode, "@",
"myTracks", payloadCode, "@",
"sigShape", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("libECAL",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_libECAL_Impl, {}, classesHeaders, /*has no C++ module*/false);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_libECAL_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_libECAL() {
  TriggerDictionaryInitialization_libECAL_Impl();
}
