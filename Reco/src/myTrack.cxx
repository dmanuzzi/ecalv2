#ifndef ROOT_myTrack_cxx
#define ROOT_myTrack_cxx

#include <myTrack.h>

ClassImp(myTrack)

void myTrack::initialise(){
	m_size = 0;
	m_id   = 0;
};

void myTrack::setSize(UInt_t size){
	m_size = size;
	m_x.reserve(m_size);
	m_y.reserve(m_size);
	m_z.reserve(m_size);
	m_t.reserve(m_size);
	m_px.reserve(m_size);
	m_py.reserve(m_size);
	m_pz.reserve(m_size);
};
	

myTrack::myTrack(){
	this->initialise();
}

myTrack::myTrack(UInt_t size){
	m_id = 0;
	this->setSize(size);
}

#endif
