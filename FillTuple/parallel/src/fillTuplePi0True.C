#include <iostream>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <TChain.h>
#include <TFile.h>
#include <TTree.h>
#include <CaloEvent.h>
#include <Photon.h>
#include <Cell.h>
#include <myParticle.h>
#include <TROOT.h>
#include <TSystem.h>
#include <TProcessID.h>
#include <TLeaf.h>
#include <TF2.h>
#include <sigShape.h>
#include <myDaVinci.h>
#include <MCtruth_TOOLS.h>
using namespace std;

Int_t main(Int_t argc, Char_t *argv[]){
    TString nfin = argv[1];
    TString nfout= argv[2];
    cout << nfin << endl;
    cout << nfout << endl;

	TFile fin(nfin, "READ");
	TTree *tin = (TTree*)fin.Get("ECALevt");
    tin->Print();
	myDaVinci tupler;
    
    function<vector<myParticle>* (CaloEvent*)> getMotherContainer = [](CaloEvent * event){return event->getContainer("pi0"); };

    function<vector<myParticle>(myParticle*)> getParts = [](myParticle *pi0){
        vector<myParticle> ret;
        vector<myProtoParticle*> gammas = pi0->get_daughters();
        if (gammas.size() <  1) {
            cout << "no daughters present\n"; 
            cout << pi0->getKey() << "   " << pi0->getID() << "   " << pi0->getMkey() << "   " << pi0->getMid() << endl;
            return ret;
        }
        //if (gammas.size() == 1) return ret;// for pi0 -> e+e-gamma
        if (gammas.size() != 2){
          //cout << "getPartRef: We have a problem: too many daughters for this pi0\n";
          //for_each(gammas.begin(), gammas.end(), [](myProtoParticle* protoGamma){
          //         cout << protoGamma->getID() << "   " << protoGamma->getMkey() << "  " << protoGamma->getMid() <<  endl;});
          return ret;  
        } 
        ret.push_back(*pi0);
        ret.push_back(*((myParticle*)gammas[0]));
        ret.push_back(*((myParticle*)gammas[1]));
        return ret;
    };
    function<void()> exceptions = [&tupler](){
        // cout << "exceptions stars\n";
        // cout.flush();
        vector<myParticle>* parts = tupler.getParts();
        myParticle* pi0    = &((*parts)[0]);
        myParticle* gamma1 = &((*parts)[1]);
        myParticle* gamma2 = &((*parts)[2]);
        CaloEvent *event = tupler.getEvent();
        pi0->setMatchedPart(0);
        gamma1->setMatchedPart(0);
        gamma2->setMatchedPart(0);
        
        myParticle* gamma1_RecoAble = MCtruth_TOOLS::isGammaRecontructible2(gamma1);
        myParticle* gamma2_RecoAble = MCtruth_TOOLS::isGammaRecontructible2(gamma2);
        Bool_t pi0_isRecoAble = (gamma1_RecoAble!=0) && (gamma2_RecoAble!=0);
        pi0->setInCaloAcc(pi0_isRecoAble);
        pi0->setHitCaloSensDet(pi0_isRecoAble);
        pi0->setMatchedPart(0);
        gamma1->setMatchedPart(gamma1_RecoAble);
        gamma2->setMatchedPart(gamma2_RecoAble);
        pi0->setPi0Type(0);
        if (!pi0_isRecoAble) return;
        pi0->setPi0Type(MCtruth_TOOLS::getPi0Type2(gamma1_RecoAble,gamma2_RecoAble, event));
        
        vector<myParticle>* pi0s_rec = event->getContainerREC("pi0_rec");
        for (auto& pi0_rec : *pi0s_rec){
            auto parts_true = MCtruth_TOOLS::pi0rec_associateHitParticles2(&pi0_rec); 
            if (parts_true.size()<3) continue;
            if (parts_true[2]==0) continue;
            if (parts_true[2]->getKey()!=pi0->getKey()) continue;
            pi0->setMatchedPart(&pi0_rec);   
            Cluster* clust1 = pi0_rec.getClusters()[0];
            Cluster* clust2 = pi0_rec.getClusters()[1];
            myParticle* g1    = (myParticle*)(clust1->getRecParticle());
            myParticle* g2    = (myParticle*)(clust2->getRecParticle());
            Double_t dist1 = pow(g1->getCaloVertexX()-gamma1_RecoAble->getCaloVertexX(),2)+
                             pow(g1->getCaloVertexY()-gamma1_RecoAble->getCaloVertexY(),2);
            Double_t dist2 = pow(g2->getCaloVertexX()-gamma1_RecoAble->getCaloVertexX(),2)+
                             pow(g2->getCaloVertexY()-gamma1_RecoAble->getCaloVertexY(),2);
            if (dist1<dist2){
                gamma1->setMatchedPart(g1);
                gamma2->setMatchedPart(g2);
            } else {
                gamma1->setMatchedPart(g2);
                gamma2->setMatchedPart(g1);
            }
            break;
        }
        if (event->getEvt()==5 && abs(pi0->getOrigPz()- 3759.04)<0.01){
            cout << "stocazzo2\n";
        }
        
        /*
        Bool_t isRecoAble = MCtruth_TOOLS::isPi0Reconstructible(pi0, event);
        pi0->setInCaloAcc(isRecoAble);
        pi0->setHitCaloSensDet(isRecoAble);
        pi0->setPi0Type(MCtruth_TOOLS::getPi0Type(pi0, event));
        pi0->setMatchedPart(0);
        gamma1->setMatchedPart(0);
        gamma2->setMatchedPart(0);
        
        if (!isRecoAble) return;
        
        vector<myParticle*> descendants1, descendants2;
        descendants1.push_back(gamma1);
        descendants2.push_back(gamma2);
        gamma1->getDescendants(descendants1);
        gamma2->getDescendants(descendants2);
        vector<myParticle>* pi0s_rec = event->getContainerREC("pi0_rec");
        for (auto& pi0_rec : *pi0s_rec){
            Cluster* clust1 = pi0_rec.getClusters()[0];
            Cluster* clust2 = pi0_rec.getClusters()[1];
            myParticle* g1    = (myParticle*)(clust1->getRecParticle());
            myParticle* g2    = (myParticle*)(clust2->getRecParticle());
        
            auto part12 = MCtruth_TOOLS::pi0rec_associateHitParticles(&pi0_rec);
            myParticle* part1 = part12.first;
            myParticle* part2 = part12.second;
            if (part1==0 || part2==0) continue;
        
            Long64_t keyA = part1->getKey();
            Long64_t keyB = part2->getKey();
            auto it1A = find_if(descendants1.begin(),descendants1.end(),[keyA](myParticle* descendant){
                return descendant->getKey() == keyA;
            });
            auto it2B = find_if(descendants2.begin(),descendants2.end(),[keyB](myParticle* descendant){
                return descendant->getKey() == keyB;
            });
        
            if (it1A != descendants1.end()) gamma1->setMatchedPart(g1);
            if (it2B != descendants2.end()) gamma2->setMatchedPart(g2);
            if (it1A != descendants1.end() && it2B != descendants2.end()){
                pi0->setMatchedPart(&pi0_rec);    
                break;
            }
        
            auto it1B = find_if(descendants1.begin(),descendants1.end(),[keyB](myParticle* descendant){
                return descendant->getKey() == keyB;
            });
            auto it2A = find_if(descendants2.begin(),descendants2.end(),[keyA](myParticle* descendant){
                return descendant->getKey() == keyA;
            });
            if (it1B != descendants1.end()) gamma1->setMatchedPart(g2);
            if (it2A != descendants2.end()) gamma2->setMatchedPart(g1);
            if (it1B != descendants1.end() && it2A != descendants2.end()){
                pi0->setMatchedPart(&pi0_rec);
                break;
            }
            
            // Bool_t isGamma11 = part1->getKey() == gamma1->getKey();
            // Bool_t isGamma21 = part2->getKey() == gamma1->getKey();
            // Bool_t isGamma12 = part1->getKey() == gamma2->getKey();
            // Bool_t isGamma22 = part2->getKey() == gamma2->getKey();
            // Bool_t isElect11 = part1->getMkey() == gamma1->getKey();
            // Bool_t isElect21 = part2->getMkey() == gamma1->getKey();
            // Bool_t isElect12 = part1->getMkey() == gamma2->getKey();
            // Bool_t isElect22 = part2->getMkey() == gamma2->getKey();


            // if ((isGamma11 && isGamma22) || (isGamma21 && isGamma12) ||
            //     (isGamma11 && isElect22) || (isGamma21 && isElect12) ||
            //     (isGamma12 && isElect21) || (isGamma22 && isElect11) ||
            //     (isElect11 && isElect22) || (isElect21 && isElect12) ){
            //     pi0->setMatchedPart(&pi0_rec);
            //     break;
            // } 
        }
        */
    };

    /*
    function<void()> exceptions = [&tupler](){
        // cout << "exceptions stars\n";
        // vector<myParticle>* parts = tupler.getParts();
        myParticle* pi0    = &((*parts)[0]);
        myParticle* gamma1 = &((*parts)[1]);
        myParticle* gamma2 = &((*parts)[2]);
        pi0->setMatchedPart(0);
        pi0->setInCaloAcc(0);
        pi0->setHitCaloSensDet(0);
        gamma1->setMatchedPart(0);
        gamma2->setMatchedPart(0);
        CaloEvent *event = tupler.getEvent();
        vector<map<Double_t, Cluster*>> clusters(2, map<Double_t,Cluster*>());
        Int_t kkk=-1;
        Bool_t daugh_inacc[2] = {false,false};
        for (auto gamma : {gamma1, gamma2}){
            ++kkk;
            if (gamma->getHitCaloSensDet()==1 && gamma->getInCaloAcc()==1){
                daugh_inacc[kkk] = true;
                auto clusts_tmp = event->getClusterFromXY(gamma->getCaloVertexX(), gamma->getCaloVertexY());
                for (auto clust_tmp : clusts_tmp){
                    auto closPart = clust_tmp->getCloserParticle(50);
                    if (closPart==0) continue;
                    if (gamma->getKey() != closPart->getKey()) continue;
                    Double_t dist = pow(clust_tmp->getCalibX()-gamma->getCaloVertexX(),2) +
                                    pow(clust_tmp->getCalibY()-gamma->getCaloVertexY(),2) ;
                    clusters[kkk][dist] = clust_tmp;
                    break;
                }
            } else {
                auto daughters = gamma->get_daughters();
                if (daughters.size()!=2) continue;
                for (auto e : daughters){
                    if (((myParticle*)e)->getHitCaloSensDet()!=1) continue;
                    if (((myParticle*)e)->getInCaloAcc()     !=1) continue;
                    if (((myParticle*)e)->getTrackable()     ==1) continue;
                    daugh_inacc[kkk] = true;
                    auto clusts_tmp = event->getClusterFromXY(e->getCaloVertexX(), e->getCaloVertexY());
                    for (auto clust_tmp : clusts_tmp){
                        auto closPart = clust_tmp->getCloserParticle(50);
                        if (closPart==0) continue;
                        if (e->getKey() != closPart->getKey()) continue;
                        Double_t dist = pow(clust_tmp->getCalibX() - e->getCaloVertexX(),2) +
                                        pow(clust_tmp->getCalibY() - e->getCaloVertexY(),2) ;
                        clusters[kkk][dist] = clust_tmp;
                        break;
                    }   
                }
            }            
        } 
        if (!daugh_inacc[0] || !daugh_inacc[1]) return;
        pi0->setInCaloAcc(1);
        pi0->setHitCaloSensDet(1);
                  
        map<Double_t,Cluster*> clusters1 = clusters[0];
        map<Double_t,Cluster*> clusters2 = clusters[1];

        if (clusters1.size()==0 || clusters2.size()==0) return;
        
        vector<myParticle>* pi0s_rec = event->getContainerREC("pi0_rec");
        myParticle* pi0_rec=0;
        myParticle* gamma1_rec=0;            
        myParticle* gamma2_rec=0;            
        Double_t dist=99999999;
         
        for (auto& pi0_rec_tmp : *pi0s_rec){
            auto pi0_clusters= pi0_rec_tmp.getClusters();
            Cluster* cluster1_rec = pi0_clusters[0];
            Cluster* cluster2_rec = pi0_clusters[1];
            Int_t key1_rec = cluster1_rec->getID();
            Int_t key2_rec = cluster2_rec->getID();
            Int_t flag = 0;
            for (auto dist_clust1 : clusters1){
                Int_t key1 = dist_clust1.second->getID();
                // flag== 0: key1 != key1_rec && key1 != key2_rec
                // flag== 1: key1 == key1_rec
                // flag==-1: key1 == key2_rec
                flag = (key1 == key1_rec) - (key1 == key2_rec);
                if (flag == 0) continue;
                for (auto dist_clust2 : clusters2){
                    Int_t key2 = dist_clust2.second->getID();
                    if (key1 == key2) continue;
                    if (flag == 1 && key2 != key2_rec) continue;
                    if (flag ==-1 && key2 != key1_rec) continue;
                    if (dist < dist_clust1.first+dist_clust2.first) continue;
                    dist = dist_clust1.first+dist_clust2.first;
                    pi0_rec = &pi0_rec_tmp;
                    if (flag == 1){
                        gamma1_rec = (myParticle*)cluster1_rec->getRecParticle();
                        gamma2_rec = (myParticle*)cluster2_rec->getRecParticle();
                    } else {
                        gamma1_rec = (myParticle*)cluster2_rec->getRecParticle();
                        gamma2_rec = (myParticle*)cluster1_rec->getRecParticle();
                    }
                    flag = 2;
                    break;
                }
                if (flag==2)  break;
            }
        }
        pi0->setMatchedPart(pi0_rec);
        gamma1->setMatchedPart(gamma1_rec);
        gamma2->setMatchedPart(gamma2_rec); 
    };
    */
    vector<string> pi0Branches = {
                "M"     , "TRUE_M",
                "PX"    , "PY"    , "PZ"    , "E"     , "PT"    ,
                "TRUE_PX"    , "TRUE_PY"    , "TRUE_PZ"    , "TRUE_E"     , "TRUE_PT"    ,
                "OVX"   , "OVY"   , "OVZ"   , "OVT"   ,
                "ENDVX" , "ENDVY" , "ENDVZ" , "ENDVT" ,
                "isMatched","isSig", "ID", 
                "pi0Type","TRUE_pi0Type",
                "ID", "MC_MOTHER_ID", "MC_GD_MOTHER_ID", 
                "KEY", "MC_MOTHER_KEY", "MC_GD_MOTHER_KEY",
                "HitCaloSensDet", "INCALOACC",
            };
    vector<string> gammaBranches = {
                "M" ,
                "PX"    , "PY"    , "PZ"    , "E"     , "PT"    ,
                "CALOPX", "CALOPY", "CALOPZ", "CALOE" , "CALOPT",
                "OVX"   , "OVY"   , "OVZ"   , "OVT"   ,
                "CALOVX", "CALOVY", "CALOVZ", "CALOVT",
                "ENDVX" , "ENDVY" , "ENDVZ" , "ENDVT" ,
                "isMatched","isSig", 
                "ID","MC_MOTHER_ID", "MC_GD_MOTHER_ID",
                "INCALOACC", "ALIVEATCALOZ", "ISTRACKABLE", "HitCaloSensDet", "isRecoveredByVanya",
                "KEY", "MC_MOTHER_KEY", "MC_GD_MOTHER_KEY",
                "TRUE_CALOVX","TRUE_CALOVY","TRUE_CALOVZ",
                "TRUE_CALOPX","TRUE_CALOPY","TRUE_CALOPZ","TRUE_CALOPT",
                "TRUE_CALOE", 
                "TRUE_ID","TRUE_MC_MOTHER_ID", "TRUE_MC_GD_MOTHER_ID",
            };
    map<string, vector<string>> allBranches;
    allBranches["pi0"]= pi0Branches;
    allBranches["gamma1"]= gammaBranches;
    allBranches["gamma2"]= gammaBranches;
    tupler.initialize(tin, {"pi0", "gamma1", "gamma2"}, allBranches, getMotherContainer, getParts);
    tupler.setCheckMatch([&tupler](){
        auto parts = tupler.getParts();
        if (parts->at(0).getMatchedPart() == 0) return false;
        else return true;
    });
    tupler.setExceptions(exceptions);
    tupler.execute();
    tupler.finalize(nfout);
    fin.Close();
    return 1;
}
