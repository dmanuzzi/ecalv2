print 'ecco submitter.py'

firstRun    = 0
lastRun     = 1999
NrunsPerJob = 10
NfilePerJob = 5


params = [
	#  ('Bd3pi'       , 'D03' 	     , 'upgrade0-B-Rect3-efh-zzz-333-000000'          	, 'full', 'PERF'),
	# ('Bd3pi'		  , 'rD03'   	 , 'upgrade0-B-Rhom5-bdefh-wyzzz-33333-0000000000'	, 'full', 'PERF'),
    # ('Bd3pi'        , 'E03'	     , 'upgrade1-B-Rect3-efh-zzz-333-000000' 	        , 'full', 'PERF'),
    # ('Bd3pi'        , 'rE03'       , 'upgrade1-B-Rhom5-bdefh-wyzzz-33333-0000000000'	, 'full', 'PERF'), 
    # ('Bd3pi'        , 'rrE03'      , 'upgrade1-B-Rhom4-befh-wzzz-3333-00000000'	    , 'full', 'PERF'),
    # ('Bd3pi'        , 'rrrE03'     , 'upgrade1-B-RhomB5-bdefh-wyzzz-33333-0000000000'	, 'full', 'REC'),
    # ('Bd3pi'        , 'cccE03'     , 'upgrade1-B-CircB5-bdefh-wyzzz-33333-0000000000'	, 'full', 'PERF'),
	# ('Bd3pi'        , 'bbb03'	     , 'upgrade2-B-Rect3-aeg-xzz-333-000000'			, 'full', 'PERF'),
	# ('Bd3pi'        , 'rbb03'	     , 'upgrade2-B-Rhom5-ccefh-xxzzz-33333-0000000000'	, 'full', 'PERF'),
	# ('Bd3pi'        , 'E01'		 , 'upgrade1-B-Rect3-efh-zzz-111-000000' 	    	, 'full', 'PERF'),
	# ('Bd3pi'        , 'rE01'	     , 'upgrade1-B-Rhom5-bdefh-wyzzz-11111-0000000000'	, 'full', 'PERF'), 
    # ('Bd3pi'        , 'rrE01'      , 'upgrade1-B-Rhom4-befh-wzzz-1111-00000000'	    , 'full', 'PERF'),
	# ('Bd3pi'        , 'E05'		 , 'upgrade1-B-Rect3-efh-zzz-555-000000' 			, 'full', 'PERF'),
	# ('Bd3pi'        , 'rE05'	     , 'upgrade1-B-Rhom5-bdefh-wyzzz-55555-0000000000'	, 'full', 'PERF'), 
    # ('Bd3pi'        , 'rrE05'      , 'upgrade1-B-Rhom4-befh-wzzz-5555-00000000'	    , 'full', 'PERF'),
	# ('Bd3pi'        , 'E07'		 , 'upgrade1-B-Rect3-efh-zzz-777-000000'	 	    , 'full', 'PERF'),
	# ('Bd3pi'        , 'rE07'	     , 'upgrade1-B-Rhom5-bdefh-wyzzz-77777-0000000000'	, 'full', 'PERF'), 
    # ('Bd3pi'        , 'rrE07'      , 'upgrade1-B-Rhom4-befh-wzzz-7777-00000000'	    , 'full', 'PERF'),
	
	# ('Bd3pi'        , 'b03'		 , 'upgrade2-B-Rect3-ceg-xzz-333-000000'			, 'full', 'PERF'),
	# ('Bd3pi'        , 'bbb01'	 , 'upgrade2-B-Rect3-aeg-xzz-111-000000'				, 'full', 'PERF'),
	#  ('Bd3pi'        , 'rb03'		 , 'upgrade2-B-Rhom5-bdefh-wyzzz-33333-0000000000'	, 'full', 'PERF'),
	# ('Bd3pi'        , 'rrb03'		 , 'upgrade2-B-Rhom4-befh-wzzz-3333-00000000'		, 'full', 'PERF'),
	#  ('Bd3pi'        , 'rrrb03'     , 'upgrade2-B-RhomB5-bdefh-wyzzz-33333-0000000000'	, 'full', 'REC'),
    #  ('Bd3pi'        , 'cccb03'     , 'upgrade2-B-CircB5-bdefh-wyzzz-33333-0000000000'	, 'full', 'REC'),
	
	# ('Bd3pi'        , 'rbbb03'	 , 'upgrade2-B-Rhom5-bbdff-wwyzz-33333-0000000000'	, 'full', 'PERF'),
	# ('Bd3pi'        , 'rbb03'		 , 'upgrade2-B-Rhom5-ccefh-xxzzz-33333-0000000000'	, 'full', 'PERF'), 
	# ('Bd3pi'        , 'nE03'	     , 'upgrade1-B-Rect4-befh-wzzz-3333-00000000'		, 'full', 'REC'),
    # ('Bd3pi'        , 'EE03'		 , 'upgrade1-B-Rect3-cfh-xzz-333-000000'		    , 'full', 'PERF'),
    # ('Bd3pi'        , 'EE03'		 , 'upgrade1-B-Rect3-cfh-xzz-333-000000'		    , 'full', 'PERF'),
    # ('Bd3pi'        , 'nEE03'      , 'upgrade1-B-Rect4-ccfh-wzzz-3333-00000000'		, 'full', 'PERF'),

    #('E04'		, 'upgrade1-B-Rect3-efh-zzz-444-000000'				, 'full', 'PERF'),
	#('E05'		, 'upgrade1-B-Rect3-efh-zzz-555-000000'				, 'full', 'PERF'),
	#('E06'		, 'upgrade1-B-Rect3-efh-zzz-666-000000'				, 'full', 'PERF'),
	#('E07'		, 'upgrade1-B-Rect3-efh-zzz-777-000000'				, 'full', 'PERF'),
	
	#('rE00'		, 'upgrade1-B-Rhom5-bdefh-wyzzz-33333-0000000000'	, 'full'	, 'PERF'),
	#('rE01'		, 'upgrade1-B-Rhom5-bdefh-wyzzz-11111-0000000000'	, 'full'	, 'PERF'),
	#('rE02'		, 'upgrade1-B-Rhom5-bdefh-wyzzz-22222-0000000000'	, 'full'	, 'PERF'),
	#('rE04'		, 'upgrade1-B-Rhom5-bdefh-wyzzz-44444-0000000000'	, 'full'	, 'PERF'),
	#('rE05'		, 'upgrade1-B-Rhom5-bdefh-wyzzz-55555-0000000000'	, 'full'	, 'PERF'),
	#('rE06'		, 'upgrade1-B-Rhom5-bdefh-wyzzz-66666-0000000000'	, 'full'	, 'PERF'),
	#('rE07'		, 'upgrade1-B-Rhom5-bdefh-wyzzz-77777-0000000000'	, 'full'	, 'PERF'),
	
	#('bbb01'	, 'upgrade2-B-Rect3-aeg-xzz-111-000000'				, 'full'	, 'PERF'),

	#('Bd3pi'         , 'rb03'	, 'upgrade2-B-Rhom5-bdefh-wyzzz-33333-0000000000'	, 'full'	, 'pi0rec_pi0true'),
	#('Bd2Kstee', 'D03'    	, 'upgrade0-B-Rect3-efh-zzz-333-000000'          	, 'full'	, 'TRUE'),
	#('Bd2Kstee', 'D03sig'    	, 'upgrade0-B-Rect3-efh-zzz-333-000000-OnlySig' , 'full'	, 'TRUE'),
	#('Bd2Kstee', 'E03'    	, 'upgrade1-B-Rect3-efh-zzz-333-000000'          	, 'full'	, 'TRUE'),
	#('Bd2Kstee', 'rE03'    	, 'upgrade1-B-Rhom5-bdefh-wyzzz-33333-0000000000'  	, 'full'	, 'TRUE'),
	#('Bd2Kstee', 'rrE03'   	, 'upgrade1-B-Rhom4-befh-wzzz-3333-00000000'       	, 'full'	, 'TRUE'),
	#('Bd2Kstee', 'rrrE03'  	, 'upgrade1-B-RhomB5-bdefh-wyzzz-33333-0000000000'     	, 'full'	, 'TRUE'),
	#('Bd2Kstee', 'rb03'  	, 'upgrade2-B-Rhom5-bdefh-wyzzz-33333-0000000000'     	, 'full'	, 'TRUE'),
	#('Bd2Kstee', 'rbb03'  	, 'upgrade2-B-Rhom5-ccefh-xxzzz-33333-0000000000'     	, 'full'	, 'TRUE'), #2,2,4,6,12 cm
	#('Bd2Kstee', 'rbbb03'  	, 'upgrade2-B-Rhom5-bbdff-wwyzz-33333-0000000000'     	, 'full'	, 'TRUE'), #1.5,1.5,3,6,6  cm

	('Bd2Kstee', 'rrrE03sig', 'upgrade1-B-RhomB5-bdefh-wyzzz-33333-0000000000-OnlySig', 'full'	, 'TRUE'),
        ('Bd2Kstee', 'rb03sig' 	, 'upgrade2-B-Rhom5-bdefh-wyzzz-33333-0000000000-OnlySig', 'full'	, 'TRUE'),
	('Bd2Kstee', 'rbb03sig'	, 'upgrade2-B-Rhom5-ccefh-xxzzz-33333-0000000000-OnlySig', 'full'	, 'TRUE'), #2,2,4,6,12 cm
	('Bd2Kstee', 'rbbb03sug', 'upgrade2-B-Rhom5-bbdff-wwyzz-33333-0000000000-OnlySig', 'full'	, 'TRUE'), #1.5,1.5,3,6,6  cm
	
]
#$rrrE03sig $rb03sig $rbb03sig $rbbb03sig
#####################################
#####################################
#####################################

import os
import sys
os.system('rm -f ./condor_log/log/*.log')
os.system('rm -f ./submit/jobs1.txt')
os.system('rm -f ./submit/jobs2.txt')
os.system('rm -f ./submit/jobs3.txt')

fjobs1 = open('./submit/jobs1.txt', 'w')
for decay,tag,TAG,simtype,opt in params:
	nu=0
	if 'upgrade0' in TAG:
		nu='upgrade0'
	elif 'upgrade1' in TAG:
		nu='upgrade1'
	elif 'upgrade2' in TAG:
		nu='upgrade2'
	#os.system('rm -f ./condor_log/out/merge1.{tag}.{simtype}*.out'.format(tag=tag, simtype=simtype))
	#os.system('rm -f ./condor_log/err/merge1.{tag}.{simtype}*.err'.format(tag=tag, simtype=simtype))
	i=firstRun
	counter=0
	while  i <= lastRun :
	    min=i
	    max=i+NrunsPerJob-1
	    if max >= lastRun: max=lastRun 
	    fjobs1.write('%s,%s,%s,%s,%d,%d,%d,%s,%s\n'%(tag, simtype, nu, TAG, min, max, counter, opt, decay))
	    i=i+NrunsPerJob
	    counter=counter+1
fjobs1.close()

os.system('condor_submit ./submit/submit1.jdl')
os.system('condor_wait ./condor_log/log/merge1.log')

fjobs2 = open('./submit/jobs2.txt', 'w')
counter-=1
for decay,tag,TAG,simtype,opt in params:
	j=0
	counter2=0
	
	while j <= counter:
		min=j
		max=j+NfilePerJob-1
		if max >= counter: max=counter
		fjobs2.write('%s,%s,%d,%d,%d,%s,%s\n'%(tag, simtype, min, max, counter2, opt, decay))
		j=j+NfilePerJob
		counter2=counter2+1	 
fjobs2.close()

os.system('condor_submit ./submit/submit2.jdl')
os.system('condor_wait ./condor_log/log/merge2.log')

fjobs3 = open('./submit/jobs3.txt', 'w')
for decay,tag,TAG,simtype,opt in params:
	fjobs3.write('%s,%s,%s,%s\n'%(tag, simtype, opt, decay))
fjobs3.close()
os.system('condor_submit ./submit/submit3.jdl')



	
