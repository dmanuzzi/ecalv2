#!/bin/bash

cd ./src
touch doAllPlots_parallel.C 
make doAllPlots_parallel
make finalizeSmearing
make finalizeResolution
make finalizePi0
make finalizeBd3pi
cd ..

a0=upgrade2-Correct2Seed-l_20.2_40.4_80.8-Rm_15_35_35
a1=upgrade2-Correct2Seed-l_20.2_40.4_80.8-Rm_15_35_35-TresFactor_0.5
a2=upgrade2-Correct2Seed-l_20.2_40.4_80.8-Rm_15_35_35-TresFactor_0.2
b0=upgrade2-noSecondSeed-l_20.2_40.4_80.8-Rm_15_35_35
b1=upgrade2-noSecondSeed-l_20.2_40.4_80.8-Rm_15_35_35-TresFactor_0.5
b2=upgrade2-noSecondSeed-l_20.2_40.4_80.8-Rm_15_35_35-TresFactor_0.2
c0=upgrade0-noSecondSeed-l_20.2_40.4_80.8-Rm_15_35_35
firstJob=0
lastJob=299
rm ./jobs.txt
for tag in $c0; do
    for i in $(seq $firstJob $lastJob); do
		echo $i, $tag >> jobs.txt
    done;
done

rm condor_log/*/*
condor_submit submitPlots.jdl
condor_wait ./condor_log/log/doAllPlots_parallel.log

for tag in $c0; do
    echo $firstJob $lastJob $tag
	python ./src/mergeAllPlots.py $firstJob $lastJob $tag
	./exe/finalizeSmearing $tag
	./exe/finalizeResolution $tag
	./exe/finalizePi0 $tag
	./exe/finalizeBd3pi $tag
done

