#!/bin/bash


pathInput='.'
pathOutput='.'


$ProjectDir/FillTuple/basic/fillTuple             $pathInput/tupleStand.root $pathOutput/fillTuple.root 20


$ProjectDir/FillTuple/parallel/fillTuplePi0       $pathOutput/fillTuple.root $pathOutput/outTuplePi0.root
$ProjectDir/FillTuple/parallel/fillTupleBd3pi     $pathOutput/fillTuple.root $pathOutput/outTupleBd3pi.root
$ProjectDir/FillTuple/parallel/fillTuplePi0True   $pathOutput/fillTuple.root $pathOutput/outTuplePi0True.root
$ProjectDir/FillTuple/parallel/fillTupleBd3piTrue $pathOutput/fillTuple.root $pathOutput/outTupleBd3piTrue.root
$ProjectDir/FillTuple/parallel/fillTupleGamma     $pathOutput/fillTuple.root $pathOutput/outTupleGamma.root
