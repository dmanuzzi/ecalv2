#ifndef Cell_cxx
#define Cell_cxx

#include <TGraphErrors.h>
#include <Cell.h>

ClassImp(Cell)
ClassImp(myProtoParticle)
Calibrations::Calibrations(vector<pair<string,vector<string>>> inputs){
  for (auto input : inputs){
    TFile fin(input.first.data(),"READ");
    Xcalib.push_back(*((TProfile*)fin.Get(input.second[0].data())));
    Xcalib.back().Print();
    Ycalib.push_back(*((TProfile*)fin.Get(input.second[1].data())));
    Ycalib.back().Print();
    Ecalib.push_back(((TH1D*)fin.Get(input.second[2].data()))->GetMean());
    cout << Ecalib.back() << endl;     
    fin.Close();
  }
}
Calibrations::Calibrations(TString nfin, std::map<TString, TString>setup){
  TFile fin(nfin,"READ");
  //Float_t lvl=0;
  for (auto p : setup){
    //g_shapes.push_back(  *((TGraphErrors*)fin.Get("TimeShape_30GeV_"+p.first)));
    //TimeDelay.push_back(sigShape::fitTime_lev(g_shapes.back().GetX(),
    //                                          g_shapes.back().GetY(),
    //                                          lvl, 0.5, 350)*1000);
    //cout <<  "TimeDelay: " << TimeDelay.back() << "\n";     
   
    Xcalib.push_back(*((TProfile*)fin.Get("calib_x_"+p.second+"_profile")));
    Xcalib.back().Print();
    Ycalib.push_back(*((TProfile*)fin.Get("calib_y_"+p.second+"_profile")));
    Ycalib.back().Print();
    Ecalib.push_back(((TH1D*)fin.Get("calibE_"+p.second))->GetMean());
    cout << Ecalib.back() << endl;     

    /*
    Xcalib.push_back(*((TProfile2D*)fin.Get("calibX_"+p.second+"_pxy")));
    Xcalib.back().Print();
    Ycalib.push_back(*((TProfile2D*)fin.Get("calibY_"+p.second+"_pxy")));
    Ycalib.back().Print();
    Ecalib.push_back(*((TProfile*)  fin.Get("calib2E_"+p.second+"_pfx")));
    Ecalib.back().Print(); 
    */
  }
  fin.Close();
}


Cell::Cell(Double_t x, Double_t y, Int_t reg) {
  initialise();
	m_x = x;
	m_y = y;
	m_region = reg;
}

void Cell::initialise() {
  m_x = 0;
  m_y = 0;
  m_z = 0;
  m_size = 0;
  m_e = 0;
  m_t = 0;
  m_id = -1;
  m_col = -1;
  m_row = -1;
  m_region = -1;
  m_nphotons = 0; 
  m_isSeed =0; 
  m_mapPhotons.clear();
  m_neighbours.clear();
  m_nearSeeds.clear();
  m_neighbours.reserve(2);
  m_photons.clear();
}

void Cell::addPhoton(myProtoParticle * photon, Double_t frac) {
  PhContrib tmpPh;
	tmpPh.m_frac = frac;
	tmpPh.m_e    = frac*photon->getCaloE();
	tmpPh.m_t    = photon->getCaloVertexT();
  m_mapPhotons[photon->getKey()] = tmpPh;
	m_photons.push_back(photon);
  ++m_nphotons;
}
void Cell::addPhoton(myProtoParticle * photon, Double_t frac, Double_t e, Double_t t) {

	//if(frac*photon->getE() < 50) return;
	PhContrib tmpPh;
	tmpPh.m_frac = frac;
	tmpPh.m_e    = e;
	tmpPh.m_t    = t;
	m_mapPhotons[photon->getKey()] = tmpPh;
	m_photons.push_back(photon);
  ++m_nphotons;
}

Double_t Cell::getDistance(Double_t x, Double_t y) {
	Double_t x1 = getX1();
	Double_t x2 = getX2();
	Double_t y1 = getY1();
	Double_t y2 = getY2();
	Double_t tmpX = 0, tmpY = 0;
	if(y < y2 && y > y1) {
		tmpY = 0;
		if(x < x2 && x > x1) {
			tmpX = 0;
		}
		else
			tmpX = min((x-x1)*(x-x1),(x-x2)*(x-x2));
	}
	else {
		tmpY = min((y-y1)*(y-y1),(y-y2)*(y-y2));
		if(x < x2 && x > x1) {
			tmpX = 0;
		}
		else
			tmpX = min((x-x1)*(x-x1),(x-x2)*(x-x2));		
	}

	return sqrt(tmpX+tmpY);
	
}

Double_t Cell::getE() {
  Double_t retVal = 0.0;
  for(auto tmp: m_mapPhotons)
    retVal += tmp.second.m_e;
  return retVal;
}
Double_t Cell::getEtrue(){
	Double_t retVal	= 0.0;
	for (auto it_photons=m_photons.begin(); it_photons!=m_photons.end(); ++it_photons){
      myProtoParticle *photon = (myProtoParticle*)(*it_photons);
      retVal += this->getPhotonFrac(photon)*photon->getCaloE();	
    }
	return retVal;
}
Double_t Cell::getT_Eavg(){
  Double_t retVal = 0, E=0;
  for(auto tmp: m_mapPhotons){
    E += tmp.second.m_e;
    retVal += tmp.second.m_e*tmp.second.m_t;
  }
  if (E<1.e-5) retVal = -999999;
  else retVal = retVal/E;
  return retVal;  
}
Double_t Cell::getT_Eavg_true(){
  Double_t retVal = 0, E=0;
  for (auto it_photons=m_photons.begin(); it_photons!=m_photons.end(); ++it_photons){
      myProtoParticle *photon = (myProtoParticle*)(*it_photons);
      Double_t frac = getPhotonFrac(photon);
      Double_t e = photon->getCaloE();
      //retVal += frac*e*photon->getTatZ(m_z);
      retVal += frac*e*photon->getCaloVertexZ();
      E +=	frac*e;
      //std::cout << frac << "     " << e << "     " << retVal << "\n";
  }
  if (E<1.e-5) retVal = -999999;
  else retVal = retVal/E;
  return retVal;  
}
Double_t Cell::getPhotonE(myProtoParticle* photon) { 
	auto it_gamma = m_mapPhotons.find(photon->getKey());
	if (it_gamma == m_mapPhotons.end()) return -999999;
	else return it_gamma->second.m_e; 
}
Double_t Cell::getPhotonT(myProtoParticle* photon) { 
	auto it_gamma = m_mapPhotons.find(photon->getKey());
	if (it_gamma == m_mapPhotons.end()) return -999999;
	else return it_gamma->second.m_t; 
}
Double_t Cell::getPhotonFrac(myProtoParticle* photon) { 
	auto it_gamma = m_mapPhotons.find(photon->getKey());
	if (it_gamma == m_mapPhotons.end()) return -999999;
	else return it_gamma->second.m_frac; 
}

vector<myProtoParticle*> Cell::getHittingPhotons(Double_t E_th){
	vector<myProtoParticle*> HittingPhotons;
  HittingPhotons.reserve(3);
  //cout  << "Cell::getHittingPhotons : " << m_nphotons;
	for (auto it_photons=m_photons.begin(); it_photons!=m_photons.end(); ++it_photons){
  //    cout << ".";
      myProtoParticle *photon = (myProtoParticle*)(*it_photons);
      Double_t photon_x=photon->getCaloVertexX();
      Double_t photon_y=photon->getCaloVertexY();
      if (photon->getCaloE()<E_th)continue; 
      if (photon_x<this->getX1()) continue;
      if (photon_x>this->getX2()) continue;
      if (photon_y<this->getY1()) continue;
      if (photon_y>this->getY2()) continue;
      HittingPhotons.push_back(photon);
    }
    //cout << endl;
    return HittingPhotons;
}
vector<myProtoParticle*> Cell::getContributingPhotons(Double_t E_th){
  vector<myProtoParticle*> retVect;
  for (auto it_photons=m_photons.begin(); it_photons!=m_photons.end(); ++it_photons){
    myProtoParticle *photon = (myProtoParticle*)(*it_photons);
    if (this->getPhotonE(photon) > E_th)
      retVect.push_back(photon); 
  }
  return retVect;
}

void Cell::smearTimeFromShape(Calibrations * calibs, Double_t factor){
		//UInt_t Nhits = m_mapPhotons.size();
    UInt_t Nhits = 0;
    std::vector<Double_t> vE; 
		std::vector<Double_t> vT;
		for (auto p : m_mapPhotons){
      ++Nhits;
			vE.push_back(p.second.m_e);
			vT.push_back(p.second.m_t/1000.);     
		}
    if (Nhits == 0) {m_t=0.0; return;}
    Double_t t_offset = *std::min_element(vT.begin(), vT.end());
    std::vector<Double_t> sig_y(500,0.0);
    auto g_shape = calibs->g_shapes.begin()+(m_region-1);
    Double_t *x = g_shape->GetX();
    Double_t *y_mean = g_shape->GetY();
    Double_t *y_mean_err = g_shape->GetEY();
    std::vector<Double_t> sig1(500,0.0), sig2(500,0.0);
    auto sum = [](Double_t a, Double_t b){return a+b;};
    for (UInt_t i=0; i<Nhits; ++i){
            //std::cout << "t_in="<< vT[i] << "  E="<<vE[i];
            sigShape::buildSignal(factor,y_mean, y_mean_err, sig1.data(), vE[i], 500);
            TGraph tmp_shape(500, x, sig1.data());
            tmp_shape.SetBit(TGraph::kIsSortedX);
            sigShape::Traslate(&tmp_shape, vT[i]-t_offset, sig2.data(), 500);
            std::transform(sig_y.begin(),sig_y.end(),sig2.begin(),sig_y.begin(),sum);
    }
    Float_t lvl=0;
    //Double_t newTime =sigShape::fitTime_lev(x,sig_y.data(), lvl, 0.5, 500); 
    m_t = (t_offset+sigShape::fitTime_lev(x,sig_y.data(), lvl, 0.5, 500)-calibs->TimeDelay[m_region-1]/1000.)*1000; 
    //std::cout <<"smearTimeFromShape: " << t_offset << "   " << newTime  <<"    " << m_t << "\n"; 
    //if (this->getNphotons()==1 && this->getE()>20000 && this->getE()<25000) 
    //  cout << "CellE: " << this->getE()<< "  t_offset: " << t_offset << "   " << newTime - calibs->TimeDelay[m_region-1]/1000. << endl;
}

Double_t Cell::getMaxDepositTime(){
  std::vector<Double_t> vE; 
  std::vector<Double_t> vT;
  for (auto p : m_mapPhotons){
    vE.push_back(p.second.m_e);
    vT.push_back(p.second.m_t);     
  }
  if (vT.size() == 0) return 0.0;
  return vT[max_element(vE.begin(),vE.end()) - vE.begin()]; 
}

void Cell::removeNearSeed(Int_t X){
  auto it = find(m_nearSeeds.begin(), m_nearSeeds.end(), X);
  if (it != m_nearSeeds.end())
    m_nearSeeds.erase(it);
  else
    cout << "Cell::removeNearSeed : WARNING! : nearSeed ID not found!\n";
}

#endif
