#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>

using namespace std;
#include "TMath.h"
#include "TString.h"
#include "TFile.h"
#include "TTree.h"
#include "TLeaf.h"

int  main(Int_t argc, Char_t *argv[]){
  TString nfin=argv[1];
  TString nfout=argv[2];
  TFile fin(nfin, "READ");
  TTree *tin = (TTree*)fin.Get("ntp");
  TFile fout(nfout,"RECREATE");
  TTree *tout= tin->CloneTree(0);
  Int_t isSig=0;
  tin->SetBranchAddress("isSig", &isSig);
  for (Int_t i=0, nREC=tin->GetEntries(); i<nREC; ++i){
    tin->GetEntry(i);
    auto evt = tin->GetLeaf("evtNumber")->GetValue();
    auto run = tin->GetLeaf("runNumber")->GetValue();
    Int_t j=i;
    vector<pair<Double_t, Int_t>>signals;
    while (j<nREC){
      if (j%10000==0){
	cout << "\r" <<j;
	cout.flush();
      }
    
      tin->GetEntry(j);
      if (tin->GetLeaf("evtNumber")->GetValue() != evt) {++j;break;}
      if (tin->GetLeaf("runNumber")->GetValue() != run) {++j;break;}
      if (isSig != 1) {
	tout->Fill();
	++j;
	continue;
      }
      Double_t chi2 = TMath::Power(tin->GetLeaf("gamma1_CALOVX")->GetValue() - tin->GetLeaf("gamma1_TRUE_CALOVX")->GetValue(),2) +
                      TMath::Power(tin->GetLeaf("gamma1_CALOVY")->GetValue() - tin->GetLeaf("gamma1_TRUE_CALOVY")->GetValue(),2) +
                      TMath::Power(tin->GetLeaf("gamma2_CALOVX")->GetValue() - tin->GetLeaf("gamma2_TRUE_CALOVX")->GetValue(),2) +
                      TMath::Power(tin->GetLeaf("gamma2_CALOVY")->GetValue() - tin->GetLeaf("gamma2_TRUE_CALOVY")->GetValue(),2) ;
      signals.emplace_back(chi2, j);
      ++j;
    }
    i=j;
    if ( signals.size() == 0 ) continue;
    std::sort(signals.begin(), signals.end());
    tin->GetEntry(signals[0].second);
    tout->Fill();
    for (auto it = signals.begin()+1; it!= signals.end(); ++it){
      tin->GetEntry(it->second);
      isSig=-1;
      tout->Fill();
    }
  }
  cout << endl;
  fout.WriteTObject(tout,"ntp", "overwrite");
  fout.Close();
  return 1;
}
