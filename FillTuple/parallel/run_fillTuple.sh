#!/bin/bash

. /cvmfs/lhcb.cern.ch/lib/LbLogin.sh
Nsubjob=$1
Nevt_perJob=$2
Tag=$3
fullSim=$4




if [ $fullSim -eq 1 ]; 
then simTypeDani=full && simTypeSte=Full;
elif [ $fullSim -eq 0 ];
then simTypeDani=stand && simTypeSte=Stand;
fi

if [[ $Tag == *"upgrade2"* ]];
then RunDani=upgrade2 && RunSte=Upgrade2; fi
if [[ $Tag == *"upgrade1"* ]];
then RunDani=upgrade1 && RunSte=Upgrade1; fi
if [[ $Tag == *"upgrade0"* ]];
then RunDani=upgrade0 && RunSte=Run1; fi



pathInput=$ProjectDir/storage/Bd_3pi_11102404_"$RunDani"_"$simTypeDani"_20200225/$Nsubjob/
pathOutput=$ProjectDir/storage/Bd_3pi_11102404_"$RunDani"_"$simTypeDani"_20200225/$Nsubjob/$Tag

echo $pathOutput
echo 'Nsubjob:  '$Nsubjob
echo 'Tag: ' $Tag
mkdir -p $pathInput
mkdir -p $pathOutput


#$ProjectDir/FillTuple/parallel/exe/fillTuple          $pathInput/tupleTES.root               $pathOutput/fillTuple.root  $Nsubjob $Nevt_perJob $Tag
#$ProjectDir/FillTuple/parallel/exe/fillTuplePi0       $pathOutput/fillTuple.root             $pathOutput/outTuplePi0.root
#$ProjectDir/FillTuple/parallel/exe/fillTupleBd3pi     $pathOutput/fillTuple.root             $pathOutput/outTupleBd3pi.root
#$ProjectDir/FillTuple/parallel/exe/fillTuplePi0True   $pathOutput/fillTuple.root             $pathOutput/outTuplePi0True.root
$ProjectDir/FillTuple/parallel/exe/fillTupleBd3piTrue $pathOutput/fillTuple.root             $pathOutput/outTupleBd3piTrue.root
#$ProjectDir/FillTuple/parallel/exe/fillTupleGamma     $pathOutput/fillTuple.root             $pathOutput/outTupleGamma.root
#$ProjectDir/FillTuple/parallel/exe/sanityChecks       $pathOutput/sanityChecks.root          $pathOutput/outTupleGamma.root $pathOutput/outTuplePi0.root $pathOutput/outTupleBd3pi.root

#####################################################
#####################################################
#####################################################
 

$ProjectDir/FillTuple/parallel/exe/fillTuplePerf  0  0  0       $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 15 15 15       $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 50 50 50       $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 75 75 75       $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag

#$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 15 50 50       $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
#$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 15 15 50       $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
#$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 15 75 75       $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
#$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 15 15 75       $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
#$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 50 75 75       $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
#$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 50 50 75       $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag

#$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 50 15 15       $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
#$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 50 50 15       $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
#$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 75 15 15       $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
#$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 75 75 15       $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
#$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 75 50 50       $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
#$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 75 75 50       $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag

#$ProjectDir/FillTuple/parallel/exe/fillTuplePerf_OnlySigEvt  15 15 15       $pathOutput/perfSigEvts          $pathOutput/outTupleBd3pi.root $Tag
#$ProjectDir/FillTuple/parallel/exe/fillTuplePerf_OnlySigEvt  50 50 50       $pathOutput/perfSigEvts          $pathOutput/outTupleBd3pi.root $Tag
#$ProjectDir/FillTuple/parallel/exe/fillTuplePerfDalitz		  15 15 15       $pathOutput/perfDalitz           $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
#$ProjectDir/FillTuple/parallel/exe/fillTuplePerfDalitz		  50 50 50       $pathOutput/perfDalitz           $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag


#####################################################

$ProjectDir/FillTuple/parallel/exe/fillTuplePerf  0  0  0  0  0 $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 15 15 15 15 15 $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 50 50 50 50 50 $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 75 75 75 75 75 $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag

##$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 15 50 50 50 50 $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
##$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 15 15 50 50 50 $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
#$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 15 15 15 50 50 $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
##$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 15 15 15 15 50 $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag

##$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 15 75 75 75 75 $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
##$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 15 15 75 75 75 $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
#$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 15 15 15 75 75 $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
##$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 15 15 15 15 75 $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag

##$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 50 75 75 75 75 $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
##$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 50 50 75 75 75 $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
##$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 50 50 50 75 75 $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
##$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 50 50 50 50 75 $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag

##$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 50 15 15 15 15 $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
#$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 50 50 15 15 15 $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
#$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 50 50 50 15 15 $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
##$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 50 50 50 50 15 $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag

##$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 75 15 15 15 15 $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
#$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 75 75 15 15 15 $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
#$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 75 75 75 15 15 $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
##$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 75 75 75 75 15 $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag

##$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 75 50 50 50 50 $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
#$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 75 75 50 50 50 $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
#$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 75 75 75 50 50 $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
##$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 75 75 75 75 50 $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag

#$ProjectDir/FillTuple/parallel/exe/fillTuplePerfDalitz		  15 15 15 15 15       $pathOutput/perfDalitz           $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
#$ProjectDir/FillTuple/parallel/exe/fillTuplePerfDalitz		  50 50 50 50 50       $pathOutput/perfDalitz           $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
#$ProjectDir/FillTuple/parallel/exe/fillTuplePerfDalitz		  75 75 75 75 75       $pathOutput/perfDalitz           $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
#$ProjectDir/FillTuple/parallel/exe/fillTuplePerfDalitz		  75 75 15 15 15       $pathOutput/perfDalitz           $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
#$ProjectDir/FillTuple/parallel/exe/fillTuplePerfDalitz		  75 75 50 50 50       $pathOutput/perfDalitz           $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag


#####################################################

$ProjectDir/FillTuple/parallel/exe/fillTuplePerf  0  0  0  0       $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 15 15 15 15       $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 50 50 50 50       $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 75 75 75 75       $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag

# #$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 15 50 50       $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
# $ProjectDir/FillTuple/parallel/exe/fillTuplePerf 15 15 50 50      $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
# ##$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 15 75 75       $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
# ##$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 15 15 75       $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
# ##$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 50 75 75       $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
# ##$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 50 50 75       $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag

# #$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 50 15 15       $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
# $ProjectDir/FillTuple/parallel/exe/fillTuplePerf 50 50 15 15      $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
# #$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 75 15 15       $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
# $ProjectDir/FillTuple/parallel/exe/fillTuplePerf 75 75 15 15       $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
# #$ProjectDir/FillTuple/parallel/exe/fillTuplePerf 75 50 50       $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
# $ProjectDir/FillTuple/parallel/exe/fillTuplePerf 75 75 50 50      $pathOutput/perfPlots          $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag

# $ProjectDir/FillTuple/parallel/exe/fillTuplePerf_OnlySigEvt  15 15 15 15       $pathOutput/perfSigEvts          $pathOutput/outTupleBd3pi.root $Tag
# $ProjectDir/FillTuple/parallel/exe/fillTuplePerf_OnlySigEvt  50 50 50 50       $pathOutput/perfSigEvts          $pathOutput/outTupleBd3pi.root $Tag
#$ProjectDir/FillTuple/parallel/exe/fillTuplePerfDalitz		  15 15 15 15      $pathOutput/perfDalitz           $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag
#$ProjectDir/FillTuple/parallel/exe/fillTuplePerfDalitz		  50 50 50 50      $pathOutput/perfDalitz           $pathOutput/outTupleBd3pi.root $pathOutput/outTupleBd3piTrue.root $Tag

