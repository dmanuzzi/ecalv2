#ifndef ROOT_myParticle_h
#define ROOT_myParticle_h
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

#include <TVector3.h>
#include <TLorentzVector.h>

#include <Cluster.h>
#include <myProtoParticle.h>
#include <myTrack.h>
using namespace std;

class myParticle : public myProtoParticle {
	private:
		vector<Cluster*> m_clusters;
		myParticle * m_matchedPart;
		Short_t m_isSig;
		Short_t m_pi0type; //1) merged, 2) resolved
		Short_t m_aliveatcaloz;
		Short_t m_trackable;
		Short_t m_incaloacc;
		Short_t m_HitCaloSensDet;
		Short_t m_isRecoveredByVanya;

		myTrack* m_track;

	public:
		myParticle();
		virtual ~myParticle() {};
		myParticle(TLorentzVector &mom, Double_t mass);

		void getMomIfOrigV(Double_t &px, Double_t &py, Double_t &pz, Double_t x, Double_t y, Double_t z);
		void addCluster(Cluster* v) { m_clusters.push_back(v); }
    	vector<Cluster*> getClusters() { return m_clusters; }
    	void setMatchedPart(myParticle *part){ m_matchedPart = part; }
    	myParticle* getMatchedPart(){ return m_matchedPart; }
    	
    	Short_t getIsSig() { return m_isSig; }
    	void setIsSig(Short_t v) { m_isSig = v; }
    	Short_t getPi0Type() { return m_pi0type;}
    	void setPi0Type(Short_t v) { m_pi0type = v; } 
		Short_t getAliveAtCaloZ()  { return m_aliveatcaloz; }
    	void setAliveAtCaloZ(Short_t v) { m_aliveatcaloz = v; }
		Short_t getTrackable() { return m_trackable; }
    	void setTrackable(Short_t v) { m_trackable = v; }
    	Short_t getInCaloAcc() { return m_incaloacc; }
    	void setInCaloAcc(Short_t v) { m_incaloacc = v; }
    	Short_t getHitCaloSensDet() { return m_HitCaloSensDet; }
    	void setHitCaloSensDet(Short_t v) { m_HitCaloSensDet = v; }
		Short_t getIsRecoveredByVanya() { return m_isRecoveredByVanya; }
    	void setIsRecoveredByVanya(Short_t v) { m_isRecoveredByVanya = v; }
    	myTrack* getTrack() { return m_track; }
    	void setTrack(myTrack* v) { m_track = v; }

    	vector<myParticle*> getParents();
    	void getDescendants(vector<myParticle*> &ret);
    	ClassDef(myParticle,1)
};

#endif
