#ifndef ROOT_myTagManager_h
#define ROOT_myTagManager_h
#include <iostream>
#include <sstream>
#include <map>
#include <string>
#include <vector>
#include <TObject.h>
#include <TFile.h>
#include <TH2D.h>
#include <numeric>
#include <utility>
#include <algorithm>
using namespace  std;

class myTagManager : public TObject {
private:
	map<string, string>                   m_CodeRecoAlg;
	map<string, vector<string>>           m_CodeRegions;
	map<string, Double_t>                 m_CodeCellSize;
	map<string, Double_t>                 m_CodeRmoliere;
	map<string, pair<Double_t, Double_t>> m_CodeEres;
	map<string, Double_t>                 m_CodeTres;
	
	string                           m_fullTag;
	string                           m_conditions;
	string                           m_recoAlg;
	vector<string>                   m_regions;
	Int_t                            m_Nregions;
	vector<Double_t>                 m_cellsize;
	vector<Double_t>                 m_Rmoliere;
	vector<Double_t>                 m_Tres;
	vector<pair<Double_t, Double_t>> m_Eres;
	string                           m_notes;
	vector<Double_t>                 m_bordersX;
	vector<Double_t>                 m_bordersY;
	vector<pair<string,vector<string>>> m_calib_inputs;
	void initialise();
	void create();

public:

	myTagManager();
	myTagManager(string fullTag);
	void setFullTag(string fullTag);
	string                           getFullTag()  { return  m_fullTag;    }
	string                           getConditons(){ return  m_conditions; }
	string                           getRecoAlg()  { return  m_recoAlg;    }
	vector<string>                   getRegions()  { return  m_regions;    }
	Int_t                            getNregions() { return  m_Nregions;   }
	vector<Double_t>                 getCellSize() { return  m_cellsize;   }
	vector<Double_t>                 getRMoliere() { return  m_Rmoliere;   }
	vector<pair<Double_t, Double_t>> getEres()     { return  m_Eres;       }
	vector<Double_t>                 getTres()     { return  m_Tres;       }
	string                           getNotes()    { return  m_notes;      }
	vector<Double_t>                 getBordersX() { return  m_bordersX;   }
	vector<Double_t>                 getBordersY() { return  m_bordersY;   }
	vector<pair<string,vector<string>>> getCalibInputs() { return m_calib_inputs; }
	ClassDef(myTagManager,1)
};

#endif