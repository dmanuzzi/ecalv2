#ifndef ROOT_Vertex_cxx
#define ROOT_Vertex_cxx
#include <Vertex.h>



Vertex::Vertex(Double_t x,Double_t y,Double_t z,Double_t t){
	m_run=-99999;
	m_evt=-99999;
	m_key=-99999;
	m_x=x;
	m_y=y;
	m_z=z;
	m_t=t;
	m_type=-1;
}
void Vertex::initialise(){
	m_run=-99999;
	m_evt=-99999;
	m_key=-99999;
	m_x=0.0;
	m_y=0.0;
	m_z=0.0;
	m_t=-99999.0;
	m_type=-1;
}

array<Double_t,3> Vertex::getPos(){
	array<Double_t,3> v = {m_x, m_y, m_z};
	return v;
}
array<Double_t,4> Vertex::getPosT(){
	array<Double_t,4> v = {m_x, m_y, m_z, m_t};
	return v;
}

#endif