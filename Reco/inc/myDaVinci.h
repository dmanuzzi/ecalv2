#ifndef ROOT_myDaVinci_h
#define ROOT_myDaVinci_h

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <TClonesArray.h>
#include "Photon.h"
#include "Cell.h"
#include "Cluster.h"
#include "CaloEvent.h"
#include <TChain.h>
#include <TF2.h>
#include <TRandom3.h>
#include "myParticle.h"
#include "myTrack.h"
#include <vector>
#include <string>
using namespace std;



class myDaVinci : public TObject {
private:
	TTree *m_tin;
	TTree *m_tout;
	TString m_nfout;
	vector<string> m_ncontainers;
	Int_t m_Ncontainers;

	map<string, vector<string>> m_allBranches; 

	function<vector<myParticle>(myParticle*)>  m_getParts;
	function<vector<myParticle>* (CaloEvent*)> m_getMotherContainer;
	function<void()> m_exceptions;
	//Double_t	
	vector<string>  m_nDvar;
	vector<function<double()>> m_Dfunctors;
	vector<Double_t> m_Dvar;
	Int_t m_NDvar;
	map<string, vector<string>>                m_nDvarUser;
	map<string, vector<function<double()>>>    m_DfunctorsUser;
	map<string, vector<Double_t>>              m_DvarUser;
	map<string, Int_t>                         m_NDvarUser;

	//Int_t	
	vector<string>          m_nIvar;
	vector<function<int()>> m_Ifunctors;
	vector<Int_t>           m_Ivar;
	Int_t m_NIvar;
	map<string, vector<string>>                m_nIvarUser;
	map<string, vector<function<int()>>>       m_IfunctorsUser;
	map<string, vector<Int_t>>                 m_IvarUser;
	map<string, Int_t>                         m_NIvarUser;

	//Long64_t	
	vector<string>               m_nLvar;
	vector<function<long int()>> m_Lfunctors;
	vector<Long64_t>             m_Lvar;
	Int_t m_NLvar;
	map<string, vector<string>>                m_nLvarUser;
	map<string, vector<function<long int()>>>  m_LfunctorsUser;
	map<string, vector<Long64_t>>              m_LvarUser;
	map<string, Int_t>                         m_NLvarUser;

	//array Double_t	
	vector<string>                                  m_nVDvar;
	vector<function<void(const char*, int)>>        m_VDfunctors;
	vector<vector<Double_t>>                        m_VDvar;
	Int_t                                           m_NVDvar;
	map<string, vector<string>>                     m_nVDvarUser;
	map<string, vector<function<void(const char*, int)>>> m_VDfunctorsUser;
	map<string, vector<vector<Double_t>>>           m_VDvarUser;
	map<string, Int_t>                              m_NVDvarUser;

	//array Int_t	
	vector<string>                                  m_nVIvar;
	vector<function<void(const char*, int)>>        m_VIfunctors;
	vector<vector<Int_t>>                           m_VIvar;
	Int_t                                           m_NVIvar;
	map<string, vector<string>>                     m_nVIvarUser;
	map<string, vector<function<void(const char*, int)>>> m_VIfunctorsUser;
	map<string, vector<vector<Int_t>>>              m_VIvarUser;
	map<string, Int_t>                              m_NVIvarUser;



	Int_t m_evtNumb, m_runNumb, m_isMatched, m_isSig;
	CaloEvent *event;
	string m_nmother;
	vector<myParticle> m_parts;
	function<Bool_t()> m_checkMatch;
	myParticle *m_part;
	myParticle *m_matchedPart;
	myParticle *m_closerTrack;
	Vertex* m_PV;
	Vertex* m_origV;
	Vertex* m_caloV;
	Vertex* m_endV;
	Vertex* m_matchedPV;
	Vertex* m_matchedOrigV;
	Vertex* m_matchedCaloV;
	Vertex* m_matchedEndV;
	Cluster* m_cluster;
	vector<myProtoParticle*> m_HittingPhotons;
	myTrack* m_track;
	myTrack* m_matchedTrack;
	
	myParticle *m_NullPart;
	Vertex* m_NullVertex;
	Cluster* m_NullCluster;
	Cell* m_NullCell;
	myTrack* m_NullTrack;
	TClonesArray * m_AllClusters; 
	vector<Cluster*> m_ClustWithin100;
	//Brem. Recovery
	vector<Cluster*> m_BremRecovClust;
	Double_t m_BremRecov_OVExtrap_x;
	Double_t m_BremRecov_OVExtrap_y;
	Double_t m_BremRecov_UTExtrap_x;
	Double_t m_BremRecov_UTExtrap_y;
	
public:
	myDaVinci();
	virtual ~myDaVinci();
	
	TTree * getOutTree();
	vector<myParticle>* getParts();
	void initialize(TTree *tin, vector<string> ncontainers, map<string, vector<string>> allBranches,
				    function<vector<myParticle>* (CaloEvent*)> getMotherContainer,
	                function<vector<myParticle>(myParticle*)> getParts);
	void execute();
	void finalize(TString nfout);

	void setCheckMatch(function<Bool_t()> checkMatch);
	void setExceptions(function<void()> exceptions);
	vector<Double_t>* getDvars() { return &m_Dvar; }
	//vector<Int_t>*    getIvars() { return &m_Ivar; }
	//vector<Long64_t>* getLvars() { return &m_Lvar; }
	//Int_t getNDvar() { return m_NDvar; }
	//Int_t getNIvar() { return m_NIvar; }
	//Int_t getNLvar() { return m_NLvar; }
	CaloEvent* getEvent(){ return event; }
	myParticle* getNullPart() { return m_NullPart; }
	void setIsSig(Int_t v){ m_isSig = v; } 
	ClassDef(myDaVinci,1)
};

#endif
