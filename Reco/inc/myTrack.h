#ifndef ROOT_myTrack_h
#define ROOT_myTrack_h

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <TObject.h>

#include "Vertex.h"

using namespace std;

class myTrack : public TObject {
private:
	UInt_t m_size;	
	Int_t m_id;
	Double_t m_measPX;
	Double_t m_measPY;
	Double_t m_measPZ;
	vector<Double_t> m_x;
	vector<Double_t> m_y;
	vector<Double_t> m_z;
	vector<Double_t> m_t;
	vector<Double_t> m_px;
	vector<Double_t> m_py;
	vector<Double_t> m_pz;


public:
	myTrack();
	myTrack(UInt_t size);
	void initialise();
	void setSize(UInt_t size);
	void setID(Int_t id)      { m_id = id; };
	void setX (Double_t * x)  { m_x.insert(m_x.begin(),    x,  x+m_size); };
	void setY (Double_t * y)  { m_y.insert(m_y.begin(),    y,  y+m_size); };
	void setZ (Double_t * z)  { m_z.insert(m_z.begin(),    z,  z+m_size); };
	void setT (Double_t * t)  { m_t.insert(m_t.begin(),    t,  t+m_size); };
	void setPX(Double_t *px)  { m_px.insert(m_px.begin(), px, px+m_size); };
	void setPY(Double_t *py)  { m_py.insert(m_py.begin(), py, py+m_size); };
	void setPZ(Double_t *pz)  { m_pz.insert(m_pz.begin(), pz, pz+m_size); };
	void setMeasPX(Double_t v){ m_measPX = v; }	
	void setMeasPY(Double_t v){ m_measPY = v; }	
	void setMeasPZ(Double_t v){ m_measPZ = v; }	

	UInt_t   getSize()      { return m_size;    }
	Int_t    getID()        { return m_id;      }
	Double_t getX (Int_t i) { return m_x.at(i); }
	Double_t getY (Int_t i) { return m_y.at(i); }
	Double_t getZ (Int_t i) { return m_z.at(i); }
	Double_t getT (Int_t i) { return m_t.at(i); }
	Double_t getPX(Int_t i) { return m_px.at(i);}
	Double_t getPY(Int_t i) { return m_py.at(i);}
	Double_t getPZ(Int_t i) { return m_pz.at(i);}
	Double_t getMeasPX()    { return m_measPX;  }
	Double_t getMeasPY()    { return m_measPY;  }
	Double_t getMeasPZ()    { return m_measPZ;  }
	
	ClassDef(myTrack,1)	
};


#endif
