RUNGAUSS = True

import os
import argparse
parser = argparse.ArgumentParser()
parser.add_argument('-C','--conditions', type = str, help = 'Select type of run [upgrade0, upgrade1, upgrade2]', 
                    dest = 'conditions', default = 'upgrade0')
parser.add_argument('-S','--simulation', type = str, help = 'Simulate detector or not [stand, full]',
                    dest = 'simulation', default = 'full')
parser.add_argument('-G','--generation', type = str, help = 'How to generate collision [Pythia, PGUN]',
                    dest = 'generation', default = 'Pythia')
parser.add_argument('-d','--decay', type = str, help = 'EventType [11102404,11124003,11102201]',
                    dest = 'decay', default = '11102404')
parser.add_argument('-n','--njobs', type = int, help = 'Number of jobs',
                    dest = 'njobs', default = 1)
parser.add_argument('-D', '--date', type = str, help = 'date of generation [20200225]',
                    dest =  'date', default = '20200225')
args = parser.parse_args()

config = { 'upgrade2' : { 'full' : { 'DEC'    : { '11102404' : ('511','Bd_pi+pi-pi0=DecProdCut,sqDalitz.dec'), 
                                                  #'11124003' : ('511','Bd_Kstee,phsp=DecProdCut.dec'), 
                                                  #'11102201' : ('511','Bd_Kstgamma=DecProdCut.dec'), 
                                                },
                                     'OPTS'   : ['Gauss-Job.py',
                                                 'Gauss-Full.py'],
                                     'Pythia' : ['Beam7000GeV-md100-nu71.5.py',
                                                 'Pythia8.py' ],
                                     'PGUN'   : ['BParticleGun.py',
                                                 'PGuns.py',],
                                   },
                          'stand' : { 'DEC'    : { '11102404' : ('511','Bd_pi+pi-pi0=DecProdCut,sqDalitz.dec'), 
                                                  #'11124003' : ('511','Bd_Kstee,phsp=DecProdCut.dec'), 
                                                  #'11102201' : ('511','Bd_Kstgamma=DecProdCut.dec'), 
                                                },
                                     'OPTS'   : ['Gauss-Job.py',
                                                 'GenStandAlone.py',
                                                 'Gauss-Upgrade-Baseline-20150522.py'],
                                     'Pythia' : ['Beam7000GeV-md100-nu71.5.py',
                                                 'Pythia8.py' ],
                                     'PGUN'   : ['BParticleGun.py',
                                                 'PGuns.py',],
                                   },
                        },
           'upgrade1' : { 'full' : { 'DEC'    : { '11102404' : ('511','Bd_pi+pi-pi0=DecProdCut,sqDalitz.dec'),
                                                  #'11124003' : ('511','Bd_Kstee,phsp=DecProdCut.dec'), 
                                                  #'11102201' : ('511','Bd_Kstgamma=DecProdCut.dec'), 
                                                },
                                     'OPTS'   : ['Gauss-Job.py',
                                                 'Gauss-Full.py'],
                                     'Pythia' : ['Beam7000GeV-md100-nu7.6.py',
                                                 'Pythia8.py' ],
                                     'PGUN'   : ['BParticleGun.py',
                                                 'PGuns.py',],
                                   },
                          'stand' : { 'DEC'    : { '11102404' : ('511','Bd_pi+pi-pi0=DecProdCut,sqDalitz.dec'),
                                                  #'11124003' : ('511','Bd_Kstee,phsp=DecProdCut.dec'), 
                                                  #'11102201' : ('511','Bd_Kstgamma=DecProdCut.dec'), 
                                                },
                                     'OPTS'   : ['Gauss-Job.py',
                                                 'GenStandAlone.py',
                                                 'Gauss-Upgrade-Baseline-20150522.py'],
                                     'Pythia' : ['Beam7000GeV-md100-nu7.6.py',
                                                 'Pythia8.py' ],
                                     'PGUN'   : ['BParticleGun.py',
                                                 'PGuns.py',],
                                   },
                        },
           'upgrade0' : { 'full' : { 'DEC'    : { '11102404' : ('511','Bd_pi+pi-pi0=DecProdCut,sqDalitz.dec'),
                                                  #'11124003' : ('511','Bd_Kstee,phsp=DecProdCut.dec'),
                                                  #'11102201' : ('511','Bd_Kstgamma=DecProdCut.dec'), 
                                                },
                                     'OPTS'   : ['Gauss-Job.py',
                                                 'Gauss-Full.py'],
                                     'Pythia' : ['Beam6500GeV-md100-nu1.6.py',
                                                 'Pythia8.py' ],
                                     'PGUN'   : ['BParticleGun.py',
                                                 'PGuns.py',],
                                   },
                          'stand' : { 'DEC'    : { '11102404' : ('511','Bd_pi+pi-pi0=DecProdCut,sqDalitz.dec'),
                                                  #'11124003' : ('511','Bd_Kstee,phsp=DecProdCut.dec'), 
                                                  #'11102201' : ('511','Bd_Kstgamma=DecProdCut.dec'), 
                                                },
                                     'OPTS'   : ['Gauss-Job.py',
                                                 'GenStandAlone.py',
                                                 'Gauss-Upgrade-Baseline-20150522.py'],
                                     'Pythia' : ['Beam6500GeV-md100-nu1.6.py',
                                                 'Pythia8.py' ],
                                     'PGUN'   : ['BParticleGun.py',
                                                 'PGuns.py',],
                                   },
                        }
}

decay=args.decay
cond=args.conditions
sim=args.simulation
date=args.date
translator = {
  'upgrade0':'Run1',
  'upgrade1':'Upgrade1',
  'upgrade2':'Upgrade2',
  'stand'   : 'Stand',
  'full'    : 'Full',
}

basedir = os.getenv('PWD')
outdir = '%s/../storage/Bd_3pi_%s_%s_%s_%s'%(basedir,decay,cond,sim,date)
if not os.path.exists(outdir): os.makedirs(outdir)
optdir = '%s/opts/'%(basedir)
srcdir = '%s/src/'%(basedir)
exedir = '%s/exe/'%(basedir)
rundir = '%s/run/'%(basedir)
submitdir='%s/submit/'%(basedir)
condorlogdir='%s/condor_log/'%(basedir)
sim_extension=''
if sim=='stand': sim_extension = 'xgen'
elif sim=='full': sim_extension= 'sim'


outFile = open('%s/run_%s_%s_%s.sh'%(rundir, cond,sim,decay),'w')
outFile.write('#!/bin/bash\n\n')
outFile.write('. $VO_LHCB_SW_DIR/lib/LbEnv\n\n')
outFile.write('index=$1\n')
outFile.write('mkdir -p %s/$index\n'%(outdir))
outFile.write('cd %s/$index\n'%(outdir))

if RUNGAUSS:
  outFile.write('cp %s/job.py %s/$index/job.py\n'%(optdir,outdir))
  outFile.write('echo "gaussGen.RunNumber = $index" >> %s/$index/job.py\n'%(outdir))
  gaussOpts = config[cond][sim]['OPTS']
  gaussOpts+= config[cond][sim]['Pythia']
  gaussOpts+= ['%s.py'%(decay)]
  gaussOptsSTR = ''
  for gaussOpt in gaussOpts:
    gaussOptsSTR += ' %s/%s'%(optdir, gaussOpt)
  outFile.write('lb-run -c x86_64-slc6-gcc49-opt Gauss/v51r2 gaudirun.py  %s job.py\n'%(gaussOptsSTR))
  outFile.write('\n\n')
  outFile.write('fileName=`ls %s/$index/*.%s`\n'%(outdir,sim_extension))
else:
  outFile.write('fileName=`ls /home/LHCB/perazzini/ECAL/Simulation.new/%s/%s/%s/out/$index/*.%s`\n'%(translator[cond],translator[sim], decay,sim_extension))
outFile.write('lb-run DaVinci/v50r6 python %s/readFullBrem.py -i $fileName\n'%(srcdir))
outFile.close()
os.chmod('%s/run_%s_%s_%s.sh'%(rundir, cond,sim,decay),0755)

outFile = open('%s/submit_%s_%s_%s.jdl'%(submitdir, cond,sim,decay),'w')
outFile.write('executable     = %s/run_%s_%s_%s.sh\n'%(rundir,cond,sim,decay))
outFile.write('arguments      = $(Process)\n')
outFile.write('JobBatchName   = "Simulation_%s_%s_%s"\n'%(cond,sim,decay))
outFile.write('request_cpus   = 1\n')
outFile.write('request_memory = 3000\n')
outFile.write('universe       = vanilla\n')
outFile.write('getenv         = True\n')
outFile.write('output         = %s/out/simulation_%s_%s_%s_$(Process).txt\n'%(condorlogdir,cond,sim,decay))
outFile.write('error          = %s/err/simulation_%s_%s_%s_$(Process).txt\n'%(condorlogdir,cond,sim,decay))
outFile.write('log            = %s/log/simulation_%s_%s_%s_$(Process).txt\n'%(condorlogdir,cond,sim,decay))
outFile.write('queue %d\n'%(args.njobs))
outFile.close()

outFile = open('%s/correct_%s_%s_%s.sh'%(rundir,cond,sim,decay),'w')
outFile.write('#!/bin/bash\n\n')
outFile.write('. $VO_LHCB_SW_DIR/lib/LbEnv\n\n')
outFile.write('index=$1\n')
outFile.write('fileName1=$ProjectDir/storage/Bd_3pi_%s_%s_%s_20200225/$index/tupleFullBrem.root\n'%(decay,cond,sim))
outFile.write('fileName2=/home/LHCB/perazzini/ECAL/Simulation.new/%s/%s/%s/out/$index/tupleVanya.root\n'%(translator[cond],translator[sim], decay))
outFile.write('fileName3=$ProjectDir/storage/Bd_3pi_%s_%s_%s_20200225/$index/tupleFullBremCorr.root\n'%(decay,cond,sim))
outFile.write('fileName4=$ProjectDir/storage/Bd_3pi_%s_%s_%s_20200225/$index/tupleTES.root\n'%(decay,cond,sim))
outFile.write('$ProjectDir/Simulation/exe/vanyaCorr $fileName1 $fileName2 $fileName3\n')
outFile.write('$ProjectDir/Simulation/exe/makeTES   $fileName3 $fileName4\n')
outFile.close()
os.chmod('%s/correct_%s_%s_%s.sh'%(rundir, cond,sim,decay),0755)

outFile = open('%s/submit_correct_%s_%s_%s.jdl'%(submitdir,cond,sim,decay),'w')
outFile.write('executable     = %s/correct_%s_%s_%s.sh\n'%(rundir,cond,sim,decay))
outFile.write('arguments      = $(Process)\n')
outFile.write('JobBatchName   = "Correction_%s_%s_%s"\n'%(cond,sim,decay))
outFile.write('request_cpus   = 1\n')
outFile.write('request_memory = 3000\n')
outFile.write('universe       = vanilla\n')
outFile.write('getenv         = True\n')
outFile.write('output         = %s/out/correction_%s_%s_%s_$(Process).txt\n'%(condorlogdir,cond,sim,decay))
outFile.write('error          = %s/err/correction_%s_%s_%s_$(Process).txt\n'%(condorlogdir,cond,sim,decay))
outFile.write('log            = %s/log/correction_%s_%s_%s_$(Process).txt\n'%(condorlogdir,cond,sim,decay))
outFile.write('queue %d\n'%(args.njobs))
outFile.close()


