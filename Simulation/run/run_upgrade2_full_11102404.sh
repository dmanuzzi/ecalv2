#!/bin/bash

. $VO_LHCB_SW_DIR/lib/LbEnv

index=$1
mkdir -p /home/LHCB-T3/dmanuzzi/newECAL/Simulation/../storage/Bd_3pi_11102404_upgrade2_full_20200225/$index
cd /home/LHCB-T3/dmanuzzi/newECAL/Simulation/../storage/Bd_3pi_11102404_upgrade2_full_20200225/$index
cp /home/LHCB-T3/dmanuzzi/newECAL/Simulation/opts//job.py /home/LHCB-T3/dmanuzzi/newECAL/Simulation/../storage/Bd_3pi_11102404_upgrade2_full_20200225/$index/job.py
echo "gaussGen.RunNumber = $index" >> /home/LHCB-T3/dmanuzzi/newECAL/Simulation/../storage/Bd_3pi_11102404_upgrade2_full_20200225/$index/job.py
lb-run -c x86_64-slc6-gcc49-opt Gauss/v51r2 gaudirun.py   /home/LHCB-T3/dmanuzzi/newECAL/Simulation/opts//Gauss-Job.py /home/LHCB-T3/dmanuzzi/newECAL/Simulation/opts//Gauss-Full.py /home/LHCB-T3/dmanuzzi/newECAL/Simulation/opts//Beam7000GeV-md100-nu71.5.py /home/LHCB-T3/dmanuzzi/newECAL/Simulation/opts//Pythia8.py /home/LHCB-T3/dmanuzzi/newECAL/Simulation/opts//11102404.py job.py


fileName=`ls /home/LHCB-T3/dmanuzzi/newECAL/Simulation/../storage/Bd_3pi_11102404_upgrade2_full_20200225/$index/*.sim`
lb-run DaVinci/v50r6 python /home/LHCB-T3/dmanuzzi/newECAL/Simulation/src//readFullBrem.py -i $fileName
