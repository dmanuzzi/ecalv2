#!/bin/bash
Nevt_perJob=20
firstSubjob=0
lastSubjob=1999
fullSim=1


#D00=upgrade0-BaselineSeed-l_40.4_60.6_121.2-Rm_35_35_35-TresFactor_1.0-TresFixed
#E00=upgrade1-BaselineSeed-l_40.4_60.6_121.2-Rm_35_35_35-TresFactor_1.0-TresFixed
#b03=upgrade2-BaselineSeed-l_20.2_40.4_80.8-Rm_15_35_35-TresFactor_0.2-TresFixed
#bb03=upgrade2-BaselineSeed-l_10.1_20.2_40.4-Rm_15_15_35-TresFactor_0.2-TresFixed
#bbb03=upgrade2-BaselineSeed-l_10.1_40.4_80.8-Rm_15_35_35-TresFactor_0.2-TresFixed

#sD00=upgrade0-BaselineSeed-l_40_60_120-Rm_35_35_35-TresFactor_1.0-TresFixed
#sE00=upgrade1-BaselineSeed-l_40_60_120-Rm_35_35_35-TresFactor_1.0-TresFixed


#rD00=upgrade0-BaselineSeed-l_15_30_40_60_120-Rm_15_15_35_35_35-TresFactor_1.0-TresFixed
#rE00=upgrade1-BaselineSeed-l_15_30_40_60_120-Rm_15_15_35_35_35-TresFactor_1.0-TresFixed
#rb00=upgrade2-BaselineSeed-l_15_30_40_60_120-Rm_15_15_35_35_35-TresFactor_1.0-TresFixed
#rb01=upgrade2-BaselineSeed-l_15_30_40_60_120-Rm_15_15_35_35_35-TresFactor_0.6-TresFixed
#rb03=upgrade2-BaselineSeed-l_15_30_40_60_120-Rm_15_15_35_35_35-TresFactor_0.2-TresFixed
#rbb03=upgrade2-BaselineSeed-l_20_20_40_60_120-Rm_15_15_35_35_35-TresFactor_0.2-TresFixed

D03=upgrade0-B-Rect3-efh-zzz-333-000000
rD03=upgrade0-B-Rhom5-bdefh-wyzzz-33333-0000000000

E01=upgrade1-B-Rect3-efh-zzz-111-000000
E02=upgrade1-B-Rect3-efh-zzz-222-000000
E03=upgrade1-B-Rect3-efh-zzz-333-000000
E04=upgrade1-B-Rect3-efh-zzz-444-000000
E05=upgrade1-B-Rect3-efh-zzz-555-000000
E06=upgrade1-B-Rect3-efh-zzz-666-000000
E07=upgrade1-B-Rect3-efh-zzz-777-000000
E08=upgrade1-B-Rect3-efh-zzz-888-000000

EE03=upgrade1-B-Rect3-cfh-xzz-333-000000       #    2,6,12 cm
nE03=upgrade1-B-Rect4-befh-wzzz-3333-00000000  #1.5,4,6,12 cm
nEE03=upgrade1-B-Rect4-ccfh-xxzz-3333-00000000  #2,2,6,12 cm

rE01=upgrade1-B-Rhom5-bdefh-wyzzz-11111-0000000000
rE02=upgrade1-B-Rhom5-bdefh-wyzzz-22222-0000000000
rE03=upgrade1-B-Rhom5-bdefh-wyzzz-33333-0000000000
rE04=upgrade1-B-Rhom5-bdefh-wyzzz-44444-0000000000
rE05=upgrade1-B-Rhom5-bdefh-wyzzz-55555-0000000000
rE06=upgrade1-B-Rhom5-bdefh-wyzzz-66666-0000000000
rE07=upgrade1-B-Rhom5-bdefh-wyzzz-77777-0000000000
rE08=upgrade1-B-Rhom5-bdefh-wyzzz-88888-0000000000

rrE01=upgrade1-B-Rhom4-befh-wzzz-1111-00000000 #1.5,4,6,12 cm
rrE03=upgrade1-B-Rhom4-befh-wzzz-3333-00000000 #1.5,4,6,12 cm
rrE05=upgrade1-B-Rhom4-befh-wzzz-5555-00000000 #1.5,4,6,12 cm
rrE07=upgrade1-B-Rhom4-befh-wzzz-7777-00000000 #1.5,4,6,12 cm

rrrE03=upgrade1-B-RhomB5-bdefh-wyzzz-33333-0000000000 #1.5,3,4,6,12 cm

cccE03=upgrade1-B-CircB5-bdefh-wyzzz-33333-0000000000 #1.5,3,4,6,12 cm, circular regions



bbb01=upgrade2-B-Rect3-aeg-xzz-111-000000             #1,4,8 cm


b03=upgrade2-B-Rect3-ceg-xzz-333-000000               #2,4,8 cm
bb03=upgrade2-B-Rect3-ace-xxz-333-000000              #1,2,4 cm
bbb03=upgrade2-B-Rect3-aeg-xzz-333-000000             #1,4,8 cm
rb03=upgrade2-B-Rhom5-bdefh-wyzzz-33333-0000000000    #1.5,3,4,6,12 cm
rbb03=upgrade2-B-Rhom5-ccefh-xxzzz-33333-0000000000   #2,2,4,6,12 cm
rbbb03=upgrade2-B-Rhom5-bbdff-wwyzz-33333-0000000000  #1.5,1.5,3,6,6  cm

rrb03=upgrade2-B-Rhom4-befh-wzzz-3333-00000000 #1.5,4,6,12 cm

rrrb03=upgrade2-B-RhomB5-bdefh-wyzzz-33333-0000000000 #1.5,3,4,6,12 cm

cccb03=upgrade2-B-CircB5-bdefh-wyzzz-33333-0000000000 #1.5,3,4,6,12 cm, circular regions



cd src
touch ./fillTuple*
#make fillTuple
#make fillTuplePi0
#make fillTuplePi0True
#make fillTupleBd3pi
#make fillTupleBd3piTrue
#make fillTupleGamma
make fillTuplePerf
#make sanityChecks
cd ..

for mode in $rrrE03 $cccE03 $rE03 $rrE03; do
    rm jobs.txt
    rm -f ./condor_log/log/fillTuple.$fullSim.$mode.log
    listOut=''
    listErr=''
    for i in $(seq $firstSubjob $lastSubjob); do
	listOut+=' './condor_log/out/fillTuple.$fullSim.$mode.$i.out;
	listErr+=' './condor_log/err/fillTuple.$fullSim.$mode.$i.err;
    done;
  #  rm -f $listOut
  #  rm -f $listErr
    for i in $(seq $firstSubjob $lastSubjob); do
#		echo $i, $Nevt_perJob, $mode, $fullSim
		echo $i, $Nevt_perJob, $mode, $fullSim >> jobs.txt;
    done;
    condor_submit submit.jdl
done
