#ifndef ROOT_Photon_h
#define ROOT_Photon_h
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

#include <TVector3.h>

using namespace std;

class Photon: public TObject {

	private:
		Long64_t m_key;
		Long64_t m_mother_key;
		Long64_t m_gd_mother_key;
		Int_t m_id;
		Int_t m_mother_id;
		Int_t m_gd_mother_id;
		Double_t m_x;
		Double_t m_y;
		Double_t m_z;
		Double_t m_t;
		Double_t m_px;
		Double_t m_py;
		Double_t m_pz;

	public:
		Photon() {};
		virtual ~Photon() {};
		Photon(TVector3 &pos, TVector3 &mom, Double_t T);

		void setKey(Long64_t k) { m_key = k; }
		void setKey(ULong64_t evtNumber, Int_t tmpKey);
		void setMkey(Long64_t k) { m_mother_key = k; }
		void setMkey(ULong64_t evtNumber, Int_t tmpKey);
		void setGmkey(Long64_t k) { m_gd_mother_key = k; }
		void setGmkey(ULong64_t evtNumber, Int_t tmpKey);
		void setID(Int_t k) { m_id = k; }
		void setMid(Int_t k) { m_mother_id = k; }
		void setGmid(Int_t k) { m_gd_mother_id = k; }

		void setX(Double_t v)  { m_x  = v; }
		void setPx(Double_t v) { m_px = v; }
		void setY(Double_t v)  { m_y  = v; }
		void setPy(Double_t v) { m_py = v; }
		void setZ(Double_t v)  { m_z  = v; }
		void setPz(Double_t v) { m_pz = v; }
		void setT(Double_t v)  { m_t  = v; }
		void setPosT(Double_t x, Double_t y, Double_t z, Double_t T);
		void setMom(Double_t x, Double_t y, Double_t z);

		Double_t getX()  { return m_x; }
		Double_t getPx() { return m_px; }
		Double_t getY()  { return m_y; }
		Double_t getPy() { return m_py; }
		Double_t getZ()  { return m_z; }
		Double_t getPz() { return m_pz; }
		Double_t getE()  { return sqrt(m_px*m_px+m_py*m_py+m_pz*m_pz); }
		Double_t getT()  { return m_t; }
 		Double_t getTatZ(Double_t z);
 		void getXYatZ(Double_t &x, Double_t &y, Double_t z);

		void getPos(TVector3 &pos) { pos.SetXYZ(m_x,m_y,m_z); }
		void getMom(TVector3 &mom) { mom.SetXYZ(m_px,m_py,m_pz); }
		Double_t getPt2(){ return m_px*m_px+m_py*m_py; }
 		
    	Long64_t getKey() { return m_key; }
		Long64_t getMkey() { return m_mother_key; }
    	Long64_t getGmkey() { return m_gd_mother_key; }
    	Int_t getID() { return m_id; }
		Int_t getMid() { return m_mother_id; }
    	Int_t getGmid() { return m_gd_mother_id; }

		ClassDef(Photon,1)


};

#endif
