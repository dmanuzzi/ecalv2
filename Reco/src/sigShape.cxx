#ifndef sigShape_cxx
#define sigShape_cxx
#include <sigShape.h>
#include <vector>

EntryTimes::EntryTimes(Int_t entry, Float_t t_pmt, Float_t t_ref){
	m_entry = entry;
	m_t_pmt = t_pmt;
	m_t_ref = t_ref; 
}

sigShape::sigShape(TString nfin, TString nfin_refSelected){
	m_nfin = nfin;
	m_nfin_refSelected = nfin_refSelected;
	m_dt_sampling = 0.200; // in ns
	RND.SetSeed(1234);
	//m_h_OrigShapes = new TH2D("h_OrigShapes", "Original Shapes; t [ns]; Signal Strength [Arbitrary Unit]",1024,0,204.6,400,0,3000);
	m_h_OrigShapes = new TProfile("h_OrigShapes", "Original Shapes; t [ns]; Signal Strength [Arbitrary Unit]",1024,0,204.6,0,3000,"S");
	m_h_t_ref = new TH1D("h_t_ref", "h_t_ref; t [ns]; Counts", 200, 27,	39);
	m_h_t_ref_res = new TH1D("h_t_ref_res", "h_t_ref_res; t [ns]; Counts", 200, -0.1,0.1);
	//m_h_res   = new TH1D("h_res"  , "h_res"  , 200, 37.5, 38.5);
	//m_h_res_2 = new TH1D("h_res_2", "h_res_2", 200, 37.5, 38.5);
	m_h_res   = new TH1D("h_res"  , "h_res"  , 200, 25., 27.);
	m_h_res_2 = new TH1D("h_res_2", "h_res_2", 200, 25., 27.);
	
	m_g_shape = NULL;
	if (m_nfin_refSelected != "none") this->readSelected();
}

sigShape::~sigShape(){
	delete m_h_OrigShapes;
	delete m_h_t_ref;
	delete m_g_shape;
	delete m_h_t_ref_res;
	delete m_h_res;
	delete m_h_res_2;
	
}
void sigShape::readSelected(){
	m_iEvents.clear();
	ifstream in(m_nfin_refSelected, ios::in);
	for (Int_t i=0; i<100000; ++i){
		int c = in.peek();
    	if (c==EOF) break;
		Int_t ID;
		Float_t t_pmt, t_ref;
		in >> ID;
		in >> t_pmt;
		in >> t_ref;
	    m_iEvents.emplace_back(ID, t_pmt, t_ref);
	}
}

Float_t sigShape::fitTime_lev(Double_t *x, Double_t *y, Float_t &lvl, Double_t lev, Int_t np){
        std::vector<Float_t> xf(np);
	std::vector<Float_t> yf(np);
	for (Int_t i=0; i<np;++i){
		xf[i] = (Float_t)x[i];
		yf[i] = (Float_t)y[i];
	}
	return sigShape::fitTime_lev(xf.data(),yf.data(),lvl,lev,np);
}
	
Float_t sigShape::fitTime_lev(Float_t *x, Float_t* y, Float_t &lvl, Float_t lev, Int_t np){
	Float_t t1=-9999;
	
	if(lev<0.01 || lev>0.99)return -1;
  	double ymin=999999, ymax=-999999;
  	int xmax=0;
  	for(int i=0; i<np; ++i){
    	//if(x[i]>x1 && x[i]<x2){
    		if(ymin>y[i])ymin=y[i];
      		if(ymax<y[i]){ymax=y[i]; xmax = i;}
    	//}
  	}
  	if(ymax<=ymin) return -1;
  	ymin = 0;
  	ymax=0;
  	for (Int_t i=-1; i<=1; ++i)
  		ymax+=y[xmax+i];
  	ymax = ymax/3.0;
  	int i1=-1;
  	lvl = ymin + lev*(ymax-ymin);
  	for(int i=0; i<np-1; ++i){
    	if(i1<0 && y[i]<lvl && y[i+1]>lvl){
      		i1=i;
      		break;
      	}
    }
  	Float_t counter=0, x1=0, x2=0, y1=0, y2=0, xy=0, X=0, Y=0;
  	for (Int_t i=-1; i<=2; ++i){
  		counter += 1.0;
  		X=x[i1+i];
  		Y=y[i1+i]; 
  		x1 += X;
		x2 += X*X;
		y1 += Y;
		y2 += Y*Y;
		xy += X*Y;
  	}
  	Float_t m = (counter * xy - x1*y1) / (counter*x2 - x1*x1);
 	Float_t q =        (y1*x2 - x1*xy) / (counter*x2 - x1*x1);
  	t1 = (lvl - q) / m;
  	return t1;
}

Float_t sigShape::getTime_lev(Float_t *x, Float_t* y, Float_t &lvl, Float_t lev, Int_t np){
	Float_t t1=-9999;
	
	if(lev<0.01 || lev>0.99)return -1;
  	double ymin=999999, ymax=-999999;
  	//int xmax=0;
  	for(int i=0; i<np; ++i){
	  if(ymin>y[i])ymin=y[i];
	  if(ymax<y[i]){
	    ymax=y[i]; 
	    //xmax = i;
	  }
  	}
  	if(ymax<=ymin) return -1;
  	ymin = 0;
  	lvl = ymin + lev*(ymax-ymin);
  
  	int i1=-1;
  	for(int i=0; i<np-1; ++i){
    	if(x[i]<32) continue;
      	if(y[i]<lvl && y[i+1]>lvl){
      		i1=i;
      		break;
      	}
  	}
  	if(i1>=0){
    	t1=          x[i1]+(x[i1+1]-x[i1])*(lvl-y[i1])/(y[i1+1]-y[i1]);
	//    	double t_max=x[i1+1]+(x[i1]-x[i1+1])*(y[i1+1]-ymin)/(y[i1+1]-y[i1]);
    	double s0=0., s1=0., s2=0.;
    	for(int i=0; i<i1; ++i){
        	if(x[i]>6 && x[i]<22){   	
        		s0+=1;
        		s1+=y[i];
        		s2+=y[i]*y[i];
      		}
    	}
    	if(s0>10){
      		lvl = lev*(ymax-s1/s0)+s1/s0;
      		i1=-1;
      		for(int i=0; i<np-1; ++i){
        		if(x[i]<32) continue;
        	  	if(i1<0 && y[i]<lvl && y[i+1]>lvl){
        	  		i1=i;
        	  		break;
        	  	}
      		}
  	    	t1=x[i1]+(x[i1+1]-x[i1])*(lvl-y[i1])/(y[i1+1]-y[i1]);
    	}
  	}
  	return t1;
}
void sigShape::GetMinMax(Float_t* y_in, Float_t &Min, Float_t &Max, Int_t np){
	Max = y_in[0];
	Min = y_in[0];
	for (Int_t i=1; i<np; ++i){
		if      (y_in[i]>Max) Max=y_in[i];
		else if (y_in[i]<Min) Min=y_in[i];
	}
}
Float_t sigShape::GetPlateau(Float_t *x_in, Float_t *y_in, Float_t xmin, Float_t xmax, Int_t np){
	Float_t s0=0, s1=0;
	for (Int_t i=0; i<np;++i){
		if (x_in[i]>xmin){
			s0 += 1.0000;
			s1 += y_in[i];
			if (x_in[i]>xmax) break;
		}
	}
	return s1/s0;
}
void sigShape::shift(Float_t *x_in, Float_t *y_in, Float_t dt, Float_t *y_out, Float_t yMax,Int_t np){
	TGraph *g_in = new TGraph(np, x_in, y_in);
	Float_t xMin=0., xMax=(np-1)*m_dt_sampling;
	for (Int_t i=0; i<np; ++i){
		Float_t i_old = x_in[i]-dt;
		if (i_old<xMin || i_old>xMax)
			y_out[i] = 0;
		else 
			y_out[i] = yMax-g_in->Eval(i_old);
	}
}
void sigShape::Traslate(TGraph* g_in, Float_t dx, Double_t *y_out, Int_t np){
	Float_t xMin=0., xMax=150.;
	Double_t *x_in = g_in->GetX();
	Double_t x_old=0.;
	for (Int_t i=0; i<np; ++i){
		x_old = x_in[i]-dx;
		if (x_old>xMin && x_old<xMax)
			y_out[i] = g_in->Eval(x_old);
		else 
			y_out[i] = 0.;
	}
}
TRandom3 sigShape::RNDstat = TRandom3(123);
void sigShape::buildSignal(Double_t *sig_mean, Double_t *sig_mean_err, Double_t *sig_out, Double_t E, Int_t np){
	for (Int_t i=0; i<np; ++i){
		Float_t mean = sig_mean[i]*(E/30000.0);
		Float_t err  = sig_mean_err[i]*TMath::Sqrt(E/30000.0);
		sig_out[i] = RNDstat.Gaus(mean, err);
	}
}
void sigShape::buildSignal(Double_t factor, Double_t *sig_mean, Double_t *sig_mean_err, Double_t *sig_out, Double_t E, Int_t np){
	
	for (Int_t i=0; i<np; ++i){
		Float_t mean = sig_mean[i]*(E/(30000.0*factor*factor));
		Float_t err  = sig_mean_err[i]*TMath::Sqrt(E/(30000.0*factor*factor));
		sig_out[i] = RNDstat.Gaus(mean, err);
	}
}
	
	
void sigShape::getAverageShape(Int_t np){
	cout << "getAverageShape ==> Start\n";
	TFile fin(m_nfin, "READ");
	TTree *tin = (TTree*)fin.Get("DATA");
	tin->SetBranchStatus("*", 0);
	tin->SetBranchStatus("pmt0_ad15",1);
	tin->SetBranchStatus("mcp1_ad12",1);
	tin->SetBranchStatus("mcp2_ad08",1);
	Float_t *pmt0_ad15 = new Float_t[np];
	Float_t *mcp1_ad12 = new Float_t[np];
	Float_t *mcp2_ad08 = new Float_t[np];
	
	tin->SetBranchAddress("pmt0_ad15",pmt0_ad15);
	tin->SetBranchAddress("mcp1_ad12",mcp1_ad12);
	tin->SetBranchAddress("mcp2_ad08",mcp2_ad08);
  
	Float_t *x    = new Float_t[np];
	Float_t *x_err= new Float_t[np];
	
	m_y_mean.clear();
	m_y_mean_err.clear();

	for (Int_t i=0; i<np;++i){
		x[i] = i*m_dt_sampling;
		x_err[i] = m_dt_sampling;
		m_y_mean.push_back(0);
		m_y_mean_err.push_back(0);	
	} 
	Float_t tmp_y=0;
	Int_t counter=0 ;	
	for (auto& iEvent : m_iEvents){
		if (counter%10==0){
			cout << "\r at entry " << iEvent.m_entry;
			cout.flush();
		}
		//if (counter > 10) break;
		tin->GetEntry(iEvent.m_entry);
		//m_h_t_ref->Fill(iEvent.m_t_ref);
		Float_t *shiftedY_pmt = new Float_t[np];
		Float_t *shiftedY_mcp1= new Float_t[np];
		Float_t *shiftedY_mcp2= new Float_t[np];
		
		Float_t yMax_mcp1 = GetPlateau(x, mcp1_ad12, 10, 25, np);
		Float_t yMax_mcp2 = GetPlateau(x, mcp2_ad08, 10, 25, np);
		Float_t yMax_pmt  = GetPlateau(x, pmt0_ad15, 10, 25, np);
		this->shift(x, mcp1_ad12, 0, shiftedY_mcp1,yMax_mcp1, np);
		this->shift(x, mcp2_ad08, 0, shiftedY_mcp2,yMax_mcp2, np);
		this->shift(x, pmt0_ad15, 0, shiftedY_pmt,yMax_pmt, np);
		Float_t lvl=0;
		Float_t t_mcp1 = sigShape::fitTime_lev(x, shiftedY_mcp1, lvl, 0.5, np);
		Float_t t_mcp2 = sigShape::fitTime_lev(x, shiftedY_mcp2, lvl, 0.5, np);
		//this->shift(x, pmt0_ad15, -iEvent.m_t_ref*m_dt_samplingyMax_pmt, np);
		Float_t t_pmt  = sigShape::fitTime_lev(x, shiftedY_pmt, lvl, 0.5, np);
		m_h_res->Fill(t_pmt-(t_mcp1+t_mcp2)/2.0);
		m_h_t_ref->Fill((t_mcp1+t_mcp2)/2.0);
		m_h_t_ref_res->Fill(iEvent.m_t_ref*m_dt_sampling-(t_mcp1+t_mcp2)/2.0);
		this->shift(x, pmt0_ad15, -(t_mcp1+t_mcp2)/2.0, shiftedY_pmt,yMax_pmt, np);
		Float_t t_pmt_2 = sigShape::fitTime_lev(x, shiftedY_pmt, lvl, 0.5, np);
		m_h_res_2->Fill(t_pmt_2);
		for (Int_t i=0; i<np; ++i){
			tmp_y = shiftedY_pmt[i];
			m_y_mean[i]    += tmp_y;
			m_y_mean_err[i]+= tmp_y*tmp_y;
			m_h_OrigShapes->Fill(x[i], tmp_y);
		}
		++counter;
		delete [] shiftedY_pmt;
	}
	cout << endl;
	fin.Close();
	for (Int_t i=0; i<np; ++i){
			m_y_mean[i] = m_y_mean[i]/(Float_t)counter;
			m_y_mean_err[i] = TMath::Sqrt(m_y_mean_err[i]/(Float_t)counter - m_y_mean[i]*m_y_mean[i]);
	}
	m_g_shape= new TGraphErrors(m_y_mean.size(), x, m_y_mean.data(), x_err, m_y_mean_err.data());
	m_g_shape->SetName("g_shape");
	m_g_shape->SetTitle("graph of average signal shape");

	delete [] pmt0_ad15;
	delete [] x;
	cout << "getAverageShape ==> Finish\n";
	
}
void sigShape::saveAverageShape(TString nfout){
	TFile fout(nfout, "RECREATE");
	TTree tout("shape", "shape");
	tout.Branch("mean", "vector<Float_t>", &m_y_mean);
	tout.Branch("StdDev", "vector<Float_t>", &m_y_mean_err);
	tout.Fill();
	fout.WriteTObject(&tout, tout.GetName(), "koverwrite");
	fout.WriteTObject(m_g_shape, m_g_shape->GetName(), "koverwrite");
	fout.WriteTObject(m_h_OrigShapes, m_h_OrigShapes->GetName(), "koverwrite");
	fout.WriteTObject(m_h_t_ref, m_h_t_ref->GetName(), "koverwrite");
	fout.WriteTObject(m_h_t_ref_res, m_h_t_ref_res->GetName(), "koverwrite");
	fout.WriteTObject(m_h_res, m_h_res->GetName(), "koverwrite");
	fout.WriteTObject(m_h_res_2, m_h_res_2->GetName(), "koverwrite");
	
	cout << "Written file: \n";


	fout.Print();
	fout.Close();

}
void sigShape::buildSignal(Float_t *sig_out,Int_t np){
	for (Int_t i=0; i<np; ++i){
		sig_out[i] = RND.Gaus(m_y_mean[i], m_y_mean_err[i]);
	}
}
void sigShape::buildSignal(Float_t *sig_out, Float_t E, Int_t np){
	for (Int_t i=0; i<np; ++i){
		Float_t mean = m_y_mean[i]*(E/30000.0);
		Float_t err  = m_y_mean_err[i]*TMath::Sqrt(E/30000.0);
		sig_out[i] = RND.Gaus(mean, err);
	}
}


void sigShape::test0(Int_t Nesp, TString nfout, TString opt_fout){
	Float_t x[1024];
	Float_t lvl =0.;
	for (Int_t i=0;i<1024;++i) x[i] = i*m_dt_sampling;
	TH1D h_test1("h_test0", "h_test0; t_HH [ns]; Counts", 1000,34.,40);
	TH1D h_lvl("h_lvl", "h_lvl; Signal Strength; Counts", 200,700,900);
	
	for (Int_t i=0; i<Nesp; ++i){
		Float_t aShape[1024];
		this->buildSignal(aShape, 800);
		//TGraph* g_aShape = new TGraph(700, x, aShape);
		//g_aShape->Print();
		//fout.WriteTObject(g_aShape, "testShape", "koverwrite");
		//Float_t t_HH = sigShape::getTime_lev(x, aShape, 0.2, lvl, 0.5, 700);
		Float_t t_HH = sigShape::fitTime_lev(x, aShape, lvl, 0.5, 700);
		
		h_lvl.Fill(lvl);
		//cout << t_HH << "\n";
		//cout << lvl << "\n";
		h_test1.Fill(t_HH);
	}
	TFile fout(nfout, opt_fout);
	fout.WriteTObject(&h_test1, h_test1.GetName(), "koverwrite");
	fout.WriteTObject(&h_lvl, h_lvl.GetName(), "koverwrite");
	
	cout << "Written file: \n";
	fout.Print();
	fout.Close();
}

TGraphErrors sigShape::test_ResAtE(vector<Float_t> E,/* TString nfout,*/ UInt_t d_sampling, Int_t Nesp){
	Float_t * Energies= new Float_t[E.size()];
	Float_t * E_err   = new Float_t[E.size()];
	Float_t * res     = new Float_t[E.size()];
	Float_t * res_err = new Float_t[E.size()];
	Float_t x[1024];
	Float_t lvl =0.;
	//TFile fout(nfout, "RECREATE");
	for (Int_t i=0;i<1024;++i) x[i] = i*m_dt_sampling;
	Int_t k = 0;	
	for (auto  e : E){
		TH1D h_test_ResAtE(Form("h_test_ResAtE_%d",k), 
						   Form("Time Res. (E = %f MeV); t_HH [ns]; Counts)",e), 1000,35,40);
		for (Int_t i=0; i<Nesp; ++i){
			Float_t aShape[1024], bShape[1024], xx[1024];
			this->buildSignal(aShape, e, 800);
			Int_t h=0;
			k=0;
			while (h<800){
				bShape[k] = aShape[h];
				xx[k] = x[h];
				h += d_sampling;
				k += 1;
			}
			Float_t t_HH = sigShape::fitTime_lev(xx, bShape, lvl, 0.5, k);
			h_test_ResAtE.Fill(t_HH);
		}
		Energies[k]= e;
		E_err[k]   = 0.;
		res[k]     = h_test_ResAtE.GetStdDev();
		res_err[k] = h_test_ResAtE.GetStdDevError();
		//fout.WriteTObject(&h_test_ResAtE, h_test_ResAtE.GetName(), "Overwrite");
		++k;
	}
	TGraphErrors g_out(E.size(), Energies, res, E_err, res_err);
	g_out.SetName("g_resAtE");
	g_out.SetTitle("Time Res. [ns] as a function of E [MeV]");
	return g_out;
	//fout.WriteTObject(&g_out, g_out.GetName(), "Overwrite");
	//fout.Close();
}
TH1D sigShape::getResBaseline(UInt_t d_sampling, Int_t np){
	TFile fin(m_nfin, "READ");
	TTree *tin = (TTree*)fin.Get("DATA");
	tin->SetBranchStatus("*", 0);
	tin->SetBranchStatus("pmt0_ad15",1);
	//tin->SetBranchStatus("mcp1_ad12",1);
	//tin->SetBranchStatus("mcp2_ad08",1);
	Float_t *pmt0_ad15 = new Float_t[np];
	//Float_t *mcp1_ad12 = new Float_t[np];
	//Float_t *mcp2_ad08 = new Float_t[np];
	
	tin->SetBranchAddress("pmt0_ad15",pmt0_ad15);
	//tin->SetBranchAddress("mcp1_ad12",mcp1_ad12);
	//tin->SetBranchAddress("mcp2_ad08",mcp2_ad08);
  
	Float_t *x    = new Float_t[np];
	TH1D hres("hres", "hres", 1000,35,40);
	for (Int_t i=0; i<np;++i){
		x[i] = i*m_dt_sampling;
	} 
	Int_t counter=0 ;	
	for (auto& iEvent : m_iEvents){
		if (counter%100==0){
			cout << "\r at entry " << iEvent.m_entry;
			cout.flush();
		}
		//if (counter > 10) break;
		++counter;
		tin->GetEntry(iEvent.m_entry);
		Float_t *shiftedY_pmt = new Float_t[np];
		//Float_t *shiftedY_mcp1= new Float_t[np];
		//Float_t *shiftedY_mcp2= new Float_t[np];
		
		//Float_t yMax_mcp1 = GetPlateau(x, mcp1_ad12, 10, 25, np);
		//Float_t yMax_mcp2 = GetPlateau(x, mcp2_ad08, 10, 25, np);
		Float_t yMax_pmt  = GetPlateau(x, pmt0_ad15, 10, 25, np);
		//this->shift(x, mcp1_ad12, 0, shiftedY_mcp1,yMax_mcp1, np);
		//this->shift(x, mcp2_ad08, 0, shiftedY_mcp2,yMax_mcp2, np);
		Float_t lvl=0;
		
		Float_t *shiftedY2_pmt = new Float_t[np];
		Float_t *x2            = new Float_t[np];
		Int_t h=0, k=0;
		while (h<1024){
			shiftedY2_pmt[k] = pmt0_ad15[h];
			x2[k] = x[h];
			h += d_sampling;
			k += 1;
		}
		this->shift(x, shiftedY2_pmt, 0, shiftedY_pmt,yMax_pmt, k);
		
		//Float_t t_mcp1 = sigShape::getTime_lev(x , shiftedY_mcp1, m_dt_sampling, lvl, 0.5, k);
		//Float_t t_mcp2 = sigShape::getTime_lev(x , shiftedY_mcp2, m_dt_sampling, lvl, 0.5, k);
		Float_t t_pmt  = sigShape::getTime_lev(x2, shiftedY_pmt,lvl, 0.5, k);
		//cout << t_pmt << "  " << t_mcp1 << " " << t_mcp2 << endl;
		//hres.Fill(t_pmt-(t_mcp1+t_mcp2)/2.0);
		hres.Fill(t_pmt-(iEvent.m_t_ref)*m_dt_sampling);
	}
	Float_t res=hres.GetStdDev();
	Float_t res_err=hres.GetStdDevError();
	cout << endl;
	cout << res << " +/- " << res_err << endl;
	return hres;
}

#endif
