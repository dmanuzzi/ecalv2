#ifndef CHECKRESOLUTION_C
#define CHECKRESOLUTION_C

#include <iostream>
#include <TFile.h>
#include <TString.h>
#include <TTree.h>
#include <TChain.h>
#include <CaloEvent.h>
#include <myParticle.h>
#include <Cell.h>
#include <Cluster.h>
#include <TROOT.h>
#include <TSystem.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TH3D.h>
#include <TProfile.h>
#include <TProfile2D.h>
#include <vector>
#include <thread>
#include <mutex>
using namespace std;


Int_t checkResolution(TTree* tin, CaloEvent* event, TString nfout, TString option){
    
    Int_t nevents = tin->GetEntries();

  	vector<Double_t> binsE = {0,1000,3000,4000,6000,10000,15000,20000,25000,30000,35000,40000,45000,50000};
  	vector<Double_t> binsRes = {-1.0};
  	for (Int_t i=1; i<151; ++i) binsRes.push_back(binsRes[i-1]+0.02);
  	vector<Double_t> binsRegion = {0.5, 1.5, 2.5, 3.5};
    TH3D h3_resE_N1("h3_res_E_N1", "Energy Resolution (Nhits == 1); Region; TRUE_E bin [MeV]; (E_corr-TRUE_E)/TRUE_E",
                binsRegion.size()-1, binsRegion.data(),
                binsE.size()-1     , binsE.data(),
                binsRes.size()-1   , binsRes.data());
    TH3D h3_resE_N2("h3_res_E_N2", "Energy Resolution (Nhits >  1); Region; TRUE_E bin [MeV]; (E_corr-TRUE_E)/TRUE_E",
                binsRegion.size()-1, binsRegion.data(),
                binsE.size()-1     , binsE.data(),
                binsRes.size()-1   , binsRes.data());
    TH3D h3_resX_N1("h3_res_X_N1", "X Resolution (Nhits == 1); Region; TRUE_E bin [MeV]; (E_corr-TRUE_E)/TRUE_E",
                binsRegion.size()-1, binsRegion.data(),
                binsE.size()-1     , binsE.data(),
                binsRes.size()-1   , binsRes.data());
    TH3D h3_resX_N2("h3_res_X_N2", "X Resolution (Nhits >  1); Region; TRUE_E bin [MeV]; (E_corr-TRUE_E)/TRUE_E",
                binsRegion.size()-1, binsRegion.data(),
                binsE.size()-1     , binsE.data(),
                binsRes.size()-1   , binsRes.data());
    TH1D h_dt_N1("n_dt_N1", "hdt_N1; log(|t_{1seed}-t_{2seed}|)", 100, -7, 7);
    TH1D h_dt_N2("n_dt_N2", "hdt_N2; log(|t_{1seed}-t_{2seed}|)", 100, -7, 7);

    TH2D h2_XY_N1("h2_XY_N1", "h2_XY_N1", 192, -3878.4, 3878.4, 156, -3151.2,3151.2);
    h3_resE_N1.Print();
    h3_resE_N2.Print();
    h3_resX_N1.Print();
    h3_resX_N2.Print();
    

    Double_t region=0., E=0., TRUE_E=0., x=0., TRUE_x=0., TRUE_y=0., cell_size=0., y=0.;
    Int_t Ncomb = 0, Nmatch=0;
    //cout << "number of events: " << nevents << "\n";
    
    for (int i=0; i<nevents; ++i){
    	//delete event;
    	//event = 0;
    	tin->GetEntry(i);
    	auto clusters = event->getClusters();
    	if (i%2 ==0){
          cout << "\revent " << i;
          cout.flush();
        }
        vector<Int_t> id2cell;
    	for (auto it_cluster = clusters->begin(); it_cluster != clusters->end(); ++it_cluster){
            Cluster * cluster = (Cluster*)(*it_cluster);
            Int_t Nhits = cluster->getHittingPhotons().size();
            Double_t etmp = -10;
            Cell * celltmp = nullptr;
            for (auto cell : cluster->getCells()){
                if(cell->getSeedType()==1) continue;
                if(cell->getE()>etmp) {
                    etmp = cell->getE();
                    celltmp = cell;
                }
            }
            if (celltmp == nullptr) continue;
            if (find(id2cell.begin(), id2cell.end(), celltmp->getID()) == id2cell.end()) {
                //continue;
                id2cell.push_back(celltmp->getID());
                if (celltmp->getE()>100){
                    if (Nhits == 1) {
                        h_dt_N1.Fill(log(fabs(celltmp->getT()-cluster->getSeed()->getT())));
                        printf("***** %g %g %g\n",celltmp->getT(),cluster->getSeed()->getT(),celltmp->getT()-cluster->getSeed()->getT());
                    }
                    else
                        h_dt_N2.Fill(log(fabs(celltmp->getT()-cluster->getSeed()->getT())));
                }
            }
      		region = (Double_t)cluster->getSeedRegion();
    		//if (region !=2) continue;
       		E = cluster->getCalibE();
       		x = cluster->getCalibX();
            y = cluster->getCalibY();
            cell_size = cluster->getSeed()->getSize();
            auto *closerPhoton = cluster->getCloserPhoton();
        	if (closerPhoton == 0){
                ++Ncomb;
                continue;
            } 
            ++Nmatch;
    		TRUE_E = closerPhoton->getCaloE();
            auto vert = closerPhoton->getCaloVertex();
            TRUE_x = vert->getX();
            TRUE_y = vert->getY();
            
            //cout << region << "    " << TRUE_E << "  "<< E << "\n";
    		cout.flush();
    		if (cluster->getHittingPhotons(50.).size() > 1) {
                h3_resE_N2.Fill(region, TRUE_E, (E-TRUE_E)/TRUE_E);
                h3_resX_N2.Fill(region, TRUE_E, (x-TRUE_x)/cell_size);
            } 
            else {
                h3_resE_N1.Fill(region, TRUE_E, (E-TRUE_E)/TRUE_E);
                h3_resX_N1.Fill(region, TRUE_E, (x-TRUE_x)/cell_size);
                if ((E-TRUE_E)/TRUE_E < -0.2) {
                    Cell* seed = cluster->getSeed();
                    h2_XY_N1.Fill(x,y);
                    printf("REGION=%.0f     E = %.1f  TRUE_E = %.1f   x = %.1f     TRUE_x = %.1f    y = %.1f   TRUE_y = %.1f",region, E,TRUE_E,x,TRUE_x,y,TRUE_y);
                    printf("  SeedID = %d   Seed_E= %.1f", seed->getID(), seed->getE());
                    auto photons = seed->getHittingPhotons(50);
                    for (auto p: photons){
                      cout << "Ph_E = " << p->getCaloE();
                    }
                    cout << endl;
                }
            }
            
    	}    
  	}
    //cout << endl;
    cout << "Ncomb/Nmatch = " << Ncomb << "/" << Nmatch << " = " << (Double_t)Ncomb/(Double_t)Nmatch <<"\n";
   
    

    
    TFile fout(nfout, option);
    fout.WriteTObject(&h3_resE_N1, h3_resE_N1.GetName(), "overwrite");
    fout.WriteTObject(&h3_resE_N2, h3_resE_N2.GetName(), "overwrite");
    fout.WriteTObject(&h3_resX_N1, h3_resX_N1.GetName(), "overwrite");
    fout.WriteTObject(&h3_resX_N2, h3_resX_N2.GetName(), "overwrite");
    fout.WriteTObject(&h2_XY_N1, h2_XY_N1.GetName(), "overwrite");
    fout.WriteTObject(&h_dt_N1, h_dt_N1.GetName(), "overwrite");
    fout.WriteTObject(&h_dt_N2, h_dt_N2.GetName(), "overwrite");
    //TProfile2D* p2_resE = h3_resE.Project3DProfile("xy");
    //p2_resE->SetName("p2_resE");
    //p2_resE->SetTitle("Energy Resolution Profile; Region; TRUE_E bin [MeV]; PROFILE_{(E_corr-TRUE_E)/TRUE_E}");
  	//fout.WriteTObject(p2_resE, p2_resE->GetName(), "overwrite");
    /*
    for (auto& h3_resE : {h3_resE_N1, h3_resE_N2, h3_resX_N1, h3_resX_N2}){
        TString obs = "E";
        TString Obs = "Energy";
        TString axisX= "E [MeV]";
        TString axisY = "#sigma_{(E-TRUE_E)/TRUE_E)}";
        TString tag = "Nhits == 1";
        TString name= "N1";
        if (TString(h3_resE.GetName()).Contains("N2")){
            tag = "Nhits  > 1";
            name="N2";
        }
        if (TString(h3_resE.GetName()).Contains("_X_")){
            obs = "X";
            Obs= "X";
            axisX="E [MeV]";
            axisY="X [cell_size]";
        }
        map<Int_t,vector<TH1D>> mapResDistrib;
        Double_t val=0., err=0.;
        vector<TH1D> vResValues;
        for (auto iregion : {1,2,3}){
            vResValues.emplace_back(Form("h_res_"+obs+"_region%d_"+name,iregion),
                                   Form(Obs+" Resolution Region %d (%s); "+axisX+"; "+axisY,iregion, tag.Data()), 
                                   binsE.size()-1   , binsE.data());
            for (UInt_t ibinE=1; ibinE<binsE.size(); ++ibinE){
                mapResDistrib[iregion].emplace_back(Form("h_res_"+obs+"_region%d_"+obs+"bin%d_"+name,iregion,ibinE),
                                                    Form(Obs+" Resolution Region %d E #in [%.0f,%.0f] (%s); "+axisY+"; Candidates",
                                                         iregion,binsE[ibinE-1], binsE[ibinE], tag.Data()), 
                                                         binsRes.size()-1   , binsRes.data());
                for (UInt_t ibinRes=1; ibinRes<binsRes.size(); ++ibinRes){
                    val = h3_resE.GetBinContent(iregion, ibinE, ibinRes);
                    err = h3_resE.GetBinError(iregion, ibinE, ibinRes);
                    mapResDistrib[iregion][ibinE-1].SetBinContent(ibinRes,val);
                    mapResDistrib[iregion][ibinE-1].SetBinError(ibinRes,err);
                }
                int bin1 = mapResDistrib[iregion][ibinE-1].FindFirstBinAbove(mapResDistrib[iregion][ibinE-1].GetMaximum()/2);
                int bin2 = mapResDistrib[iregion][ibinE-1].FindLastBinAbove (mapResDistrib[iregion][ibinE-1].GetMaximum()/2);
                double fwhm = mapResDistrib[iregion][ibinE-1].GetBinCenter(bin2) - mapResDistrib[iregion][ibinE-1].GetBinCenter(bin1);
                vResValues[iregion-1].SetBinContent(ibinE, fwhm/2.355);
                //vResValues[iregion-1].SetBinError(ibinE, mapResDistrib[iregion][ibinE-1].GetStdDevError());  
            }
        } 
        for (auto it = mapResDistrib.begin(); it != mapResDistrib.end(); ++it){
          for (auto& ResDistrib : it->second)
              fout.WriteTObject(&(ResDistrib), 
                                  ResDistrib.GetName(), 
                                  "Overwrite");
        }
        for (auto& ResValues : vResValues){
          fout.WriteTObject(&(ResValues),ResValues.GetName(), "Overwrite");
        }
    }
    */
    fout.Print();
    fout.Close();
  	return 1;
}


#endif
