#ifndef ROOT_myTagManager_cxx
#define ROOT_myTagManager_cxx

#include <myTagManager.h>

ClassImp(myTagManager)

myTagManager::myTagManager(){
	this->initialise();
}
void myTagManager::initialise(){
	m_CodeRecoAlg["A"] = "Correct2Seed";
	m_CodeRecoAlg["B"] = "BaselineSeed";
	m_CodeRecoAlg["T"] = "Test";
	//m_CodeRegions["Rhom5"] = {"$ProjectDir/regionDef/region_byPhilip.root" , "hregions_byPhilip"};
	//m_CodeRegions["Rect3"] = {"$ProjectDir/regionDef/regions_calib.root"   , "region_rectangular"};
	
	m_CodeRegions["Rhom5"] = {"$ProjectDir/regionDef/RegionDefinitions.root", "ECAL_Rhom5"};
	m_CodeRegions["RhomB5"]= {"$ProjectDir/regionDef/RegionDefinitions.root", "ECAL_Rhom5b"};
	m_CodeRegions["Rhom4"] = {"$ProjectDir/regionDef/RegionDefinitions.root", "ECAL_Rhom4"};
	m_CodeRegions["Rect3"] = {"$ProjectDir/regionDef/RegionDefinitions.root", "ECAL_Rect3"};
	m_CodeRegions["Rect4"] = {"$ProjectDir/regionDef/RegionDefinitions.root", "ECAL_Rect4"};
	m_CodeRegions["CircB5"]= {"$ProjectDir/regionDef/RegionDefinitions.root", "ECAL_Circ5b"};
	
	m_CodeCellSize["a"] = 10.1;
	m_CodeCellSize["b"] = 15.15;
	m_CodeCellSize["c"] = 20.2;
	m_CodeCellSize["d"] = 30.3;
	m_CodeCellSize["e"] = 40.4;
	m_CodeCellSize["f"] = 60.6;
	m_CodeCellSize["g"] = 80.8;
	m_CodeCellSize["h"] =121.2;
	
	// m_CodeCellSize["a"] = 10;
	// m_CodeCellSize["b"] = 15;
	// m_CodeCellSize["c"] = 20;
	// m_CodeCellSize["d"] = 30;
	// m_CodeCellSize["e"] = 40;
	// m_CodeCellSize["f"] = 60;
	// m_CodeCellSize["g"] = 80;
	// m_CodeCellSize["h"] =120;

	m_CodeRmoliere["w"] = 13.0;
	m_CodeRmoliere["x"] = 15.0;
	m_CodeRmoliere["y"] = 30.0;
	m_CodeRmoliere["z"] = 35.0;

	m_CodeEres["1"] = make_pair(0.05, 0.01);
	m_CodeEres["2"] = make_pair(0.05, 0.02);
	m_CodeEres["3"] = make_pair(0.10, 0.01);
	m_CodeEres["4"] = make_pair(0.10, 0.02);
	m_CodeEres["5"] = make_pair(0.15, 0.01);
	m_CodeEres["6"] = make_pair(0.15, 0.02);
	m_CodeEres["7"] = make_pair(0.20, 0.01);
	m_CodeEres["8"] = make_pair(0.20, 0.02);

	m_CodeTres["00"] = 0.000;
	m_CodeTres["10"] = 0.010;
	m_CodeTres["15"] = 0.015;
	m_CodeTres["20"] = 0.020;
	m_CodeTres["30"] = 0.030;
	m_CodeTres["50"] = 0.050;
	m_CodeTres["75"] = 0.075;

	m_regions.clear();
	m_cellsize.clear();
	m_Rmoliere.clear();
	m_Tres.clear();
	m_Eres.clear();
	m_bordersX.clear();
	m_bordersY.clear();
	m_calib_inputs.clear();
	m_notes = "";
}

myTagManager::myTagManager(string fullTag){
	this->initialise();
	m_fullTag = fullTag;
	this->create();
}
void myTagManager::setFullTag(string fullTag){
	this->initialise();
	fullTag = fullTag;
	this->create();
}

void myTagManager::create(){
	vector<string> tags;
	stringstream stream_fullTag(m_fullTag.data());
	string tag;
    while (std::getline(stream_fullTag, tag, '-')) {
        //cout << tag << endl;
        tags.push_back(string(tag.data()));
    }
    m_conditions = tags[0];
   	m_recoAlg = m_CodeRecoAlg[tags[1]];
    m_regions = m_CodeRegions[tags[2]];
    if (tags[3].back() == 'h' || tags[3].back() == 'f') m_regions[1] += "_121";
    if (tags[3].back() == 'g' ) m_regions[1] += "_80";

    m_Nregions= atoi(&(tags[2].back()));
    for (Int_t i=0; i<m_Nregions; ++i){
    	m_cellsize.push_back(m_CodeCellSize[{tags[3][i]}]);
    	m_Rmoliere.push_back(m_CodeRmoliere[{tags[4][i]}]);
    	m_Eres.push_back(m_CodeEres[{tags[5][i]}]);
    	string codeTres = {tags[6][i*2], tags[6][i*2+1]};
    	m_Tres.push_back(m_CodeTres[codeTres]);
    	string nfinCalib = Form("$ProjectDir/calibrations/out/calibs_1000_alpha%.0fbeta%.0f.root", m_Eres.back().first*100, m_Eres.back().second*100);
    	vector<string> nhistCalib = {
    		Form("calib_x_l_%.2f_Rm_%.2f_profile", m_cellsize.back(), m_Rmoliere.back()),
			Form("calib_y_l_%.2f_Rm_%.2f_profile", m_cellsize.back(), m_Rmoliere.back()),
			Form("calibE_l_%.2f_Rm_%.2f"         , m_cellsize.back(), m_Rmoliere.back())
    	};
    	m_calib_inputs.emplace_back(nfinCalib, nhistCalib);
    	// printf("region: %d;  cell side: %.2f; Rm: %.2f  alpha: %.2f; beta: %.2f;  Tres: %.2f;\n",i+1, m_cellsize.back(), m_Rmoliere.back(), m_Eres.back().first, m_Eres.back().second, m_Tres.back() );
    	// printf("Calib file: %s \n", nfinCalib.data());
    	// printf("Calib hists:  %s;  %s;  %s\n", nhistCalib[0].data(),nhistCalib[1].data(),nhistCalib[2].data());	
    } 
    if (tags.size() == 8)
    	m_notes = tags[7];
    if (abs(m_cellsize[0]-15)<1){
    	m_bordersX.push_back(242.4);
    	m_bordersY.push_back(242.4);
    	// m_bordersX.push_back(333.3);
    	// m_bordersY.push_back(333.3);
    	//m_bordersX.push_back(360.0);
    	//m_bordersY.push_back(360.0);
    } else {
    	m_bordersX.push_back(242.4);
    	m_bordersY.push_back(242.4);
    	// m_bordersX.push_back(323.2);
    	// m_bordersY.push_back(323.2);
    }
    m_bordersX.push_back(3878.4);
    m_bordersY.push_back(3151.2);
    //m_bordersX.push_back(3840.0);
    //m_bordersY.push_back(3120.0);


}


#endif
