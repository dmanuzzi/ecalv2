############################################################################
# File for running Gauss with all Baseline Upgrade detectors as of May 2015
############################################################################

from Gauss.Configuration import *
from Configurables import Gauss, CondDB, LHCbApp

CondDB().Upgrade = True
if "Calo_NoSPDPRS" not in CondDB().AllLocalTagsByDataType:
    CondDB().AllLocalTagsByDataType += ["Calo_NoSPDPRS"]

Gauss().DetectorGeo  = { "Detectors": ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Ecal', 'Hcal', 'Muon', 'Magnet', 'Infrastructure' ] }
Gauss().DetectorSim  = { "Detectors": ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Ecal', 'Magnet', 'Infrastructure' ] }
Gauss().DetectorMoni = { "Detectors": ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Ecal', 'Magnet', 'Infrastructure' ] }

for det in ["Spd", "Prs","Muon"]:
    if det in Gauss().DetectorGeo["Detectors"]:
        Gauss().DetectorGeo["Detectors"].remove(det)

for det in ["Spd", "Prs","Muon"]:
    if det in Gauss().DetectorSim["Detectors"]:
        Gauss().DetectorSim["Detectors"].remove(det)

for det in ["Spd", "Prs","Muon"]:
    if det in Gauss().DetectorMoni["Detectors"]:
        Gauss().DetectorMoni["Detectors"].remove(det)

Gauss().DataType = "Upgrade"
"""

Gauss().DetectorGeo  = {'Detectors': ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Ecal','Magnet']}
Gauss.DetectorSim = {"Detectors": ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Ecal','Magnet'] }
Gauss.DetectorMoni ={"Detectors": ['Magnet'] }
"""
