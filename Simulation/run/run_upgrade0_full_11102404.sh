#!/bin/bash
. /cvmfs/lhcb.cern.ch/lib/LbLogin.sh
#. $VO_LHCB_SW_DIR/lib/LbEnv

index=$1
mkdir -p /home/LHCB-T3/dmanuzzi/newECAL/Simulation/../storage/Bd_3pi_11102404_upgrade0_full_20200225/$index
cd /home/LHCB-T3/dmanuzzi/newECAL/Simulation/../storage/Bd_3pi_11102404_upgrade0_full_20200225/$index
fileName=`ls /home/LHCB/perazzini/ECAL/Simulation.new/Run1/Full/11102404/out/$index/*.sim`
lb-run DaVinci/v50r6 python /home/LHCB-T3/dmanuzzi/newECAL/Simulation/src//readFullBrem.py -i $fileName
