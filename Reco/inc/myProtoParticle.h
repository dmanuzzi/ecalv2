#ifndef ROOT_myProtoParticle_h
#define ROOT_myProtoParticle_h
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

#include <TVector3.h>
#include <TLorentzVector.h>

#include <Vertex.h>
using namespace std;

class myProtoParticle: public TObject {
	protected:
		Long64_t m_key;
		Long64_t m_mother_key;
		Long64_t m_gd_mother_key;
		Int_t m_id;
		Int_t m_mother_id;
		Int_t m_gd_mother_id;
		Double_t m_orig_px;
		Double_t m_orig_py;
		Double_t m_orig_pz;
		Double_t m_orig_e;
		Double_t m_calo_px;
		Double_t m_calo_py;
		Double_t m_calo_pz;
		Double_t m_calo_e;

		Float_t  m_charge;
		Double_t m_mass;
		myProtoParticle * m_mother;
		myProtoParticle * m_gd_mother;
		vector<myProtoParticle*> m_daughter;
		vector<myProtoParticle*> m_gd_daughter;
		
		Vertex* m_PV;
		Vertex* m_OV;
		Vertex* m_CALOV;
		Vertex* m_ENDV;

	public:
		myProtoParticle();
		virtual ~myProtoParticle() {};
		myProtoParticle(TLorentzVector &mom, Double_t mass);

		void setKey(Long64_t k) { m_key = k; }
		void setKey(Long64_t runNumber, Long64_t evtNumber, Int_t tmpKey);
		void setMkey(Long64_t k) { m_mother_key = k; }
		void setMkey(Long64_t runNumber, Long64_t evtNumber, Int_t tmpKey);
		void setGmkey(Long64_t k) { m_gd_mother_key = k; }
		void setGmkey(Long64_t runNumber, Long64_t evtNumber, Int_t tmpKey);
		void setID(Int_t k) { m_id = k; }
		void setMid(Int_t k) { m_mother_id = k; }
		void setGmid(Int_t k) { m_gd_mother_id = k; }

		void setOrigPx(Double_t v) { m_orig_px = v; }
		void setOrigPy(Double_t v) { m_orig_py = v; }
		void setOrigPz(Double_t v) { m_orig_pz = v; }
		void setOrigE (Double_t v) { m_orig_e  = v; }
		void setCaloPx(Double_t v) { m_calo_px = v; }
		void setCaloPy(Double_t v) { m_calo_py = v; }
		void setCaloPz(Double_t v) { m_calo_pz = v; }
		void setCaloE (Double_t v) { m_calo_e  = v; }
		void setM (Double_t v) { m_mass  = v; }
		
		void setOrigMom(Double_t x, Double_t y, Double_t z);
		void setCaloMom(Double_t x, Double_t y, Double_t z);

		
		Double_t getOrigPx() { return m_orig_px; }
		Double_t getOrigPy() { return m_orig_py; }
		Double_t getOrigPz() { return m_orig_pz; }
		Double_t getOrigE()  { return m_orig_e; }
		Double_t getOrigPt2(){ return m_orig_px*m_orig_px+m_orig_py*m_orig_py; }
		Double_t getOrigPt() { return sqrt(getOrigPt2()); }
		Double_t getCaloPx() { return m_calo_px; }
		Double_t getCaloPy() { return m_calo_py; }
		Double_t getCaloPz() { return m_calo_pz; }
		Double_t getCaloE()  { return m_calo_e; }
		Double_t getCaloPt2(){ return m_calo_px*m_calo_px+m_calo_py*m_calo_py; }
		Double_t getCaloPt() { return sqrt(getCaloPt2()); }
		Double_t getM()      { return m_mass; }
		Double_t getTatZ(Double_t z);
 		void getXYatZ(Double_t &x, Double_t &y, Double_t z);

		void getOrigMom(TVector3 &mom) { mom.SetXYZ(m_orig_px,m_orig_py,m_orig_pz); }
		void getCaloMom(TVector3 &mom) { mom.SetXYZ(m_calo_px,m_calo_py,m_calo_pz); }

    	Long64_t getKey() { return m_key; }
		Long64_t getMkey() { return m_mother_key; }
    	Long64_t getGmkey() { return m_gd_mother_key; }
    	Int_t getID() { return m_id; }
		Int_t getMid() { return m_mother_id; }
    	Int_t getGmid() { return m_gd_mother_id; }


    	void set_mother(myProtoParticle * p)      { m_mother = p; }
    	void set_gd_mother(myProtoParticle * p)   { m_gd_mother = p; }
		void add_daughter(myProtoParticle * p)    { m_daughter.push_back(p); }
    	void add_gd_daughter(myProtoParticle * p) { m_gd_daughter.push_back(p); }
    	void sort_daughters();
    	void sort_gd_daughters();

    	myProtoParticle * get_mother()              { return m_mother; }
    	myProtoParticle * get_gd_mother()           { return m_gd_mother; }
		vector<myProtoParticle*> get_daughters()    { return m_daughter; }
    	vector<myProtoParticle*> get_gd_daughters() { return m_gd_daughter; }

		void setPV        (Vertex* vert) { m_PV    = vert; }    	
    	void setOrigVertex(Vertex* vert) { m_OV    = vert; }    	
    	void setCaloVertex(Vertex* vert) { m_CALOV = vert; }    	
    	void setEndVertex (Vertex* vert) { m_ENDV  = vert; }    	
    	Vertex* getPV()         { return m_PV; }
		Vertex* getOrigVertex() { return m_OV; }
		Vertex* getCaloVertex() { return m_CALOV; }
		Vertex* getEndVertex()  { return m_ENDV; }

		Double_t getPVX()          { return m_PV->getX();     }
		Double_t getPVY()          { return m_PV->getY();     }
		Double_t getPVZ()          { return m_PV->getZ();     }
		Double_t getPVT()          { return m_PV->getT();     }
		Double_t getPVkey()        { return m_PV->getKey();   }
		Double_t getOrigVertexX()  { return m_OV->getX();     }
		Double_t getOrigVertexY()  { return m_OV->getY();     }
		Double_t getOrigVertexZ()  { return m_OV->getZ();     }
		Double_t getOrigVertexT()  { return m_OV->getT();     }
		Double_t getOrigVertexKey(){ return m_OV->getKey();   }
		Double_t getCaloVertexX()  { return m_CALOV->getX();  }
		Double_t getCaloVertexY()  { return m_CALOV->getY();  }
		Double_t getCaloVertexZ()  { return m_CALOV->getZ();  }
		Double_t getCaloVertexT()  { return m_CALOV->getT();  }
		Double_t getCaloVertexKey(){ return m_CALOV->getKey();}
		Double_t getEndVertexX()   { return m_ENDV->getX();   }
		Double_t getEndVertexY()   { return m_ENDV->getY();   }
		Double_t getEndVertexZ()   { return m_ENDV->getZ();   }
		Double_t getEndVertexT()   { return m_ENDV->getT();   }
		Double_t getEndVertexKey() { return m_ENDV->getKey(); }

		ClassDef(myProtoParticle,1)


};


#endif
