import os, sys

firstJob = int(sys.argv[1])
lastJob  = int(sys.argv[2])
RunCond  = str(sys.argv[3])

pathIn   = ''
if 'upgrade0' in RunCond:
	pathIn = '$ProjectDir/storage/stand_Bd_3pi_nu2_20200106/evtType_11102404/'
elif 'upgrade2' in RunCond:
	pathIn = '$ProjectDir/storage/stand_Bd_3pi_nu71_20200106/evtType_11102404/'
pathOut  = '$ProjectDir/Ana/plots/'+RunCond+'/'
if not os.path.exists(pathOut): 
	os.system('mkdir %s\n'%pathOut)

files = ['testSmearing.root','testResolution.root','testCalibs.root', 'testPi0.root', 'testBd3pi.root']
#files = ['testBd3pi.root']
#files = ['testPi0.root']
#files = ['generalTrueInfo.root']
#files = ['testPi0.root', 'testBd3pi.root']
#files = ['testSmearing.root']
#files = ['testResolution.root', 'testPi0.root', 'testBd3pi.root', 'testSmearing.root']
#files = ['testResolution.root']
for file in  files:
	outputFile = pathOut+file
	inputFiles = ''
	for i in range(firstJob, lastJob):
                if i == 14: continue
		inputFiles += ' {pathIn}/{ijob}/{RunCond}/plots/{file}'.format(
			pathIn=pathIn, ijob=i, RunCond=RunCond, file=file
			)
	command = 'hadd -f {outputFile} {inputFiles}\n'.format(outputFile=outputFile, inputFiles=inputFiles) 
	#print command
	os.system(command)
