#ifndef CaloEvent_cxx
#define CaloEvent_cxx

#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <cassert>
#include <CaloEvent.h>
#include <myProtoParticle.h>
#include <myParticle.h>
#include <Cell.h>
#include <Cluster.h>
#include <Vertex.h>
#include <TLeaf.h>
#include <TFile.h>
#include <TF2.h>
#include <TCanvas.h>
#include <TTreeReader.h>
#include <TTreeReaderValue.h>
#include <TStyle.h>
#include <TList.h>
using namespace std;

ClassImp(Cell)
ClassImp(CaloEvent)
ClassImp(myProtoParticle)
ClassImp(myParticle)

ClassImp(Cluster)
ClassImp(Vertex)

//TClonesArray *CaloEvent::m_fg_particles = 0;
TClonesArray *CaloEvent::m_fg_cells     = 0;
TClonesArray *CaloEvent::m_fg_clusters  = 0;
TClonesArray *CaloEvent::m_fg_vertices  = 0;

TRandom3 CaloEvent::myRNDM = TRandom3(1234);
Double_t CaloEvent::getTimeResShape(Double_t E){
  return -6.121 + 11862 / sqrt(E);
}

myTracks::myTracks(){
  x.clear();
  y.clear();
  t.clear();
  part.clear();
}
myTracks::myTracks(Int_t n){
  myTracks();
  x.reserve(n);
  y.reserve(n);
  t.reserve(n);
  part.reserve(n);
}

void myTracks::addTrack(myParticle* p){
  x.push_back(p->getCaloVertexX());
  y.push_back(p->getCaloVertexY());
  t.push_back(p->getCaloVertexT());
  part.push_back(p);
}


CaloEvent::CaloEvent() {
  //if (!m_fg_particles)m_fg_particles = new TClonesArray("myParticle" , 10000);
  if (!m_fg_cells)    m_fg_cells     = new TClonesArray("Cell"   , 1000000);
  if (!m_fg_clusters) m_fg_clusters  = new TClonesArray("Cluster", 1000000);
  if (!m_fg_vertices) m_fg_vertices  = new TClonesArray("Vertex",  1000000);

  //m_particles = m_fg_particles;
  m_cells   = m_fg_cells;
  m_clusters= m_fg_clusters;
  m_vertices= m_fg_vertices;
  m_smearE  = false;
  m_smearT  = false;
  m_alpha   = 0.0;
  m_beta    = 0.0;
  m_sigmaT  = 0.0;
  calibs    = 0;
  m_hregions= 0;
  m_containers_true.clear();
  m_containers_rec.clear();
  //m_containers.clear();
  m_run = 0;
  m_evt = 0;
  m_nparticles_rec = m_ncells = m_nclusters = m_nvertices = 0;
  m_nameChargedContainers = {"e-","e+","pi-","pi+"};

}

CaloEvent::~CaloEvent() {
  Reset();
}

void CaloEvent::Clear(Option_t *option) {
  //m_particles->Clear(option);
   m_cells->Clear(option);
   m_clusters->Clear(option);
   m_vertices->Clear(option);
   m_cellMap.clear();
   m_clusterMap.clear();
   m_particleMap.clear();
   m_vertexMap.clear();
   m_overlapClusters.clear();
   m_SeedCells.clear();
   //m_containers.clear();
   m_containers_true.clear();
   m_containers_rec.clear();
   m_nparticles_rec = m_ncells = m_nclusters = m_nvertices = 0;
}

void CaloEvent::Reset(Option_t *) {
   //delete m_fg_particles; m_fg_particles = 0;
   delete m_fg_cells;     m_fg_cells     = 0;
   delete m_fg_clusters;  m_fg_clusters  = 0;
   delete m_fg_vertices;  m_fg_vertices  = 0;
   //m_containers.clear();
   m_containers_true.clear();
   m_containers_rec.clear();
}

CaloEvent::CaloEvent(vector<Double_t> &xs, vector<Double_t> &ys,
	                   vector<Double_t> &sizes, vector<Double_t> mRs,
	                   Int_t nregions, Double_t z) {

	if(nregions != (Int_t)xs.size()-1 ||
	   nregions != (Int_t)ys.size()-1 ||
	   nregions != (Int_t)sizes.size() ||
	   nregions != (Int_t)mRs.size()) exit(0);
	m_nregions = nregions;
  m_z = z;
	for(Int_t ireg = 0; ireg < m_nregions; ++ireg) {
		m_bordersX.push_back(xs[ireg+1]);
		m_bordersY.push_back(ys[ireg+1]);
		m_cellSizes.push_back(sizes[ireg]);
		m_molierRadiuses.push_back(mRs[ireg]);
	}
  m_nparticles_rec = m_ncells = m_nclusters = m_nvertices = 0;

}

Double_t CaloEvent::func(Double_t *var, Double_t *par) {

  Double_t x = var[0];
  Double_t y = var[1];
  Double_t f = par[0];
  f = 0.1032578;//probability of core/tail
  Double_t s = 1.57363;//
  Double_t s2 = s*s;
  Double_t R = 0.13315;
  Double_t r = sqrt(x*x+y*y);
  Double_t value = f/s2*exp(-(r*r)/s2/2) + (1-f)*2*pow(R,2)*pow(r*r+R*R,-2);
  return value/(2.0*TMath::Pi());
}

Double_t CaloEvent::fracEnergy(Double_t xMin, Double_t xMax, Double_t yMin, Double_t yMax) {

  TF2 f("f_fracEnergy",func,xMin,xMax,yMin,yMax);
  Double_t ret = f.Integral(xMin,xMax,yMin,yMax);
  return ret;
}

void CaloEvent::setGeometry(vector<Double_t> &xs, vector<Double_t> &ys,
                            vector<Double_t> &sizes, vector<Double_t> mRs,
                            Int_t nregions, Double_t z) {

   if(nregions != (Int_t)xs.size()-1 ||
      nregions != (Int_t)ys.size()-1 ||
      nregions != (Int_t)sizes.size() ||
      nregions != (Int_t)mRs.size()) exit(0);
   m_nregions = nregions;
   m_z = z;
   m_bordersX.push_back(xs[0]);
   m_bordersY.push_back(ys[0]);
   for(Int_t ireg = 0; ireg < m_nregions; ++ireg) {
      m_bordersX.push_back(xs[ireg+1]);
      m_bordersY.push_back(ys[ireg+1]);
      m_cellSizes.push_back(sizes[ireg]);
      m_molierRadiuses.push_back(mRs[ireg]);
   }

}

void CaloEvent::setGeometry(vector<Double_t> &xs, vector<Double_t> &ys,
                            vector<Double_t> &sizes, vector<Double_t> mRs,
                            Int_t nregions, Double_t z, TH2D* hregions){
  if( nregions != (Int_t)sizes.size() ||
      nregions != (Int_t)mRs.size()) exit(0);
  m_cellSizes      = sizes;
  m_molierRadiuses = mRs;
  m_nregions       = nregions;
  m_z              = z;
  m_bordersX       = xs;
  m_bordersY       = ys;
  m_hregions       = hregions;
  m_hregions->Print();
}
/*
void CaloEvent::setGeometry(vector<Double_t> &xs, vector<Double_t> &ys,
                            vector<Double_t> &sizes, vector<Double_t> mRs,
                            Int_t nregions, Double_t z, TH2Poly* hregions){
  if( nregions != (Int_t)sizes.size() ||
      nregions != (Int_t)mRs.size()) exit(0);
  m_cellSizes      = sizes;
  m_molierRadiuses = mRs;
  m_nregions       = nregions;
  m_z              = z;
  m_bordersX       = xs;
  m_bordersY       = ys;
  m_hregions       = hregions;
  m_hregions->Print();
}
*/

void CaloEvent::createCellsFromHist(){
  cout << "CaloEvent::createCellsFromHist starts \n";
  Double_t xMax    = m_bordersX.back();
  Double_t yMax    = m_bordersY.back();
  Int_t    Nsteps_x= m_hregions->GetXaxis()->GetNbins();
  Double_t Nsteps_y= m_hregions->GetYaxis()->GetNbins();
  Double_t  step   = (xMax*2.0)/(Double_t)Nsteps_x;
  for (Int_t j=0; j<Nsteps_y; ++j){
    for (Int_t i=0; i<Nsteps_x; ++i){
      Double_t x0 = -xMax + i*step;
      Double_t y0 = -yMax + j*step;
      Int_t nbin  = m_hregions->GetBin(i+1, j+1);
      Int_t region= m_hregions->GetBinContent(nbin);
      if (region < 1) continue;
      Double_t step_div = m_cellSizes[region-1];
      for (Int_t h=0, Ndiv=step/step_div; h<Ndiv; ++h){
        for (Int_t w=0; w<Ndiv; ++w){
          Double_t x1 = x0 + step_div*w;
          Double_t x2 = x0 + step_div*(w+1);
          Double_t y1 = y0 + step_div*h;
          Double_t y2 = y0 + step_div*(h+1);
	  if (abs(x1)+1e-3<m_bordersX[0] && abs(y1)+1e-3<m_bordersY[0]) continue;
	  if (abs(x1)+1e-3<m_bordersX[0] && abs(y2)+1e-3<m_bordersY[0]) continue;
	  if (abs(x2)+1e-3<m_bordersX[0] && abs(y1)+1e-3<m_bordersY[0]) continue;
	  if (abs(x2)+1e-3<m_bordersX[0] && abs(y2)+1e-3<m_bordersY[0]) continue;
          Double_t x  = (x1+x2)/2.0;
          Double_t y  = (y1+y2)/2.0;
          Int_t    row= (j << 4) + h;
          Int_t    col= (i << 4) + w;
          Cell *cell = AddCell();
          cell->setX(x);
          cell->setY(y);
          cell->setZ(m_z);
          cell->setSize(m_cellSizes[region-1]);
          cell->setID(col, row, region);
          m_cellMap[cell->getID()] = m_ncells-1;
          // printf("- CELL: %d %d %d %.3f %.3f %d %d %d %d\n", col, row, region,x,y,i,w,j,h);
          // auto id = this->getIDFromXY(x,y);
          // printf("%d %d\n", cell->getID(), id);
          // auto test = this->getCellFromID(id);
          // printf("* CELL: %d %d %d %.3f %.3f\n", test->getCol(), test->getRow(), test->getRegion(),
          //                                                  test->getX(),test->getY());
        }
      }
    }
  }
  vector<Int_t> neigh;
  neigh.reserve(20);
  for (auto it_cell=m_cells->begin(); it_cell!=m_cells->end(); ++it_cell){
    Cell * cell = (Cell*)(*it_cell);
    neigh.clear();
    this->getNeighbours(cell->getID(), neigh);
    cell->setNeighbours(neigh);
  }

  cout << "CaloEvent::createCellsFromHist ends \n";
}

/*
void CaloEvent::createCellsFromHist(){
  cout << "CaloEvent::createCellsFromHist starts \n";
  TList* bins = m_hregions->GetBins();
  Int_t Nbins = m_hregions->GetNumberOfBins();
  for (Int_t i = 0; i < Nbins; ++i){
    TH2PolyBin* bin = (TH2PolyBin*)bins->At(i);
    Double_t x1 = bin->GetXMin();
    Double_t x2 = bin->GetXMax();
    Double_t y1 = bin->GetYMin();
    Double_t y2 = bin->GetYMax();
    Int_t region= m_hregions->GetBinContent(i+1);
    Cell *cell = AddCell();
    Double_t x = (x1+x2)/2.0;
    Double_t y = (y1+y2)/2.0;
    Double_t size = m_cellSizes[region-1];
    cell->setX(x);
    cell->setY(y);
    cell->setZ(m_z);
    cell->setSize(size);
    //cell->setID(round(trunc(x*1000.)), round(trunc(y*1000)), region);
    cell->setID(i+1);
    cell->setRegion(region);

    m_cellMap[cell->getID()] = m_ncells-1;
    //m_cellsRowColID[round(trunc(x*1000))][round(trunc(y*1000))] = i+1;
    // printf("- CELL: %d %d %.2f %.2f \n", region,i,x,y);
    // auto id = this->getIDFromXY(x,y);
    // printf("%d %d\n", cell->getID(), id);
    // auto test = this->getCellFromID(id);
    // printf("* CELL: %d %.2f %.2f\n", test->getRegion(),test->getX(),test->getY());

  }

  for (auto it_cell=m_cells->begin(); it_cell!=m_cells->end(); ++it_cell){
    Cell * cell = (Cell*)(*it_cell);
    vector<Int_t> neigh;
    this->getNeighbours(cell->getID(), neigh);
    cell->setNeighbours(neigh);
  }
  // sort(m_cellXYID.begin(), m_cellXYID.end(), [](pair<vector<Double>,Int_t> a, pair<vector<Double>,Int_t> b){
  //   if (abs(a.first[1]-b.first[1])>0.0001)
  //     return (a.first[1]<b.first[1]);
  //   if (abs(a.first[0]-b.first[0])>0.0001)
  //     return (a.first[0]<b.first[0]);
  //   cout << "CaloEvent::createCellsFromHist  :  BAD GEOMETRY DEFINITION!!! EXIT\n";
  //   cout.flush();
  //   exit(0);
  // });


  cout << "CaloEvent::createCellsFromHist ends \n";
}
*/
void CaloEvent::createCells(Int_t region) {
  cout << "CaloEvent::createCells starts \n";
  if(region < 0 || region > m_nregions) exit(0);
  Cell * cell;
  Int_t nx = (Int_t)round(m_bordersX[region]/m_cellSizes[region-1]);
  Int_t ny = (Int_t)round(m_bordersY[region]/m_cellSizes[region-1]);
  Double_t x = 0, y = 0;
  for(Int_t ix = 0; ix < 2*nx; ++ix) {
    for(Int_t iy = 0; iy < 2*ny; ++iy) {
      x = -m_bordersX[region] + (ix+0.5)*m_cellSizes[region-1];
      y = -m_bordersY[region] + (iy+0.5)*m_cellSizes[region-1];
      if(fabs(x) < m_bordersX[region-1] && fabs(y) < m_bordersY[region-1])
        continue;
      cell = AddCell();
      cell->setX(x);
      cell->setY(y);
      cell->setZ(m_z);
      cell->setSize(m_cellSizes[region-1]);
      cell->setID(ix,iy,region);
      m_cellMap[cell->getID()] = m_ncells-1;
      //printf("%d %d %d %d\n",ix,iy,m_ncells-1,region);
    }
  }
  vector<Int_t> neigh;
  neigh.reserve(20);
  for (auto it_cell=m_cells->begin(); it_cell!=m_cells->end(); ++it_cell){
    cell = (Cell*)(*it_cell);
    neigh.clear();
    this->getNeighbours(cell->getID(), neigh);
    cell->setNeighbours(neigh);
  }
  cout << "CaloEvent::createCells ends \n";

}
/*
void CaloEvent::getNeighbours(Int_t cellID, vector<Int_t> &ids) {

  Cell * cell = getCellFromID(cellID);
  //printf("CHECK CELLID %d %d\n",cellID,cell->getID());
  Double_t x = cell->getX();
  Double_t y = cell->getY();
  Double_t cellSize = cell->getSize();
  Int_t tmpID = 0;
  Double_t step = *min_element(m_cellSizes.begin(),m_cellSizes.end());
  step = step*step/(*max_element(m_cellSizes.begin(),m_cellSizes.end()));
 //printf("CHEKCING CELL %d %g %g\n",cellID,x,y);
  Double_t tmpX = 0, tmpY = 0;
  for(Int_t i = -1; i < 2; ++i) {
    for(Int_t j = -1; j < 2; ++j) {
      if(i==0&&j==0) continue;
      tmpX = x + i*(0.5*cellSize + step);
      tmpY = y + j*(0.5*cellSize + step);
      tmpID = getIDFromXY(tmpX,tmpY);
      //printf("TEST %g %g %d %d %d\n",tmpX,tmpY,i,j,tmpID);
      if(tmpID < 0) continue;
      if(find(ids.begin(),ids.end(),tmpID)!=ids.end()) continue;
      cell = getCellFromID(tmpID);
      //printf("PUSHING BACK ID %d %g %g %g %g\n",tmpID,cell->getX1(),cell->getX2(),cell->getY1(),cell->getY2());
      ids.push_back(tmpID);
    }
  }
}
*/
void CaloEvent::getNeighbours(Int_t cellID, vector<Int_t> &ids) {
  Cell * cell = getCellFromID(cellID);
  //printf("CHECK CELLID %d %d\n",cellID,cell->getID());
  //for (auto id : ids)
  //  cout << id << "   ";
  //cout << endl;
  Double_t x = cell->getX();
  Double_t y = cell->getY();
  Double_t cellSize = cell->getSize();
  Int_t tmpID = 0;
  Double_t step = *min_element(m_cellSizes.begin(),m_cellSizes.end());
  step = step*step/(*max_element(m_cellSizes.begin(),m_cellSizes.end()));
  step = 0.1;
  //printf("CHEKCING CELL %d %g %g\n",cellID,x,y);
  Double_t tmpX = 0, tmpY = 0;
  vector<pair<Int_t,Int_t>> pos;
  pos.emplace_back(-1,-1);
  pos.emplace_back( 0,-1);
  pos.emplace_back( 0,+1);
  pos.emplace_back(+1,+1);

  for(UInt_t i=0; i<pos.size(); ++i) {
    for(UInt_t j=0; j<pos.size(); ++j) {
      if(pos[i].first==0 && pos[j].first==0) continue;
      tmpX = x + pos[i].first*0.5*cellSize + pos[i].second*step;
      tmpY = y + pos[j].first*0.5*cellSize + pos[j].second*step;
      tmpID = getIDFromXY(tmpX,tmpY);
      //printf("TEST %g %g %d %d %d\n",tmpX,tmpY,i,j,tmpID);
      if(tmpID < 0) continue;
      if(find(ids.begin(),ids.end(),tmpID)!=ids.end()) continue;
      //cell = getCellFromID(tmpID);
      //printf("PUSHING BACK ID %d %g %g %g %g\n",tmpID,cell->getX1(),cell->getX2(),cell->getY1(),cell->getY2());
      ids.push_back(tmpID);
    }
  }
}

void CaloEvent::getCellsWithinR(myParticle * photon, vector<Int_t> &ids, Int_t nR) {
  // cout << "CaloEvent::getCellsWithinR starts \n";
  // ids.clear();

  Vertex * vert =  photon->getCaloVertex();
  if (abs(vert->getZ()-m_z)>100) {
    cout << "CaloEvent::getCellsWithinR: We have a problem !!!\n";
  }
  Double_t x = vert->getX();
  Double_t y = vert->getY();
  Int_t cellID = getIDFromXY(x,y);
  //printf("PHOTON AT %g %g, hence CELL %d\n",x,y,cellID);
  if(cellID<0) { return; }
  ids.push_back(cellID);
  Int_t tmpSize = 0;
  Cell * cell;
  cell = getCellFromID(cellID);

  //Double_t mR = *min_element(m_molierRadiuses.begin(),m_molierRadiuses.end()); ///?????????
  Double_t mR = m_molierRadiuses[cell->getRegion()-1];
  //printf("MOLIERE RADIUS %g %g\n",mR,mR*nR);
  while(1) {
    tmpSize = (Int_t) ids.size();
    // printf("SIZE COMPARISON 1 %d %d\n",tmpSize,(Int_t)ids.size());
    auto newids = ids;
    for(auto id: newids){
      cell = getCellFromID(id);
      for_each(cell->getNeigBegin(), cell->getNeigEnd(), [&ids](Int_t xx){
        if (find(ids.begin(), ids.end(), xx) == ids.end())
          ids.push_back(xx);
      });
      //getNeighbours(id,ids);
    }
    //printf("SIZE COMPARISON 2 %d %d\n",tmpSize,(Int_t)ids.size());
    for(vector<Int_t>::iterator it = ids.begin() ; it != ids.end(); ) {
      cell = getCellFromID(*it);
      // printf("CHECKING MOLIERE DISTANCE FOR CELL %d\n",cell->getID());
      if(cell->getDistance(x,y) > nR*mR) {
        // printf("REMOVING CELL %d %g %g because at dstance %g\n",*it,cell->getX(),cell->getY(),cell->getDistance(x,y));
        //auto oldit = it;
        it = ids.erase(it);
      }
      else
        ++it;
    }
    // printf("SIZE COMPARISON 3 %d %d\n",tmpSize,(Int_t)ids.size());
    if(tmpSize==(Int_t)ids.size()) break;

  }
  // cout << "CaloEvent::getCellsWithinR ends \n";
  // cout.flush();
}

vector<Cluster*> CaloEvent::getClustersWithinR(Double_t x, Double_t y, Double_t R){
  vector<Cluster*> ret;
  for (auto it_cluster = m_clusters->begin(); it_cluster != m_clusters->end(); ++it_cluster){
    Cluster* cluster = (Cluster*)(*it_cluster);
    if (pow(x-cluster->getCalibX(),2)+pow(y-cluster->getCalibY(),2)>R*R) continue;
    ret.push_back(cluster);
  }
  return ret;
}



Int_t CaloEvent::getIDFromXY(Double_t x, Double_t y) {
  //cout << "CaloEvent::getIDFromXY starts\n";
  //cout.flush();
  if (abs(x) < m_bordersX[0]     && abs(y) < m_bordersY[0])     return -1;
  if (abs(x) > m_bordersX.back() || abs(y) > m_bordersY.back()) return -1;
  /*
  if (m_hregions != 0){
    auto ibin = m_hregions->FindBin(x,y);
    auto region = m_hregions->GetBinContent(ibin);
    return (region>0)? ibin : -1;
    //auto it = lower_bound(m_cellXYID.begin(), m_cellXYID.end(), [](pair<vector<Double_t>, Int_t> a, pair<vector<Double_t>, Int_t> b){});
  }
  */

  if (m_hregions != 0){
    //printf("x=%.3f y=%.3f \n", x,y);
    Int_t nbin  = m_hregions->FindBin(x,y);
    //cout << "nbin: " << nbin << endl;
    Int_t region= m_hregions->GetBinContent(nbin);
    //cout << "region: " << region << endl;
    if (region<0.5) return -1;
    Int_t xbin, ybin, zbin;
    m_hregions->GetBinXYZ(nbin, xbin, ybin, zbin);
    Int_t i=xbin-1, j=ybin-1;
    //printf("i=%d j=%d\n", i,j);
    Double_t xMax    = m_bordersX.back();
    Double_t yMax    = m_bordersY.back();
    Int_t    Nsteps_x= m_hregions->GetXaxis()->GetNbins();
    Double_t  step   = (xMax*2.0)/(Double_t)Nsteps_x;
    Int_t w = trunc(abs(x + xMax - i*step)/m_cellSizes[region-1]);
    Int_t h = trunc(abs(y + yMax - j*step)/m_cellSizes[region-1]);
    //printf("w=%d h=%d\n", w,h);
    //cout << "CaloEvent::getIDFromXY ends\n";
    //cout.flush();
    return (((i << 4) + w) << 18) + (((j << 4) + h) << 4) + region;
  }

   Int_t region = 0;
   if     (abs(x) < m_bordersX[0] && abs(y) < m_bordersY[0]) return -1;
   else if(abs(x) < m_bordersX[1] && abs(y) < m_bordersY[1]) region = 1;
   else if(abs(x) < m_bordersX[2] && abs(y) < m_bordersY[2]) region = 2;
   else if(abs(x) < m_bordersX[3] && abs(y) < m_bordersY[3]) region = 3;
   else return -1;
   Double_t cellSize = m_cellSizes[region-1];
   Int_t col = (Int_t)((x+m_bordersX[region])/cellSize);
   Int_t row = (Int_t)((y+m_bordersY[region])/cellSize);
   return ( (col << 18) + (row << 4) + region );
}



Cell * CaloEvent::getCellFromID(Int_t id) {
  if(id <= 0) {
      //printf("ID OUT OF RANGE\n");
      return 0;
  }
  auto it = m_cellMap.find(id);
  Int_t pos = it->second;
  if (it == m_cellMap.end()){
    printf("CaloEvent::getCellFromID  cell ID: %d not found !!!!!!!!!!!!!!!!!!!!!!!!!!\n", id);
    return 0;
  }
  return (Cell *) m_cells->ConstructedAt(pos);
}
Cluster * CaloEvent::getClusterFromID(Int_t id){
  if(id <= 0) {
      return 0;
  }
  auto it = m_clusterMap.find(id);
  Int_t pos = m_clusterMap.find(id)->second;

  if (it == m_clusterMap.end()){
    printf("CaloEvent::getClusterFromID  cluster ID: %d not found !!!!!!!!!!!!!!!!!!!!!!!!!!\n", id);
    return 0;
  }
   return (Cluster *) m_clusters->ConstructedAt(pos);
}
vector<Cluster*> CaloEvent::getClusterFromXY(Double_t x, Double_t y){
  vector<Cluster*> ret;
  Int_t id = this->getIDFromXY(x,y);
  if (id<0) return ret;
  Cell* cell = this->getCellFromID(id);
  if (cell==0) return ret;
  auto seeds = cell->getNearSeedsID();
  auto nearSeeds = seeds.size();
  if (nearSeeds==0) return ret;
  map<Double_t, Cluster*> map_dist_cluster;
  for (auto seed : seeds){
    auto cluster = this->getClusterFromID(seed);
    Double_t tmp_dist = pow(x-cluster->getCalibX(),2) + pow(y-cluster->getCalibY(),2);
    map_dist_cluster[tmp_dist] = cluster;
  }
  for (auto it : map_dist_cluster){
    ret.push_back(it.second);
  }
  return ret;
  // if (nearSeeds==1) return this->getClusterFromID(seeds[0]);
  // Double_t dist = 999999., tmp_dist=0.;
  // Cluster *closerCluster=0, *cluster=0;
  //   if (tmp_dist>dist) continue;
  //   dist = tmp_dist;
  //   closerCluster = cluster;
  // }
  // return closerCluster;
}
// myParticle * CaloEvent::getParticleFromKey(Long64_t key) {
//    if(key <= 0) {
//       //printf("KEY OUT OF RANGE\n");
//       return NULL;
//    }
//    Int_t pos = m_particleMap.find(key)->second;
//    return (myParticle *) m_particles->ConstructedAt(pos);
// }

Double_t CaloEvent::getEnergyFraction(myParticle * photon, Cell * cell) {

  if(!cell) { /*printf("NO CELL IDENTIFIED\n");*/ return -1; }
  Vertex * vert =  photon->getCaloVertex();
  Double_t x = vert->getX();
  Double_t y = vert->getY();
  Int_t region = cell->getRegion();
  Double_t mR = m_molierRadiuses[region-1];
  Double_t xMin = (cell->getX1()-x)/mR,
           xMax = (cell->getX2()-x)/mR,
           yMin = (cell->getY1()-y)/mR,
           yMax = (cell->getY2()-y)/mR;
  //printf("%d %g %g %g %g %g %g %g %g\n",region,x,y,cell->getX(),cell->getY(),xMin,xMax,yMin,yMax);
  Double_t frac = fracEnergy(xMin,xMax,yMin,yMax);
  return frac;

}

void CaloEvent::distributeEnergy(myParticle * photon) {
  //cout << "CaloEvent::distributeEnergy starts \n";
  cout.flush();
  vector<Int_t> ids;
  getCellsWithinR(photon,ids,5);
  if(ids.size() == 0) return;
  Cell * cell;
  Double_t frac = 0.;
  //printf("DISRTIBUTING ENERGY OF PHOTON %lld with Energy %g\n",photon->getKey(),photon->getCaloE());

  for(auto id: ids) {
    cell = getCellFromID(id);
    frac = getEnergyFraction(photon,cell);

    if (frac*photon->getCaloE() < 1) continue;
    //printf("GIVING %g MeV to Cell %d\n",frac*photon->getCaloE(),cell->getID());
    cell->addPhoton(photon,frac);
  }
  //cout << "CaloEvent::distributeEnergy ends \n";
  cout.flush();
}

void CaloEvent::Build() {
  cout << "CaloEvent::Build starts\n";
  if (m_run == 0 && m_evt == 0 ){
    cout << "CaloEvent::Build ->  Warning: have you set eventNumber and runNumber ????\n";
    return;
  }
  Int_t ObjectNumber = TProcessID::GetObjectCount();


  if (m_bordersX.size() == 0 || m_bordersY.size() == 0 || m_cellSizes.size() == 0 || m_molierRadiuses.size() == 0){
    cout << "=========== ERROR: Geometry is no correctly set. CaloEvent::Build is NOT EXECUTED\n";
    cout.flush();
    return;
  }

  // check geometry
  if (m_hregions != 0){
    createCellsFromHist();
  } else {
    createCells(1);
    createCells(2);
    createCells(3);
  }
  vector<vector<myParticle>*> conts;
  if (m_containers_true.find("gamma") != m_containers_true.end()) conts.push_back(m_containers_true.at("gamma"));
  if (m_containers_true.find("e-")    != m_containers_true.end()) conts.push_back(m_containers_true.at("e-"   ));
  if (m_containers_true.find("e+")    != m_containers_true.end()) conts.push_back(m_containers_true.at("e+"   ));
  for (auto& cont : conts){
    for (auto& part : *cont){

      if (part.getIsSig()!=1) continue;

      if (part.getOrigPz()<0)          continue;
      if (part.getInCaloAcc()!=1)      continue;
      if (part.getHitCaloSensDet()!=1) continue;
      if (part.getAliveAtCaloZ()!=1)   continue;
      if (getIDFromXY(part.getCaloVertexX(),part.getCaloVertexY()) < 0 ) continue;
      distributeEnergy(&part);
    }
  }
  TProcessID::SetObjectCount(ObjectNumber);
  this->setTracks();
  cout.flush();
  cout << "CaloEvent::Build ends\n";
  cout.flush();
}


Cell * CaloEvent::AddCell() {
   TClonesArray & cells = *m_cells;
   Cell *cell = new(cells[m_ncells++]) Cell();
   return cell;
}

Cluster * CaloEvent::AddCluster(Int_t seedID){
  TClonesArray & clusters = *m_clusters;
  Cluster *cluster = new(clusters[m_nclusters++]) Cluster();
  cluster->setID(seedID);
  cluster->setZ(m_z);
  m_clusterMap[seedID] = m_nclusters-1;
  return cluster;
}

Cluster * CaloEvent::AddCluster(Cell *SeedCell){
  //cout << "CaloEvent::AddCluster starts\n";
  TClonesArray & clusters = *m_clusters;
  Cluster *cluster = new(clusters[m_nclusters++]) Cluster(SeedCell);
  m_clusterMap[cluster->getID()] = m_nclusters-1;
  for (auto it_cellID = SeedCell->getNeigBegin(); it_cellID != SeedCell->getNeigEnd(); ++it_cellID){
    Cell * cell = getCellFromID(*it_cellID);
    //printf("\nID: %d      region: %d    ", *it_cellID, (*it_cellID) & 15);
    //printf("%d %f %f %f", cell->getID(), cell->getX(), cell->getY(), cell->getE());
    if (cell->getE()<1) continue;
    cluster->addCell(cell);
  }
  cluster->finalize(calibs);
  //cout << "CaloEvent::AddCluster ends\n";
  return cluster;
}

Vertex * CaloEvent::AddVertex(Double_t x,Double_t y,Double_t z,Double_t t){
  TClonesArray & vertices = *m_vertices;
  Vertex *vertex = new(vertices[m_nvertices++]) Vertex(x,y,z,t);
  vertex->setRun(m_run);
  vertex->setEvt(m_evt);
  vertex->setKey(m_nvertices);  return vertex;
}
Vertex  * CaloEvent::FindVertex(Double_t x,Double_t y,Double_t z,Double_t t){
  for (auto it_vert=m_vertices->begin(); it_vert != m_vertices->end(); ++it_vert){
    Vertex * vert = (Vertex*)(*it_vert);
    if (abs(vert->getT()-t)>1.e-4) continue;
    if (abs(vert->getZ()-z)>1.e-4) continue;
    if (abs(vert->getY()-y)>1.e-4) continue;
    if (abs(vert->getX()-x)>1.e-4) continue;
    return vert;
  }
  return 0;
}

Bool_t CaloEvent::dumpCells_csv(TString nfout, TString opt, Int_t region, Double_t E_th){
  ofstream os;
  if (opt == "RECREATE" || opt == "recreate")
    os.open(nfout, ios_base::out | ios_base::trunc);
  else if (opt == "UPDATE" || opt == "update")
    os.open(nfout, ios_base::out | ios_base::app);

  for (auto it_cell=m_cells->begin(); it_cell!=m_cells->end(); ++it_cell){
    Cell * cell = (Cell*)(*it_cell);
    if (cell->getRegion() != region) continue;
    os <<m_run<<","<<m_evt<<","<<cell->getRow()<<","<<cell->getCol()<<","<<cell->getE()<<","<<cell->getT_Eavg()<<",";
    Bool_t hasHit = (cell->getHittingPhotons(E_th).size()==0)?false:true;
    if (hasHit) os<<1;
    else os<<0;
    os<<endl;
  }
  os.close();
  return true;
}

void CaloEvent::doReco(Double_t alpha, Double_t beta, Double_t sigmaT, Double_t factor_Tres){
    m_alpha = alpha;
    m_beta  = beta;
    m_sigmaT= sigmaT;
    if (m_alpha>1e-3 || m_beta>1e-4) m_smearE = true;
    if (abs(m_sigmaT+1) <1e-6) m_smearT = -1;
    if (abs(m_sigmaT+2) <1e-6) m_smearT = -2;
    if (abs(m_sigmaT+3) <1e-6) m_smearT = -3;
    if (abs(m_sigmaT)   <1e-6) m_smearT =  0;
    if (    m_sigmaT    >1e-6) m_smearT = +1;
    cout << "RNDM Seed: " << CaloEvent::myRNDM.GetSeed() << endl;
    //int counter=0;
    for (auto it_cell = m_cells->begin(); it_cell != m_cells->end(); ++it_cell){ // loop on all cells
      Cell * cell = (Cell*)(*it_cell);
      if (m_smearT==-1)
        cell->smearTimeFromShape(calibs, factor_Tres);
      if (m_smearT==0)
        cell->setT(cell->getT_Eavg());
      if (m_smearT==1){
          cell->setT(cell->getT_Eavg() + CaloEvent::myRNDM.Gaus(0.0, m_sigmaT)*factor_Tres);
      }
      if (m_smearT==-3)
        cell->setT(cell->getT_Eavg()+CaloEvent::myRNDM.Gaus(0.0, (61.28+133.9/sqrt(cell->getE()/1000.))/1000.*factor_Tres)); // time resolution formula by: https://indico.cern.ch/event/869209/contributions/3664603/attachments/1958296/3253712/SPACAL_WG_TestBeam.pdf
      //printf("\n%d) alpha: %.2f  beta: %.2f   sigmaT: %.3f  TresFactor: %.2f    ",++counter, m_alpha, m_beta, m_sigmaT, factor_Tres);
      for (auto hit = cell->m_mapPhotons.begin(); hit != cell->m_mapPhotons.end(); ++ hit){
        if (m_smearE){
          //printf("ID: %d, Etrue: %.3f",cell->getID(), hit->second.m_e);
          hit->second.m_e += (myRNDM.Gaus(0.0, m_alpha*sqrt(hit->second.m_e*1000.))+
                              myRNDM.Gaus(0.0, m_beta*hit->second.m_e)         );
          //hit->second.m_e += CaloEvent::myRNDM.Gaus(0.0, m_alpha*sqrt(hit->second.m_e*1000.));
          if (hit->second.m_e<0.0) hit->second.m_e=0.0;
          //printf("  Erec: %.3f\n",hit->second.m_e);
        }
        // if (m_smearT==1){
        //   hit->second.m_t += CaloEvent::myRNDM.Gaus(0.0, m_sigmaT);
        // }
      }
      if (m_smearT==-2)
        cell->setT(cell->getMaxDepositTime()+
                   CaloEvent::myRNDM.Gaus(0.0, this->getTimeResShape(cell->getE())*factor_Tres));
    }
}

void CaloEvent::doReco(vector<pair<Double_t,Double_t>> Eres, vector<Double_t> Tres){
  Double_t sigmaT= 0.000;
  Double_t alpha = 0.000;
  Double_t beta  = 0.000;
  cout << "RNDM Seed: " << CaloEvent::myRNDM.GetSeed() << endl;
  //int counter = 0;
  for (auto it_cell = m_cells->begin(); it_cell != m_cells->end(); ++it_cell){ // loop on all cells
    Cell * cell = (Cell*)(*it_cell);
    Int_t iregion= cell->getRegion()-1;
    sigmaT = Tres[iregion];
    alpha  = Eres[iregion].first;
    beta   = Eres[iregion].second;
    if (sigmaT < 0.001)
      cell->setT(cell->getT_Eavg());
    else
      cell->setT(cell->getT_Eavg() + CaloEvent::myRNDM.Gaus(0.0, sigmaT));
    if (alpha < 0.001 && beta < 0.001) continue;
    //printf("\n%d) alpha: %.2f  beta: %.2f   sigmaT: %.3f  TresFactor: 0.00    ",++counter, alpha, beta, sigmaT);
    for (auto hit = cell->m_mapPhotons.begin(); hit != cell->m_mapPhotons.end(); ++ hit){
      //printf("ID: %d, Etrue: %.3f",cell->getID(), hit->second.m_e);
      hit->second.m_e += (myRNDM.Gaus(0.0, alpha*sqrt(hit->second.m_e*1000.))+
                          myRNDM.Gaus(0.0, beta*hit->second.m_e)            );
      //hit->second.m_e += CaloEvent::myRNDM.Gaus(0.0, alpha*sqrt(hit->second.m_e*1000.));
      if (hit->second.m_e<0.0) hit->second.m_e=0.0;
      //printf("  Erec: %.3f\n",hit->second.m_e);
    }
  }
}

void CaloEvent::setSeeds_LocalMaxima(Double_t Eth){
  Int_t count = 0;
  Int_t cellID =0;
  auto f_addNearSeed = [&cellID](Cell * neigh_cell){
    neigh_cell->addNearSeed(cellID);
    //cout << cellID << ", " << neigh_cell->getX() << ", " <<  neigh_cell->getY() << "\n";
  };
  for (auto it_cell = m_cells->begin(); it_cell != m_cells->end(); ++it_cell){ // loop on all cells
    Cell * cell = (Cell*)(*it_cell);
    cell->setSeedType(0);
    Double_t E = cell->getE();
    // if (E<Eth) continue;
    Double_t RT2 = cell->getX()*cell->getX()+cell->getY()*cell->getY();
    Double_t ET = sqrt(RT2/(RT2+m_z*m_z))*cell->getE();
    if (ET<Eth) continue;
    if (cell->getNearSeedsN()>0) continue;

    Bool_t isSeed=true;
    isSeed = isSeed;
    cellID = cell->getID();
    vector<Int_t> IDs_neig = cell->getNeighbours();
    //getNeighbours(cellID, IDs_neig);
    vector<Cell*>neigh_cells;
    neigh_cells.reserve(8);
    for (auto ID_neig : IDs_neig){
      Cell * neigh_cell = getCellFromID(ID_neig);
      if (neigh_cell->getE() > E) {
        isSeed = false;
        break;
      }
      neigh_cells.push_back(neigh_cell);
    }
    if (isSeed==false) continue;
    ++count;
    m_SeedCells.push_back(cell);
    //cout << "CaloEvent::setSeeds_LocalMaxima" << cell->getX() << "   " << cell->getY() << endl;
    cell->setSeedType(1);
    //cell->setNeighbours(IDs_neig);
    cell->addNearSeed(cellID);
    for_each(neigh_cells.begin(), neigh_cells.end(), f_addNearSeed);
  }
  cout << "================= Number of seeds: " << count << "\n";
}
void CaloEvent::setSeeds_2Seed(Double_t Eth){
  Int_t count   = 0;
  Int_t ID1seed = 0;
  Int_t ID2seed = 0;
  auto f_addNearSeed = [&ID2seed, this](Int_t neigh_cellID){
    Cell* neigh_cell = this->getCellFromID(neigh_cellID);
    neigh_cell->addNearSeed(ID2seed);
  };
  vector<Cell*> SeedCells2;
  SeedCells2.reserve(50);
  Double_t Emax = 0., neigh_cellE=0.;
  Cell *cell2seed = 0;
  for (auto it_seed = m_SeedCells.begin(); it_seed != m_SeedCells.end(); ++it_seed){ // loop on all cells
    Cell * cell1seed = (Cell*)(*it_seed);
    ID1seed = cell1seed->getID();
    Emax = Eth;
    cell2seed = 0;
    vector<Int_t> neigh_cellIDs = cell1seed->getNeighbours();
    //getNeighbours(cell1seed->getID(), neigh_cellIDs);
    for (auto neigh_cellID : neigh_cellIDs){
      if (neigh_cellID == ID1seed) continue;
      Cell * neigh_cell = getCellFromID(neigh_cellID);
      neigh_cellE = neigh_cell->getE();
      if (neigh_cell->getSeedType() !=0 ) continue;
      if (neigh_cellE<Emax) continue;
      Emax = neigh_cellE;
      cell2seed = neigh_cell;
    }
    if (cell2seed == 0) continue;
    ++count;
    SeedCells2.push_back(cell2seed);
    cell2seed->setSeedType(5);
    cell1seed->setSeedType(4);
    ID2seed = cell2seed->getID();
    cell2seed->addNearSeed(ID2seed);
    //getNeighbours(ID2seed, neigh2seedIDs);
    //cell2seed->setNeighbours(neigh2seedIDs);
    //for_each(neigh2seedIDs.begin(), neigh2seedIDs.end(), f_addNearSeed);
    for_each(cell2seed->getNeigBegin(), cell2seed->getNeigEnd(), f_addNearSeed);
  }
  m_SeedCells.insert(m_SeedCells.end(), SeedCells2.begin(), SeedCells2.end());
  cout << "================= Number of 2Seeds: " << count << "\n";
}

Cell* CaloEvent::find2Seed_mergedPi0(Cell* seed1){
  Cell*    seed2    = 0 ;
  Double_t seed2E   = 0.;
  if (seed1->getSeedType() != 4) { return seed2; }
  Int_t seed1ID     = seed1->getID();
  auto nearSeed1IDs = seed1->getNearSeedsID();
  for (auto tmp_seed2ID : nearSeed1IDs){
    if (tmp_seed2ID == seed1ID) continue;
    Cell* tmp_seed2 = getCellFromID(tmp_seed2ID);
    if (tmp_seed2->getSeedType() != 5) continue;
    if (tmp_seed2->getE() < seed2E)    continue;
    seed2   = tmp_seed2;
    seed2E  = tmp_seed2->getE();
  }
  return seed2;
}

void CaloEvent::turnOff2Seeds_mergedPi0(){
  cout << "CaloEvent::turnOff2Seeds_mergedPi0 starts\n";
  Double_t pi0_px=0., pi0_py=0., pi0_pz=0., pi0_E=0., pi0_M2=0., pi0_M=0;
  Int_t counter=0;
  for (auto seed1 : m_SeedCells){
    if (seed1->getSeedType() != 4) continue;
    Cell* seed2 = find2Seed_mergedPi0(seed1);
    if (seed2 == 0) {
      seed1->setSeedType(1);
      if (seed1->getNearSeedsN() != 1){
        printf(")))))))))))))))))))))))))))  x = %.2f ; y = %.2f; NnearSeeds = %d \n", seed1->getX(), seed1->getY(), seed1->getNearSeedsN());
        cout << "CaloEvent::turnOff2Seeds_mergedPi0 : WARNING! : seed2 is supposed to different from 0 here!\n";
      }
      continue;
    }

    Cluster *cluster_1 = getClusterFromID(seed1->getID());
    Cluster *cluster_2 = getClusterFromID(seed2->getID());
    pi0_px = cluster_1->getPx() + cluster_2->getPx();
    pi0_py = cluster_1->getPy() + cluster_2->getPy();
    pi0_pz = cluster_1->getPz() + cluster_2->getPz();
    pi0_E  = cluster_1->getCalibE() + cluster_2->getCalibE();
    pi0_M2 = pi0_E*pi0_E-pi0_px*pi0_px-pi0_py*pi0_py-pi0_pz*pi0_pz;
    pi0_M  = (pi0_M2>0.)? sqrt(pi0_M2) : 0.;
    //cout << pi0_M << "         " << 2*m_z*pi0_M/pi0_E << "             " << 1.8*seed1->getSize() << endl;
    if (pi0_M > 95 && pi0_M <215 && 2*m_z*pi0_M/pi0_E < 1.8*seed1->getSize()) continue;
    seed2->setSeedType(0);
    for(auto ID : seed2->getNeighbours()){
      Cell* cell = this->getCellFromID(ID);
      cell->removeNearSeed(seed2->getID());
    }
    seed2->removeNearSeed(seed2->getID());

    auto it = find(m_SeedCells.begin(), m_SeedCells.end(), seed2);
    m_SeedCells.erase(it);
    seed1->setSeedType(1);
    ++counter;
  }
  cout << "Number of 2Seed switched off "<< counter << "\n";
  cout << "CaloEvent::turnOff2Seeds_mergedPi0 ends\n";
}

void CaloEvent::setSeeds_Correct2Seed(Double_t Eth){
  Int_t count   = 0;
  Int_t ID1seed = 0;
  Int_t ID2seed = 0;
  auto f_addNearSeed = [&ID2seed, this](Int_t neigh_cellID){
    Cell* neigh_cell = this->getCellFromID(neigh_cellID);
    neigh_cell->addNearSeed(ID2seed);
  };
  vector<Cell*> SeedCells2;
  SeedCells2.reserve(50);
  Double_t Emax = 0., neigh_cellE=0.;
  Cell *cell2seed = 0;
  for (auto it_seed = m_SeedCells.begin(); it_seed != m_SeedCells.end(); ++it_seed){ // loop on all cells
    Cell * cell1seed = (Cell*)(*it_seed);
    ID1seed = cell1seed->getID();
    if (cell1seed->getHittingPhotons().size() == 0) continue;
    Emax = Eth;
    cell2seed = 0;
    vector<Int_t> neigh_cellIDs = cell1seed->getNeighbours();
    //getNeighbours(cell1seed->getID(), neigh_cellIDs);
    for (auto neigh_cellID : neigh_cellIDs){
      if (neigh_cellID == ID1seed) continue;
      Cell * neigh_cell = getCellFromID(neigh_cellID);
      neigh_cellE = neigh_cell->getE();
      if (neigh_cellE<Emax) continue;
      Emax = neigh_cellE;
      if (neigh_cell->getHittingPhotons(Eth).size()<1) continue;
      cell2seed = neigh_cell;
    }
    if (cell2seed == 0) continue;
    ++count;
    SeedCells2.push_back(cell2seed);
    cell2seed->setSeedType(2);
    ID2seed = cell2seed->getID();
    cell2seed->addNearSeed(ID2seed);
    //vector<Int_t> neigh2seedIDs;
    //getNeighbours(ID2seed, neigh2seedIDs);
    // cell2seed->setNeighbours(neigh2seedIDs);
    //for_each(neigh2seedIDs.begin(), neigh2seedIDs.end(), f_addNearSeed);
    for_each(cell2seed->getNeigBegin(),cell2seed->getNeigEnd(), f_addNearSeed);
  }
  m_SeedCells.insert(m_SeedCells.end(), SeedCells2.begin(), SeedCells2.end());
  cout << "================= Number of Correct2Seeds: " << count << "\n";
}

void CaloEvent::setSeeds_TruePhoton(Double_t Eth){
  Int_t count  =0;
  Int_t cellID =0;
  Bool_t isSeed=false;
  auto f_addNearSeed = [&cellID](Cell * neigh_cell){
    neigh_cell->addNearSeed(cellID);
  };
  auto f_checkTrueSeed = [&isSeed, Eth](myProtoParticle* photon){
    if (photon->getCaloE()>Eth) isSeed = true;
  };
  for (auto it_cell = m_cells->begin(); it_cell != m_cells->end(); ++it_cell){ // loop on all cells
    Cell * cell = (Cell*)(*it_cell);
    cell->setSeedType(0);
    auto photons = cell->getHittingPhotons(Eth);
    isSeed=false;
    for_each(photons.begin(), photons.end(), f_checkTrueSeed);
    if (isSeed==false) continue;
    ++count;
    cellID = cell->getID();
    vector<Int_t> IDs_neig = cell->getNeighbours();
    //getNeighbours(cellID, IDs_neig);
    vector<Cell*>neigh_cells;
    neigh_cells.reserve(8);
    for (auto ID_neig : IDs_neig){
      Cell * neigh_cell = getCellFromID(ID_neig);
      neigh_cells.push_back(neigh_cell);
    }
    m_SeedCells.push_back(cell);
    cell->setSeedType(1);
    //cell->setNeighbours(IDs_neig);
    cell->addNearSeed(cellID);
    for_each(neigh_cells.begin(), neigh_cells.end(), f_addNearSeed);
  }
  cout << "================= Number of seeds TruePhoton: " << count << "\n";
}

void CaloEvent::setSeeds_TruePhotonFromPi0(Double_t Eth){
  Int_t count = 0;
  Int_t cellID =0;
  auto f_addNearSeed = [&cellID](Cell * neigh_cell){
    neigh_cell->addNearSeed(cellID);
  };
  for (auto it_cell = m_cells->begin(); it_cell != m_cells->end(); ++it_cell){ // loop on all cells
    Cell * cell = (Cell*)(*it_cell);
    cell->setSeedType(0);
    Double_t E = cell->getE();
    if (E<Eth) continue;
    Bool_t isSeed=false;
    isSeed = isSeed;
    auto photons = cell->getHittingPhotons();
    for (auto photon : photons){
      if (photon->getMid() != 111) continue;
      isSeed=true;
      break;
    }
    if (isSeed==false) continue;
    ++count;
    cellID = cell->getID();
    vector<Int_t> IDs_neig = cell->getNeighbours();
    //getNeighbours(cellID, IDs_neig);
    vector<Cell*>neigh_cells;
    neigh_cells.reserve(8);
    for (auto ID_neig : IDs_neig){
      Cell * neigh_cell = getCellFromID(ID_neig);
      neigh_cells.push_back(neigh_cell);
    }
    m_SeedCells.push_back(cell);
    cell->setSeedType(3);
    //cell->setNeighbours(IDs_neig);
    cell->addNearSeed(cellID);
    for_each(neigh_cells.begin(), neigh_cells.end(), f_addNearSeed);
  }
  cout << "================= Number of TRUE seeds from pi0: " << count << "\n";
}


void CaloEvent::findOverlappingClusters(){
  cout << "CaloEvent::findOverlappingClusters starts\n";
  m_overlapClusters.clear();
  m_overlapClusters.reserve(m_nclusters*3);
  Int_t seedID=0;
  auto fill_overlapCluster = [&seedID, this](Int_t other_seed){
    if (seedID < other_seed) this->m_overlapClusters.emplace_back(seedID, other_seed);
    else if (seedID > other_seed) this->m_overlapClusters.emplace_back(other_seed, seedID);
  };
  for (auto it_cluster = m_clusters->begin(); it_cluster != m_clusters->end(); ++it_cluster){ // loop on all cells
    Cluster * cluster = (Cluster*)(*it_cluster);
    seedID = cluster->getID();
    auto cells = cluster->getCells();
    for (auto it_cell = cells.begin(); it_cell != cells.end(); ++it_cell){
      Cell * cell = (Cell*)(*it_cell);
      if (cell->getNearSeedsN()<2) continue;
      auto nearSeeds = cell->getNearSeedsID();
      sort(nearSeeds.begin(),nearSeeds.end());
      for_each(nearSeeds.begin(), nearSeeds.end(), fill_overlapCluster);
    }
  }
  sort(m_overlapClusters.begin(), m_overlapClusters.end());
  auto last = std::unique(m_overlapClusters.begin(), m_overlapClusters.end());
  m_overlapClusters.erase(last, m_overlapClusters.end());
  cout << "CaloEvent::findOverlappingClusters ends\n";
}


void CaloEvent::makeClusters(Int_t iterations){
  cout << "CaloEvent::makeClusters starts\n";
  cout.flush();
  iterations = iterations;
  Cell * SeedCell=nullptr;
  cout << "*********************** Number of seeds: " << m_SeedCells.size() << "\n";
  cout.flush();
  for (auto it_SeedCell = m_SeedCells.begin(); it_SeedCell != m_SeedCells.end(); ++it_SeedCell){ // loop on all Seed Cells
    SeedCell = (Cell*)(*it_SeedCell);
    //printf("AddCluster %f %f\n", SeedCell->getX(), SeedCell->getY());
    AddCluster(SeedCell);
  }
  findOverlappingClusters();
  // energy redistribution

  for (auto p : m_overlapClusters)
    energy_redistribution_pro(p.first, p.second, iterations);

  setClusterTrackDistance();
  cout << "CaloEvent::makeClusters ends\n";
  cout.flush();
}

void CaloEvent::clearClusters(){
  cout << "CaloEvent::clearClusters starts\n";
  m_clusters->Clear("C");
  m_nclusters = 0;
  m_clusterMap.clear();
  m_overlapClusters.clear();
  cout << "CaloEvent::clearClusters ends\n";
}
void CaloEvent::energy_redistribution_pro(Int_t ID_I, Int_t ID_J, Int_t iterations){

  Cluster * cl_I = getClusterFromID(ID_I);
  Cluster * cl_J = getClusterFromID(ID_J);

  auto vec_E_I = cl_I->getCellEs();
  auto vec_E_J = cl_J->getCellEs();
  auto vec_cells_I = cl_I->getCellIDs();
  auto vec_cells_J = cl_J->getCellIDs();
  Double_t E_I = cl_I->getE() - cl_J->getSeed()->getE();
  Double_t E_J = cl_J->getE() - cl_I->getSeed()->getE();
  if (find(vec_cells_I.begin(), vec_cells_I.end(), cl_J->getSeed()->getID()) == vec_cells_I.end())
     E_I += cl_J->getSeed()->getE();
  if (find(vec_cells_J.begin(), vec_cells_J.end(), cl_I->getSeed()->getID()) == vec_cells_J.end())
     E_J += cl_I->getSeed()->getE();
  Double_t frac_shared_I = E_I / (E_I+E_J);
  Double_t frac_shared_J = E_J / (E_I+E_J);
  vector<Int_t> shared_cells_IDs;
  shared_cells_IDs.reserve(6);
  Double_t E_i=0., E_j=0.;
  Int_t cell_ID=0;
  Cell* cell_i=nullptr;
  //Cell* cell_j=nullptr;
  //start of step 0
  map<Int_t, array<Double_t,7> > shared_cells_info;
  array<Double_t,7> shared_cell_info;
  map<Int_t, array<Int_t,2> > shared_cells_pos;

  for (UInt_t i=0; i<vec_cells_I.size(); ++i){
    cell_ID = vec_cells_I[i];
    if (find(vec_cells_J.begin(), vec_cells_J.end(), cell_ID) == vec_cells_J.end()) continue;
    shared_cells_IDs.push_back(cell_ID);
    cell_i = cl_I->getCellFromID(cell_ID);
    shared_cell_info[0] = m_molierRadiuses[cell_i->getRegion()-1];
    shared_cell_info[1] = cell_i->getX1();
    shared_cell_info[2] = cell_i->getX2();
    shared_cell_info[3] = cell_i->getY1();
    shared_cell_info[4] = cell_i->getY2();
    E_i = vec_E_I[i];
    shared_cell_info[5] = E_i;
    shared_cells_info[cell_ID] = shared_cell_info;
    shared_cells_pos[cell_ID][0] = i;
    if      (cell_ID == ID_I) continue;
    else if (cell_ID == ID_J) cl_I->setE_fromIndex(i, 0.0);
    else cl_I->setE_fromIndex(i,frac_shared_I*E_i);
  }
  cl_I->finalize(calibs);

  for (UInt_t j=0; j<vec_cells_J.size(); ++j){
    cell_ID = vec_cells_J[j];
    if (find(shared_cells_IDs.begin(), shared_cells_IDs.end(), cell_ID) == shared_cells_IDs.end()) continue;
    E_j = vec_E_J[j];
    shared_cells_info[cell_ID][6] = E_j;
    shared_cells_pos[cell_ID][1] = j;
    if      (cell_ID == ID_J) continue;
    else if (cell_ID == ID_I) cl_J->setE_fromIndex(j, 0.0);
    else cl_J->setE_fromIndex(j,frac_shared_J*E_j);
  }
  cl_J->finalize(calibs);
  //end of step 0

  Double_t X_I(0.), Y_I(0.), X_J(0.), Y_J(0.), Rmoliere(0.);
  for (Int_t iter = 0; iter < iterations; ++iter){
    X_I = cl_I->getCalibX();
    Y_I = cl_I->getCalibY();
    X_J = cl_J->getCalibX();
    Y_J = cl_J->getCalibY();
    for (auto& p_shared_cell_info : shared_cells_info){
      cell_ID = p_shared_cell_info.first;
      shared_cell_info = p_shared_cell_info.second;
      Rmoliere = shared_cell_info[0];
      E_i = E_I * fracEnergy( (shared_cell_info[1] - X_I)/Rmoliere,
                              (shared_cell_info[2] - X_I)/Rmoliere,
                              (shared_cell_info[3] - Y_I)/Rmoliere,
                              (shared_cell_info[4] - Y_I)/Rmoliere );
      E_j = E_J * fracEnergy( (shared_cell_info[1] - X_J)/Rmoliere,
                              (shared_cell_info[2] - X_J)/Rmoliere,
                              (shared_cell_info[3] - Y_J)/Rmoliere,
                              (shared_cell_info[4] - Y_J)/Rmoliere );
      cl_I->setE_fromIndex(shared_cells_pos[cell_ID][0], shared_cell_info[5] * E_i / (E_i + E_j));
      cl_J->setE_fromIndex(shared_cells_pos[cell_ID][1], shared_cell_info[6] * E_j / (E_i + E_j));
    }
    cl_I->finalize(calibs);
    cl_J->finalize(calibs);
  }
}

vector<TH2D> CaloEvent::monitor(TString nfout, TString option){
  //gStyle->SetPalette(57);
  vector<TH2D> h_monitors;
  vector<Double_t>v_clusters_x;
  vector<Double_t>v_clusters_y;
  vector<Double_t>v_seeds_x;
  vector<Double_t>v_seeds_y;
  vector<Double_t>v_closerPart_x;
  vector<Double_t>v_closerPart_y;

  for (Int_t i=1; i<=m_nregions; ++i){
    Double_t minX, maxX, minY, maxY;
    if (m_hregions != 0){
      minX=-m_bordersX.back();
      maxX= m_bordersX.back();
      minY=-m_bordersY.back();
      maxY= m_bordersY.back();
    } else {
      minX=-m_bordersX[i];
      maxX= m_bordersX[i];
      minY=-m_bordersY[i];
      maxY= m_bordersY[i];
    }
    Int_t nbinsX = (Int_t)round((maxX-minX)/m_cellSizes[i-1]);
    Int_t nbinsY = (Int_t)round((maxY-minY)/m_cellSizes[i-1]);
    h_monitors.emplace_back(Form("h_monitor_run%d_evt%d_region%d",m_run, (Int_t)m_evt,i),
                            Form("Energy Monitor, run %d, event %d; x[mm]; y [mm]; E [MeV]", m_run, (Int_t)m_evt),
                            nbinsX, minX, maxX,
                            nbinsY, minY, maxY);
  }
  Double_t x=0.,y=0., E=0.;
  Int_t iregion=0;
  Double_t Emin=1, Emax=50000;
  for (auto it_cell = m_cells->begin(); it_cell != m_cells->end(); ++it_cell){ // loop on all cells
    Cell * cell = (Cell*)(*it_cell);
    x = cell->getX();
    y = cell->getY();
    E = cell->getE();
    iregion = cell->getRegion();
    h_monitors[iregion-1].Fill(x,y,E);
  }
  for (auto& h : h_monitors)
    h.SetAxisRange(Emin,Emax,"Z");

  for (auto it_cluster = m_clusters->begin(); it_cluster != m_clusters->end(); ++it_cluster){ // loop on all clusters
    Cluster * cluster = (Cluster*)(*it_cluster);
    v_seeds_x.push_back(cluster->getSeed()->getX());
    v_seeds_y.push_back(cluster->getSeed()->getY());
    v_clusters_x.push_back(cluster->getCalibX());
    v_clusters_y.push_back(cluster->getCalibY());
    auto closerPart = cluster->getCloserParticle();
    if (closerPart == 0) continue;
    v_closerPart_x.push_back(closerPart->getCaloVertexX());
    v_closerPart_y.push_back(closerPart->getCaloVertexY());
  }

  TGraph g_seeds(v_seeds_x.size(), v_seeds_x.data(), v_seeds_y.data());
  g_seeds.SetName(Form("g_seeds_run%d_evt%d",m_run, (Int_t)m_evt));
  g_seeds.SetTitle(Form("Seed Monitor, run %d, event %d; x[mm]; y [mm]", m_run, (Int_t)m_evt));
  g_seeds.SetMarkerStyle(25);
  g_seeds.SetMarkerColor(kMagenta);

  TGraph g_clusters(v_clusters_x.size(), v_clusters_x.data(), v_clusters_y.data());
  g_clusters.SetName(Form("g_clusters_run%d_evt%d",m_run, (Int_t)m_evt));
  g_clusters.SetTitle(Form("Baricenter Monitor, run %d, event %d; x[mm]; y [mm]", m_run, (Int_t)m_evt));
  g_clusters.SetMarkerStyle(6);
  g_clusters.SetMarkerColor(kMagenta);

  TGraph g_closerPart(v_closerPart_x.size(), v_closerPart_x.data(), v_closerPart_y.data());
  g_closerPart.SetName(Form("g_closerPart_run%d_evt%d",m_run, (Int_t)m_evt));
  g_closerPart.SetTitle(Form("Seed Monitor, run %d, event %d; x[mm]; y [mm]", m_run, (Int_t)m_evt));
  g_closerPart.SetMarkerStyle(2);
  g_closerPart.SetMarkerColor(kBlack);


  TGraph g_tracks(m_tracks.size(), m_tracks.x.data(), m_tracks.y.data());
  g_tracks.SetName(Form("g_tracks_run%d_evt%d",m_run, (Int_t)m_evt));
  g_tracks.SetTitle(Form("Tracks Monitor, run %d, event %d; x[mm]; y [mm]", m_run, (Int_t)m_evt));
  g_tracks.SetMarkerStyle(5);
  g_tracks.SetMarkerColor(kRed);

  if (nfout != "none"){
    TFile fout(nfout, option);
    TCanvas* canv = new TCanvas(Form("canv_Energy_monitor_run%d_evt%d", m_run,(Int_t)m_evt),
                               Form("canv_Energy_monitor_run%d_evt%d", m_run,(Int_t)m_evt),
                               1000,800);
    canv->cd();
    Int_t kk=0;
    for (auto it = h_monitors.rbegin(); it != h_monitors.rend(); ++it){
      auto& h = *it;
      fout.WriteTObject(&h, h.GetName(),"Overwrite");
      if (kk==0) h.Draw("COLORZ");
      else h.Draw("COLsame");
      ++kk;
    }
    g_seeds.Draw("Psame");
    fout.WriteTObject(&g_seeds, g_seeds.GetName(),"Overwrite");
    g_clusters.Draw("Psame");
    fout.WriteTObject(&g_clusters, g_clusters.GetName(),"Overwrite");
    g_closerPart.Draw("Psame");
    fout.WriteTObject(&g_closerPart, g_closerPart.GetName(),"Overwrite");
    g_tracks.Draw("Psame");
    fout.WriteTObject(&g_tracks, g_tracks.GetName(),"Overwrite");
    fout.WriteTObject(canv, canv->GetName(),"Overwrite");
    cout << "Writing file: " << nfout << endl;
    fout.Print();
    fout.Close();
  }
  return h_monitors;
}

vector<TH2D> CaloEvent::monitorT(TString nfout, TString option){
  gStyle->SetPalette(kThermometer);
  vector<TH2D> h_monitors;
  for (Int_t i=1; i<=m_nregions; ++i){
    Double_t minX=-m_bordersX[i];
    Double_t maxX= m_bordersX[i];
    Int_t nbinsX = (maxX-minX)/m_cellSizes[i-1];
    Double_t minY=-m_bordersY[i];
    Double_t maxY= m_bordersY[i];
    Int_t nbinsY = (maxY-minY)/m_cellSizes[i-1];
    h_monitors.emplace_back(Form("h_monitorT_run%d_evt%d_region%d",m_run, (Int_t)m_evt,i),
                            Form("Time Monitor, run %d, event %d; x[mm]; y [mm]; E [MeV]", m_run, (Int_t)m_evt),
                            nbinsX, minX, maxX,
                            nbinsY, minY, maxY);
  }
  Double_t x=0.,y=0., E=0.;
  Int_t iregion=0;
  Double_t Emin=41000, Emax=43000;
  for (auto it_cell = m_cells->begin(); it_cell != m_cells->end(); ++it_cell){ // loop on all cells
    Cell * cell = (Cell*)(*it_cell);
    x = cell->getX();
    y = cell->getY();
    E = cell->getT() - sqrt(x*x+y*y+m_z*m_z)/299.792458 + 42 ;
    iregion = cell->getRegion();
    if (cell->getE()>0.1){
      h_monitors[iregion-1].Fill(x,y,1000*E);
    }
  }

  for (auto& h : h_monitors){
    h.SetAxisRange(Emin,Emax,"Z");
  }

  if (nfout != "none"){
    TFile fout(nfout, option);
    TCanvas* canv = new TCanvas(Form("canv_Time_monitor_run%d_evt%d", m_run,(Int_t)m_evt),
                               Form("canv_Time_monitor_run%d_evt%d", m_run,(Int_t)m_evt),
                               1000,800);
    canv->cd();
    Int_t kk=0;
    for (auto it = h_monitors.rbegin(); it != h_monitors.rend(); ++it){
      auto& h = *it;
      fout.WriteTObject(&h, h.GetName(),"Overwrite");
      if (kk==0) h.Draw("COLORZ");
      else h.Draw("COLsame");
      ++kk;
    }

    fout.WriteTObject(canv, canv->GetName(),"Overwrite");
    cout << "Writing file: " << nfout << endl;
    fout.Print();
    fout.Close();
  }
  return h_monitors;
}

void CaloEvent::addContainerReconstructed(TString ncontainer){
  cout << "CaloEvent::addContainerReconstructed starts\n";
  cout.flush();
  //auto cont = &(m_containers[ncontainer.Data()]);
  m_containers_rec[ncontainer.Data()].reserve(m_nclusters);
  for (auto it_cluster = m_clusters->begin(); it_cluster != m_clusters->end(); ++it_cluster){
    Cluster * cluster = (Cluster*)(*it_cluster);
    myParticle part;
    part.setM(0.0);
    part.setOrigPx(0.0);
    part.setOrigPy(0.0);
    part.setOrigPz(0.0);
    part.setOrigE(0.0);
    part.setCaloPx(cluster->getPx());
    part.setCaloPy(cluster->getPy());
    part.setCaloPz(cluster->getPz());
    part.setCaloE(cluster->getCalibE());
    part.setKey(m_run, m_evt, cluster->getID());
    part.setID( 22 );
    // Vertex * vert_calo = FindVertex(cluster->getCalibX(),cluster->getCalibY(),m_z,cluster->getT());
    // if (vert_calo==0) vert_calo = AddVertex(cluster->getCalibX(),cluster->getCalibY(),m_z,cluster->getT());
    Vertex * vert_calo = AddVertex(cluster->getCalibX(),cluster->getCalibY(),m_z,cluster->getSeed()->getT());
    vert_calo->setType(-3);
    part.setCaloVertex(vert_calo);
    part.setEndVertex(vert_calo);
    part.addCluster(cluster);
    myParticle* truePhoton = (myParticle*)cluster->getCloserPhoton();
    part.setMatchedPart(truePhoton);
    m_containers_rec[ncontainer.Data()].push_back(part);

    if (truePhoton != 0) truePhoton->setMatchedPart(&(m_containers_rec[ncontainer.Data()].back()));
    cluster->setRecParticle( &(m_containers_rec[ncontainer.Data()].back()));
  }
  printf("Number of seeds    : %d\n", (Int_t)m_SeedCells.size());
  printf("Number of clusters : %d\n", (Int_t)m_clusters->GetEntries());
  printf("Number of reco     : %d\n", (Int_t)m_containers_rec[ncontainer.Data()].size());


  cout << "CaloEvent::addContainerReconstructed ends\n";
  cout.flush();
}
/*
void CaloEvent::addContainers(map<string,Int_t>toSearch, TTree* input, Bool_t checkAcc){
  cout << "CaloEvent::addContainers starts\n";
  for (auto p : toSearch){
    m_containers[p.first];
    m_containers[p.first].reserve(20000);
  }
  //int count=0;
  cout << "EventNumber" << m_evt << "\n";
  Long64_t runNumber = 0, eventNumber = 0;
  //Int_t isTrash = 0;

  int count_tracks=0;

  for(Int_t ievt = 0, nevts = input->GetEntries(); ievt < nevts; ++ievt) {
    input->GetEntry(ievt);
    runNumber   = input->GetLeaf("runNumber")->GetValueLong64();
    eventNumber = input->GetLeaf("eventNumber")->GetValueLong64();
    if(runNumber < (Long64_t)(m_run)) continue;
    if(runNumber > (Long64_t)(m_run)) break;
    if(eventNumber < (Long64_t)m_evt) continue;
    if(eventNumber > (Long64_t)m_evt) break;
    //isTrash = input->GetLeaf("X_TRASH")->GetValue();
    Int_t PID = input->GetLeaf("X_ID")->GetValue();
    //if (isTrash==1 && abs(PID)!=511 && abs(PID)!=111) continue;
    Int_t  mpid = input->GetLeaf("X_MID")->GetValue();
    Double_t opx = input->GetLeaf("X_PX")->GetValue();
    Double_t opy = input->GetLeaf("X_PY")->GetValue();
    Double_t opz = input->GetLeaf("X_PZ")->GetValue();
    Double_t oz = input->GetLeaf("X_OVZ")->GetValue();

    if (opz<0) continue;
    //if (opx*opx+opy*opy<20) continue;
    checkAcc = checkAcc;
    //if (checkAcc && getIDFromXY(input->GetLeaf("X_CALOVX")->GetValue(),input->GetLeaf("X_CALOVY")->GetValue()) < 0 ) continue;
    if (oz>m_z) continue;
    //if (abs(oz)>500.0) continue;
    //if (abs(PID)==211 && abs(mpid)!=511) continue;
    for (auto p : toSearch){
      if (p.second != PID) continue;

      myParticle part;
      Double_t ox    = input->GetLeaf("X_OVX")->GetValue();
      Double_t oy    = input->GetLeaf("X_OVY")->GetValue();
      Double_t ot    = input->GetLeaf("X_OVT")->GetValue();
      Double_t calox = input->GetLeaf("X_CALOVX")->GetValue();
      Double_t caloy = input->GetLeaf("X_CALOVY")->GetValue();
      Double_t caloz = input->GetLeaf("X_CALOVZ")->GetValue();
      Double_t calot = input->GetLeaf("X_CALOVT")->GetValue();
      Double_t endx  = input->GetLeaf("X_ENDVX")->GetValue();
      Double_t endy  = input->GetLeaf("X_ENDVY")->GetValue();
      Double_t endz  = input->GetLeaf("X_ENDVZ")->GetValue();
      Double_t endt  = input->GetLeaf("X_ENDVT")->GetValue();
      Double_t m     = input->GetLeaf("X_M")->GetValue();
      Double_t calopx= input->GetLeaf("X_CALOPX")->GetValue();
      Double_t calopy= input->GetLeaf("X_CALOPY")->GetValue();
      Double_t calopz= input->GetLeaf("X_CALOPZ")->GetValue();
      Double_t oE    = sqrt(   opx*opx   +   opy*opy   +   opz*opz   +m*m);
      Double_t caloE = sqrt(calopx*calopx+calopy*calopy+calopz*calopz+m*m);
      Int_t   key = input->GetLeaf("X_KEY")->GetValue();
      Int_t  mkey = input->GetLeaf("X_MKEY")->GetValue();
      Int_t gmkey = input->GetLeaf("X_GMKEY")->GetValue();
      Int_t gmpid = input->GetLeaf("X_GMID")->GetValue();
      Short_t isSig = input->GetLeaf("X_ISSIG")->GetValue();
      Short_t aliveatcaloz = input->GetLeaf("X_ALIVEATCALOZ")->GetValue();
      Short_t trackable = input->GetLeaf("X_ISTRACKABLE")->GetValue();
      Short_t incaloacc = input->GetLeaf("X_INCALOACC")->GetValue();
      Short_t HitCaloSensDet = input->GetLeaf("HitCaloSensDet")->GetValue();
      Short_t isRecoveredByVanya = input->GetLeaf("isRecoveredByVanya")->GetValue();

      mkey  = ( mpid == 0)? -1 :  mkey;
      gmkey = (gmpid == 0)? -1 : gmkey;

      Vertex * vert_o = FindVertex(ox,oy,oz,ot);
      if (vert_o==0) vert_o = AddVertex(ox,oy,oz,ot);
      Vertex * vert_calo = FindVertex(calox,caloy,caloz,calot);
      if (vert_calo==0) vert_calo = AddVertex(calox,caloy,caloz,calot);
      Vertex * vert_end = FindVertex(endx,endy,endz,endt);
      if (vert_end==0) vert_end = AddVertex(endx,endy,endz,endt);

      part.setM(m);

      part.setOrigPx(opx);
      part.setOrigPy(opy);
      part.setOrigPz(opz);
      part.setOrigE(oE);
      part.setCaloPx(calopx);
      part.setCaloPy(calopy);
      part.setCaloPz(calopz);
      part.setCaloE(caloE);
      part.setKey(  runNumber, eventNumber,   key);
      part.setMkey( runNumber, eventNumber,  mkey);
      part.setGmkey(runNumber, eventNumber, gmkey);
      part.setID(PID);
      part.setMid(   mpid);
      part.setGmid( gmpid);
      part.setOrigVertex(vert_o);
      part.setCaloVertex(vert_calo);
      part.setEndVertex(vert_end);
      part.setIsSig(isSig);
      part.setAliveAtCaloZ(aliveatcaloz);
      part.setTrackable(trackable);
      part.setInCaloAcc(incaloacc);
      part.setHitCaloSensDet(HitCaloSensDet);
      part.setIsRecoveredByVanya(isRecoveredByVanya);
      m_containers[p.first].push_back(part);
      if ((abs(PID)==11) && trackable==1 && incaloacc==1 ) ++count_tracks;
      break;
    }
  }
  cout << "Tracks: " << count_tracks << endl;
  cout << "CaloEvent::addContainers ends\n";
}
*/
/*
void CaloEvent::addContainersFromTES(map<string,Int_t>containers, TTree* input){
  cout << "CaloEvent::addContainersFromTES starts\n";
  myTESevent* TESevent = new myTESevent();
  input->SetBranchAddress("TES", &TESevent);
  TESevent->reset();
  input->GetEntry(m_evt-1);
  //m_vertex = *(TESevent->getVertices();
  for (auto it : containers)
    m_containers[it.first] = *(TESevent->getParticles(it.second));
  cout << "CaloEvent::addContainersFromTES starts\n";
}
*/
void CaloEvent::addContainersFromTES(map<string,Int_t>containers, myTESevent* TESevent){
  cout << "CaloEvent::addContainersFromTES starts\n";
  for (auto it : containers)
    m_containers_true[it.first] = TESevent->getParticles(it.second);
  cout << "CaloEvent::addContainersFromTES starts\n";
}
/*
void CaloEvent::linkRelativesParts(map<string,Int_t>containers){
  Long64_t this_part_mkey=0, other_part_key=0;
  for (auto it_thisCont = m_containers.begin(); it_thisCont != m_containers.end(); ++it_thisCont){
    if (containers.find(it_thisCont->first) == containers.end()) continue;
    //cout << "\nthis part in container: "<< p.first << "\n";
    for (auto this_part = it_thisCont->second.begin(); this_part != it_thisCont->second.end(); ++this_part){
      this_part_mkey = this_part->getMkey();
      //printf("this_part: %d %lld \n", this_part->getID(), this_part->getKey());
      for (auto it_otherCont = m_containers.begin(); it_otherCont != m_containers.end(); ++it_otherCont){
        if (containers.find(it_otherCont->first) == containers.end()) continue;
        if (it_thisCont->first == it_otherCont->first) continue;
        //cout << "other part in container: "<< it_otherCont->first<< "\n";
        for (auto other_part = it_otherCont->second.begin(); other_part != it_otherCont->second.end(); ++other_part){
          other_part_key = other_part->getKey();
          if (this_part_mkey == other_part_key){
            this_part->set_mother(&(*other_part));
            other_part->add_daughter(&(*this_part));
            //printf("...Found mother %lld %lld\n", this_part_mkey, other_part_key);
            break;
          }
        }
      }
    }
  }
}

void CaloEvent::setGDparents(){ // necessary because bug in GMkey/GMid was found once
  cout << "CaloEvent::setGDparents starts\n";
  for (auto it_cont = m_containers.begin(); it_cont != m_containers.end(); ++it_cont){
    for (auto it_part = it_cont->second.begin(); it_part != it_cont->second.end(); ++it_part){
      myProtoParticle* mother = it_part->get_mother();
      if (mother == 0) continue;
      myProtoParticle* gd_mother = mother->get_mother();
      if (gd_mother == 0) continue;
      it_part->set_gd_mother(gd_mother);
      it_part->setGmid(gd_mother->getID());
      it_part->setGmkey(gd_mother->getKey());
      gd_mother->add_gd_daughter(&(*it_part));
    }
  }
  cout << "CaloEvent::setGDparents ends\n";
}
*/
void CaloEvent::sortDaughters(){
  cout << "CaloEvent::sortDaughters starts\n";
  for (auto it_cont = m_containers_true.begin(); it_cont != m_containers_true.end(); ++it_cont){
    for (auto it_part = it_cont->second->begin(); it_part != it_cont->second->end(); ++it_part){
      it_part->sort_daughters();
      it_part->sort_gd_daughters();
    }
  }
  for (auto it_cont = m_containers_rec.begin(); it_cont != m_containers_rec.end(); ++it_cont){
    for (auto it_part = it_cont->second.begin(); it_part != it_cont->second.end(); ++it_part){
      it_part->sort_daughters();
      it_part->sort_gd_daughters();
    }
  }
  cout << "CaloEvent::sortDaughters ends\n";
}

void CaloEvent::combineParticles_resolvPi0(TString ncontainer){
  cout << "CaloEvent::combineParticles_resolvPi0 starts\n";
  cout.flush();
  if (m_containers_rec.find(ncontainer.Data()) == m_containers_rec.end()){
    m_containers_rec[ncontainer.Data()];
    m_containers_rec[ncontainer.Data()].reserve(24000);
  }

  auto container = &(m_containers_rec.at(ncontainer.Data()));
  Double_t i_px=0., i_py=0., i_pz=0., i_E=0., j_E=0.;
  Double_t tot_px=0., tot_py=0., tot_pz=0., tot_E=0.;
  Int_t pi0_PID=111;
  Double_t ij_pt2min = 200*200;
  Double_t M2=0, M2min=40*40, M2max=230*230;
  auto it_cluster_i = m_clusters->begin();
  while (it_cluster_i != m_clusters->end()){
    Cluster* cluster_i = (Cluster*)(*it_cluster_i);
    Int_t seedType_i = cluster_i->getSeed()->getSeedType();
    if (seedType_i == 4 || seedType_i == 5) {++it_cluster_i; continue;}
    if (cluster_i->getPt2()<ij_pt2min) {++it_cluster_i; continue;}
    i_px = cluster_i->getPx();
    i_py = cluster_i->getPy();
    i_pz = cluster_i->getPz();
    i_E  = cluster_i->getCalibE();
    auto IDs_i = cluster_i->getCellIDs();
    auto it_cluster_j = it_cluster_i;
    ++it_cluster_j;
    while ( it_cluster_j != m_clusters->end()){
      Cluster *cluster_j = (Cluster*)(*it_cluster_j);
      Int_t seedType_j = cluster_j->getSeed()->getSeedType();
      if (seedType_j == 4 || seedType_j == 5) {++it_cluster_j; continue;}
      if (cluster_j->getPt2()<ij_pt2min) {++it_cluster_j; continue;}

      //if (find(IDs_i.begin(),IDs_i.end(), cluster_j->getID()) != IDs_i.end()) {++it_cluster_j; continue;}
      tot_px= i_px+cluster_j->getPx();
      tot_py= i_py+cluster_j->getPy();
      tot_pz= i_pz+cluster_j->getPz();
      j_E   = cluster_j->getCalibE();
      tot_E = i_E + j_E;

      M2 =  tot_E *tot_E
           -tot_px*tot_px
           -tot_py*tot_py
           -tot_pz*tot_pz;

      if (M2 < M2min) {++it_cluster_j; continue;}
      if (M2 > M2max) {++it_cluster_j; continue;}
      //printf("%.3f %.3f %.3f %.3f %.3f \n", tot_px, tot_py, tot_pz, tot_E, sqrt(M2));
      myParticle part;
      part.setOrigPx(tot_px);
      part.setOrigPy(tot_py);
      part.setOrigPz(tot_pz);
      part.setOrigE(tot_E);
      part.setM(sqrt(M2));
      part.setID(pi0_PID);
      part.setKey(m_run, m_evt, ++m_nparticles_rec);
      part.setPi0Type(2);
      part.addCluster(cluster_i);
      part.addCluster(cluster_j);
      part.add_daughter(cluster_i->getRecParticle());
      part.add_daughter(cluster_j->getRecParticle());
      container->push_back(part);
      ++it_cluster_j;
      //++count;
      //printf("Found resolved pi0: %lld ", part.getKey());
      //printf("from clusters: %d  %d \n", part.getClusters()[0]->getID(),part.getClusters()[1]->getID());
      //printf("px = %.3f, py = %.3f, pz = %.3f, E = %.3f, M2 = %.3f \n", tot_px, tot_py, tot_pz, tot_E, M2);
    }
    ++it_cluster_i;
  }
  //cout << "combine particle: " << count << "   vs.    " << m_containers[ncontainer.Data()].size() << endl;
  cout << "CaloEvent::combineParticles_resolvPi0 ends\n";
  cout.flush();

}

void CaloEvent::combineParticles_mergedPi0(TString ncontainer){
  if (m_containers_rec.find(ncontainer.Data()) == m_containers_rec.end()){
    m_containers_rec[ncontainer.Data()].reserve(1200);
  }
  auto container = &(m_containers_rec.at(ncontainer.Data()));

  Double_t pi0_px=0., pi0_py=0., pi0_pz=0., pi0_E=0., pi0_M=0.;
  for (auto seed1 : m_SeedCells){
    if (seed1->getSeedType() != 4) continue;
    Cell* seed2 = find2Seed_mergedPi0(seed1);
    if (seed2 == 0) {
      cout << "CaloEvent::combineParticles_mergedPi0 : WARNING! : seed2 is supposed to different from 0 here!\n";
      continue;
    }
    Cluster *cluster_1 = getClusterFromID(seed1->getID());
    Cluster *cluster_2 = getClusterFromID(seed2->getID());
    pi0_px = cluster_1->getPx() + cluster_2->getPx();
    pi0_py = cluster_1->getPy() + cluster_2->getPy();
    pi0_pz = cluster_1->getPz() + cluster_2->getPz();
    pi0_E  = cluster_1->getCalibE() + cluster_2->getCalibE();
    pi0_M  = sqrt( pi0_E*pi0_E-pi0_px*pi0_px-pi0_py*pi0_py-pi0_pz*pi0_pz );
    if (pi0_M < 95) continue;
    if (pi0_M >215) continue;
    if  (2*m_z*pi0_M/pi0_E > 1.8*seed1->getSize()) continue;
    myParticle part;
    part.setOrigPx(pi0_px);
    part.setOrigPy(pi0_py);
    part.setOrigPz(pi0_pz);
    part.setOrigE(pi0_E);
    part.setM(pi0_M);
    part.setID(111);
    part.setKey(m_run, m_evt, ++m_nparticles_rec);
    part.setPi0Type(1);
    part.addCluster(cluster_1);
    part.addCluster(cluster_2);
    part.add_daughter(cluster_1->getRecParticle());
    part.add_daughter(cluster_2->getRecParticle());
    container->push_back(part);
  }
}


void CaloEvent::matchParticles_Pi0(TString ncontainer_TRUE, TString ncontainer_REC){
  auto cont_true = this->getContainerTRUE(ncontainer_TRUE.Data());
  auto cont_rec  = this->getContainerREC(ncontainer_REC.Data());
  for (auto& part_rec : *cont_rec){
    auto clusters = part_rec.getClusters();
    if (clusters.size() != 2) {
      cout << "******* matchParticles_resolvedPi0: WARNING thers something wrong! .. somewhere... \n";
      return;
    }
    Cluster* cluster_i = clusters[0];
    Cluster* cluster_j = clusters[1];
    myProtoParticle * photon_i = cluster_i->getCloserPhoton();
    myProtoParticle * photon_j = cluster_j->getCloserPhoton();
    if (photon_i == 0 || photon_j == 0)    continue;
    Long64_t Mkey = photon_i->getMkey();
    if (Mkey != photon_j->getMkey())       continue;
    if (photon_i->getMid() != 111)         continue;
    for (auto& part_true : *cont_true){
      if (part_true.getMatchedPart() != 0) continue;
      if (part_true.getKey() != Mkey)      continue;
      part_rec.setMatchedPart(&part_true);
      part_true.setMatchedPart(&part_rec);
    }
  }
}

vector<myParticle>* CaloEvent::getContainer(TString ncontainer){
  if (m_containers_rec.find(ncontainer.Data())!=m_containers_rec.end())
    return this->getContainerREC(ncontainer);
  if (m_containers_true.find(ncontainer.Data())!=m_containers_true.end())
    return this->getContainerTRUE(ncontainer);
  printf("container \"%s\" not found\n", ncontainer.Data());
  return 0;
}

void CaloEvent::combineParticles_Bd3pi(TString ncont_Bd, TString ncont_pip, TString ncont_pim, TString ncont_pi0){
  cout << "CaloEvent::combineParticles_Bd3pi starts\n";
  cout.flush();
  m_containers_rec[ncont_Bd.Data()].reserve(800);
  // auto cont_Bd = &(m_containers.at(ncont_Bd.Data()));
  // auto cont_pip= &(m_containers.at(ncont_pip.Data()));
  // auto cont_pim= &(m_containers.at(ncont_pim.Data()));
  // auto cont_pi0= &(m_containers.at(ncont_pi0.Data()));
  auto cont_Bd = this->getContainer(ncont_Bd.Data());
  auto cont_pip= this->getContainer(ncont_pip.Data());
  auto cont_pim= this->getContainer(ncont_pim.Data());
  auto cont_pi0= this->getContainer(ncont_pi0.Data());


  Long64_t pip_Mkey=0, pim_Mkey=0;
  Double_t pip_px=0., pip_py=0., pip_pz=0., pip_E=0.;
  Double_t pim_px=0., pim_py=0., pim_pz=0., pim_E=0.;
  Double_t pi0_px=0., pi0_py=0., pi0_pz=0., pi0_E=0.;
  Double_t Bd_px =0., Bd_py= 0., Bd_pz=0. , Bd_E=0. , Bd_M2=0.;
  Double_t Bd_M2_min = 3000*3000, Bd_M2_max = 7000*7000;
  Int_t B0_ID = 511;
  for (auto& pip : *cont_pip){
    if (pip.getIsSig() != 1 || abs(pip.getMid())!=511 || ((myParticle*)(pip.get_mother()))->getIsSig() != 1 ) continue;
    if (abs(pip.getMid()) != 511) continue;
    pip_Mkey = pip.getMkey();
    pip_px = pip.getOrigPx();
    pip_py = pip.getOrigPy();
    pip_pz = pip.getOrigPz();
    pip_E  = pip.getOrigE();
    for (auto& pim : *cont_pim){
      pim_Mkey = pim.getMkey();
      if (pim_Mkey != pip_Mkey) continue;
      if (pim.getIsSig() != 1){
        cout << "CaloEvent::combineParticles_Bd3pi :  WARNING! this sould not happen!!!\n";
        continue;
      }
      pim_px = pim.getOrigPx();
      pim_py = pim.getOrigPy();
      pim_pz = pim.getOrigPz();
      pim_E  = pim.getOrigE();
      auto vert = pim.getOrigVertex();
      cout << "here \n";
      cout << "Number of reco pi0s: " << cont_pi0->size() << endl;
      printf("* B0 decay vertex: (%.1f,%.1f,%.1f,%.1f)\n", vert->getX(), vert->getY(), vert->getZ(), vert->getT());
      printf("- B0 decay vertex: (%.1f,%.1f,%.1f,%.1f)\n", pim.getOrigVertexX(), pim.getOrigVertexY(), pim.getOrigVertexZ(), pim.getOrigVertexT());
      cout.flush();
      for (auto& pi0 : *cont_pi0){
        pi0.getMomIfOrigV(pi0_px,pi0_py,pi0_pz, vert->getX(), vert->getY(), vert->getZ());
        pi0_E  = pi0.getOrigE();
        Bd_px = pi0_px+pip_px+pim_px;
        Bd_py = pi0_py+pip_py+pim_py;
        Bd_pz = pi0_pz+pip_pz+pim_pz;
        Bd_E  = pi0_E +pip_E +pim_E ;
        Bd_M2 =  Bd_E * Bd_E
                -Bd_px*Bd_px
                -Bd_py*Bd_py
                -Bd_pz*Bd_pz;
        //printf("rec B0_M = %f\n", sqrt(Bd_M2));
        if (Bd_M2 < Bd_M2_min) continue;
        if (Bd_M2 > Bd_M2_max) continue;
        myParticle part;
        part.setOrigPx(Bd_px);
        part.setOrigPy(Bd_py);
        part.setOrigPz(Bd_pz);
        part.setOrigE(Bd_E);
        part.setM(sqrt(Bd_M2));
        part.setID(B0_ID);
        part.setKey(m_run, m_evt, ++m_nparticles_rec);
        part.add_daughter(&pip);
        part.add_daughter(&pim);
        part.add_daughter(&pi0);
        part.setEndVertex(vert);
        cont_Bd->push_back(part);
      }
      break;
    }
  }
  cout << "CaloEvent::combineParticles_Bd3pi ends\n";
  cout.flush();
}

void CaloEvent::matchParticles_Bd3pi(/*TString ncontainer_TRUE,*/ TString ncontainer_REC){
  cout << "CaloEvent::matchParticles_Bd3pi starts\n";
  cout.flush();
  //auto cont_true = this->getContainer(ncontainer_TRUE.Data());
  auto cont_rec  = this->getContainer(ncontainer_REC.Data());
  cout << "Number of Reconstructed B0: " << cont_rec->size() << endl;
  for (auto& B_rec : *cont_rec){
    if (B_rec.getMatchedPart() != 0) continue;
    auto pions = B_rec.get_daughters();
    if (pions.size() != 3) {
      cout << "******* matchParticles_Bd3pi: WARNING there is something wrong! .. somewhere... \n";
      return;
    }
    myParticle* pip = (myParticle*)pions[0];
    myParticle* pim = (myParticle*)pions[1];
    if (pip->getID() != 211 || pim->getID() != -211 || pip->getMkey() != pim->getMkey() || abs(pip->getMid()) != 511){
      cout << "******* matchParticles_Bd3pi: WARNING thers something wrong with charged pions! .. somewhere... \n";
      return;
    }
    myParticle* pi0 = (myParticle*)pions[2];
    myParticle* truePi0 = pi0->getMatchedPart();
    if (truePi0 == 0) {
      B_rec.setMatchedPart(0);
      continue;
    }
    if (truePi0->getID() != 111){
      cout << "******* matchParticles_Bd3pi: WARNING thers something wrong with neutral pion! .. somewhere... \n";
      return;
    }
    auto Mkey = truePi0->getMkey();
    if (Mkey != pim->getMkey()){
      B_rec.setMatchedPart(0);
      continue;
    }
    auto B_true = (myParticle*)(truePi0->get_mother());
    if (B_true->getMatchedPart() != 0){
      cout << "******* matchParticles_Bd3pi: WARNING thers something wrong with B0 matching! .. somewhere... \n";
      continue;
    }

    B_rec.setMatchedPart(B_true);
    B_true->setMatchedPart(&B_rec);
    // for (auto& B_true : *cont_true){
    //   if (B_true.getMatchedPart() != 0) continue;
    //   if (B_true.getKey() != Mkey)      continue;
    //   B_rec.setMatchedPart(&B_true);
    //   B_true.setMatchedPart(&B_rec);
    // }
  }
  cout << "CaloEvent::matchParticles_Bd3pi ends\n";
  cout.flush();
}

void CaloEvent::flagSig_Bd3pi(TString ncont_B1, TString ncont_B2, TChain * genChain){
  Long64_t run, evt;
  Int_t id, mid, mk;
  map<Long64_t, vector<Int_t>> allInfo;
  auto cont_B1 = this->getContainer(ncont_B1.Data());
  for (auto& B : *cont_B1)
    allInfo[B.getKey()];
  auto cont_B2 = this->getContainer(ncont_B2.Data());
  for (auto& B : *cont_B2)
    allInfo[B.getKey()];


  for (Int_t i=0,  nentries=genChain->GetEntries(); i<nentries; ++i){
    genChain->GetEntry(i);
    run = genChain->GetLeaf("runNumber")->GetValueLong64();
    if ((UInt_t)   run < m_run) continue;
    if ((UInt_t)   run > m_run) break;
    evt = genChain->GetLeaf("eventNumber")->GetValueLong64();
    if ((ULong64_t)evt < m_evt) continue;
    if ((ULong64_t)evt > m_evt) break;
    if (genChain->GetLeaf("X_ISSIG")->GetValue()!=1) continue;
    mk  = genChain->GetLeaf("X_MKEY")->GetValue();
    auto it = allInfo.find(Long64_t( Long64_t(run<<48) | Long64_t(evt<<32) |  Long64_t(mk)));
    if (it == allInfo.end()) continue;
    mid = genChain->GetLeaf("X_MID")->GetValue();
    if (mid == 0) continue;
    id  = genChain->GetLeaf("X_ID")->GetValue();
    it->second.push_back(id);
  }
  for (auto& cont : {cont_B1, cont_B2}){
    for (auto& B : *cont){
      auto pids = allInfo.at(B.getKey());
      Int_t npip=0, npim=0, npi0=0, nother=0;
      for_each(pids.begin(), pids.end(), [&npip, &npim, &npi0, &nother](Int_t ii){
        if      (ii == 211) ++npip;
        else if (ii ==-211) ++npim;
        else if (ii == 111) ++npi0;
        else if (ii !=  22) ++nother;
      });
      vector<myParticle*> pions;
      vector<myProtoParticle*> protoPions = B.get_daughters();
      for_each(protoPions.begin(), protoPions.end(), [&pions](myProtoParticle* protoPion){pions.push_back((myParticle*)protoPion);});
      vector<myParticle*> gammas;
      vector<myProtoParticle*> protoGammas = B.get_gd_daughters();
      for_each(protoGammas.begin(), protoGammas.end(), [&gammas](myProtoParticle* protoGamma){gammas.push_back((myParticle*)protoGamma);});

      if (npip==1 && npim==1 && npi0==1 && nother==0){
        B.setIsSig(1);
        for_each(pions.begin(), pions.end(), [](myParticle *pion){pion->setIsSig(1);});
        for_each(gammas.begin(), gammas.end(), [](myParticle *gamma){gamma->setIsSig(1);});
      } else{
        B.setIsSig(0);
        for_each(pions.begin(), pions.end(), [](myParticle *pion){pion->setIsSig(0);});
        for_each(gammas.begin(), gammas.end(), [](myParticle *gamma){gamma->setIsSig(0);});
      }
    }
  }
}

void CaloEvent::flagPi0(TString ncont_pi0){
  cout << "CaloEvent::flagPi0 starts \n";
  auto cont_pi0 = this->getContainer(ncont_pi0.Data());
  for (auto& pi0 : *cont_pi0){
    auto daughters = pi0.get_daughters();
    if (daughters.size() != 2) continue;
    auto photon1 = daughters[0];
    auto photon2 = daughters[1];
    Vertex* calov1 = photon1->getCaloVertex();
    if (calov1==0) continue;
    auto ID1 = getIDFromXY(calov1->getX(), calov1->getY());
    if (ID1 < 0) continue;
    Vertex* calov2 = photon2->getCaloVertex();
    if (calov2==0) continue;
    auto ID2 = getIDFromXY(calov2->getX(), calov2->getY());
    if (ID2 < 0) continue;
    // vector<Int_t> neig1;
    // getNeighbours(ID1, neig1);
    // auto it = find(neig1.begin(), neig1.end(), ID2);
    // if (it != neig1.end() || ID1 == ID2) pi0.setPi0Type(1);
    Cell* cell1 = getCellFromID(ID1);
    auto it = find(cell1->getNeigBegin(), cell1->getNeigEnd(), ID2);
    if (it != cell1->getNeigEnd() || ID1 == ID2) pi0.setPi0Type(1);
    else pi0.setPi0Type(2);
  }
  cout << "CaloEvent::flagPi0 ends \n";
}

void CaloEvent::setTracks(){
  cout << "CaloEvent::setTracks starts \n";
  Int_t N=0;
  for (auto ncont : m_nameChargedContainers){
    if (m_containers_true.find(ncont) == m_containers_true.end()) continue;
    N += m_containers_true.at(ncont)->size();
  }
  m_tracks = myTracks(N);
  for (auto ncont : m_nameChargedContainers){
    if (m_containers_true.find(ncont) == m_containers_true.end()) continue;
    for (auto& part : *(m_containers_true.at(ncont))){
      //printf("%f %f | %f %f || %d %d\n",part.getOrigVertexX(), part.getOrigVertexY(), part.getCaloVertexX(), part.getCaloVertexY(),
      //  part.getCaloPz(), part.getInCaloAcc(), part.getTrackable() );
      if (part.getOrigPz()<0)          continue;
      if (part.getInCaloAcc()!=1)      continue;
      if (part.getTrackable()!=1)      continue;
      m_tracks.addTrack(&part);
    }
  }
  cout << "Number of charged tracks: " << m_tracks.size() << endl;
  cout.flush();
  cout << "CaloEvent::setTracks ends \n";
  cout.flush();
}

void CaloEvent::setClusterTrackDistance(){
  Double_t closer_dist = -1, tmp_dist = -1;
  Int_t i_closer=-1;
  for (auto it = m_clusters->begin(); it != m_clusters->end(); ++ it){
    Cluster* cluster = (Cluster*)(*it);
    closer_dist = -1;
    i_closer    = -1;
    for (Int_t i=0; i<m_tracks.size(); ++i){
      tmp_dist = pow(cluster->getCalibX()-m_tracks.x[i], 2) + pow(cluster->getCalibY()-m_tracks.y[i], 2);
      if (tmp_dist>closer_dist && closer_dist>0) continue;
      closer_dist = tmp_dist;
      i_closer=i;
    }
    if (i_closer != -1) cluster->setCloserTrack(m_tracks.part[i_closer]);
    else                cluster->setCloserTrack(0);
  }
}


void CaloEvent::removeEmptyCells(){
  cout << "CaloEvent::removeEmptyCells starts\n";
  Int_t i=-1;
  vector<Int_t> toRemove;
  printf("1) number of cells: %d\n, cells size: %d\n", m_cells->GetEntries(), (Int_t)m_cells->GetSize());
  for (auto it_cell=m_cells->begin(); it_cell!=m_cells->end(); ++it_cell){
    Cell * cell = (Cell*)(*it_cell);
    ++i;
    if (cell->getE()>1) continue;
    toRemove.push_back(i);
  }
  for (auto j: toRemove)
    m_cells->RemoveAt(j);
  printf("2) number of cells: %d\n, cells size: %d\n", m_cells->GetEntries(), (Int_t)m_cells->GetSize());
  m_cells->Compress();
  printf("3) number of cells: %d\n, cells size: %d\n", m_cells->GetEntries(), (Int_t)m_cells->GetSize());
  cout << "CaloEvent::removeEmptyCells ends\n";
}


#endif
