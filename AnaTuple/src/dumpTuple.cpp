#include <iostream>
#include "TFile.h"
#include "TTree.h"
#include "TLeaf.h"
using namespace std;
void dumpTuple(){
  TFile *fin = TFile::Open("./tuples/b0_stand_REC.bak.root");
  TTree *tin = (TTree*)fin->Get("ntp");
  tin->SetBranchStatus("*",1);
  Int_t isSig = 0;
  tin->SetBranchAddress("isSig", &isSig);
  for (int i=0, n=tin->GetEntries(); i<n;++i){
    tin->GetEntry(i);
    if (isSig!=1) continue;
    cout << i<< ",";
    //    cout << isSig << ",";
    cout << tin->GetLeaf("runNumber")->GetValue()<< ",";
    cout << tin->GetLeaf("evtNumber")->GetValue()<< ",";
    printf("%lld,",  tin->GetLeaf("gamma1_TRUE_KEY")->GetValueLong64());
    printf("%lld,",  tin->GetLeaf("gamma2_TRUE_KEY")->GetValueLong64());
    cout << tin->GetLeaf("gamma1_PT")->GetValue()<< ",";
    cout << tin->GetLeaf("gamma2_PT")->GetValue()<< ",";
    cout << tin->GetLeaf("gamma1_CALOVX")->GetValue()<< ",";
    cout << tin->GetLeaf("gamma1_CALOVY")->GetValue()<< ",";
    cout << tin->GetLeaf("gamma2_CALOVX")->GetValue()<< ",";
    cout << tin->GetLeaf("gamma2_CALOVY")->GetValue()<< ",";
    cout << tin->GetLeaf("B0_M")->GetValue()<< ",";
    cout << tin->GetLeaf("pi0_M")->GetValue()<< ",";

    cout << endl;
  }

}
