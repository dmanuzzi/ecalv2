#ifndef ROOT_myTESevent_h
#define ROOT_myTESevent_h

#include <iostream>
#include <vector>
#include <map>
using namespace std;

#include "TFile.h"
#include "TString.h"
#include "TTree.h"
#include "TObject.h"

#include <myParticle.h>
#include <Vertex.h>
#include <myTrack.h>

class myTESevent : public TObject {
private:
	Int_t m_run;
	Int_t m_evt;
	map<Int_t, vector<myParticle> > m_particles; //PID, container
	map<Int_t, vector<Vertex>     > m_vertices;  //type, vertex container
	vector<myTrack> m_tracks;       
	Int_t m_nparticles;
	Int_t m_nvertices;
	Int_t m_ntracks;
public:
	myTESevent();
	myTESevent(Int_t run, Int_t evt);
	void setRun(Int_t v) { m_run = v; }
	void setEvt(Int_t v) { m_evt = v; }
	Int_t getRun()  { return m_run; }
	Int_t getEvt()  { return m_evt; }
	void                              prepareParticleContainer(Int_t pid, UInt_t size);
	void                              prepareVertexContainer  (Int_t typ, UInt_t size);
	void                              prepareTrackContainer   (           UInt_t size);
	map<Int_t, vector<myParticle> >*  getParticles()          { return &m_particles;        }
	map<Int_t, vector<Vertex>     >*  getVertices()           { return &m_vertices;         }
	vector<myParticle>*               getParticles(Int_t pid) { return &(m_particles[pid]); }
	vector<Vertex>*                   getVertices(Int_t typ)  { return &(m_vertices[typ]);  }
	vector<myTrack>*                  getTracks()             { return &m_tracks;           }
	myParticle* addParticle(myParticle& p);
	Vertex*     addVertex(Vertex& v);
	myTrack*    addTrack(myTrack& track);
	
	void clear();
	void reset();
	void print();
	Vertex* findVertex(Int_t typ, Double_t x, Double_t y, Double_t z, Double_t t);
	Vertex* findVertex(Int_t typ, Int_t run, Int_t evt,  Int_t key);

	void linkRelativesParts();
	void setGDparents();

	ClassDef(myTESevent,1)
};






#endif
