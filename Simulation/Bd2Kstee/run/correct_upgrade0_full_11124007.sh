#!/bin/bash

#. $VO_LHCB_SW_DIR/lib/LbEnv
. /cvmfs/lhcb.cern.ch/lib/LbLogin.sh
index=$1
pathOut=$ProjectDir/storage/Bd2Kstee_11124007_upgrade0_full_20200526/$index/
fileName1=$pathOut/tupleFullBrem.root
fileName2=$pathOut/tupleVanya.root
fileName3=$pathOut/tupleFullBremCorr.root
fileName4=$pathOut/tupleTES.root
$ProjectDir/Simulation/exe/vanyaCorr $fileName1 $fileName2 $fileName3
$ProjectDir/Simulation/exe/makeTES   $fileName3 $fileName4
